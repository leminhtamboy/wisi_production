<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
/** Include PHPExcel */
function RemoveSign($str){

    $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
    ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
        "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
    ,"ờ","ớ","ợ","ở","ỡ",
        "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
        "ỳ","ý","ỵ","ỷ","ỹ",
        "đ",
        "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
    ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
        "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
        "Ì","Í","Ị","Ỉ","Ĩ",
        "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
    ,"Ờ","Ớ","Ợ","Ở","Ỡ",
        "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
        "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
        "Đ","ê","ù","à");
    $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
    ,"a","a","a","a","a","a",
        "e","e","e","e","e","e","e","e","e","e","e",
        "i","i","i","i","i",
        "o","o","o","o","o","o","o","o","o","o","o","o"
    ,"o","o","o","o","o",
        "u","u","u","u","u","u","u","u","u","u","u",
        "y","y","y","y","y",
        "d",
        "A","A","A","A","A","A","A","A","A","A","A","A"
    ,"A","A","A","A","A",
        "E","E","E","E","E","E","E","E","E","E","E",
        "I","I","I","I","I",
        "O","O","O","O","O","O","O","O","O","O","O","O"
    ,"O","O","O","O","O",
        "U","U","U","U","U","U","U","U","U","U","U",
        "Y","Y","Y","Y","Y",
        "D","e","u","a");
    return str_replace($coDau,$khongDau,$str);
}
require_once("Classes/PHPExcel.php");
require_once("Classes/PHPExcel/IOFactory.php");
//Create a PHPExcel object
$name_file = $_POST["file_name"];;
$objPHPExcel = PHPExcel_IOFactory::load("import/".$name_file);
$_categoryFactory = $objectManager->get('Magento\Catalog\Model\CategoryFactory');
$dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$base_url = $storeManager->getStore()->getBaseUrl();
$base_media_url = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
$id_provider = -1;
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$attributeValues = [];
$array_attribute = array();
$associatedProductIds = [];
//echo "============================BEGIN IMPORT EXCEL FILE CONFIGRUABLE PRODUCT=============================".PHP_EOL;
//echo "DELETING DATA IMPORT IN DATABASE".PHP_EOL;
//$sql_delete_import_data = "DELETE FROM `libero_import_product`;";
//$connection->query($sql_delete_import_data);
//echo "END DELETING DATA IMPORT IN DATABASE".PHP_EOL;
//echo "IMPORTING TO DATABASE".PHP_EOL;
$array_result = array();
$array_result["error"] = "false";
$array_result["msg"] = "TẤT CẢ DỮ LIỆU ĐÃ HỢP LỆ";
$array_result["tong_dong"] = 0;
$array_result["so_dong_hop_le"] =  0;
$array_result["so_dong_khong_hop_le"] = 0;
$array_list_error = array();
$array_valid_extension = ["jpg","jpeg","png"];
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    $worksheetTitle     = $worksheet->getTitle();
    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
    $highestRowCount = 1; // Bỏ qua dòng đầu tiên chứa cột A , B , C
    //Not good at all but good to detect hightest row
    for ($row = 2; $row <= $highestRow; ++ $row) {
        if($worksheet->getCellByColumnAndRow(1, $row)->getCalculatedValue() != ""){
            //If one column have empty cell this is your fault not me !!!!!!
            $highestRowCount++;
        }
    }
    //End 
    $highestRow = $highestRowCount;
    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
    $nrColumns = ord($highestColumn) - 64;
    $array_result["tong_dong"] = $highestRow;
    $tong_dong_hop_le = 0;
    $tong_dong_khong_hop_le = 0;
    for ($row = 2; $row <= $highestRow; ++ $row) {
        //Insert database
        $la_hop_le_hinh = false;
        $sku = $id_provider."-";
        $type_product = $worksheet->getCellByColumnAndRow(1, $row)->getCalculatedValue();
        if($type_product=="simple"){
            $name_product = $worksheet->getCellByColumnAndRow(2, $row)->getCalculatedValue();
            $danh_muc = $worksheet->getCellByColumnAndRow(3, $row)->getCalculatedValue();
            $array_danh_muc = explode("/",$danh_muc);
            $array_category = array();
            $last_category_id = "";
            foreach($array_danh_muc as $_cate){
                $collection = $_categoryFactory->create()->getCollection()->addAttributeToFilter("name",array("like" => "".$_cate.""))->setPageSize(1);
                if ($collection->getSize()) {
                    $categoryId = $collection->getFirstItem()->getId();
                    $array_category[] = $categoryId;
                    $sku.=$categoryId."-";
                    $last_category_id = $categoryId;
                }
            }
			$sku .= time();
            $price = $worksheet->getCellByColumnAndRow(6, $row)->getCalculatedValue();
            $special_price = $worksheet->getCellByColumnAndRow(7, $row)->getCalculatedValue();
            $free_shipment = 0;
            $tax = $worksheet->getCellByColumnAndRow(9, $row)->getCalculatedValue();
			$class_tax_id = 0 ;
			if($tax == "VAT 5%"){
				$class_tax_id = 5;
			}
			if($tax == "VAT 10%"){
				$class_tax_id = 4;
			}
            $qty = $worksheet->getCellByColumnAndRow(10, $row)->getCalculatedValue();
            $url_key = RemoveSign($name_product)."-".$sku;
            $min_qty = $worksheet->getCellByColumnAndRow(11, $row)->getCalculatedValue();;
            $status = 1;
            $weight = $worksheet->getCellByColumnAndRow(12, $row)->getCalculatedValue();
            $length = $worksheet->getCellByColumnAndRow(13, $row)->getCalculatedValue();
            $height = $worksheet->getCellByColumnAndRow(14, $row)->getCalculatedValue();
            $width = $worksheet->getCellByColumnAndRow(15, $row)->getCalculatedValue();
            $short_description = $worksheet->getCellByColumnAndRow(16, $row)->getCalculatedValue();
            $description_text_1 = $worksheet->getCellByColumnAndRow(17, $row)->getCalculatedValue();
            $description_img_1 = $base_media_url."".$worksheet->getCellByColumnAndRow(18, $row)->getCalculatedValue();
			$img_1 = $worksheet->getCellByColumnAndRow(18, $row)->getCalculatedValue();
			$make_img_1 = "";
			if($img_1 != ""){
				$is_http = strrpos($img_1,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_1, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-1-".time().".".$ext_sub_img;
				}
				$make_img_1 = "<p></p><img src='$description_img_1' width='100%' height='auto' />";
			}
			
			$description_text_2 = $worksheet->getCellByColumnAndRow(19, $row)->getCalculatedValue();
            $description_img_2 = $base_media_url."".$worksheet->getCellByColumnAndRow(20, $row)->getCalculatedValue();
			$img_2 = $worksheet->getCellByColumnAndRow(20, $row)->getCalculatedValue();
			$make_img_2 = "";
			if($img_2 != ""){
				$is_http = strrpos($img_2,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_2, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-2-".time().".".$ext_sub_img;
				}
				$make_img_2 = "<p></p><img src='$description_img_2' width='100%' height='auto' />";
			}
			
			$description_text_3 = $worksheet->getCellByColumnAndRow(21, $row)->getCalculatedValue();
            $description_img_3 = $base_media_url."".$worksheet->getCellByColumnAndRow(22, $row)->getCalculatedValue();
			$img_3 = $worksheet->getCellByColumnAndRow(22, $row)->getCalculatedValue();
			$make_img_3 = "";
			if($img_3 != ""){
				$is_http = strrpos($img_3,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_3, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-3-".time().".".$ext_sub_img;
				}
				$make_img_3 = "<p></p><img src='$description_img_3' width='100%' height='auto' />";
			}
			
            $description = $description_text_1.$make_img_1.$description_text_2.$make_img_2.$description_text_3.$make_img_3;
            $quick_detail = $worksheet->getCellByColumnAndRow(23, $row)->getCalculatedValue();
			$json_quick_detail = "";
			if($quick_detail != ""){
				$array_quick_detail = explode(";",$quick_detail);
				$q = 0;
				$length_quick_detail = count($array_quick_detail);
				$array_json_quick_detail = array();
				$array_json_quick_detail["data"] = array();
				$array_json_quick_detail["is_have"] = "false";
				$array_content_quick_detail = array();
				for($q = 0 ; $q < $length_quick_detail ; $q++){
					$array_child  = array();
					if($array_quick_detail[$q] != ""){
						$item = explode(":",$array_quick_detail[$q]);
						$array_child["title"] = $item[0];
						if(isset($item[1])){
							$array_child["content"] = $item[1];
							$array_content_quick_detail[] = $array_child;
						}
					}
				}
				if(count($array_content_quick_detail) > 0){
					$array_json_quick_detail["data"] = $array_content_quick_detail;
					$array_json_quick_detail["is_have"] = "true";
				}
				$json_quick_detail = json_encode($array_json_quick_detail,JSON_UNESCAPED_UNICODE);
			}
            $meta_title = $worksheet->getCellByColumnAndRow(24, $row)->getCalculatedValue();
            $meta_keyword = $worksheet->getCellByColumnAndRow(25, $row)->getCalculatedValue();
            $meta_description = $worksheet->getCellByColumnAndRow(26, $row)->getCalculatedValue();
            $base_img = $worksheet->getCellByColumnAndRow(27,$row)->getCalculatedValue();
            $danh_sach_hinh_khac = $worksheet->getCellByColumnAndRow(28,$row)->getCalculatedValue();
            //Processing contains http
            $ext_sub_img = pathinfo($base_img, PATHINFO_EXTENSION);
            $array_error_base_img = array();
            if(!in_array($ext_sub_img,$array_valid_extension)){
                $array_error_base_img["stt"] = $row;
                $array_error_base_img["msg"] = " --- Dòng thứ $row lỗi đuôi hình đại diện [".$base_img."].";
                $tong_dong_khong_hop_le++;
                $array_list_error[] = $array_error_base_img;
            }else{
                $tong_dong_hop_le ++;
            }
            
            //End processing
            $array_path_img = array();
            $image_data = array(); // Remember this case save json as key 'main' and variant (if is config) in main is key id main product
            $main_img_child = array();
            $array_main_child_image = array();
            $array_temp_sub_img = array();
            $array_path_img = explode(",",$danh_sach_hinh_khac);
            foreach($array_path_img as $_img_path) {
                $ext_sub_img = pathinfo($base_img, PATHINFO_EXTENSION);
                $array_error_sub_img = array();
                if($_img_path != ""){
                    if(!in_array($ext_sub_img,$array_valid_extension)){
                        $array_error_sub_img["stt"] = $row;
                        $array_error_sub_img["msg"] = " --- Dòng thứ $row lỗi đuôi hình phụ [".$_img_path."].";
                        $tong_dong_khong_hop_le++;
                        $array_list_error[] = $array_error_sub_img;
                    }else{
                        $tong_dong_hop_le++;
                    }
                }
            }
            
        }
        if($type_product == "consadfigurable"){
            $name_product = $worksheet->getCellByColumnAndRow(2, $row)->getCalculatedValue();
            $danh_muc = $worksheet->getCellByColumnAndRow(3, $row)->getCalculatedValue();
            $array_danh_muc = explode("/",$danh_muc);
            $array_category = array();
            $last_category_id = "";
            $string_category = "";
            foreach($array_danh_muc as $_cate){
                $collection = $_categoryFactory->create()->getCollection()->addAttributeToFilter("name",array("like" => "".$_cate.""))->setPageSize(1);
                if ($collection->getSize()) {
                    $categoryId = $collection->getFirstItem()->getId();
                    $array_category[] = $categoryId;
                    $sku.=$categoryId;
                    $sku.=$categoryId."-";
                    $last_category_id = $categoryId;
                    $string_category.= $categoryId."|";
                }
            }
            $string_category = substr($string_category,0,strlen($string_category) - 1);
            $sku .= time();
            $color = $worksheet->getCellByColumnAndRow(4, $row)->getCalculatedValue();
            $size = $worksheet->getCellByColumnAndRow(5, $row)->getCalculatedValue();
            $price = $worksheet->getCellByColumnAndRow(6, $row)->getCalculatedValue();
            $special_price = $worksheet->getCellByColumnAndRow(7, $row)->getCalculatedValue();
            $free_shipment = 0;
            $tax = $worksheet->getCellByColumnAndRow(9, $row)->getCalculatedValue();
			$class_tax_id = 0 ;
			if($tax == "VAT 5%"){
				$class_tax_id = 5;
			}
			if($tax == "VAT 10%"){
				$class_tax_id = 4;
			}
            $qty = $worksheet->getCellByColumnAndRow(10, $row)->getCalculatedValue();
            $url_key = RemoveSign($name_product)."-".$id_provider;
            $min_qty = 1;
            $status = 1;
            $weight = $worksheet->getCellByColumnAndRow(12, $row)->getCalculatedValue();
            $length = $worksheet->getCellByColumnAndRow(13, $row)->getCalculatedValue();
            $height = $worksheet->getCellByColumnAndRow(14, $row)->getCalculatedValue();
            $width = $worksheet->getCellByColumnAndRow(15, $row)->getCalculatedValue();
            $short_description = $worksheet->getCellByColumnAndRow(16, $row)->getCalculatedValue();
            $description_text_1 = $worksheet->getCellByColumnAndRow(17, $row)->getCalculatedValue();
            $description_img_1 = $base_media_url."/".$worksheet->getCellByColumnAndRow(18, $row)->getCalculatedValue();
            $img_1 = $worksheet->getCellByColumnAndRow(18, $row)->getCalculatedValue();
			$make_img_1 = "";
			if($img_1 != ""){
				$is_http = strrpos($img_1,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_1, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-1-".time().".".$ext_sub_img;
                    //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($img_1));
                    $ch = curl_init($img_1);
                    $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                    $filename = $name_sub_image;
                    $complete_save_loc = $my_save_dir . $filename;
                    $fp = fopen($complete_save_loc, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
					$description_img_1 = $base_media_url."temp_image_product/".$name_sub_image;
				}
				$make_img_1 = "<p></p><img src=$description_img_1 width=100% height=auto />";
			}
			
			$description_text_2 = $worksheet->getCellByColumnAndRow(19, $row)->getCalculatedValue();
            $description_img_2 = $base_media_url."".$worksheet->getCellByColumnAndRow(20, $row)->getCalculatedValue();
			$img_2 = $worksheet->getCellByColumnAndRow(20, $row)->getCalculatedValue();
			$make_img_2 = "";
			if($img_2 != ""){
				$is_http = strrpos($img_2,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_2, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-2-".time().".".$ext_sub_img;
                    //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($img_2));
                    $ch = curl_init($img_2);
                    $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                    $filename = $name_sub_image;
                    $complete_save_loc = $my_save_dir . $filename;
                    $fp = fopen($complete_save_loc, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
					$description_img_2 = $base_media_url."temp_image_product/".$name_sub_image;
				}
				$make_img_2 = "<p></p><img src=$description_img_2 width=100% height=auto />";
			}
			
			$description_text_3 = $worksheet->getCellByColumnAndRow(21, $row)->getCalculatedValue();
            $description_img_3 = $base_media_url."".$worksheet->getCellByColumnAndRow(22, $row)->getCalculatedValue();
			$img_3 = $worksheet->getCellByColumnAndRow(22, $row)->getCalculatedValue();
			$make_img_3 = "";
			if($img_3 != ""){
				$is_http = strrpos($img_3,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_3, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-3-".time().".".$ext_sub_img;
                    //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($img_3));
                    $ch = curl_init($img_3);
                    $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                    $filename = $name_sub_image;
                    $complete_save_loc = $my_save_dir . $filename;
                    $fp = fopen($complete_save_loc, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
					$description_img_3 = $base_media_url."temp_image_product/".$name_sub_image;
				}
				$make_img_3 = "<p></p><img src=$description_img_3 width=100% height=auto />";
			}
			
            $description = $description_text_1.$make_img_1.$description_text_2.$make_img_2.$description_text_3.$make_img_3;
            $quick_detail = $worksheet->getCellByColumnAndRow(23, $row)->getCalculatedValue();
			$json_quick_detail = "";
			if($quick_detail != ""){
				$array_quick_detail = explode(";",$quick_detail);
				$q = 0;
				$length_quick_detail = count($array_quick_detail);
				$array_json_quick_detail = array();
				$array_json_quick_detail["data"] = array();
				$array_json_quick_detail["is_have"] = "false";
				$array_content_quick_detail = array();
				for($q = 0 ; $q < $length_quick_detail ; $q++){
					$array_child  = array();
					if($array_quick_detail[$q] != ""){
						$item = explode(":",$array_quick_detail[$q]);
						$array_child["title"] = $item[0];
						if(isset($item[1])){
							$array_child["content"] = $item[1];
							$array_content_quick_detail[] = $array_child;
						}
					}
				}
				if(count($array_content_quick_detail) > 0){
					$array_json_quick_detail["data"] = $array_content_quick_detail;
					$array_json_quick_detail["is_have"] = "true";
				}
				$json_quick_detail = json_encode($array_json_quick_detail,JSON_UNESCAPED_UNICODE);
			}
            $meta_title = $worksheet->getCellByColumnAndRow(24, $row)->getCalculatedValue();
            $meta_keyword = $worksheet->getCellByColumnAndRow(25, $row)->getCalculatedValue();
            $meta_description = $worksheet->getCellByColumnAndRow(26, $row)->getCalculatedValue();
            $base_img = $worksheet->getCellByColumnAndRow(27,$row)->getCalculatedValue();
            $danh_sach_hinh_khac = $worksheet->getCellByColumnAndRow(28,$row)->getCalculatedValue();
            $sql_insert = "";
            $string_insert = "INSERT INTO `libero_import_product` (`id_import`, `sku`, `type`, `name`, `category`, `color`, `size`, `price`, `special_price`, `free_shipment`, `tax`, `qty`, `min_qty`, `weight`, `length`, `height`, `width`, `short_description`, `text_des_1`, `img_des_1`, `text_des_2`, `img_des_2`, `text_des_3`, `img_des_3`, `quick_detail`, `seo_title`, `seo_keyword`, `seo_des`, `base_img`, `thumb_img`, `id_provider`,`last_category`)  
            VALUES (NULL, '', '$type_product', '$name_product', '$string_category', '$color', '$size', '$price', '$special_price', '0', '$class_tax_id', '$qty', '$min_qty', '$weight', '$length', '$height', '$width', '$short_description', '$description', '', '', '', '', '', '$json_quick_detail', '$meta_title', '$meta_keyword', '$meta_description', '$base_img', '$danh_sach_hinh_khac', '$id_provider','$last_category_id');";
            $connection->query($string_insert);
        }
    }
    break;
}
if($tong_dong_khong_hop_le > 0){
    $array_result["error"] = "true";
    $array_result["msg"] = "DỮ LIỆU KHÔNG HỢP LỆ";
}else{
    $array_result["msg"] = "TẤT CẢ DỮ LIỆU CỦA FILE [$name_file] ĐÃ HỢP LỆ";
}
if($array_result["tong_dong"] > 2000){
    $array_result["error"] = "true";
    $array_result["msg"] = "DỮ LIỆU CÓ SỐ DÒNG VƯỢT QUÁ 250 DÒNG. XIN VUI LÒNG CHIA NHỎ FILE UPLOAD SẢN PHẨM";
}
$array_result["so_dong_khong_hop_le"] = $tong_dong_khong_hop_le;
$array_result["so_dong_hop_le"] = $tong_dong_hop_le;
$array_result["error_list"] = $array_list_error;
echo json_encode($array_result);
