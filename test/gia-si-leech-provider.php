<?php header('Access-Control-Allow-Origin: *'); ?>
<?php
$url= $_POST['url_provider'];
$header=array(
"Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
"Accept-Encoding:gzip, deflate, br",
"Accept-Language:en-US,en;q=0.9,vi;q=0.8,ko;q=0.7,fr;q=0.6",
"Connection:keep-alive",
"Cookie:_ga=GA1.2.2046070054.1539335323; _gid=GA1.2.187175533.1539573248; ci_session=97fc7f950c6e1b7f73562f73439edce0608d773f; _gat=1",
"Host:www.webgiasi.vn",
"Upgrade-Insecure-Requests: 1",
"User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36"
);
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
curl_setopt($ch, CURLOPT_TIMEOUT_MS, 60000);
$content = curl_exec($ch);
curl_close($ch);
echo $content;