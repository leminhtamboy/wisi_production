<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$object_Manager = $bootstraps->getObjectManager();
$state = $object_Manager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$deliverys = array("giao_hang_nhanh|Giao Hàng Nhanh","giao_hang_tiet_kiem|Giao Hàng Tiết Kiệm");
$sql = "select * from libero_customer_seller_company";
$resource = $object_Manager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$data_seller = $connection->fetchAll($sql);
date_default_timezone_set("Asia/Bangkok");
$curDateTime = date("Y-m-d H:i:s");
foreach($data_seller as $_seller){
	$id_seller = $_seller["id_seller_company"];
	foreach($deliverys as $delivery){
		$deli_arr = explode("|",$delivery);
		$code = $deli_arr[0];
		$name = $deli_arr[1];
		$shipping = $object_Manager->create("Libero\Onestepcheckout\Model\Shipping");
		$shipping->setData("shipping_code",$code);
		$shipping->setData("name_shipping_method",$name);
		$shipping->setData("title_shipping_method",$name); 
		$shipping->setData("status_shipping_method",$status);
		$shipping->setData("id_seller",$id_seller); 
		$shipping->setData("price",$price);
		$shipping->setData('created_at', $curDateTime);
		$shipping->setData('updated_at', $curDateTime);
		$shipping->save();
	}
}