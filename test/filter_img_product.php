<?php
include_once("database.php");
$data = new database();
//Xử lý excel file ngay chỗ này
$sql = "select * from libero_dowload_products";
$data_dowload_product = $data->ExcuteArrayList($sql);
//Local
//$base_dir = $_SERVER["DOCUMENT_ROOT"]."/pub/media/import/thitruongsi";
//Server
$base_dir = "/var/www/wisi_production/pub/media/import/thitruongsi";
$sql_update = "";
foreach($data_dowload_product as $_product){
    $base_image = $_product["base_image"];
    $thumb_image = $_product["thumbnail_image"];
    $id_dowload = $_product["id_dowload"];
    $price = $_product["price"];
    $price = str_replace("đ","", $price);
    //get extension image in url
    $ext_base_img = pathinfo($base_image, PATHINFO_EXTENSION);
    $name_base_image = "base-img-$id_dowload.".$ext_base_img;
    $ext_thump_img = pathinfo($thumb_image, PATHINFO_EXTENSION);
    $name_thump_image = "thump-img-$id_dowload.".$ext_thump_img;
    file_put_contents($base_dir."/".$name_base_image,file_get_contents($base_image));
    file_put_contents($base_dir."/".$name_thump_image,file_get_contents($thumb_image));
    $path_base_img = "/thitruongsi/$name_base_image";
    $path_thump_img = "/thitruongsi/$name_thump_image";
    $path_small_img = "/thitruongsi/$name_thump_image";
	$addition_path_img = "";
	$addtional_img_row = $_product["additional_images"];
	$array_addition_img = explode(",",$addtional_img_row);
	$img_index = 0;
	foreach($array_addition_img as $add_img){
		$img_index++;
		$ext_add_img = pathinfo($add_img, PATHINFO_EXTENSION);
		$name_add_img = "addition-img-$id_dowload-$img_index.".$ext_add_img;
		file_put_contents($base_dir."/".$name_add_img,file_get_contents($add_img));
		$addition_path_img .= "/thitruongsi/$name_add_img".",";
	}
	if($addition_path_img == ""){
		$addition_path_img = $path_small_img;
	}
	$addition_path_img  = substr($addition_path_img,0,strlen($addition_path_img) -1 );
    $sql_update .= "UPDATE `libero_dowload_products` SET `thumbnail_image` = '$path_base_img', `base_image` = '$path_base_img', `additional_images` = '$addition_path_img',`small_image`='$path_small_img',`price` = '$price' where `id_dowload` = '$id_dowload';";
    //$url_image = "";
}
//echo $sql_update;
$data->ExcuteNonQuery($sql_update);