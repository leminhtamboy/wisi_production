<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$sql_get_cron = "SELECT * FROM  `libero_order_cron_status_shipping` WHERE `status_run` = 0";
$data_cron = $connection->fetchAll($sql_get_cron);
$helperCheckout = $objectManager->get("\Libero\Onestepcheckout\Helper\Data");
foreach($data_cron as $_cron){
    //Tiet kiem first
    $shipping_company = $_cron["shipping_company"];
    $shipping_id = $_cron["shipping_id"];
    $entity_id = $_cron["order_id"];
    if($shipping_company == "giao_hang_tiet_kiem"){
        $data_post = array();
        $data_post["order_code"] = $shipping_id;
        $order_information = json_decode($helperCheckout->getOrderGHTK($data_post),true);
        $status = $order_information["order"]["status"];
        $sql_get_status_mapping = "SELECT * FROM  `libero_order_mapping_status_shipping` inner join `libero_order_status` on `libero_order_mapping_status_shipping`.id_wisi_status = `libero_order_status`.id_status  WHERE `code_shipping_status` = '$status' AND `shipping_company` = '$shipping_company'";
        $data_mapping = $connection->fetchAll($sql_get_status_mapping);
        $wisi_status = $data_mapping[0]["label_status"];
        $sql_update = "UPDATE `seller_sales_order` set `status` = '$wisi_status' WHERE `entity_id` = '$entity_id'";
        $connection->query($sql_update);
    }
}