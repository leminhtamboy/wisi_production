<div class='admin__control-table-wrapper'>
	<table class='admin__dynamic-rows data-grid' data-role='grid'>
		<thead>
			<tr>
				<th class='data-grid-th' data-repeat-index='0'>Image</th>
				<th class='data-grid-th' data-repeat-index='1'>Name</th>
				<th class='data-grid-th' data-repeat-index='2'>SKU</th>
				<th class='data-grid-th' data-repeat-index='3'>Price</th>
				<th class='data-grid-th' data-repeat-index='4'>Quantity</th>
				<th class='data-grid-th' data-repeat-index='5'>Weight</th>
				<th class='data-grid-th' data-repeat-index='6'>Status</th>
				<th class='data-grid-th' data-repeat-index='7'>Attributes</th>
				<th class='data-grid-th' data-repeat-index='8'>Actions</th>
			</tr>
		</thead>
		<tbody>
			<tr class='data-row' data-repeat-index='0'>
				<td class='admin__control-fields _no-header' data-index='thumbnail_image_container'>
					<fieldset class='admin__field' data-index='thumbnail_image_container'>
						<legend class='admin__field-label'>
							<span>Image</span>
						</legend>
						<div class='admin__field-control admin__control-fields _no-header'>
							<div class='admin__field' data-index='thumbnail_image_edit'>
								<div class='admin__field-control'>
									<div class='data-grid-file-uploader' data-role='drop-zone'>
										<div class='data-grid-file-uploader-inner'>
											<div class='file-uploader-area'>
												<input type='file' class='file-uploader-input' id='F280VPG' name='image'>
												<label class='file-uploader-button' for='F280VPG' title='Upload Image'><span>Upload Image</span></label>
												<span class='file-uploader-spinner'></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</td>
				<td class='admin__control-fields _no-header' data-index='name_container'>
					<fieldset class='admin__field' data-index='name_container'>
						<legend class='admin__field-label'>
							<span>Name</span>
						</legend>
						<div class='admin__field-control admin__control-fields _no-header'>
							<div class='admin__field' data-index='name_edit'>
								<div class='admin__field-control'>
									<input class='admin__control-text' type='text' name='configurable-matrix[0][name]' placeholder='' aria-describedby='notice-UB00CJX' id='UB00CJX'>
								</div>
							</div>
						</div>
					</fieldset>
				</td>
				<td class='admin__control-fields _no-header' data-index='sku_container'>
					<fieldset class='admin__field' data-index='sku_container'>
						<legend class='admin__field-label'>
							<span>SKU</span>
						</legend>
						<div class='admin__field-control admin__control-fields _no-header'>
							<div class='admin__field' data-index='sku_edit'>
								<div class='admin__field-control'>
									<input class='admin__control-text' type='text' name='configurable-matrix[0][sku]' placeholder='' aria-describedby='notice-O2W759A' id='O2W759A'>
								</div>
							</div>
						</div>
					</fieldset>
				</td>
				<td class='admin__control-fields _no-header' data-index='price_container'>
					<fieldset class='admin__field' data-index='price_container'>
						<legend class='admin__field-label'>
							<span>Price</span>
						</legend>
						<div class='admin__field-control admin__control-fields _no-header'>
							<div class='admin__field' data-index='price_edit'>
						
								<div class='admin__field-control'>
									<div class='admin__control-addon'>
										
										<input class='admin__control-text' type='text' name='configurable-matrix[0][price]' placeholder='' aria-describedby='notice-YA2648P' id='YA2648P'>
										<label class='admin__addon-prefix'  for='YA2648P'>
										<span data-bind='text: addbefore'>₫</span>
										</label>
									</div>
								</div>
							</div>
						</div>
					</fieldset>
				</td>
				<td class='admin__control-fields _no-header' data-index='quantity_container'>
					<fieldset class='admin__field' data-index='quantity_container'>
						<legend class='admin__field-label'>
							<span>Quantity</span>
						</legend>
						<div class='admin__field-control admin__control-fields _no-header'>
							<div class='admin__field' data-index='quantity_edit'>
								<div class='admin__field-control'>
									<input class='admin__control-text' type='text' name='configurable-matrix[0][qty]' placeholder='' aria-describedby='notice-QB0IBT5' id='QB0IBT5'>
								</div>
							</div>
						</div>
					</fieldset>
				</td>
				<td class='admin__control-fields _no-header' data-index='price_weight'>
					<fieldset class='admin__field' data-index='price_weight'>
						<legend class='admin__field-label'>
							<span>Weight</span>
						</legend>
						<div class='admin__field-control admin__control-fields _no-header'>
							<div class='admin__field' data-index='weight_edit'>
								<div class='admin__field-control'>
									<input class='admin__control-text' type='text' name='configurable-matrix[0][weight]' placeholder='' aria-describedby='notice-RXBVLDQ' id='RXBVLDQ'>
								</div>
							</div>
						</div>
					</fieldset>
				</td>
				<td class='_no-header' data-index='status'>
					<div class='control-table-text'>
						<span data-index='status'>Enabled</span>
					</div>
				</td>
				<td class='_no-header' data-index='attributes'>
					<div class='admin__field _no-header' data-index='attributes'>
						<label class='admin__field-label' for='K2A5D0C' style='display: none;'>
						<span>Attributes</span>
						</label>
						<div class='admin__field-control'>
							
							<div class='control-table-text'>
								<span data-index='attributes'>Color: Green</span>
							</div>
						
						</div>
					</div>
				</td>
			
				<td class='data-grid-actions-cell _fit _no-header' data-index='actionsList'>
					<div class='action-select-wrap'>
						<button class='action-select'>
						<span data-bind='i18n: 'Select''>Select</span>
						</button>
						<ul class='action-menu'>
							<li>
								<a class='action-menu-item'>Choose a different Product</a>
							</li>
							<li>
								<a class='action-menu-item'>Disable Product</a>
							</li>
							<li>
								<a class='action-menu-item'>Remove Product</a>
							</li>
						</ul>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</div>