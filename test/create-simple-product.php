<?php 
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$object_Manager = $bootstraps->getObjectManager();
$state = $object_Manager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$resource = $object_Manager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$attribute_code = $_GET["attribute_codes"];
$attribute_code = substr($attribute_code,0,strlen($attribute_code) - 1);
$array_attribute_code = explode("|",$attribute_code);
$attribute_value = $_GET["attribute_value"];
$attribute_value = substr($attribute_value,0,strlen($attribute_value) - 1);
$array_attribute_value = explode("|",$attribute_value);
$name = $_GET["name_product"];
$sku = $_GET["sku_product"];
$seller_id =  $_GET["seller_id"];
$price = $_GET["price_product"];
$special_price = $_GET["special_price_product"];
$json_data_products  = $_GET["json_products"];
$qty = $_GET["qty"];
$weight = $_GET["weight"];
$width = $_GET["width"];
$height = $_GET["height"];
$length = $_GET["length"];
$array_products = json_decode($json_data_products,true);
$associatedProductIds = [];
$attributeValues = [];
$array_result = array();
$array_variant = array();
$html= "";
$action = "add";
if(isset($_REQUEST['action'])){
    $action = "edit";
}
if($action == "add"){
$html .= "<div class='admin__control-table-wrapper'>
<table class='admin__dynamic-rows data-grid' data-role='grid'>
    <thead>
        <tr>
            <th class='data-grid-th' data-repeat-index='0'>Hình Ảnh</th>
            <th class='data-grid-th' data-repeat-index='1'>Tên</th>
            <th class='data-grid-th' data-repeat-index='2'>Sku</th>
            <th class='data-grid-th' data-repeat-index='3'>Giá<div class=\"tooltip\">Ex: 2000đ, 5000đ,etc</div></th>
            <th class='data-grid-th' data-repeat-index='3'>Giá Giảm</th>
            <th class='data-grid-th' data-repeat-index='4'>Số lượng tồn kho<div class=\"tooltip\">Ex: 10, 15,20, etc</div></th>
            <th class='data-grid-th' data-repeat-index='5'>Khối lượng<div class=\"tooltip\">Ex: 50g, 100g, 200g</div></th>
            <th class='data-grid-th' data-repeat-index='5'>Chiều dài<div class=\"tooltip\">Ex: 50cm, 100cm, 200cm</div></th>
            <th class='data-grid-th' data-repeat-index='5'>Chiều cao<div class=\"tooltip\">Ex: 50cm, 100cm, 200cm</div></th>
            <th class='data-grid-th' data-repeat-index='5'>Chiều rộng<div class=\"tooltip\">Ex: 50cm, 100cm, 200cm</div></th>
            <th class='data-grid-th' data-repeat-index='5'>Kho</th>
            <th class='data-grid-th' data-repeat-index='7'>Thuộc tính</th>
            <th class='data-grid-th' data-repeat-index='8'>Hành động</th>
        </tr>
    </thead>
    <tbody>";
}
$length_products = count($array_products);
$installer = $object_Manager->create(CategorySetup::class);
$attributeSetId = $installer->getAttributeSetId('catalog_product', 'Default');
$storeManager = $object_Manager->get('\Magento\Store\Model\StoreManagerInterface');
$base_url = $storeManager->getStore()->getBaseUrl();
for($i=0;$i < $length_products;$i++){
    $product_temp = $array_products[$i];
    $where_options_value = str_replace("|",",",$product_temp);
    $sql_getoption_label = "SELECT * FROM `eav_attribute_option_value` WHERE option_id in ($where_options_value)";
    $data_option = $connection->fetchAll($sql_getoption_label);
    $name_temp_product = "";
    $sku_simple = $sku."-";
    $productRepository = $object_Manager->get(ProductRepositoryInterface::class);
    foreach($data_option as $_value){
        $name_temp_product.= $_value["value"]."-";
        $sku_simple .= $_value["value"]."-";
    }
    $name_temp_product = substr($name_temp_product,0,strlen($name_temp_product) - 1);
    $sku_simple = substr($sku_simple,0,strlen($sku_simple) - 1);
    $array_options_code = explode("|",$product_temp);
    $sku_simple .= "-".$seller_id."-".time();
    $name_product = $name."-". $name_temp_product;
    $product = $object_Manager->create(Product::class);
    $html_option = "";
    $product->setTypeId(Type::TYPE_SIMPLE)
        ->setAttributeSetId($attributeSetId)
        ->setWebsiteIds([1])
        ->setName($name_product)
        ->setSku($sku_simple)
        ->setPrice($price)
        ->setWeight($weight)
        ->setVisibility(Visibility::VISIBILITY_NOT_VISIBLE)
        ->setStatus(Status::STATUS_ENABLED)
        ->setData("id_seller",$seller_id)
        ->setStockData(['use_config_manage_stock' => 1, 'qty' => $qty, 'is_qty_decimal' => 0, 'is_in_stock' => 1]);
    foreach($array_attribute_code as $_code){
        $eavConfig = $object_Manager->create(\Magento\Eav\Model\Config::class);
        $eavConfig->clear();
        $attribute = $eavConfig->getAttribute('catalog_product', $_code);
        $options = $attribute->getOptions();
        array_shift($options);
        foreach ($options as $option) {
            $value_option = $option->getValue();
            if(in_array($value_option,$array_options_code)){
                $attribute_code = $attribute->getData("attribute_code");
                $attribute_front = $attribute->getData("frontend_label");
                $option_value_product = $option->getLabel();
                $product->setData($attribute_code,$option->getValue());
                $attributeValues[] = [
                    'label' => $option->getLabel(),
                    'attribute_id' => $attribute->getId(),
                    'value_index' => $option->getValue(),
                ];
                $html_option .= "<div class='control-table-text'>
                                    <span data-index='attributes'>$attribute_front: $option_value_product</span>
                                </div>";
            }
        }
    }
    $product = $productRepository->save($product);
    $variant["id_product"] = $product->getId();
    $variant["name"] = $name_product;
    $variant["option_value_product"] = $name_temp_product;
    $variant["price"] = $price;
    $associatedProductIds[] = $product->getId();
    $array_variant[] = $variant;
    $id_product = $product->getId();
    $url_edit_product = $base_url."customer/admin/editproduct/id/".$id_product;
    $html .= "
                <tr class='data-row' data-repeat-index='0' id='variant-row-$id_product'>
                    <td class='admin__control-fields _no-header' data-index='thumbnail_image_container'>
                        <fieldset class='admin__field' data-index='thumbnail_image_container'>
                            <legend class='admin__field-label'>
                                <span>Image</span>
                            </legend>
                            <div class='admin__field-control admin__control-fields _no-header'>
                                <div class='admin__field' data-index='thumbnail_image_edit'>
                                    <div class='admin__field-control'>
                                        <div class='data-grid-file-uploader' data-role='drop-zone'>
                                            <div class='data-grid-file-uploader-inner' id ='wrap-img-$id_product'>
                                                <form name='upload_image_variant' enctype='multipart/form-data'>                                 
                                                    <div class='file-uploader-area'>
                                                        <input type='file' class='file-uploader-input upload-img-variant' id='img-$id_product' name='image'>
                                                        <label class='file-uploader-button' for='img-$id_product' title='Upload Image'><span>Upload Image</span></label>
                                                        <span class='file-uploader-spinner'></span>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class='action-select-wrap'>
                                                <button type='button' class='action-select select-action-img' id='select-action-img-id-$id_product'></button>
                                                <ul class='action-menu' id='wrap-ul-select-$id_product' style='z-index: 9999;'>
                                                    <li>
                                                        <a class='action-menu-item delete-img-variant' variant_id='delete-$id_product'>No Image</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td class='admin__control-fields _no-header' data-index='name_container'>
                        <fieldset class='admin__field' data-index='name_container'>
                            <legend class='admin__field-label'>
                                <span>Name</span>
                            </legend>
                            <div class='admin__field-control admin__control-fields _no-header'>
                                <div class='control-table-text'>
                                <span data-index='name_text'><a href='$url_edit_product' target='_blank'>$name_product</a></span>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td class='admin__control-fields _no-header' data-index='sku_container'>
                        <fieldset class='admin__field' data-index='sku_container'>
                            <legend class='admin__field-label'>
                                <span>SKU</span>
                            </legend>
                            <div class='admin__field-control admin__control-fields _no-header'>
                                <div class='admin__field' data-index='sku_edit'>
                                    <div class='admin__field-control'>
                                        <input class='admin__control-text' type='text' name='configurable-matrix[0][sku]' placeholder='' value='$sku_simple' aria-describedby='notice-O2W759A' id='O2W759A'>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td class='admin__control-fields _no-header' data-index='price_container'>
                        <fieldset class='admin__field' data-index='price_container'>
                            <legend class='admin__field-label'>
                                <span>Price</span>
                            </legend>
                            <div class='admin__field-control admin__control-fields _no-header'>
                                <div class='admin__field' data-index='price_edit'>
                            
                                    <div class='admin__field-control'>
                                        <div class='admin__control-addon'>
                                            <input class='admin__control-text' type='hidden' name='configurable-matrix-variant-id[]' placeholder='' value='$id_product' aria-describedby='notice-QB0IBT5' id='QB0IBT5'>
                                            <input class='admin__control-text' style='background-color: unset;border-color: unset;' type='text' name='configurable-matrix-price[]' placeholder='' value='$price' aria-describedby='notice-YA2648P' id='YA2648P'>
                                            <!--<label class='admin__addon-prefix'  for='YA2648P'>
                                            <span data-bind='text: addbefore'>₫</span>
                                            </label>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td class='admin__control-fields _no-header' data-index='price_container'>
                        <fieldset class='admin__field' data-index='price_container'>
                            <div class='admin__field-control admin__control-fields _no-header'>
                                <div class='admin__field' data-index='price_edit'>
                                    <div class='admin__field-control'>
                                        <div class='admin__control-addon'>                                                                                                
                                            <input class='admin__control-text' style='background-color: unset;border-color: unset;' type='text' name='configurable-matrix-special-price[]' placeholder='' value='$special_price'>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td class='admin__control-fields _no-header' data-index='quantity_container'>
                        <fieldset class='admin__field' data-index='quantity_container'>
                            <legend class='admin__field-label'>
                                <span>Quantity</span>
                            </legend>
                            <div class='admin__field-control admin__control-fields _no-header'>
                                <div class='admin__field' data-index='quantity_edit'>
                                    <div class='admin__field-control'>
                                        <input class='admin__control-text' type='text' name='configurable-matrix-qty[]' placeholder='' value='$qty' aria-describedby='notice-QB0IBT5' id='QB0IBT5'>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td class='admin__control-fields _no-header' data-index='price_weight'>
                        <fieldset class='admin__field' data-index='price_weight'>
                            <legend class='admin__field-label'>
                                <span>Weight</span>
                            </legend>
                            <div class='admin__field-control admin__control-fields _no-header'>
                                <div class='admin__field' data-index='weight_edit'>
                                    <div class='admin__field-control'>
                                        <input class='admin__control-text' type='text' name='configurable-matrix-weight[]' placeholder='' value='$weight' aria-describedby='notice-RXBVLDQ' id='RXBVLDQ'>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td class='admin__control-fields _no-header' data-index='price_weight'>
                        <fieldset class='admin__field' data-index='price_weight'>
                            <div class='admin__field-control admin__control-fields _no-header'>
                                <div class='admin__field' data-index='weight_edit'>
                                    <div class='admin__field-control'>
                                        <input class='admin__control-text' type='text' name='configurable-matrix-length[]' placeholder='' value='$length' aria-describedby='notice-RXBVLDQ' id='RXBVLDQ'>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td class='admin__control-fields _no-header' data-index='price_weight'>
                        <fieldset class='admin__field' data-index='price_weight'>
                            <div class='admin__field-control admin__control-fields _no-header'>
                                <div class='admin__field' data-index='weight_edit'>
                                    <div class='admin__field-control'>
                                        <input class='admin__control-text' type='text' name='configurable-matrix-height[]' placeholder='' value='$height' aria-describedby='notice-RXBVLDQ' id='RXBVLDQ'>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td class='admin__control-fields _no-header' data-index='price_weight'>
                        <fieldset class='admin__field' data-index='price_weight'>
                            <div class='admin__field-control admin__control-fields _no-header'>
                                <div class='admin__field' data-index='weight_edit'>
                                    <div class='admin__field-control'>
                                        <input class='admin__control-text' type='text' name='configurable-matrix-width[]' placeholder='' value='$width' aria-describedby='notice-RXBVLDQ' id='RXBVLDQ'>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td class='admin__control-fields _no-header' data-index='price_weight'>
                        <fieldset class='admin__field' data-index='price_weight'>
                            <div class='admin__field-control admin__control-fields _no-header'>
                                <div class='admin__field' data-index='weight_edit'>
                                    <div class='admin__field-control'>
                                        <select class='admin__control-select' name='configurable-matrix-variant-stock[]'>
                                        <option  value='1'>Còn hàng</option>
                                        <option  value='0'>Hết hàng</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </td>
                    <td class='_no-header' data-index='attributes'>
                        <div class='admin__field _no-header' data-index='attributes'>
                            <label class='admin__field-label' for='K2A5D0C' style='display: none;'>
                            <span>Attributes</span>
                            </label>
                            <div class='admin__field-control'>$html_option</div>
                        </div>
                    </td>
                    <td class='data-grid-actions-cell _fit _no-header' data-index='actionsList'>
                        <div class='action-select-wrap'>
                            <button title='Delete' type='button' style='z-index: 9999;' class='action-default primary mr0 delete-simple-product' data-product-id='$id_product'><span>Delete</span></button>
                        </div>
                    </td>
                </tr>";
    $html_option = "";
    /*foreach($array_attribute_code as $_code){
        $eavConfig = $object_Manager->create(\Magento\Eav\Model\Config::class);
        $eavConfig->clear();
        $attribute = $eavConfig->getAttribute('catalog_product', $_code);
        $productRepository = $object_Manager->get(ProductRepositoryInterface::class);
        $installer = $object_Manager->create(CategorySetup::class);
        /* Create simple products per each option value*/
        /** @var AttributeOptionInterface[] $options */
        /*$options = $attribute->getOptions();
        $attributeSetId = $installer->getAttributeSetId('catalog_product', 'Default');
        array_shift($options); //remove the first option which is empty
        
        foreach ($options as $option) {
            $value_option = $option->getValue();
            if(in_array($value_option,$array_attribute_value)){
                $attribute_code = $attribute->getData("attribute_code");
                $variant = array();
                $product = $object_Manager->create(Product::class);
                $name_product = $name."-". $option->getLabel();
                $option_value_product = $option->getValue();
                $sku = "simple_".$sku."_".$option->getLabel()."_".time();
                $product->setTypeId(Type::TYPE_SIMPLE)
                    ->setAttributeSetId($attributeSetId)
                    ->setWebsiteIds([1])
                    ->setName($name_product)
                    ->setSku($sku)
                    ->setPrice($price)
                    ->setWeight(1000)
                    ->setData($attribute_code,$option->getValue())
                    ->setVisibility(Visibility::VISIBILITY_NOT_VISIBLE)
                    ->setStatus(Status::STATUS_ENABLED)
                    ->setData("id_seller",$seller_id)
                    ->setStockData(['use_config_manage_stock' => 1, 'qty' => 100, 'is_qty_decimal' => 0, 'is_in_stock' => 1]);
                $product = $productRepository->save($product);
                $attributeValues[] = [
                    'label' => $option->getLabel(),
                    'attribute_id' => $attribute->getId(),
                    'value_index' => $option->getValue(),
                ];
                $variant["id_product"] = $product->getId();
                $variant["name"] = $name_product;
                $variant["option_value_product"] = $option_value_product;
                $variant["price"] = $price;
                $associatedProductIds[] = $product->getId();
                $array_variant[] = $variant;
                $html .= "<div class='admin__control-table-wrapper'>
                <table class='admin__dynamic-rows data-grid' data-role='grid'>
                    <thead>
                        <tr>
                            <th class='data-grid-th' data-repeat-index='0'>Image</th>
                            <th class='data-grid-th' data-repeat-index='1'>Name</th>
                            <th class='data-grid-th' data-repeat-index='2'>SKU</th>
                            <th class='data-grid-th' data-repeat-index='3'>Price</th>
                            <th class='data-grid-th' data-repeat-index='4'>Quantity</th>
                            <th class='data-grid-th' data-repeat-index='5'>Weight</th>
                            <th class='data-grid-th' data-repeat-index='6'>Status</th>
                            <th class='data-grid-th' data-repeat-index='7'>Attributes</th>
                            <th class='data-grid-th' data-repeat-index='8'>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class='data-row' data-repeat-index='0'>
                            <td class='admin__control-fields _no-header' data-index='thumbnail_image_container'>
                                <fieldset class='admin__field' data-index='thumbnail_image_container'>
                                    <legend class='admin__field-label'>
                                        <span>Image</span>
                                    </legend>
                                    <div class='admin__field-control admin__control-fields _no-header'>
                                        <div class='admin__field' data-index='thumbnail_image_edit'>
                                            <div class='admin__field-control'>
                                                <div class='data-grid-file-uploader' data-role='drop-zone'>
                                                    <div class='data-grid-file-uploader-inner'>
                                                        <div class='file-uploader-area'>
                                                            <input type='file' class='file-uploader-input' id='F280VPG' name='image'>
                                                            <label class='file-uploader-button' for='F280VPG' title='Upload Image'><span>Upload Image</span></label>
                                                            <span class='file-uploader-spinner'></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </td>
                            <td class='admin__control-fields _no-header' data-index='name_container'>
                                <fieldset class='admin__field' data-index='name_container'>
                                    <legend class='admin__field-label'>
                                        <span>Name</span>
                                    </legend>
                                    <div class='admin__field-control admin__control-fields _no-header'>
                                        <div class='admin__field' data-index='name_edit'>
                                            <div class='admin__field-control'>
                                                <input class='admin__control-text' type='text' name='configurable-matrix[0][name]' value='$name_product' placeholder='' aria-describedby='notice-UB00CJX' id='UB00CJX'>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </td>
                            <td class='admin__control-fields _no-header' data-index='sku_container'>
                                <fieldset class='admin__field' data-index='sku_container'>
                                    <legend class='admin__field-label'>
                                        <span>SKU</span>
                                    </legend>
                                    <div class='admin__field-control admin__control-fields _no-header'>
                                        <div class='admin__field' data-index='sku_edit'>
                                            <div class='admin__field-control'>
                                                <input class='admin__control-text' type='text' name='configurable-matrix[0][sku]' placeholder='' value='$sku' aria-describedby='notice-O2W759A' id='O2W759A'>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </td>
                            <td class='admin__control-fields _no-header' data-index='price_container'>
                                <fieldset class='admin__field' data-index='price_container'>
                                    <legend class='admin__field-label'>
                                        <span>Price</span>
                                    </legend>
                                    <div class='admin__field-control admin__control-fields _no-header'>
                                        <div class='admin__field' data-index='price_edit'>
                                    
                                            <div class='admin__field-control'>
                                                <div class='admin__control-addon'>
                                                    
                                                    <input class='admin__control-text' type='text' name='configurable-matrix[0][price]' placeholder='' value='$price' aria-describedby='notice-YA2648P' id='YA2648P'>
                                                    <label class='admin__addon-prefix'  for='YA2648P'>
                                                    <span data-bind='text: addbefore'>₫</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </td>
                            <td class='admin__control-fields _no-header' data-index='quantity_container'>
                                <fieldset class='admin__field' data-index='quantity_container'>
                                    <legend class='admin__field-label'>
                                        <span>Quantity</span>
                                    </legend>
                                    <div class='admin__field-control admin__control-fields _no-header'>
                                        <div class='admin__field' data-index='quantity_edit'>
                                            <div class='admin__field-control'>
                                                <input class='admin__control-text' type='text' name='configurable-matrix[0][qty]' placeholder='' value='' aria-describedby='notice-QB0IBT5' id='QB0IBT5'>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </td>
                            <td class='admin__control-fields _no-header' data-index='price_weight'>
                                <fieldset class='admin__field' data-index='price_weight'>
                                    <legend class='admin__field-label'>
                                        <span>Weight</span>
                                    </legend>
                                    <div class='admin__field-control admin__control-fields _no-header'>
                                        <div class='admin__field' data-index='weight_edit'>
                                            <div class='admin__field-control'>
                                                <input class='admin__control-text' type='text' name='configurable-matrix[0][weight]' placeholder='' value='' aria-describedby='notice-RXBVLDQ' id='RXBVLDQ'>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </td>
                            <td class='_no-header' data-index='status'>
                                <div class='control-table-text'>
                                    <span data-index='status'>Enabled</span>
                                </div>
                            </td>
                            <td class='_no-header' data-index='attributes'>
                                <div class='admin__field _no-header' data-index='attributes'>
                                    <label class='admin__field-label' for='K2A5D0C' style='display: none;'>
                                    <span>Attributes</span>
                                    </label>
                                    <div class='admin__field-control'>
                                        
                                        <div class='control-table-text'>
                                            <span data-index='attributes'>Color: Green</span>
                                        </div>
                                    
                                    </div>
                                </div>
                            </td>
                            <td class='data-grid-actions-cell _fit _no-header' data-index='actionsList'>
                                <div class='action-select-wrap'>
                                    <button class='action-select'>
                                    <span data-bind='i18n: 'Select''>Select</span>
                                    </button>
                                    <ul class='action-menu'>
                                        <li>
                                            <a class='action-menu-item'>Choose a different Product</a>
                                        </li>
                                        <li>
                                            <a class='action-menu-item'>Disable Product</a>
                                        </li>
                                        <li>
                                            <a class='action-menu-item'>Remove Product</a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>";
            }
        }

    }*/
}
if($action == "add"){
    $html .= "</tbody>
    </table>
    </div>";
}
$array_result["linked"] = $associatedProductIds;
$array_result["attribute_code"] = $array_attribute_code;
$array_result["variants"] = $array_variant;
$array_result["attribute_value"] = json_encode($attributeValues,JSON_UNESCAPED_UNICODE);
$array_result["html"] = $html;
echo json_encode($array_result,JSON_UNESCAPED_UNICODE);
