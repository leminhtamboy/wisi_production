<?php
use \Magento\Framework\App\Bootstrap;
include_once('../../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();        
$balanceId = $_POST["id"];
$in_charge_confirm = $_POST["inCharge"];

try {
    date_default_timezone_set("Asia/Bangkok");
    $curDateTime = date("Y-m-d H:i:s");
    $paid_status = "2";                  //paid_status 2: Paid

    $sql_update_balance_status = "UPDATE libero_provider_balance SET `in_charge_confirm` = '$in_charge_confirm', `updated_at` = '$curDateTime' , `status` = '$paid_status', `reply_request_date` = '$curDateTime' WHERE id = '$balanceId';";
    $connection->query($sql_update_balance_status);

    $sql_update_order_paid_status = "SELECT order_ids FROM libero_provider_balance WHERE id = '$balanceId';";
    $order_ids = $connection->fetchAll($sql_update_order_paid_status)[0]["order_ids"];

    $sql_update_status = "UPDATE `seller_sales_order` SET `updated_at` = '$curDateTime', `paid_status` = '$paid_status', `reply_request_date` = '$curDateTime' WHERE `entity_id` IN ($order_ids); ";
    $connection->query($sql_update_status);

} catch (LocalizedException $e) {
    file_put_contents("error_cron_balance.txt",$e->getMessage().PHP_EOL,FILE_APPEND);
} catch (\Exception $e) {
    file_put_contents("error_cron_balance.txt",$e->getMessage().PHP_EOL,FILE_APPEND);
}

$json_result  = array();
$json_result["success"] = "true";
$json_result["msg"] = "Confirm Paid Request Successfully";
echo json_encode($json_result);