<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('C:/wamp64/www/wisi_production/app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
try {
    date_default_timezone_set("Asia/Bangkok");
    $curDateTime = date("Y-m-d H:i:s");
    
    $complete_date = date("Y-m-d", strtotime("today"));
    $estimate_pay_at = date("Y-m-d", strtotime($complete_date . "+15 days"));
    
    $sql_group_orders = "SELECT sso.seller_id, GROUP_CONCAT(sso.entity_id ORDER BY sso.entity_id separator ',') as order_ids, GROUP_CONCAT(sso.increment_id ORDER BY sso.increment_id separator ',') as increment_ids, sso.provider_name , SUM(sso.grand_total) as group_total, SUM(sso.base_tax_amount) as group_base_tax_amount, SUM(sso.base_shipping_amount) as group_base_shipping_amount /* sso.shipping_description, ssog.payment_method */ FROM seller_sales_order as sso JOIN seller_sales_order_grid as ssog ON sso.increment_id = ssog.increment_id WHERE complete_date LIKE '$complete_date%' AND balance_grouped_id = 0 GROUP BY seller_id;";
    $group_orders = $connection->fetchAll($sql_group_orders);

    foreach ($group_orders as $_group_order) {
        $order_ids = $_group_order["order_ids"];
        $increment_ids = $_group_order["increment_ids"];
        $group_total = round($_group_order["group_total"]);
        $status = 0;              //0: Pending        1: Requested        2: Paid     3: Reject
        $updated_at = $curDateTime;
        $created_at = $curDateTime;
        $provider_id = $_group_order["seller_id"];
        $provider_name = $_group_order["provider_name"];
        $group_base_tax_amount = round($_group_order["group_base_tax_amount"]);
        $group_base_subtotal = round($group_total - $group_base_tax_amount);
        $group_base_shipping_amount = round($_group_order["group_base_shipping_amount"]);
        $commision_admin = 0;
        $sql_get_items  = "SELECT * FROM `seller_sales_order_item` WHERE order_id in ($order_ids)";
        $data_items = $connection->fetchAll($sql_get_items);
        foreach($data_items as $_items){
            $product_id = $_items["product_id"];
            $price = $_items["price_incl_tax"];
            $qty = round($_items["qty_ordered"],0);
            $product = $objectManager->create("\Magento\Catalog\Model\Product")->load($product_id);
            $categories = $product->getCategoryIds();
            $last_cate = $categories[count($categories) - 1];
            //get model category
            $categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
            $category = $categoryFactory->create()->load($last_cate);
            $commision = $category->getData("commission");
            if($commision == ""){
                $commision = 0;
            }
            $commision_admin += ($price * $qty * ($commision/100));
        }
        $real_receive = $group_total - $commision_admin;
        $sql_insert_group_orders = "INSERT INTO `libero_provider_balance` (`order_ids`, `increment_ids`, `group_total`, `status`, `updated_at`, `created_at`, `estimate_pay_at`, `provider_id`, `complete_date`, `provider_name`,`commision`,`real_receive`, `group_base_subtotal`, `group_base_tax_amount`, `group_base_shipping_amount`) VALUES ('$order_ids', '$increment_ids', '$group_total', '$status', '$updated_at', '$created_at', '$estimate_pay_at', '$provider_id', '$complete_date', '$provider_name','$commision_admin','$real_receive', '$group_base_subtotal', '$group_base_tax_amount', '$group_base_shipping_amount');";
        $insert = $connection->query($sql_insert_group_orders);
        $lastInsertId = $connection->lastInsertId();
        
        $sql_update_group_id = "UPDATE `seller_sales_order` SET `balance_grouped_id`='$lastInsertId', `updated_at` = '$updated_at' WHERE `entity_id` in ($order_ids);";
        $connection->query($sql_update_group_id);
    }    
} catch (LocalizedException $e) {
    file_put_contents("error_cron_balance.txt",$e->getMessage().PHP_EOL,FILE_APPEND);
} catch (\Exception $e) {
    file_put_contents("error_cron_balance.txt",$e->getMessage().PHP_EOL,FILE_APPEND);
}