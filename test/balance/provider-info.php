<?php
use \Magento\Framework\App\Bootstrap;
include_once('../../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();        
$id_seller = $_GET["id_seller"];

try {
    $sql_provider_info = "SELECT * FROM libero_customer_seller_company where `id_seller_company` = '$id_seller';";
    $result = $connection->fetchAll($sql_provider_info);
} catch (LocalizedException $e) {
    file_put_contents("error_cron_balance.txt",$e->getMessage().PHP_EOL,FILE_APPEND);
} catch (\Exception $e) {
    file_put_contents("error_cron_balance.txt",$e->getMessage().PHP_EOL,FILE_APPEND);
}

echo json_encode($result[0]);