<?php
use \Magento\Framework\App\Bootstrap;
include_once('../../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();        
$balanceId = $_POST["id"];
$cancel_reason = $_POST["cancel_reason"];
$in_charge_confirm = $_POST["inCharge"];

try {
    date_default_timezone_set("Asia/Bangkok");
    $curDateTime = date("Y-m-d H:i:s");
    $paid_status = "3";                  //paid_status 3: Reject

    $sql_update_balance_status = "UPDATE libero_provider_balance SET `in_charge_confirm` = '$in_charge_confirm', `updated_at` = '$curDateTime' , `cancel_pay_balance_reason` = '$cancel_reason', `reply_request_date` = '$curDateTime', `request_at` = '$curDateTime', `status` = '$paid_status' WHERE id = '$balanceId';";
    $connection->query($sql_update_balance_status);

    $sql_update_order_paid_status = "SELECT order_ids FROM libero_provider_balance WHERE id = '$balanceId';";
    $order_ids = $connection->fetchAll($sql_update_order_paid_status)[0]["order_ids"];

    $sql_update_status = "UPDATE `seller_sales_order` SET `updated_at` = '$curDateTime', `paid_status` = '$paid_status', `cancel_pay_balance_reason` = '$cancel_reason', `request_at` = '$curDateTime', `reply_request_date` = '$curDateTime' WHERE `entity_id` IN ($order_ids); ";
    $connection->query($sql_update_status);

} catch (LocalizedException $e) {
    file_put_contents("error_cron_balance.txt",$e->getMessage().PHP_EOL,FILE_APPEND);
} catch (\Exception $e) {
    file_put_contents("error_cron_balance.txt",$e->getMessage().PHP_EOL,FILE_APPEND);
}

$json_result  = array();
$json_result["success"] = true;
$json_result["msg"] = "Send Request Successfully";
echo json_encode($json_result);