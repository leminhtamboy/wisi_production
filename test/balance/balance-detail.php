<?php
use \Magento\Framework\App\Bootstrap;
include_once('../../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();        
$balanceId = $_POST["balanceId"];

try {
    $sql_order_ids = "SELECT * FROM libero_provider_balance WHERE id = '$balanceId';";
    $order_ids = $connection->fetchAll($sql_order_ids);
    $order_ids = $order_ids[0]["order_ids"];

    $sql_orders = "SELECT *,seller_sales_order.status as status_parent, seller_sales_order.paid_status as paid_status, seller_sales_order_grid.payment_method, seller_sales_order.shipping_description FROM seller_sales_order inner join  seller_sales_order_grid on seller_sales_order.entity_id = seller_sales_order_grid.order_id  where seller_sales_order.entity_id IN ($order_ids);";
    $orders = $connection->fetchAll($sql_orders);

    $json_result  = array();
    $json_result["data"] = $orders;
    $json_result["success"] = true;
    $json_result["msg"] = "Send Request Successfully";
    echo json_encode($json_result);
} catch (LocalizedException $e) {
    file_put_contents("error_cron_balance.txt",$e->getMessage().PHP_EOL,FILE_APPEND);
} catch (\Exception $e) {
    file_put_contents("error_cron_balance.txt",$e->getMessage().PHP_EOL,FILE_APPEND);
}

