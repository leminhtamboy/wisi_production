<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$product_id = $_POST["product_id"];
$file_img = $_POST["file_img"];
$file_profuct = $_POST["file_profuct"];
$type_img = $_POST["type_img"];
$imageProcessor = $objectManager->create('\Magento\Catalog\Model\Product\Gallery\Processor');
if(strpos($file_profuct,"temp") == false){
    //Truong hop khong phai la temp image trước khi cron image dc push vào
    $product = $objectManager->create("\Magento\Catalog\Model\Product")->load($product_id);
    $imageProcessor->removeImage($product,$file_profuct);
    $image_data = $product->getData("image_data");
    $array_img = json_decode($image_data,true);
    if($type_img == "main-thumb"){
        $arrayImages = $array_img["main"][$product_id]["sub_img"];
        $index_delete = -1;
        foreach($arrayImages as $_images){
            $index_delete++;
            if($_images == $file_profuct){
                //delete here
                unset($arrayImages[$index_delete]);
            }
        }
    }
    $json_save_image_data = json_encode($array_img,JSON_UNESCAPED_UNICODE);
    $product->setData("image_data",$json_save_image_data);
    $product->save();
}else{
    //Xử lý cho image data luôn
    $image_data = $product->getData("image_data");
    $array_img = json_decode($image_data,true);
    if($type_img == "main-thumb"){
        $arrayImages = $array_img["main"][$product_id]["sub_img"];
        $index_delete = -1;
        foreach($arrayImages as $_images){
            $index_delete++;
            if($_images == $file_profuct){
                //delete here
                unset($arrayImages[$index_delete]);
            }
        }
    }
    if($type_img == "main-base"){
        $base_img = $array_img["main"][$product_id]["base_img"];
        if($base_img == $file_profuct){
            //delete here
            $array_img["main"][$product_id]["base_img"] = "";
        }
    }
    //trả ngược về lại
    $json_save_image_data = json_encode($array_img,JSON_UNESCAPED_UNICODE);
    $product->setData("image_data",$json_save_image_data);
    $product->save();
}
@unlink($file_img);