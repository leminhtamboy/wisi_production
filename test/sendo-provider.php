<?php header('Access-Control-Allow-Origin: *'); ?>
<?php
include_once("simple_html_dom.php");
include_once("database.php");
$database = new database();
$cat = 8;
for($p = 1 ; $p < 99999 ; $p++){
    $url = "https://www.sendo.vn/m/wap_v2/category/product?category_id=$cat&p=$p&s=1000&sortType=vasup_desc";
    $header=array(
    "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "Accept-Encoding:gzip, deflate, br",
    "Accept-Language:en-US,en;q=0.9,vi;q=0.8,ko;q=0.7,fr;q=0.6",
    "Connection:keep-alive",
    "Upgrade-Insecure-Requests: 1",
    "User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36"
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
    curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 60000);
    $content = curl_exec($ch);
    curl_close($ch);
    $json_list_product = json_decode($content,true);
    $list_product = $json_list_product["result"]["data"];
    if(count($list_product) == 0 ){
        break;
    }
    $sql_insert = "INSERT INTO `libero_import_sendo_provider` (`id_provider`, `shop_name`, `sdt`, `email`, `dia_chi`, `da_xac_thuc`, `good_review`,`url_shop`) VALUES ";
    $is_last = false;
    foreach($list_product as $_product){
        $link_product = str_replace(".html","",$_product["cat_path"]);
        $da_xac_thuc = $_product["shop_info"]["is_certified"]; // 1 la da co , 0 la ko co
        $shop_name = $_product["shop_info"]["shop_name"];
        $sql_xet_trung = "SELECT `shop_name` FROM `libero_import_sendo_provider` WHERE `shop_name`='$shop_name'";
        $data_trung = $database->ExcuteObjectList($sql_xet_trung);
        if(count($data_trung) > 0){
            continue;
        }
        $good_review_percent = $_product["shop_info"]["good_review_percent"];
        $url_detail_product = "https://www.sendo.vn/m/wap_v2/full/san-pham/".$link_product;
        $header_detail=array(
        "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Encoding:gzip, deflate, br",
        "Accept-Language:en-US,en;q=0.9,vi;q=0.8,ko;q=0.7,fr;q=0.6",
        "Connection:keep-alive",
        "Upgrade-Insecure-Requests: 1",
        "User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36"
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url_detail_product);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header_detail);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 60000);
        $content_detail = curl_exec($ch);
        curl_close($ch);
        //ra duoc content lay duoc dt cua shop
        $json_detail = json_decode($content_detail,true);
        $shop_detail = $json_detail["result"]["data"];
        $sdt = "";
        $email = "";
        $website_url = "";
        $dia_chi = "";
        echo "=== GO TO PRODUCT ".$link_product.PHP_EOL;
        if(count($shop_detail) > 0){
            $_shop = $shop_detail;
            $sdt = $_shop["shop_info"]["phone_number"];
            $link_shop = $_shop["shop_info"]["shop_url"];
            $info_shop = "https://www.sendo.vn/$link_shop/thong-tin-shop/";
            $header_shop=array(
                "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "Accept-Encoding:gzip, deflate, br",
                "Accept-Language:en-US,en;q=0.9,vi;q=0.8,ko;q=0.7,fr;q=0.6",
                "Connection:keep-alive",
                "Upgrade-Insecure-Requests: 1",
                "User-Agent:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36"
                );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$info_shop);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
            curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
            curl_setopt($ch, CURLOPT_HTTPHEADER,$header_shop);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 60000);
            $content_shop = curl_exec($ch);
            curl_close($ch);
            $dom = new simple_html_dom();
            $html = $dom->load($content_shop);
            $i = 0;
            foreach($html->find('div[class=address-shop-rw]') as $address){
                $thong_tin = $address->find("span",0);
                if($i == 0){
                    $dia_chi = $thong_tin->innertext;
                }
                if($i == 2){
                    $email  = $thong_tin->innertext;
                }
                if($i == 3){
                    $website_url  = $thong_tin->innertext;
                }
                $i++;
            }
            echo "===========> DA LAY XONG PROVIDER".$shop_name.PHP_EOL;
        }else{
            continue;
        }
        $sql_insert .= "(NULL, '$shop_name', '$sdt', '$email', '$dia_chi', '$da_xac_thuc', '$good_review_percent','$website_url'),";
        $is_last = true;
    }
    if($is_last == true){
        $sql_insert = substr($sql_insert,0,strlen($sql_insert) - 1);
        $sql_insert.=";";
        $database->ExcuteNonQuery($sql_insert);
    }
}
?>