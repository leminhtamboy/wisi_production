<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('/var/www/wisi_production/app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$sql_get_cron = "SELECT * FROM libero_cron_execute_background WHERE `status` = 0";
$data_cron = $connection->fetchAll($sql_get_cron);
$dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
$imageProcessor = $objectManager->create('\Magento\Catalog\Model\Product\Gallery\Processor');
foreach($data_cron as $_cron){
    $id_cron = $_cron["id_cron"];
	$image_error = "";
    try{
        //Upload image
        $image_data = $_cron["content_excute"];
        $type_cron = $_cron["type_cron"];
        if($type_cron == "CRON_PRODUCT_IMAGE"){
            $array_image_data = json_decode($image_data,true);
            $array_image_variant_data = array();
            //Split image data;
            if(count($array_image_data) > 0 ){
                foreach($array_image_data["main"] as $key => $_data){
                    $productId = $key;
                    break;
                }
                $base_img = $array_image_data["main"][$productId]["base_img"];
                $arrayImages = $array_image_data["main"][$productId]["sub_img"];
                if(isset($array_image_data["variant"])){
                    $array_base_image_variant = $array_image_data["variant"];
                    foreach($array_base_image_variant as $key => $value){
                        $array_image_variant_data[$key] = $value["base_img"];
                    }
                }
				//Deleteting database all
				$sql_select_value_id = "SELECT * FROM catalog_product_entity_media_gallery_value where entity_id = '$productId'";
				//echo $sql_select_value_id;
				$data_value_id = $connection->fetchAll($sql_select_value_id);
				$string_value_id = "";
				foreach($data_value_id as $_value_id){
					$string_value_id .= $_value_id["value_id"].",";
				}
				if($string_value_id != ""){
					$string_value_id = substr($string_value_id,0,strlen($string_value_id) - 1);
					$sql_delete = "DELETE FROM `catalog_product_entity_media_gallery_value` WHERE entity_id = '$productId'";
					$connection->query($sql_delete);
					//echo $sql_delete;
					$sql_delete_value_id = "DELETE FROM `catalog_product_entity_media_gallery` WHERE value_id in ($string_value_id);";
					//echo $sql_delete_value_id;
					$connection->query($sql_delete_value_id);
					
					$sql_delete = "DELETE FROM `catalog_product_entity_media_gallery_value_to_entity` WHERE entity_id = '$productId'";
					$connection->query($sql_delete);
					//echo $sql_delete;
				}
				/*$product = $objectManager->create("\Magento\Catalog\Model\Product")->load($productId);
                $arrayImagesRemove = $product->getMediaGalleryImages();
                foreach($arrayImagesRemove as $_child){
                    $imageProcessor->removeImage($product, $_child->getFile());
                }
				$product->save();*/
				//die;
                //Add base image for main
				$product_base_img = $objectManager->create("Magento\Catalog\Model\Product")->load($productId);
                $product_base_img->addImageToMediaGallery($dir->getPath('media')."/".$base_img, array('image', 'small_image', 'thumbnail'), false, false);
                $product_base_img->save();
                //Add thumbnail for main
				$product_thumb = $objectManager->create("Magento\Catalog\Model\Product")->load($productId);
                foreach($arrayImages as $_img_path) {
					if($_img_path != ""){
						try{
							$product_thumb->addImageToMediaGallery($dir->getPath('media')."/".$_img_path, array(), false, false);
						}catch(\Exception $e){
							$error_log = $time_update."___ERROR_SUB_IMG___CRON [".$id_cron."] CO LOI LA ".$image_error."".$e->getMessage().PHP_EOL;
							echo $error_log;
						}
					}
                }
				$product_thumb->save();	
                //Add base image for variant
                foreach($array_image_variant_data as $variant_id => $value){
                    //Have variant
					try{
						
                    $variant = $objectManager->create("\Magento\Catalog\Model\Product")->load($variant_id);
                    $arrayImagesRemoveVariant = $variant->getMediaGalleryImages();
                    foreach($arrayImagesRemoveVariant as $_child){
                        $imageProcessor->removeImage($variant, $_child->getFile());
                    }
                    $variant_img = $value;
                    $variant->addImageToMediaGallery($dir->getPath('media')."/".$variant_img, array('image', 'small_image', 'thumbnail'), false, false);
                    foreach($arrayImages as $_img_path) {
						if($_img_path != ""){
							$variant->addImageToMediaGallery($dir->getPath('media')."/".$_img_path, array(), false, false);
						}
                    }
                    //$variant->setStoreId(0);
                    $variant->save();
					
					}catch(\Exception $e){
						$error_log = $time_update."___ERROR_VARIANT_IMG___CRON [".$id_cron."] CO LOI LA ".$image_error."".$e->getMessage().PHP_EOL;
						echo $error_log;
					}
                }
            }
        }
        if($type_cron == "CRON_PRODUCT_STATUS"){
            $data_status = json_decode($image_data,true);
            $product_id = $data_status["product_id"];
            $status = $data_status["status"];
            $product = $objectManager->create("\Magento\Catalog\Model\Product")->load($product_id);
            $product->setData("status",$status);
            $product->save();
        }
        //Update CRON SUCCESS;
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $time_update = date("Y-m-d H:i:s");
        $sql_update_cron = "UPDATE `libero_cron_execute_background` SET `status` = '1',`etc` = '$time_update' WHERE `id_cron` = '$id_cron';";
        $connection->query($sql_update_cron);
    }catch(\Exception $e){
        //Update CRON FAIL;
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $time_update = date("Y-m-d H:i:s");
        $error_log = $time_update."___ERROR_OUT___CRON [".$id_cron."] CO LOI LA ".$image_error."".$e->getMessage().PHP_EOL;
		echo $error_log;
        $error_log = str_replace("'","",$error_log);
        $sql_update_cron = "UPDATE `libero_cron_execute_background` SET `status` = '2',`etc` = '$error_log' WHERE `id_cron` = '$id_cron';";        
        $connection->query($sql_update_cron);
    }
}