<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
/** Include PHPExcel */
function RemoveSign($str){

    $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
    ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
        "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
    ,"ờ","ớ","ợ","ở","ỡ",
        "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
        "ỳ","ý","ỵ","ỷ","ỹ",
        "đ",
        "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
    ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
        "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
        "Ì","Í","Ị","Ỉ","Ĩ",
        "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
    ,"Ờ","Ớ","Ợ","Ở","Ỡ",
        "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
        "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
        "Đ","ê","ù","à");
    $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
    ,"a","a","a","a","a","a",
        "e","e","e","e","e","e","e","e","e","e","e",
        "i","i","i","i","i",
        "o","o","o","o","o","o","o","o","o","o","o","o"
    ,"o","o","o","o","o",
        "u","u","u","u","u","u","u","u","u","u","u",
        "y","y","y","y","y",
        "d",
        "A","A","A","A","A","A","A","A","A","A","A","A"
    ,"A","A","A","A","A",
        "E","E","E","E","E","E","E","E","E","E","E",
        "I","I","I","I","I",
        "O","O","O","O","O","O","O","O","O","O","O","O"
    ,"O","O","O","O","O",
        "U","U","U","U","U","U","U","U","U","U","U",
        "Y","Y","Y","Y","Y",
        "D","e","u","a");
    return str_replace($coDau,$khongDau,$str);
}
require_once("Classes/PHPExcel.php");
require_once("Classes/PHPExcel/IOFactory.php");
//Create a PHPExcel object
$name_file = "topten.xlsx";
$objPHPExcel = PHPExcel_IOFactory::load("import/".$name_file);
$_categoryFactory = $objectManager->get('Magento\Catalog\Model\CategoryFactory');
$dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
$id_provider = 10;
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    $worksheetTitle     = $worksheet->getTitle();
    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
    $nrColumns = ord($highestColumn) - 64;
    echo "<br>The worksheet ".$worksheetTitle." has ";
    echo $nrColumns . ' columns (A-' . $highestColumn . ') ';
    echo ' and ' . $highestRow . ' row.';
    echo '<br>Data: <table border="1"><tr>';
    for ($row = 2; $row <= $highestRow; ++ $row) {
        //Insert database
        $sku = $id_provider;
        $type_product = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
        $name_product = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
        $danh_muc = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
        $array_danh_muc = explode("/",$danh_muc);
        $array_category = array();
        $last_category_id = "";
        foreach($array_danh_muc as $_cate){
            $collection = $_categoryFactory->create()->getCollection()->addAttributeToFilter("name",array("like" => "%".$_cate."%"))->setPageSize(1);
            if ($collection->getSize()) {
                $categoryId = $collection->getFirstItem()->getId();
                $array_category[] = $categoryId;
                $sku.=$categoryId;
                $last_category_id = $categoryId;
            }
        }
        $sku.=time();
        $price = $worksheet->getCellByColumnAndRow(6, $row)->getValue();
        $special_price = $worksheet->getCellByColumnAndRow(7, $row)->getValue();
        $free_shipment = 0;
        $tax = "None";
        $qty = $worksheet->getCellByColumnAndRow(10, $row)->getValue();
        $url_key = RemoveSign($name_product)."-".$id_provider;
        $min_qty = 1;
        $status = 1;
        $weight = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
        $length = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
        $height = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
        $width = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
        $short_description = $worksheet->getCellByColumnAndRow(16, $row)->getValue();
        $description = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
        $quick_detail = $worksheet->getCellByColumnAndRow(23, $row)->getValue();
        $array_quick_detail = explode(";",$quick_detail);
        $q = 0;
        $length_quick_detail = count($array_quick_detail);
        $array_json_quick_detail = array();
        $array_json_quick_detail["data"] = array();
        $array_json_quick_detail["is_have"] = "false";
        $array_content_quick_detail = array();
        for($q = 0 ; $q < $length_quick_detail ; $q++){
            $array_child  = array();
            $item = explode(":",$array_quick_detail[$q]);
            $array_child["title"]  = $item[0];
            $array_child["content"]  = $item[1];
            $array_content_quick_detail[] = $array_child;
        }
        if(count($array_content_quick_detail) > 0){
            $array_json_quick_detail["data"] = $array_content_quick_detail;
            $array_json_quick_detail["is_have"] = "true";
        }
        $json_quick_detail = json_encode($array_json_quick_detail,JSON_UNESCAPED_UNICODE);
        $meta_title = $worksheet->getCellByColumnAndRow(24, $row)->getValue();
        $meta_keyword = $worksheet->getCellByColumnAndRow(25, $row)->getValue();
        $meta_description = $worksheet->getCellByColumnAndRow(26, $row)->getValue();
        try{
            $product = $objectManager->get("\Magento\Catalog\Model\Product");
            $product->setSku($sku); // Set your sku here
            $product->setName($name_product); // Name of Product
            $product->setTypeId('simple');
            $product->setAttributeSetId(4); // Attribute set id
            $product->setStatus($status); // Status on product enabled/ disabled 1/0
            $product->setWeight($weight);
            $product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
            $product->setTaxClassId(0); // Tax class id
            $product->setStockData(['use_config_manage_stock' => 1,'min_sale_qty' => $min_qty,'is_in_stock' => 1,"qty" => $qty]);
            echo $price;
            $product->setPrice($price); // price of product
            $product->setSpecialPrice($special_price); // special price of product
            $product->setFreeShipment($free_shipment); // free_shipment enabled/ disabled 1/0
            $product->setData("quick_detail",$json_quick_detail);
            $product->setDescription($description);
            $product->setCategoryIds($array_category);
            $product->setShortDescription($short_description);
            $product->setLastCategoryId($last_category_id);
            $product->setData("url_key",$url_key);
            $product->setData("meta_title",$meta_title);
            $product->setMetaKeyword($meta_keyword);
            $product->setData("meta_description",$meta_description);
            $product->setData("id_seller",$id_provider);
            $product->setWebsiteIds(array(1));
            $product->setData("length",$length);
            $product->setData("height",$height);
            $product->setData("width",$width);
            $product->save();
            //Xử lý phức tạp hình ảnh với trường là url
            $product_id = $product->getId();
            //File top ten can 1 url
                $base_img = $worksheet->getCellByColumnAndRow(27,$row)->getValue();
                $base_img = "http://toptenhanggiadung.com/".$base_img;
                $ext_base_img = pathinfo($base_img, PATHINFO_EXTENSION);
                $name_base_image = "base-img-".time().".".$ext_base_img;
                file_put_contents($dir->getPath('media')."/temp_image_product/".$name_base_image,file_get_contents($base_img));

                $danh_sach_hinh_khac = "http://toptenhanggiadung.com/".$worksheet->getCellByColumnAndRow(28,$row)->getValue();
                $ext_sub_img = pathinfo($danh_sach_hinh_khac, PATHINFO_EXTENSION);
                $name_sub_image = "sub-img-".time().".".$ext_sub_img;
                file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($danh_sach_hinh_khac));
                $array_path_img = array();
                $image_data = array(); // Remember this case save json as key 'main' and variant (if is config) in main is key id main product
                $main_img_child = array();
                $array_main_child_image = array();
                $array_temp_sub_img = array();
                $array_path_img [] = "temp_image_product/".$name_sub_image;
                //First create for main product , in main have key is product id in key have base image is key ann value is path image
                $array_main_child_image['base_img'] = "temp_image_product/".$name_base_image;        
                foreach($array_path_img as $_img_path) {
                    $array_temp_sub_img[] = $_img_path;
                }
                $array_main_child_image['sub_img'] = $array_temp_sub_img;
                $main_img_child[$product->getId()] = $array_main_child_image;
                $image_data["main"] = $main_img_child;
                $json_image_data = json_encode($image_data,JSON_UNESCAPED_UNICODE);
            //End
            $product->setData("image_data",$json_image_data);
            $product->save();
            //Add to cron job image
                //Put save image to cron job
                $type_cron = "CRON_PRODUCT_IMAGE";
                $cron_code = "CRON_EX15G79_PRODUCT_".$product_id."_UPDATE";
                $content_cron_excute = $json_image_data;
                date_default_timezone_set("Asia/Ho_Chi_Minh");
                $time_update = date("Y-m-d H:i:s");
                $created_at = $time_update;
                $status = 0; // Processsing;
                $etc = "";
                $sql_insert_cron = "INSERT INTO `libero_cron_execute_background` (`id_cron`, `type_cron`, `cron_code`, `content_excute`, `created_at`, `status`, `etc`) 
                VALUES (NULL, '$type_cron', '$cron_code', '$content_cron_excute', '$created_at', '0', '$etc');";
                $connection->query($sql_insert_cron);
            //End add to cron job image
        }catch(Exception $e){
            $text = "SAN PHAM CO TEN LA ".$product_name." BI LOI ".$e->getMessage().PHP_EOL;
            file_put_contents("error_import.txt",$text,FILE_APPEND);
        }
        /*for ($col = 0; $col < $highestColumnIndex; ++ $col) {
            $cell = $worksheet->getCellByColumnAndRow($col, $row);
            $val = $cell->getValue();
            //$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
            //echo '<td>' . $val . '<br>(Typ ' . $dataType . ')</td>';
            //Insert columns in here
        }*/
    }
    break;
}