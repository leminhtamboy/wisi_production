<?php
echo json_encode(array("error" => "false"));
die();
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$request_body = file_get_contents('php://input');
$data = json_decode($request_body,true);
$variant_id = $data["variant_id"];
$url_base_img = $data["url_base_img"];
$array_thumbnail_img = $data["thumbnail"];
$dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
try{
    $variant = $objectManager->create("\Magento\Catalog\Model\Product")->load($variant_id);
    $variant_img = $url_base_img;
    //$variant->addImageToMediaGallery($dir->getPath('media')."/".$variant_img, array('image'), false, false);
    foreach($array_thumbnail_img as $_img_path) {
        //$variant->addImageToMediaGallery($dir->getPath('media')."/".$_img_path, array('small_image'), false, false);
    }
    //$variant->save();
    echo json_encode(array("error" => "false"));
}catch(\Exception $e){
    echo json_encode(array("error" => "true","msg" => $e->getMessage()));
}
