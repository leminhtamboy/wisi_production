<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$object_Manager = $bootstraps->getObjectManager();
$state = $object_Manager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
function RemoveSign($str){
    $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
    ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
        "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
    ,"ờ","ớ","ợ","ở","ỡ",
        "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
        "ỳ","ý","ỵ","ỷ","ỹ",
        "đ",
        "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
    ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
        "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
        "Ì","Í","Ị","Ỉ","Ĩ",
        "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
    ,"Ờ","Ớ","Ợ","Ở","Ỡ",
        "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
        "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
        "Đ","ê","ù","à");
    $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
    ,"a","a","a","a","a","a",
        "e","e","e","e","e","e","e","e","e","e","e",
        "i","i","i","i","i",
        "o","o","o","o","o","o","o","o","o","o","o","o"
    ,"o","o","o","o","o",
        "u","u","u","u","u","u","u","u","u","u","u",
        "y","y","y","y","y",
        "d",
        "A","A","A","A","A","A","A","A","A","A","A","A"
    ,"A","A","A","A","A",
        "E","E","E","E","E","E","E","E","E","E","E",
        "I","I","I","I","I",
        "O","O","O","O","O","O","O","O","O","O","O","O"
    ,"O","O","O","O","O",
        "U","U","U","U","U","U","U","U","U","U","U",
        "Y","Y","Y","Y","Y",
        "D","e","u","a");
    return str_replace($coDau,$khongDau,$str);
}
function rewriteUrl($store_name){
    $urlRewrite = trim(RemoveSign($store_name));
    $urlRewrite = preg_replace('#[^0-9a-z]+#i', '-', $urlRewrite);
    $urlRewrite = strtolower($urlRewrite);
    return $urlRewrite;
}
function rewriteEmail($email){
    $urlRewrite = trim(RemoveSign($email));
    $urlRewrite = preg_replace('#[^0-9a-z]+#i', '_', $urlRewrite);
    return $urlRewrite;
}
$postData = $_POST;
$phone = $postData["sdt_ban"];
$email = $postData["email"];
if(count(explode("khongcoemail",$email)) > 1){
    $email = rewriteEmail($email);
    $email = str_replace("khongcoemail","@gmail",$email);
    $email = str_replace("_com",".com",$email);
}
$sellerName = $postData["agent_name"];
$password = "zxcasdqwe123";
$hot_line = $postData["hot_line"];
$store_name = $postData["store_name"];
//Company information
$biz_type = "1";
$company = $postData["company_name"];
$address = $postData["dia_chi"];
$city = "Hồ Chí Minh";
$district = "Quận 1";
$street = "123 Nguyen Huu Canh";
$tax = $postData["sdt_ban"];
$home_page = $postData["url_web"];
$agent_name = $postData["agent_name"];
$registration_number = $postData["company_registration"];
$bank_name = "";
$bank_account = "";
$etc = "";
$is_manufacture = 1;
$is_genuine = 0;
$logo_url = $postData["url_logo"];
$banner_url = $postData["url_banner"];
$description = $postData["description"];
$rating = 0;
$string_category = $postData["string_category"];
$verified = $postData["verified"];
if($verified == "not_furia"){
    $biz_type = "1";
}else{
    $biz_type = "2";
}
//Create Provider

$url = \Magento\Framework\App\ObjectManager::getInstance();
//Upload image
$fileSystem = $object_Manager->create('\Magento\Framework\Filesystem');
$sellerNameRewrite = rewriteUrl($sellerName);
$path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('upload_manager/'.$sellerNameRewrite."/".$email."/".date("d-m-Y"));
@mkdir($path,0777,true);
@chmod($path, 0777);
$pathSave = '/var/www/wisi_production/pub/media/upload_manager/'.$sellerNameRewrite."/".$email."/".date("d-m-Y")."/noimage.png";
@file_put_contents($pathSave,file_get_contents("/var/www/wisi_production/test/noimage.png"));
$result = "noimage.png";
$path_save_database = 'upload_manager/'.$sellerNameRewrite."/".$email."/".date("d-m-Y")."/noimage.png";
$imagepath_1= $path_save_database;
$imagepath_2= $path_save_database;
$imagepath_3 = $path_save_database;

$data_logo  = file_get_contents($logo_url);
$ext_logo = pathinfo($logo_url,PATHINFO_EXTENSION);
$path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('seller_logo')."/";
$image_name_2 = 'img-logo-' . time() . ".$ext_logo";
$pathSave = $path.$image_name_2;
$file = fopen($pathSave,"w+");
fputs($file,$data_logo);
fclose($file);
$imagepath_logo = "seller_logo/".$image_name_2;
if($banner_url == "")
{
    $banner_url = "/var/www/wisi_production/test/noimage.png";
}else{

}
$data_banner = file_get_contents($banner_url);
$ext_banner = pathinfo($banner_url,PATHINFO_EXTENSION);
$path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('seller_banner')."/";
$image_name_3 = 'img-banner-' . time() . ".$ext_banner";
$pathSave = $path.$image_name_3;
$file = fopen($pathSave,"w+");
fputs($file,$data_banner);
fclose($file);
$imagepath_4 = "seller_banner/".$image_name_2;;

$delivery_type = "wisi_delivery";
$description_seller = $postData["description"];

date_default_timezone_set("Asia/Bangkok");
$curDateTime = date("Y-m-d H:i:s");

//End upload

$customerFactory = $object_Manager->get('\Magento\Customer\Model\CustomerFactory');
$storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');
$websiteId = $storeManager->getWebsite()->getWebsiteId();
$store = $storeManager->getStore();  // Get Store ID
$storeId = $store->getStoreId();
$modelCustomerSellerCompany = $object_Manager->create("\Libero\Customer\Model\CustomerSeller");
$modelCustomerSellerCompany->setData("company",$company);
$modelCustomerSellerCompany->setData("address",$address);
$modelCustomerSellerCompany->setData("city",$city);
$modelCustomerSellerCompany->setData("district",$district);
$modelCustomerSellerCompany->setData("street",$street);
$modelCustomerSellerCompany->setData("full_address",$address." ".$street." ".$district." ".$city);
$modelCustomerSellerCompany->setData("telephone",$phone);
$modelCustomerSellerCompany->setData("tax",$tax);
$modelCustomerSellerCompany->setData("home_page",$home_page);
$modelCustomerSellerCompany->setData("agent_name",$agent_name);
$modelCustomerSellerCompany->setData("bank_name",$bank_name);
$modelCustomerSellerCompany->setData("bank_account",$bank_account);
$modelCustomerSellerCompany->setData("etc",$etc);
$modelCustomerSellerCompany->setData("is_manufacture",$is_manufacture);
$modelCustomerSellerCompany->setData("is_genuine",$is_genuine);
$modelCustomerSellerCompany->setData("biz_type",$biz_type);
$modelCustomerSellerCompany->setData("company_registration",$registration_number);
$modelCustomerSellerCompany->setData("up_load_cer_1",$imagepath_1);
$modelCustomerSellerCompany->setData("up_load_cer_2",$imagepath_2);
$modelCustomerSellerCompany->setData("up_load_cer_3",$imagepath_3);
$modelCustomerSellerCompany->setData("store_name",$store_name);
$modelCustomerSellerCompany->setData("status",1);
$modelCustomerSellerCompany->setData("message_denied","");
$modelCustomerSellerCompany->setData("id_category",$string_category);
$modelCustomerSellerCompany->setData("logo_url",$imagepath_logo);
$modelCustomerSellerCompany->setData("banner_url1",$imagepath_4);
$modelCustomerSellerCompany->setData("description",$description_seller);
$modelCustomerSellerCompany->setData("hot_line",$hot_line);
$modelCustomerSellerCompany->setData("rating_sumary",0);
$modelCustomerSellerCompany->setData('created_at', $curDateTime);
$modelCustomerSellerCompany->setData('updated_at', $curDateTime);
$modelCustomerSellerCompany->setData('delivery_type',$delivery_type);
//Save customer
$customer = $customerFactory->create();
$customer->setWebsiteId($websiteId);
$load_by_email = $customer->loadByEmail($email);
$is_save_seller = false;
if(!$load_by_email->getEntityId()){
    $customer->setWebsiteId($websiteId);
    $customer->setEmail($email);
    $customer->setFirstname($sellerName);
    $customer->setLastname("");
    $customer->setPassword($password);
    //$customer->setDob($dateOfBirth);
    $customer->setGroupId(2);
    $customer->save();
    $is_save_seller = true;
}
$postCode = "1442"."-"."20101";
//Save address customer
$addresssModel = $object_Manager->get('\Magento\Customer\Model\AddressFactory');
$addressModel = $addresssModel->create();
if($sellerName == "")
{
    $sellerName = "noName";
}
$addressModel->setCustomerId($customer->getId())
    ->setFirstname($sellerName)
    ->setLastname($company)
    ->setCountryId('VN')
    ->setPostcode($postCode)
    ->setCity($city)
    ->setTelephone($phone)
    ->setFax($tax)
    ->setCompany($company)
    ->setCompany($company)
    ->setStreet($address." ".$street." ".$district)
    ->setIsDefaultBilling('1')
    ->setIsDefaultShipping('1')
    ->setSaveInAddressBook('1');
$addressModel->save();
if($is_save_seller){
    $modelCustomerSellerCompany->setData("id_customer",$customer->getId());
    $modelCustomerSellerCompany->save();
}
$id_seller = $modelCustomerSellerCompany->getId();
$status  = 1;
$price = 0;

$deliverys = array();
foreach($deliverys as $delivery){
    $deli_arr = explode("|",$delivery);
    $code = $deli_arr[0];
    $name = $deli_arr[1];
    $shipping = $object_Manager->create("Libero\Onestepcheckout\Model\Shipping");
    $shipping->setData("shipping_code",$code);
    $shipping->setData("name_shipping_method",$name);
    $shipping->setData("title_shipping_method",$name); 
    $shipping->setData("status_shipping_method",$status);
    $shipping->setData("id_seller",$id_seller); 
    $shipping->setData("price",$price);
    $shipping->setData('created_at', $curDateTime);
    $shipping->setData('updated_at', $curDateTime);
    $shipping->save();
}