<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('/var/www/wisi_production/app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$object_Manager = $bootstraps->getObjectManager();
$state = $object_Manager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
/** Include PHPExcel */
require_once("Classes/PHPExcel.php");
//Create a PHPExcel object
$objPHPExcel = new PHPExcel();

//Set document properties
$objPHPExcel->getProperties()->setCreator("Wisi")
							 ->setLastModifiedBy("Wisi")
							 ->setTitle("User's Information")
							 ->setSubject("User's Personal Data")
							 ->setDescription("Description of User's")
							 ->setKeywords("")
							 ->setCategory("");

// Set default font
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
                                          ->setSize(10);

//Set the first row as the header row
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Sku')
							  ->setCellValue('B1', 'Loại Sản Phẩm')
                              ->setCellValue('C1', 'Tên Sản Phẩm')
                              ->setCellValue('D1', 'Danh Mục')
                              ->setCellValue('E1', 'Màu')
                              ->setCellValue('F1', 'Kích Thước')
                              ->setCellValue('G1', 'Giá Bản Lẻ')
                              ->setCellValue('H1', 'Giá Giảm')
                              ->setCellValue('I1', 'Cho phép Free Ship')
                              ->setCellValue('J1', 'Tax')
                              ->setCellValue('K1', 'Số Lượng Tồn Kho')
                              ->setCellValue('L1', 'Số Lượng Mua Ít Nhất')
                              ->setCellValue('M1', 'Khối Lượng')
                              ->setCellValue('N1', 'Chiều Dài')
                              ->setCellValue('O1', 'Chiều Cao')
                              ->setCellValue('P1', 'Chiều Rộng')
                              ->setCellValue('Q1', 'Mô Tả Ngắn')
                              ->setCellValue('R1', 'Text Mô Tả Dài 1')
                              ->setCellValue('S1', 'Hình Mô Tả Dài 1')
                              ->setCellValue('T1', 'Text Mô Tả Dài 2')
                              ->setCellValue('U1', 'Hình Mô Tả Dài 2')
                              ->setCellValue('V1', 'Text Mô Tả Dài 3')
                              ->setCellValue('W1', 'Hình Mô Tả Dài 3')
                              ->setCellValue('X1', 'Mô Tả Chung')
                              ->setCellValue('Y1', 'SEO Tiêu Đề')
                              ->setCellValue('Z1', 'SEO Từ Khóa')
                              ->setCellValue('AA1', 'SEO Mô Tả')
                              ->setCellValue('AB1', 'Hình Đại Diện')
                              ->setCellValue('AC1', 'Các Hình Khác');
		  
//Rename the worksheet
$objPHPExcel->getActiveSheet()->setTitle('Products');

//Set active worksheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
//Create List Sheet For Category,Tax,Free Shipment,type product,Color,Size
$sheet_contain_list = $objPHPExcel->createSheet(1);
$sheet_contain_list->setTitle("Reference");
$sheet_contain_list->setCellValue("A1","List Category")
                    ->setCellValue("B1","List Tax")
                    ->setCellValue("C1","Color")
                    ->setCellValue("D1","Size");
//End of List
//Set Comment Column
//Set for Type product
$objPHPExcel->getActiveSheet()
    ->getComment('B1')
    ->setAuthor('Wisi');
$objCommentRichText = $objPHPExcel->getActiveSheet()
    ->getComment('B1')
    ->getText()->createTextRun('Loại sản phẩm:');
$objCommentRichText->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()
    ->getComment('B1')
    ->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()
    ->getComment('B1')
    ->getText()->createTextRun('Simple: loại sản phẩm đơn không chứa các thuộc tính như màu sắc hay kích thước(Vui lòng để trống 2 trường Color và Size). Configruable: loại sản phẩm có chứa thuộc tính là màu sắc hay kích thước');
$objPHPExcel->getActiveSheet()->getComment('B1')->setWidth("170pt");
$objPHPExcel->getActiveSheet()->getComment('B1')->setHeight("170pt");

//Set Name comment
$objPHPExcel->getActiveSheet()
    ->getComment('C1')
    ->setAuthor('Wisi');
$objCommentRichText = $objPHPExcel->getActiveSheet()
    ->getComment('C1')
    ->getText()->createTextRun('Tên Sản Phẩm:');
$objCommentRichText->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()
    ->getComment('C1')
    ->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()
    ->getComment('C1')
    ->getText()->createTextRun('Tên sản phẩm có độ dài dưới 120 kí tự');
$objPHPExcel->getActiveSheet()->getComment('C1')->setWidth("170pt");
$objPHPExcel->getActiveSheet()->getComment('C1')->setHeight("170pt");

//Set Qty comment
$objPHPExcel->getActiveSheet()
    ->getComment('K1')
    ->setAuthor('Wisi');
$objCommentRichText = $objPHPExcel->getActiveSheet()
    ->getComment('K1')
    ->getText()->createTextRun('Số lượng:');
$objCommentRichText->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()
    ->getComment('K1')
    ->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()
    ->getComment('K1')
    ->getText()->createTextRun('Số lượng tối đa là 5000. Nếu vượt quá số này hệ thống sẽ chuyển về là 5000');
$objPHPExcel->getActiveSheet()->getComment('K1')->setWidth("170pt");
$objPHPExcel->getActiveSheet()->getComment('K1')->setHeight("170pt");

//Set for description

$objPHPExcel->getActiveSheet()
    ->getComment('R1')
    ->setAuthor('Wisi');
$objCommentRichText = $objPHPExcel->getActiveSheet()
    ->getComment('R1')
    ->getText()->createTextRun('Bài viết:');
$objCommentRichText->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()
    ->getComment('R1')
    ->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()
    ->getComment('R1')
    ->getText()->createTextRun('Điền vào dạng chữ cho mô tả sản phẩm của bạn. Nếu có 1 dòng chữ nào muốn là heading để SEO thì dùng thẻ <h1>[dòng chữ cần cho thành heading]</h1>,  tương tự như vậy muốn xuống dòng thì dùng thẻ <br/> giữa 2 text. muốn bôi đậm dùng thẻ <strong>[dòng chữ thành strong]</strong> ');
$objPHPExcel->getActiveSheet()->getComment('R1')->setWidth("170pt");
$objPHPExcel->getActiveSheet()->getComment('R1')->setHeight("170pt");

$objPHPExcel->getActiveSheet()
    ->getComment('S1')
    ->setAuthor('Wisi');
$objCommentRichText = $objPHPExcel->getActiveSheet()
    ->getComment('S1')
    ->getText()->createTextRun('Hình Mô Tả:');
$objCommentRichText->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()
    ->getComment('S1')
    ->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()
    ->getComment('S1')
    ->getText()->createTextRun('điền vào temp_image_product/<ten_hinh>.jpg hoặc png');
$objPHPExcel->getActiveSheet()->getComment('S1')->setWidth("170pt");
$objPHPExcel->getActiveSheet()->getComment('S1')->setHeight("170pt");


//Set for quickdetail
$objPHPExcel->getActiveSheet()
    ->getComment('W1')
    ->setAuthor('Wisi');
$objCommentRichText = $objPHPExcel->getActiveSheet()
    ->getComment('W1')
    ->getText()->createTextRun('Mô tả chung:');
$objCommentRichText->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()
    ->getComment('W1')
    ->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()
    ->getComment('W1')
    ->getText()->createTextRun('Ví dụ: Thương hiệu:Nike;Nơi sản xuất:Cali;Xuất xứ:Mỹ;Ngày sản xuất:d/m/y...... Lưu ý mỗi cái phải cách nhau bằng dấu chấm phẩy (;)');
$objPHPExcel->getActiveSheet()->getComment('W1')->setWidth("170pt");
$objPHPExcel->getActiveSheet()->getComment('W1')->setHeight("170pt");

//Set for Các hình khác

$objPHPExcel->getActiveSheet()
    ->getComment('AC1')
    ->setAuthor('Wisi');
$objCommentRichText = $objPHPExcel->getActiveSheet()
    ->getComment('AC1')
    ->getText()->createTextRun('Hình Ảnh Khác:');
$objCommentRichText->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()
    ->getComment('AC1')
    ->getText()->createTextRun("\r\n");
$objPHPExcel->getActiveSheet()
    ->getComment('AC1')
    ->getText()->createTextRun('điền vào temp_image_product/<ten_hinh>.jpg hoặc png. Mỗi hình phân cách nhau bằng dấu phẩy (,)');
$objPHPExcel->getActiveSheet()->getComment('AB1')->setWidth("170pt");
$objPHPExcel->getActiveSheet()->getComment('AB1')->setHeight("170pt");

//End Of Comment Column

//Category List
$modelCagegory = $object_Manager->get("\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory");
$categorylist = $modelCagegory->create();
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$taxClassObj = $objectManager->create('Magento\Tax\Model\TaxClass\Source\Product');
$taxClassess = $taxClassObj->getAllOptions();
$string_category = "";
$c = 2;
$ctax = 2;
$c_color = 2;
$c_size  = 2;

$start = 2;
$start_tax = 2;
$start_color = 2;
$start_size = 2;

$end = 0;
$end_tax = 0;
$end_color = 0;
$end_size = 0;

$code_color = 93;
$code_size = 141;
foreach($categorylist as $_category){
    $path = $_category->getData("path");
    $array_path = explode("/",$path);
    $item = "";
    foreach($array_path as $id_category){
        if($id_category == 1){
            continue;
        }
        if($id_category == 2){
            $item.="Wisi Category";
            continue;
        }
        $modelCate = $object_Manager->create("\Magento\Catalog\Model\Category");
        $cate = $modelCate->load($id_category);
        $name_cate = $cate->getName();
        $item.="/".$name_cate;
    }
    if($item != ""){
        $item = str_replace("'","",$item);
        $string_category.=$item.",";
    }
    $sheet_contain_list->setCellValue('A'.$c,$item);
    $c++;
}
$end = $c;
foreach($taxClassess as $_tax){
    $title = $_tax["label"];
    $value = $_tax["value"];
    $sheet_contain_list->setCellValue('B'.$ctax,$title);
    $ctax++;
}
$end_tax = $ctax;
//////
//Color
$eavConfig = $object_Manager->create(\Magento\Eav\Model\Config::class);
$eavConfig->clear();
$attribute = $eavConfig->getAttribute('catalog_product', $code_color);
$installer = $object_Manager->create(\Magento\Catalog\Setup\CategorySetup::class);
$eavConfig->clear();
$options = $attribute->getOptions();
$attribute_id = $attribute->getData("attribute_id");
foreach($options as $_option){
    $sheet_contain_list->setCellValue('C'.$c_color,$_option->getLabel());
    $c_color++;
}
$end_color = $c_color;
//Size
$eavConfig = $object_Manager->create(\Magento\Eav\Model\Config::class);
$eavConfig->clear();
$attribute = $eavConfig->getAttribute('catalog_product', $code_size);
$installer = $object_Manager->create(\Magento\Catalog\Setup\CategorySetup::class);
$eavConfig->clear();
$options = $attribute->getOptions();
$attribute_id = $attribute->getData("attribute_id");
foreach($options as $_option){
    $sheet_contain_list->setCellValue('D'.$c_size,$_option->getLabel());
    $c_size++;
}
$end_size = $c_size;
//////
$string_category = substr($string_category,0,strlen($string_category) - 1);
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
//Get Products
$modelProducts = $object_Manager->get("\Magento\Catalog\Model\Product");
$products = $modelProducts->getCollection()->addAttributeToSelect("*")->setOrder("entity_id","desc");
$i = 1;
$id_parent = "";
$sql_product = "SELECT * from `libero_import_product`";
$data_product = $connection->fetchAll($sql_product);
$count_product = count($data_product);
$k=0;
for($k=0;$k<$count_product;$k++)
{
	$validation = $objPHPExcel->getActiveSheet()->getCell('D'.($i+1))
        ->getDataValidation();
        $validation->setType("list");
        $validation->setErrorStyle("information");
        $validation->setAllowBlank(false);
        $validation->setShowInputMessage(true);
        $validation->setShowErrorMessage(true);
        $validation->setShowDropDown(true);
        $validation->setErrorTitle('Input error');
        $validation->setError('Value is not in list.');
        $validation->setPromptTitle('Select category');
        $validation->setPrompt('Categories.');
        $validation->setFormula1('=\'Reference\'!$A$'.$start.':$A$'.$end.'');
        
        $validation_tax = $objPHPExcel->getActiveSheet()->getCell('J'.($i+1))
        ->getDataValidation();
        $validation_tax->setType("list");
        $validation_tax->setErrorStyle("information");
        $validation_tax->setAllowBlank(false);
        $validation_tax->setShowInputMessage(true);
        $validation_tax->setShowErrorMessage(true);
        $validation_tax->setShowDropDown(true);
        $validation_tax->setErrorTitle('Input error');
        $validation_tax->setError('Value is not in list.');
        $validation_tax->setPromptTitle('Select tax');
        $validation_tax->setPrompt('Tax.');
        $validation_tax->setFormula1('=\'Reference\'!$B$'.$start_tax.':$B$'.$end_tax.'');
        
        $validation_type_product = $objPHPExcel->getActiveSheet()->getCell('B'.($i+1))
        ->getDataValidation();
        $validation_type_product->setType("list");
        $validation_type_product->setErrorStyle("information");
        $validation_type_product->setAllowBlank(false);
        $validation_type_product->setShowInputMessage(true);
        $validation_type_product->setShowErrorMessage(true);
        $validation_type_product->setShowDropDown(true);
        $validation_type_product->setErrorTitle('Input error');
        $validation_type_product->setError('Type product is not in list.');
        $validation_type_product->setPromptTitle('Select Type Product');
        $validation_type_product->setPrompt('Type Product.');
        $validation_type_product->setFormula1('"simple,configurable"');

        $validation_freeshipment = $objPHPExcel->getActiveSheet()->getCell('I'.($i+1))
        ->getDataValidation();
        $validation_freeshipment->setType("list");
        $validation_freeshipment->setErrorStyle("information");
        $validation_freeshipment->setAllowBlank(false);
        $validation_freeshipment->setShowInputMessage(true);
        $validation_freeshipment->setShowErrorMessage(true);
        $validation_freeshipment->setShowDropDown(true);
        $validation_freeshipment->setErrorTitle('Input error');
        $validation_freeshipment->setError('Type product is not in list.');
        $validation_freeshipment->setPromptTitle('Lựa chọn sản phẩm miễn phí ship');
        $validation_freeshipment->setPrompt('Miên phí ship.');
        $validation_freeshipment->setFormula1('"Yes,No"');

        $validation_color = $objPHPExcel->getActiveSheet()->getCell('E'.($i+1))->getDataValidation();
        $validation_color->setType("list");
        $validation_color->setErrorStyle("information");
        $validation_color->setAllowBlank(false);
        $validation_color->setShowInputMessage(true);
        $validation_color->setShowErrorMessage(true);
        $validation_color->setShowDropDown(true);
        $validation_color->setErrorTitle('Input error');
        $validation_color->setError('Value is not in list.');
        $validation_color->setPromptTitle('Lựa Chọn Màu Sắc');
        $validation_color->setPrompt('Màu Sắc.');
        $validation_color->setFormula1('=\'Reference\'!$C$'.$start_color.':$C$'.$end_color.'');

        $validation_color = $objPHPExcel->getActiveSheet()->getCell('F'.($i+1))->getDataValidation();
        $validation_color->setType("list");
        $validation_color->setErrorStyle("information");
        $validation_color->setAllowBlank(false);
        $validation_color->setShowInputMessage(true);
        $validation_color->setShowErrorMessage(true);
        $validation_color->setShowDropDown(true);
        $validation_color->setErrorTitle('Input error');
        $validation_color->setError('Value is not in list.');
        $validation_color->setPromptTitle('Lựa Chọn Kích Thước');
        $validation_color->setPrompt('Kích Thước.');
        $validation_color->setFormula1('=\'Reference\'!$D$'.$start_size.':$D$'.$end_size.'');
        $sku = "";
        $type_product = "simple";
        $name_product = $data_product[$k]["name"];
        $string_category_product = $data_product[$k]["category"];
        $show_color = "";
        $show_size = "";
        $price = $data_product[$k]["price"];
        $special_price = $data_product[$k]["special_price"];
        $show_free_ship = "No";
        $show_tax = "None";
        $qty = 0;
        $min_qty = 1;
        $weight = 0;
        $length = 0;
        $height = 0;
        $width = 0;
        $short_description = $data_product[$k]["short_description"];
        $description = $data_product[$k]["description"];
        $string_quick_detail = "";
        $meta_title = $data_product[$k]["seo_title"];
        $meta_keyword = $data_product[$k]["seo_keyword"];
        $meta_description = $data_product[$k]["seo_des"];
        $base_img = $data_product[$k]["base_img"];
        $string_url_thumnbnail_img = $data_product[$k]["thumb_img"];
        $objPHPExcel->getActiveSheet()
                    ->setCellValue('A'.($i+1), $sku)
                    ->setCellValue('B'.($i+1), $type_product)
                    ->setCellValue('C'.($i+1), $name_product)
                    ->setCellValue('D'.($i+1), $string_category_product)
                    ->setCellValue('E'.($i+1), $show_color)
                    ->setCellValue('F'.($i+1), $show_size)
                    ->setCellValue('G'.($i+1), $price)
                    ->setCellValue('H'.($i+1), $special_price)
                    ->setCellValue('I'.($i+1), $show_free_ship)
                    ->setCellValue('J'.($i+1), $show_tax)
                    ->setCellValue('K'.($i+1), $qty)
                    ->setCellValue('L'.($i+1), $min_qty)
                    ->setCellValue('M'.($i+1), $weight)
                    ->setCellValue('N'.($i+1), $length)
                    ->setCellValue('O'.($i+1), $height)
                    ->setCellValue('P'.($i+1),$width)
                    ->setCellValue('Q'.($i+1), $short_description)
                    ->setCellValue('R'.($i+1), $description)
                    ->setCellValue('S'.($i+1), "")
                    ->setCellValue('T'.($i+1), "")
                    ->setCellValue('U'.($i+1), "")
                    ->setCellValue('V'.($i+1), "")
                    ->setCellValue('W'.($i+1), "")
                    ->setCellValue('X'.($i+1), $string_quick_detail)
                    ->setCellValue('Y'.($i+1), $meta_title)
                    ->setCellValue('Z'.($i+1), $meta_keyword)
                    ->setCellValue('AA'.($i+1), $meta_description)
                    ->setCellValue('AB'.($i+1), $base_img)
                    ->setCellValue('Ac'.($i+1), $string_url_thumnbnail_img);
    $i++;
}

/*************** Fetching data from database ***************/
/*$sql = "select * from `user_details`";
$res = mysqli_query($con, $sql);
if(mysqli_num_rows($res)>0)
{
	 $i = 1;
	 while($row = mysqli_fetch_object($res)) {
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+1), $row->id)
									  ->setCellValue('B'.($i+1), $row->name)
									  ->setCellValue('C'.($i+1), $row->mobile)
									  ->setCellValue('D'.($i+1), $row->country);					  
	    $i++;
	 }
}
							  
$objPHPExcel->getActiveSheet()->setCellValue("B10","Item B");*/
//Dynamic name, the combination of date and time
$filename = "export_products_".date('d-m-Y_H-i-s').".xlsx";

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

//if you want to save the file on the server instead of downloading, 
//comment the last 3 lines and remove the comment from the next line
//$objWriter->save(str_replace('.php', '.xlsx', $filename));
//header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename='.$filename);
$objWriter->save("php://output");
