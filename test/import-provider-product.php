<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
/** Include PHPExcel */
function RemoveSign($str){

    $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
    ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
        "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
    ,"ờ","ớ","ợ","ở","ỡ",
        "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
        "ỳ","ý","ỵ","ỷ","ỹ",
        "đ",
        "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
    ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
        "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
        "Ì","Í","Ị","Ỉ","Ĩ",
        "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
    ,"Ờ","Ớ","Ợ","Ở","Ỡ",
        "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
        "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
        "Đ","ê","ù","à");
    $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
    ,"a","a","a","a","a","a",
        "e","e","e","e","e","e","e","e","e","e","e",
        "i","i","i","i","i",
        "o","o","o","o","o","o","o","o","o","o","o","o"
    ,"o","o","o","o","o",
        "u","u","u","u","u","u","u","u","u","u","u",
        "y","y","y","y","y",
        "d",
        "A","A","A","A","A","A","A","A","A","A","A","A"
    ,"A","A","A","A","A",
        "E","E","E","E","E","E","E","E","E","E","E",
        "I","I","I","I","I",
        "O","O","O","O","O","O","O","O","O","O","O","O"
    ,"O","O","O","O","O",
        "U","U","U","U","U","U","U","U","U","U","U",
        "Y","Y","Y","Y","Y",
        "D","e","u","a");
    return str_replace($coDau,$khongDau,$str);
}
require_once("Classes/PHPExcel.php");
require_once("Classes/PHPExcel/IOFactory.php");
//Create a PHPExcel object
$name_file = $_POST["file_name"];;
$objPHPExcel = PHPExcel_IOFactory::load("import/".$name_file);
$_categoryFactory = $objectManager->get('Magento\Catalog\Model\CategoryFactory');
$dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$base_url = $storeManager->getStore()->getBaseUrl();
$base_media_url = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
$id_provider = $_POST["id_seller"];
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$attributeValues = [];
$array_attribute = array();
$associatedProductIds = [];
echo "============================BEGIN IMPORT EXCEL FILE CONFIGRUABLE PRODUCT=============================".PHP_EOL;
echo "DELETING DATA IMPORT IN DATABASE".PHP_EOL;
$sql_delete_import_data = "DELETE FROM `libero_import_product`;";
$connection->query($sql_delete_import_data);
echo "END DELETING DATA IMPORT IN DATABASE".PHP_EOL;
echo "IMPORTING TO DATABASE".PHP_EOL;
foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
    $worksheetTitle     = $worksheet->getTitle();
    $highestRow         = $worksheet->getHighestRow(); // e.g. 10
    $highestRowCount = 1; // Bỏ qua dòng đầu tiên chứa cột A , B , C
    //Not good at all but good to detect hightest row
    for ($row = 2; $row <= $highestRow; ++ $row) {
        if($worksheet->getCellByColumnAndRow(1, $row)->getCalculatedValue() != ""){
            //If one column have empty cell this is your fault not me !!!!!!
            $highestRowCount++;
        }
    }
    //End 
    $highestRow = $highestRowCount;
    $highestColumn      = $worksheet->getHighestColumn(); // e.g 'F'
    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
    $nrColumns = ord($highestColumn) - 64;
    for ($row = 2; $row <= $highestRow; ++ $row) {
        //Insert database
        $sku = $id_provider."-";
        $type_product = $worksheet->getCellByColumnAndRow(1, $row)->getCalculatedValue();
        if($type_product=="simple"){
            $name_product = $worksheet->getCellByColumnAndRow(2, $row)->getCalculatedValue();
            $danh_muc = $worksheet->getCellByColumnAndRow(3, $row)->getCalculatedValue();
            $array_danh_muc = explode("/",$danh_muc);
            $array_category = array();
            $last_category_id = "";
            foreach($array_danh_muc as $_cate){
                $collection = $_categoryFactory->create()->getCollection()->addAttributeToFilter("name",array("like" => "".$_cate.""))->setPageSize(1);
                if ($collection->getSize()) {
                    $categoryId = $collection->getFirstItem()->getId();
                    $array_category[] = $categoryId;
                    $sku.=$categoryId."-";
                    $last_category_id = $categoryId;
                }
            }
			$sku .= time();
            $price = $worksheet->getCellByColumnAndRow(6, $row)->getCalculatedValue();
            $special_price = $worksheet->getCellByColumnAndRow(7, $row)->getCalculatedValue();
            $free_shipment = 0;
            $tax = $worksheet->getCellByColumnAndRow(9, $row)->getCalculatedValue();
			$class_tax_id = 0 ;
			if($tax == "VAT 5%"){
				$class_tax_id = 5;
			}
			if($tax == "VAT 10%"){
				$class_tax_id = 4;
			}
            $qty = $worksheet->getCellByColumnAndRow(10, $row)->getCalculatedValue();
            $url_key = RemoveSign($name_product)."-".$sku;
            $min_qty = $worksheet->getCellByColumnAndRow(11, $row)->getCalculatedValue();;
            $status = 1;
            $weight = $worksheet->getCellByColumnAndRow(12, $row)->getCalculatedValue();
            $length = $worksheet->getCellByColumnAndRow(13, $row)->getCalculatedValue();
            $height = $worksheet->getCellByColumnAndRow(14, $row)->getCalculatedValue();
            $width = $worksheet->getCellByColumnAndRow(15, $row)->getCalculatedValue();
            $short_description = $worksheet->getCellByColumnAndRow(16, $row)->getCalculatedValue();
            $description_text_1 = $worksheet->getCellByColumnAndRow(17, $row)->getCalculatedValue();
            $description_img_1 = $base_media_url."".$worksheet->getCellByColumnAndRow(18, $row)->getCalculatedValue();
			$img_1 = $worksheet->getCellByColumnAndRow(18, $row)->getCalculatedValue();
			$make_img_1 = "";
			if($img_1 != ""){
				$is_http = strrpos($img_1,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_1, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-1-".time().".".$ext_sub_img;
                    //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($img_1));
                    $ch = curl_init($img_1);
                    $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                    $filename = $name_sub_image;
                    $complete_save_loc = $my_save_dir . $filename;
                    $fp = fopen($complete_save_loc, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
					$description_img_1 = $base_media_url."temp_image_product/".$name_sub_image;
				}
				$make_img_1 = "<p></p><img src='$description_img_1' width='100%' height='auto' />";
			}
			
			$description_text_2 = $worksheet->getCellByColumnAndRow(19, $row)->getCalculatedValue();
            $description_img_2 = $base_media_url."".$worksheet->getCellByColumnAndRow(20, $row)->getCalculatedValue();
			$img_2 = $worksheet->getCellByColumnAndRow(20, $row)->getCalculatedValue();
			$make_img_2 = "";
			if($img_2 != ""){
				$is_http = strrpos($img_2,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_2, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-2-".time().".".$ext_sub_img;
                    //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($img_2));
                    $ch = curl_init($img_2);
                    $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                    $filename = $name_sub_image;
                    $complete_save_loc = $my_save_dir . $filename;
                    $fp = fopen($complete_save_loc, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
					$description_img_2 = $base_media_url."temp_image_product/".$name_sub_image;
				}
				$make_img_2 = "<p></p><img src='$description_img_2' width='100%' height='auto' />";
			}
			
			$description_text_3 = $worksheet->getCellByColumnAndRow(21, $row)->getCalculatedValue();
            $description_img_3 = $base_media_url."".$worksheet->getCellByColumnAndRow(22, $row)->getCalculatedValue();
			$img_3 = $worksheet->getCellByColumnAndRow(22, $row)->getCalculatedValue();
			$make_img_3 = "";
			if($img_3 != ""){
				$is_http = strrpos($img_3,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_3, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-3-".time().".".$ext_sub_img;
                    //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($img_3));
                    $ch = curl_init($img_3);
                    $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                    $filename = $name_sub_image;
                    $complete_save_loc = $my_save_dir . $filename;
                    $fp = fopen($complete_save_loc, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
					$description_img_3 = $base_media_url."temp_image_product/".$name_sub_image;
				}
				$make_img_3 = "<p></p><img src='$description_img_3' width='100%' height='auto' />";
			}
			
            $description = $description_text_1.$make_img_1.$description_text_2.$make_img_2.$description_text_3.$make_img_3;
            $quick_detail = $worksheet->getCellByColumnAndRow(23, $row)->getCalculatedValue();
			$json_quick_detail = "";
			if($quick_detail != ""){
				$array_quick_detail = explode(";",$quick_detail);
				$q = 0;
				$length_quick_detail = count($array_quick_detail);
				$array_json_quick_detail = array();
				$array_json_quick_detail["data"] = array();
				$array_json_quick_detail["is_have"] = "false";
				$array_content_quick_detail = array();
				for($q = 0 ; $q < $length_quick_detail ; $q++){
					$array_child  = array();
					if($array_quick_detail[$q] != ""){
						$item = explode(":",$array_quick_detail[$q]);
						$array_child["title"] = $item[0];
						if(isset($item[1])){
							$array_child["content"] = $item[1];
							$array_content_quick_detail[] = $array_child;
						}
					}
				}
				if(count($array_content_quick_detail) > 0){
					$array_json_quick_detail["data"] = $array_content_quick_detail;
					$array_json_quick_detail["is_have"] = "true";
				}
				$json_quick_detail = json_encode($array_json_quick_detail,JSON_UNESCAPED_UNICODE);
			}
            $meta_title = $worksheet->getCellByColumnAndRow(24, $row)->getCalculatedValue();
            $meta_keyword = $worksheet->getCellByColumnAndRow(25, $row)->getCalculatedValue();
            $meta_description = $worksheet->getCellByColumnAndRow(26, $row)->getCalculatedValue();
            try{
                $product = $objectManager->create("\Magento\Catalog\Model\Product");
                $product->setSku($sku); // Set your sku here
                $product->setName($name_product); // Name of Product
                $product->setTypeId('simple');
                $product->setAttributeSetId(4); // Attribute set id
                $product->setStatus($status); // Status on product enabled/ disabled 1/0
                $product->setWeight($weight);
                $product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
                $product->setTaxClassId($class_tax_id); // Tax class id
                $product->setStockData(['use_config_manage_stock' => 1,'min_sale_qty' => $min_qty,'is_in_stock' => 1,"qty" => $qty]);
                echo $name_product.PHP_EOL;
                $product->setPrice($price); // price of product
                $product->setSpecialPrice($special_price); // special price of product
                $product->setFreeShipment($free_shipment); // free_shipment enabled/ disabled 1/0
                $product->setData("quick_detail",$json_quick_detail);
                $product->setDescription($description);
                $product->setCategoryIds($array_category);
                $product->setShortDescription($short_description);
                $product->setLastCategoryId($last_category_id);
                $product->setData("url_key",$url_key);
                $product->setData("meta_title",$meta_title);
                $product->setMetaKeyword($meta_keyword);
                $product->setData("meta_description",$meta_description);
                $product->setData("id_seller",$id_provider);
                $product->setWebsiteIds(array(1));
                $product->setData("length",$length);
                $product->setData("height",$height);
                $product->setData("width",$width);
                $product->setStoreId(0);
                $product->save();
                //Xử lý phức tạp hình ảnh với trường là url
                $product_id = $product->getId();
                //File top ten can 1 url
                    $base_img = $worksheet->getCellByColumnAndRow(27,$row)->getCalculatedValue();
                    $danh_sach_hinh_khac = $worksheet->getCellByColumnAndRow(28,$row)->getCalculatedValue();
					//Processing contains http
					$is_http = strrpos($base_img,"http");
					if($is_http > -1){
						//có tồn tại
						$ext_sub_img = pathinfo($base_img, PATHINFO_EXTENSION);
						$name_sub_image = "tam-main-img-".time().".".$ext_sub_img;
                        //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($base_img));
                        $ch = curl_init($base_img);
                        $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                        $filename = $name_sub_image;
                        $complete_save_loc = $my_save_dir . $filename;
                        $fp = fopen($complete_save_loc, 'wb');
                        curl_setopt($ch, CURLOPT_FILE, $fp);
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_exec($ch);
                        curl_close($ch);
                        fclose($fp);

						$base_img = "temp_image_product/".$name_sub_image;
					}
					//End processing
                    $array_path_img = array();
                    $image_data = array(); // Remember this case save json as key 'main' and variant (if is config) in main is key id main product
                    $main_img_child = array();
                    $array_main_child_image = array();
                    $array_temp_sub_img = array();
                    $array_path_img = explode(",",$danh_sach_hinh_khac);
                    //First create for main product , in main have key is product id in key have base image is key ann value is path image
                    $array_main_child_image['base_img'] = $base_img;        
                    foreach($array_path_img as $_img_path) {
						$is_http = strrpos($_img_path,"http");
						if($is_http > -1){
							//có tồn tại
							$ext_sub_img = pathinfo($base_img, PATHINFO_EXTENSION);
							$name_sub_image = "tam-sub-img-product-".time().".".$ext_sub_img;
                            //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($_img_path));
                            ///
                            $ch = curl_init($_img_path);
                            $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                            $filename = $name_sub_image;
                            $complete_save_loc = $my_save_dir . $filename;
                            $fp = fopen($complete_save_loc, 'wb');
                            curl_setopt($ch, CURLOPT_FILE, $fp);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_exec($ch);
                            curl_close($ch);
                            fclose($fp);
                            ///
							$_img_path = "temp_image_product/".$name_sub_image;
						}
                        $array_temp_sub_img[] = $_img_path;
                    }
                    $array_main_child_image['sub_img'] = $array_temp_sub_img;
                    $main_img_child[$product->getId()] = $array_main_child_image;
                    $image_data["main"] = $main_img_child;
                    $json_image_data = json_encode($image_data,JSON_UNESCAPED_UNICODE);
                //End
                $product->setData("image_data",$json_image_data);
                $product->save();
                //Add to cron job image
                    //Put save image to cron job
                    $type_cron = "CRON_PRODUCT_IMAGE";
                    $cron_code = "CRON_EX15G79_PRODUCT_".$product_id."_IMPORT_".$id_provider;
                    $content_cron_excute = $json_image_data;
                    date_default_timezone_set("Asia/Ho_Chi_Minh");
                    $time_update = date("Y-m-d H:i:s");
                    $created_at = $time_update;
                    $status = 0; // Processsing;
                    $etc = "";
                    $sql_insert_cron = "INSERT INTO `libero_cron_execute_background` (`id_cron`, `type_cron`, `cron_code`, `content_excute`, `created_at`, `status`, `etc`) 
                    VALUES (NULL, '$type_cron', '$cron_code', '$content_cron_excute', '$created_at', '0', '$etc');";
                    $connection->query($sql_insert_cron);
                //End add to cron job image
            }catch(Exception $e){
                $text = "SAN PHAM CO TEN LA ".$name_product." BI LOI ".$e->getMessage().PHP_EOL;
                file_put_contents("error_import.txt",$text,FILE_APPEND);
            }
        }else{
            $name_product = $worksheet->getCellByColumnAndRow(2, $row)->getCalculatedValue();
            $danh_muc = $worksheet->getCellByColumnAndRow(3, $row)->getCalculatedValue();
            $array_danh_muc = explode("/",$danh_muc);
            $array_category = array();
            $last_category_id = "";
            $string_category = "";
            foreach($array_danh_muc as $_cate){
                $collection = $_categoryFactory->create()->getCollection()->addAttributeToFilter("name",array("like" => "".$_cate.""))->setPageSize(1);
                if ($collection->getSize()) {
                    $categoryId = $collection->getFirstItem()->getId();
                    $array_category[] = $categoryId;
                    $sku.=$categoryId;
                    $sku.=$categoryId."-";
                    $last_category_id = $categoryId;
                    $string_category.= $categoryId."|";
                }
            }
            $string_category = substr($string_category,0,strlen($string_category) - 1);
            $sku .= time();
            $color = $worksheet->getCellByColumnAndRow(4, $row)->getCalculatedValue();
            $size = $worksheet->getCellByColumnAndRow(5, $row)->getCalculatedValue();
            $price = $worksheet->getCellByColumnAndRow(6, $row)->getCalculatedValue();
            $special_price = $worksheet->getCellByColumnAndRow(7, $row)->getCalculatedValue();
            $free_shipment = 0;
            $tax = $worksheet->getCellByColumnAndRow(9, $row)->getCalculatedValue();
			$class_tax_id = 0 ;
			if($tax == "VAT 5%"){
				$class_tax_id = 5;
			}
			if($tax == "VAT 10%"){
				$class_tax_id = 4;
			}
            $qty = $worksheet->getCellByColumnAndRow(10, $row)->getCalculatedValue();
            $url_key = RemoveSign($name_product)."-".$id_provider;
            $min_qty = 1;
            $status = 1;
            $weight = $worksheet->getCellByColumnAndRow(12, $row)->getCalculatedValue();
            $length = $worksheet->getCellByColumnAndRow(13, $row)->getCalculatedValue();
            $height = $worksheet->getCellByColumnAndRow(14, $row)->getCalculatedValue();
            $width = $worksheet->getCellByColumnAndRow(15, $row)->getCalculatedValue();
            $short_description = $worksheet->getCellByColumnAndRow(16, $row)->getCalculatedValue();
            $description_text_1 = $worksheet->getCellByColumnAndRow(17, $row)->getCalculatedValue();
            $description_img_1 = $base_media_url."/".$worksheet->getCellByColumnAndRow(18, $row)->getCalculatedValue();
            $img_1 = $worksheet->getCellByColumnAndRow(18, $row)->getCalculatedValue();
			$make_img_1 = "";
			if($img_1 != ""){
				$is_http = strrpos($img_1,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_1, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-1-".time().".".$ext_sub_img;
                    //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($img_1));
                    $ch = curl_init($img_1);
                    $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                    $filename = $name_sub_image;
                    $complete_save_loc = $my_save_dir . $filename;
                    $fp = fopen($complete_save_loc, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
					$description_img_1 = $base_media_url."temp_image_product/".$name_sub_image;
				}
				$make_img_1 = "<p></p><img src=$description_img_1 width=100% height=auto />";
			}
			
			$description_text_2 = $worksheet->getCellByColumnAndRow(19, $row)->getCalculatedValue();
            $description_img_2 = $base_media_url."".$worksheet->getCellByColumnAndRow(20, $row)->getCalculatedValue();
			$img_2 = $worksheet->getCellByColumnAndRow(20, $row)->getCalculatedValue();
			$make_img_2 = "";
			if($img_2 != ""){
				$is_http = strrpos($img_2,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_2, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-2-".time().".".$ext_sub_img;
                    //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($img_2));
                    $ch = curl_init($img_2);
                    $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                    $filename = $name_sub_image;
                    $complete_save_loc = $my_save_dir . $filename;
                    $fp = fopen($complete_save_loc, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
					$description_img_2 = $base_media_url."temp_image_product/".$name_sub_image;
				}
				$make_img_2 = "<p></p><img src=$description_img_2 width=100% height=auto />";
			}
			
			$description_text_3 = $worksheet->getCellByColumnAndRow(21, $row)->getCalculatedValue();
            $description_img_3 = $base_media_url."".$worksheet->getCellByColumnAndRow(22, $row)->getCalculatedValue();
			$img_3 = $worksheet->getCellByColumnAndRow(22, $row)->getCalculatedValue();
			$make_img_3 = "";
			if($img_3 != ""){
				$is_http = strrpos($img_3,"http");
				if($is_http > -1){
					//có tồn tại
					$ext_sub_img = pathinfo($img_3, PATHINFO_EXTENSION);
					$name_sub_image = "des-img-3-".time().".".$ext_sub_img;
                    //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($img_3));
                    $ch = curl_init($img_3);
                    $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                    $filename = $name_sub_image;
                    $complete_save_loc = $my_save_dir . $filename;
                    $fp = fopen($complete_save_loc, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
					$description_img_3 = $base_media_url."temp_image_product/".$name_sub_image;
				}
				$make_img_3 = "<p></p><img src=$description_img_3 width=100% height=auto />";
			}
			
            $description = $description_text_1.$make_img_1.$description_text_2.$make_img_2.$description_text_3.$make_img_3;
            $quick_detail = $worksheet->getCellByColumnAndRow(23, $row)->getCalculatedValue();
			$json_quick_detail = "";
			if($quick_detail != ""){
				$array_quick_detail = explode(";",$quick_detail);
				$q = 0;
				$length_quick_detail = count($array_quick_detail);
				$array_json_quick_detail = array();
				$array_json_quick_detail["data"] = array();
				$array_json_quick_detail["is_have"] = "false";
				$array_content_quick_detail = array();
				for($q = 0 ; $q < $length_quick_detail ; $q++){
					$array_child  = array();
					if($array_quick_detail[$q] != ""){
						$item = explode(":",$array_quick_detail[$q]);
						$array_child["title"] = $item[0];
						if(isset($item[1])){
							$array_child["content"] = $item[1];
							$array_content_quick_detail[] = $array_child;
						}
					}
				}
				if(count($array_content_quick_detail) > 0){
					$array_json_quick_detail["data"] = $array_content_quick_detail;
					$array_json_quick_detail["is_have"] = "true";
				}
				$json_quick_detail = json_encode($array_json_quick_detail,JSON_UNESCAPED_UNICODE);
			}
            $meta_title = $worksheet->getCellByColumnAndRow(24, $row)->getCalculatedValue();
            $meta_keyword = $worksheet->getCellByColumnAndRow(25, $row)->getCalculatedValue();
            $meta_description = $worksheet->getCellByColumnAndRow(26, $row)->getCalculatedValue();
            $base_img = $worksheet->getCellByColumnAndRow(27,$row)->getCalculatedValue();
            $danh_sach_hinh_khac = $worksheet->getCellByColumnAndRow(28,$row)->getCalculatedValue();
            $sql_insert = "";
            $string_insert = "INSERT INTO `libero_import_product` (`id_import`, `sku`, `type`, `name`, `category`, `color`, `size`, `price`, `special_price`, `free_shipment`, `tax`, `qty`, `min_qty`, `weight`, `length`, `height`, `width`, `short_description`, `text_des_1`, `img_des_1`, `text_des_2`, `img_des_2`, `text_des_3`, `img_des_3`, `quick_detail`, `seo_title`, `seo_keyword`, `seo_des`, `base_img`, `thumb_img`, `id_provider`,`last_category`)  
            VALUES (NULL, '', '$type_product', '$name_product', '$string_category', '$color', '$size', '$price', '$special_price', '0', '$class_tax_id', '$qty', '$min_qty', '$weight', '$length', '$height', '$width', '$short_description', '$description', '', '', '', '', '', '$json_quick_detail', '$meta_title', '$meta_keyword', '$meta_description', '$base_img', '$danh_sach_hinh_khac', '$id_provider','$last_category_id');";
            $connection->query($string_insert);
        }
    }
    break;
}
echo "=======================END IMPORT TO DATABASE===================================".PHP_EOL;
echo "BEGIN MAKE CONFIGRUABLE PRODUCT ".PHP_EOL;
//Processing Configruable Product
$sql_product  = "SELECT `name` FROM `libero_import_product` group by `name`";
$data_name =  $connection->fetchAll($sql_product);
foreach($data_name as $_item){
    //Properties parent
    $parent_sku = "";
    $parent_name = "";
    $parent_category = "";
    $parent_last_category = "";
    $parent_status = 1;
    $parent_visibility = 4;
    $parent_short_description = "";
    $parent_description = "";
    $parent_weight = "";
    $parent_length = "";
    $parent_height = "";
    $parent_width = "";
    $parent_free_shipment = 0;
    $parent_tax = "";
    $parent_quickdetail = "";
    $parent_meta_title = "";
    $parent_meta_keyword = "";
    $parent_meta_description = "";
    $parent_base_img = "";
    $parent_thumb_img = "";
    $parent_provider_id = "";
    $parent_url_key = "";
    $set_first = 1;
    //End of properties
    //Select variant product
    $_name = $_item["name"];
    $parent_name = $_name;
    $sql_variant = "SELECT * FROM `libero_import_product` WHERE `name`  = '$_name'";
    $variant_item = $connection->fetchAll($sql_variant);
    foreach($variant_item as $_v_item){
        $name_product = $_v_item["name"];
        $danh_muc = $_v_item["category"];
        $array_category = explode("|",$danh_muc);
        $sku = $id_provider."-";
        foreach($array_category as $_cate){
            $sku.= $_cate."-";
        }
        $last_category_id = $_v_item["last_category"];
        $sku .= time();
        $price = $_v_item["price"];
        $special_price = $_v_item["special_price"];
        $free_shipment = 0;
        $tax = $_v_item["tax"];
        $qty = $_v_item["qty"];
        $min_qty = 1;
        $status = 1;
        $weight = $_v_item["weight"];
        $length = $_v_item["length"];
        $height = $_v_item["height"];
        $width = $_v_item["width"];
        $short_description = $_v_item["short_description"];
        $description_text_1 = $_v_item["text_des_1"];
        $description = $description_text_1;
        $quick_detail = $_v_item["quick_detail"];
        $color = $_v_item["color"];
        $size = $_v_item["size"];
        $attribute_color = 93;
        $attribute_size = 141;
        $name_product_variant = $name_product;
        if($color != ""){
            $array_attribute["color"] = $attribute_color;
            $name_product_variant = $name_product_variant."-".$color;
        }
        if($size != ""){
            $array_attribute["size"] = $attribute_size;
            $name_product_variant = $name_product_variant."-".$size;
        }
        $json_quick_detail = $quick_detail;
        $meta_title = $_v_item["seo_title"];
        $meta_keyword = $_v_item["seo_keyword"];
        $meta_description = $_v_item["seo_des"];
        
        
        $url_key = RemoveSign($name_product_variant)."-".$sku;
        //Add value to properties
        if($set_first == 1){
            $parent_category = $danh_muc;
            $parent_last_category = $last_category_id;
            $parent_tax = $tax;
            $parent_short_description = $short_description;
            $parent_description = $description;
            $parent_quickdetail = $json_quick_detail;
            $parent_meta_title  =  $meta_title;
            $parent_meta_keyword  =  $meta_keyword;
            $parent_meta_description  =  $meta_description;
            $parent_provider_id = $id_provider;
            $parent_free_shipment = $free_shipment;
            $parent_weight = $weight;
            $parent_length = $length;
            $parent_height = $height;
            $parent_width = $width;
			$attributeValues = [];
			$associatedProductIds = [];
        }
        try{
            $variant = $objectManager->create("\Magento\Catalog\Model\Product");
            $variant->setSku($sku); // Set your sku here
            $variant->setName($name_product_variant); // Name of Product
            $variant->setTypeId('simple');
            $variant->setAttributeSetId(4); // Attribute set id
            $variant->setStatus($status); // Status on product enabled/ disabled 1/0
            $variant->setWeight($weight);
            $variant->setVisibility(1); // visibilty of product (catalog / search / catalog, search / Not visible individually)
            $variant->setTaxClassId($tax); // Tax class id
            $variant->setStockData(['use_config_manage_stock' => 1,'min_sale_qty' => $min_qty,'is_in_stock' => 1,"qty" => $qty]);
            $variant->setPrice($price); // price of product
            $variant->setSpecialPrice($special_price); // special price of product
            $variant->setFreeShipment($free_shipment); // free_shipment enabled/ disabled 1/0
            $variant->setData("quick_detail",$json_quick_detail);
            $variant->setDescription($description);
            $variant->setCategoryIds($array_category);
            $variant->setShortDescription($short_description);
            $variant->setLastCategoryId($last_category_id);
            $variant->setData("url_key",$url_key);
            $variant->setData("meta_title",$meta_title);
            $variant->setMetaKeyword($meta_keyword);
            $variant->setData("meta_description",$meta_description);
            $variant->setData("id_seller",$id_provider);
            $variant->setWebsiteIds(array(1));
            $variant->setData("length",$length);
            $variant->setData("height",$height);
            $variant->setData("width",$width);
            $variant->setStoreId(0);
            //Get label option
            $sql_value_option = "";
            foreach($array_attribute as $key => $_attr){
                $label = "";
                if($key == "color"){
                    $sql_value_option = "SELECT * FROM `eav_attribute_option_value` WHERE `value` LIKE '$color'";
                    $label = $color;
                }
                if($key == "size"){
                    $sql_value_option = "SELECT * FROM `eav_attribute_option_value` WHERE `value` LIKE '$size'";
                    $label = $size;
                }
                $data_option = $connection->fetchAll($sql_value_option);
                $option_id = "";
                foreach($data_option as $_option){
                    $option_id = $_option["option_id"];
                }
                $variant->setData($key,$option_id);
                $attributeValues[] = [
                    'label' => $label,
                    'attribute_id' => $_attr,
                    'value_index' => $option_id,
                ];
            }
            $variant->save();
            $product_id = $variant->getId();
            $associatedProductIds[] = $product_id;
            //Xử lý phức tạp hình ảnh với trường là url
                $base_img = $_v_item["base_img"];
                $danh_sach_hinh_khac = $_v_item["thumb_img"];
                $is_http = strrpos($base_img,"http");
                if($is_http > -1){
                    //có tồn tại
                    $ext_sub_img = pathinfo($base_img, PATHINFO_EXTENSION);
                    $name_sub_image = "variant-main-img-".time().".".$ext_sub_img;
                    //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($base_img));
                    $ch = curl_init($base_img);
                    $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                    $filename = $name_sub_image;
                    $complete_save_loc = $my_save_dir . $filename;
                    $fp = fopen($complete_save_loc, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
                    $base_img = "temp_image_product/".$name_sub_image;
                }
                //End processing
                $array_path_img = array();
                $image_data = array(); // Remember this case save json as key 'main' and variant (if is config) in main is key id main product
                $main_img_child = array();
                $array_main_child_image = array();
                $array_temp_sub_img = array();
                $array_path_img = explode(",",$danh_sach_hinh_khac);
                //First create for main product , in main have key is product id in key have base image is key ann value is path image
                $array_main_child_image['base_img'] = $base_img;
                $child = 1; 
                foreach($array_path_img as $_img_path) {
                    $is_http = strrpos($_img_path,"http");
                    if($is_http > -1){
                        //có tồn tại
                        $ext_sub_img = pathinfo($base_img, PATHINFO_EXTENSION);
                        $name_sub_image = "variant-sub-img-product-".$child."-".time().".".$ext_sub_img;
                        //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($_img_path));
                        $ch = curl_init($_img_path);
                        $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                        $filename = $name_sub_image;
                        $complete_save_loc = $my_save_dir . $filename;
                        $fp = fopen($complete_save_loc, 'wb');
                        curl_setopt($ch, CURLOPT_FILE, $fp);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                        curl_setopt($ch, CURLOPT_HEADER, 0);
                        curl_exec($ch);
                        curl_close($ch);
                        fclose($fp);
                        $_img_path = "temp_image_product/".$name_sub_image;
                        $child++;
                    }
                    $array_temp_sub_img[] = $_img_path;
                }
                $array_main_child_image['sub_img'] = $array_temp_sub_img;
                $main_img_child[$product_id] = $array_main_child_image;
                $image_data["main"] = $main_img_child;
                $json_image_data = json_encode($image_data,JSON_UNESCAPED_UNICODE);
            //End
            if($set_first == 1){
                $parent_base_img = $base_img;
                $parent_thumb_img = $danh_sach_hinh_khac;
            }
            $variant->setData("image_data",$json_image_data);
            $variant->save();
            //Add to cron job image
                //Put save image to cron job
                $type_cron = "CRON_PRODUCT_IMAGE";
                $cron_code = "CRON_EX15GV8_PRODUCT_".$product_id."_IMPORT_".$id_provider;
                $content_cron_excute = $json_image_data;
                date_default_timezone_set("Asia/Ho_Chi_Minh");
                $time_update = date("Y-m-d H:i:s");
                $created_at = $time_update;
                $status = 0; // Processsing;
                $etc = "";
                $sql_insert_cron = "INSERT INTO `libero_cron_execute_background` (`id_cron`, `type_cron`, `cron_code`, `content_excute`, `created_at`, `status`, `etc`) 
                VALUES (NULL, '$type_cron', '$cron_code', '$content_cron_excute', '$created_at', '0', '$etc');";
                $connection->query($sql_insert_cron);
            //End add to cron job image
            $set_first++;
        }catch(\Exception $e){
            $text = "SAN PHAM CO TEN LA ".$name_product." BI LOI ".$e->getMessage().PHP_EOL;
            echo $text;
            file_put_contents("error_import.txt",$text,FILE_APPEND);
        }
    }
    //Create parent product
    try{
        $parent_product =  $objectManager->create("\Magento\Catalog\Model\Product");
        $parent_sku = $id_provider."-";
        $array_parent_category = explode("|",$parent_category);
        foreach($array_parent_category as $_cate){
            $parent_sku .= $_cate."-";
        }
        $parent_sku .= time();
        $parent_url_key = RemoveSign($parent_name)."-".$parent_sku;
        $parent_product->setSku($parent_sku);
        $parent_product->setName($parent_name);
        $parent_product->setTypeId('configruable');
        $parent_product->setAttributeSetId(4); // Attribute set id
        $parent_product->setStatus($parent_status); // Status on product enabled/ disabled 1/0
        $parent_product->setWeight($parent_weight);
        $parent_product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
        $parent_product->setTaxClassId($parent_tax); // Tax class id    
        $parent_product->setFreeShipment($parent_free_shipment); // free_shipment enabled/ disabled 1/0
        $parent_product->setData("quick_detail",$parent_quickdetail);
        $parent_product->setDescription($parent_description);
        $parent_product->setCategoryIds($array_parent_category);
        $parent_product->setShortDescription($parent_short_description);
        $parent_product->setLastCategoryId($parent_last_category);
        $parent_product->setData("url_key",$parent_url_key);
        $parent_product->setData("meta_title",$parent_meta_title);
        $parent_product->setMetaKeyword($parent_meta_keyword);
        $parent_product->setData("meta_description",$parent_meta_description);
        $parent_product->setData("id_seller",$id_provider);
        $parent_product->setWebsiteIds(array(1));
        $parent_product->setData("length",$parent_length);
        $parent_product->setData("height",$parent_height);
        $parent_product->setData("width",$parent_width);
        $configurableAttributesData = array();
        foreach($array_attribute  as $key => $attribute_id){
            $eavConfig = $objectManager->create("\Magento\Eav\Model\Config");
            $eavConfig->clear();
            $attribute = $eavConfig->getAttribute('catalog_product', $attribute_id);
            $array_entity = array();
            $array_entity["attribute_id"] = $attribute->getId();
            $array_entity["code"] = $attribute->getAttributeCode();
            $array_entity["label"] = $attribute->getStoreLabel();
            $array_entity["values"] = $attributeValues;
            $configurableAttributesData[] = $array_entity;
        }
		/////
        $optionsFactory = $objectManager->create("\Magento\ConfigurableProduct\Helper\Product\Options\Factory");
        $configurableOptions = $optionsFactory->create($configurableAttributesData);
        $extensionConfigurableAttributes = $parent_product->getExtensionAttributes();
        $extensionConfigurableAttributes->setConfigurableProductOptions($configurableOptions);
        $extensionConfigurableAttributes->setConfigurableProductLinks($associatedProductIds);
        $parent_product->setExtensionAttributes($extensionConfigurableAttributes);
        $parent_product->setTypeId("configurable")->setAttributeSetId(4)->setWebsiteIds([1])->setStockData(['use_config_manage_stock' => 1, 'is_in_stock' => 1]);
        $parent_product->setStoreId(0);
        $parent_product->save();
        $product_id = $parent_product->getId();
        //Xử lý phức tạp hình ảnh với trường là url
            $base_img = $parent_base_img;
            $danh_sach_hinh_khac = $parent_thumb_img;
            //Processing contains http
            $is_http = strrpos($base_img,"http");
            if($is_http > -1){
                //có tồn tại
                $ext_sub_img = pathinfo($base_img, PATHINFO_EXTENSION);
                $name_sub_image = "main-img-".time().".".$ext_sub_img;
                //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($base_img));
                $ch = curl_init($base_img);
                $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                $filename = $name_sub_image;
                $complete_save_loc = $my_save_dir . $filename;
                $fp = fopen($complete_save_loc, 'wb');
                curl_setopt($ch, CURLOPT_FILE, $fp);
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_exec($ch);
                curl_close($ch);
                fclose($fp);
                $base_img = "temp_image_product/".$name_sub_image;
            }
            //End processing
            $array_path_img = array();
            $image_data = array(); // Remember this case save json as key 'main' and variant (if is config) in main is key id main product
            $main_img_child = array();
            $array_main_child_image = array();
            $array_temp_sub_img = array();
            $array_path_img = explode(",",$danh_sach_hinh_khac);
            //First create for main product , in main have key is product id in key have base image is key ann value is path image
            $array_main_child_image['base_img'] = $base_img;
            $main_parent = 1; 
            foreach($array_path_img as $_img_path) {
                $is_http = strrpos($_img_path,"http");
                if($is_http > -1){
                    //có tồn tại
                    $ext_sub_img = pathinfo($base_img, PATHINFO_EXTENSION);
                    $name_sub_image = "sub-img-product-".$main_parent."-".time().".".$ext_sub_img;
                    //file_put_contents($dir->getPath('media')."/temp_image_product/".$name_sub_image,file_get_contents($_img_path));
                    $ch = curl_init($_img_path);
                    $my_save_dir = $dir->getPath('media')."/temp_image_product/";
                    $filename = $name_sub_image;
                    $complete_save_loc = $my_save_dir . $filename;
                    $fp = fopen($complete_save_loc, 'wb');
                    curl_setopt($ch, CURLOPT_FILE, $fp);
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_exec($ch);
                    curl_close($ch);
                    fclose($fp);
                    $_img_path = "temp_image_product/".$name_sub_image;
                    $main_parent++;
                }
                $array_temp_sub_img[] = $_img_path;
            }
            $array_main_child_image['sub_img'] = $array_temp_sub_img;
            $main_img_child[$product_id] = $array_main_child_image;
            $image_data["main"] = $main_img_child;
            $json_image_data = json_encode($image_data,JSON_UNESCAPED_UNICODE);
        //End
        $parent_product->setData("image_data",$json_image_data);
        $parent_product->save();
        //Add to cron job image
            //Put save image to cron job
            $type_cron = "CRON_PRODUCT_IMAGE";
            $cron_code = "CRON_EX15GV8_PRODUCT_PARENT_".$product_id."_IMPORT_".$id_provider;
            $content_cron_excute = $json_image_data;
            date_default_timezone_set("Asia/Ho_Chi_Minh");
            $time_update = date("Y-m-d H:i:s");
            $created_at = $time_update;
            $status = 0; // Processsing;
            $etc = "";
            $sql_insert_cron = "INSERT INTO `libero_cron_execute_background` (`id_cron`, `type_cron`, `cron_code`, `content_excute`, `created_at`, `status`, `etc`) 
            VALUES (NULL, '$type_cron', '$cron_code', '$content_cron_excute', '$created_at', '0', '$etc');";
            $connection->query($sql_insert_cron);
        //End add to cron job image
    }catch(\Exception $e){
        $text = "SAN PHAM CO TEN LA ".$name_product." BI LOI ".$e->getMessage().PHP_EOL;
        echo $text;
        file_put_contents("error_import.txt",$text,FILE_APPEND);
    }
}
echo "=======================END CREATE CONFIGRUABLE PRODUCT===================================".PHP_EOL;
echo "END SCRIPT".PHP_EOL;
//End Processign Configruable