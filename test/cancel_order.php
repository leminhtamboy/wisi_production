<?php
use \Magento\Framework\App\Bootstrap;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$id_order = $_GET["id_order"];
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$stockItemRepository = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface');
$connection = $resource->getConnection();

$sql_cancel = "UPDATE seller_sales_order SET `status` = 'cancel' where entity_id = '$id_order'";
$connection->query($sql_cancel);

$sql_update_grid_status = "UPDATE `seller_sales_order_grid` SET `status` = 'cancel' WHERE order_id = '$id_order'";
$connection->query($sql_update_grid_status);

$sql_get_cancel_product = "SELECT * FROM `seller_sales_order_item` WHERE order_id = '$id_order' and `product_type` != 'configurable'";
$cancel_products = $connection->query($sql_get_cancel_product);
foreach($cancel_products as $_product) {
    $qty_ordered = $_product["qty_ordered"];
    $product_id = $_product["product_id"];
    $sql_update_product_cancel_qty = "UPDATE `seller_sales_order_item` SET `qty_canceled` = '$qty_ordered' WHERE `order_id` = '$id_order' AND `product_id` = '$product_id'";
    $connection->query($sql_update_product_cancel_qty);
    $curQty = round($stockItemRepository->getStockItem($product_id)->getData("qty"),0);
    $qty_ordered = round($qty_ordered);
    $newQty = $curQty + $qty_ordered;
    $productModel = $objectManager->create('\Magento\Catalog\Model\Product');
    $stockRegistry = $objectManager->create("\Magento\CatalogInventory\Api\StockRegistryInterface");
    $stockItem = $stockRegistry->getStockItem($product_id);
    $stockItem->setData("is_in_stock",1);
    $stockItem->setData("qty",$newQty);
    $stockItem->setData("manage_stock",1);
    $stockItem->setData("use_config_notify_stock_qty",1);
    $stockItem->save();
}

$json_result  = array();
$json_result["success"] = "true";
$json_result["msg"] = "Cancel Order Successfully";
echo json_encode($json_result);