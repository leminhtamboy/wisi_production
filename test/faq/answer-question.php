<?php
use \Magento\Framework\App\Bootstrap;

include_once('../../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
try {
    $reply_question = $util->removeQuoteInString($_POST["reply"]);
    $id_faq = $_POST["id_faq"];
    $sender = $_POST["sender"];
    $provider_id = $_POST["provider_id"];
    $update_question = "A";
    $product_sku = $_POST["product_sku"];
    $product_name = $_POST["product_name"];
    date_default_timezone_set("Asia/Bangkok");
    $curDateTime = date("Y-m-d H:i:s");
    $modelFAQ = $objectManager->get("\Libero\Customer\Model\Faq");
    $modelFAQ->setData("sender",$sender);
    $modelFAQ->setData("provider_id",$provider_id);
    $modelFAQ->setData("type_faq",$update_question);
    $modelFAQ->setData("product_sku",$product_sku);
    $modelFAQ->setData("product_name",$product_name);
    $modelFAQ->setData("content_question",$reply_question);
    $modelFAQ->setData("answer",0);
    $modelFAQ->save();
    $parent_answer = $modelFAQ->getId();
    $faq = $objectManager->create("\Libero\Customer\Model\Faq")->load($id_faq);
    $faq->setData("answer","1");
    $faq->setData("parent_question",$parent_answer);
    $faq->save();

    $sql_update_question = "UPDATE `libero_faq` SET `answered_at` = '$curDateTime' WHERE `id_faq` = '$id_faq' ";
    $connection->query($sql_update_question);

    $sql_update_answer = "UPDATE `libero_faq` SET `created_at` = '$curDateTime' WHERE `id_faq` = '$parent_answer' ";
    $connection->query($sql_update_answer);

    $json_result  = array();
    $json_result["success"] = true;
    $json_result["msg"] = "Answer Question Successfully";
    echo json_encode($json_result);
}catch(Exception $e){
    echo json_encode(array("error" => "true","msg" => $e->getMessage()));
}
