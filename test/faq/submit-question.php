<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
//$request_body = file_get_contents('php://input');
//$data = json_decode($request_body,true);
$question_type = explode("|",$util->removeQuoteInString($_POST["question_type_code"]));
$question_type_code = $question_type[0];
$question_type_code_text = $question_type[1];
$sender = $util->removeQuoteInString($_POST["sender"]);
$question = $util->removeQuoteInString($_POST["question"]);
$provider_id = $_POST["provider_id"];
$provider_name= $_POST["provider_name"];
$product_sku = $_POST["product_sku"];
$product_name = $_POST["product_name"];
$sender_id = $_POST["sender_id"];
$title = $_POST["title"];
$type_faq = "Q";
$parent_question = "";
date_default_timezone_set("Asia/Bangkok");
$curDateTime = date("Y-m-d H:i:s");
try{
    if(strlen($question) < 501){
        $sql_insert = "INSERT INTO `libero_faq_room` (`id_room`, `status`, `content`, `sender_id`, `sender_name`, `created_at`, `sku`, `provider_id`, `question_type`, `question_type_text`,`title`) 
        VALUES (NULL, '2', '$question', '$sender_id', '$sender', '$curDateTime', '$product_sku', '$provider_id', '$question_type_code', '$question_type_code_text','$title')";
        $connection->query($sql_insert);
        $lastInsertId = $connection->lastInsertId();
        $sql_insert = "INSERT INTO `libero_faq` (`id_faq`, `sender`,`sender_id`,`provider_id`, `type_faq`, `question_type_code`, `question_type_code_text`, `parent_question`, `provider_name`, `content_question`,`product_sku`,`product_name`,`answer`, `created_at`,`id_room`)
        VALUES (NULL, '$sender','$sender_id','$provider_id', '$type_faq', '$question_type_code', '$question_type_code_text', '$parent_question', '$provider_name', '$question','$product_sku','$product_name','0', '$curDateTime','$lastInsertId')";
        $connection->query($sql_insert);
        echo json_encode(array("error" => false));
    }else{
        echo json_encode(array("error" => true,"msg" => "Nội dung câu hỏi không được quá 500 kí tự"));
    }
}catch(Exception $e){
    echo json_encode(array("error" => true,"msg" => $e->getMessage()));
}
