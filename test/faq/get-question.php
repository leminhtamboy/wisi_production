<?php 
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
$util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");

$room_id = $_POST["room_id"];
$sender_id = $_POST["sender_id"];
$question_type_code = $_POST["question_type_code"];
$provider_id = $_POST["provider_id"];
$provider_name = $_POST["provider_name"];
$product_sku = $_POST["product_sku"];
$product_name = $_POST["product_name"];
$customer_id = $_POST["customer_id"];

$sql_get_question_inroom = "SELECT * FROM `libero_faq` WHERE `id_room` = '$room_id' AND `type_faq` = 'Q'";
$data_question = $connection->fetchAll($sql_get_question_inroom);
$sql_get_room = "SELECT * FROM `libero_faq_room` WHERE `id_room` = '$room_id'";
$data_room = $connection->fetchAll($sql_get_room);
$sender_name = $data_room[0]["sender_name"];
?>
<ul style="position:relative" id="list-q-a">
<?php
foreach($data_question as $_question){
    $content_question = $_question["content_question"];
    $post_date = $_question["created_at"];
    $type_question = $_question["type_faq"];
    $answer_id = $_question["parent_question"];
    $provider_name = $_question["provider_name"];
    ?>
    <?php if($type_question == "Q"){ ?>
    <li style="display:block">
        <strong>
            <?php echo $data_room[0]["sender_name"]; ?>
        </strong>
        <span><?php echo $post_date; ?></span>
        <p style="margin-left:10px;margin-top:10px">
            <?php echo  $content_question; ?>
        </p>
    </li>
    <?php if($answer_id != ""){ ?>
        <?php 
        $sql_answer = "SELECT * FROM `libero_faq` WHERE `id_faq`= $answer_id";
        $data_answer = $connection->fetchAll($sql_answer)[0];
        $answer_date = $data_answer["created_at"];
        $answer_content = $data_answer["content_question"];
        ?>
        <li style="display:block;margin-left:30px">
            <strong>
                <?php echo $provider_name; ?>
            </strong>
            <p>
                <?php echo  $answer_content; ?> <span><?php echo $answer_date; ?></span>
            </p>
        </li>
        <?php } ?>
    <?php } ?>
    <?php
}
?>
</ul>
<?php if($customer_id == $sender_id){ ?>
    <hr/>
    <input type="hidden" id="hidden_id_room" value="<?php echo $room_id; ?>" />
    <input type="hidden" id="hidden_sender_name" value="<?php echo $sender_name; ?>" />
    <input type="hidden" id="hidden_question_type_code" value="<?php echo $question_type_code; ?>" />
    <fieldset class="fieldset review-fieldset" data-hasrequired="">
        <div class="field review-field-text required">
            <label for="review_field_faq" class="label"><span><?php echo __("Content Question") ?></span></label>
            <div class="control">
                <textarea name="detail" id="ajax_review_field_faq" cols="5" rows="3"></textarea>
            </div>
        </div>
    </fieldset>
    <div class="actions-toolbar review-form-actions">
        <div class="primary actions-primary">
            <button type="button" class="action submit primary post-question"><span><?php echo __("Post Question"); ?></span></button>
        </div>
    </div>
<?php } ?>
