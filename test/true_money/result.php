<?php 
require_once "truemoney.constants.php";

$access_key     = $_REQUEST['access_key'];
$order_id       = $_REQUEST['order_id'];
$trans_ref      = $_REQUEST['trans_ref'];
$payment_type   = $_REQUEST['payment_type'];
$result_code    = $_REQUEST['result_code'];
$description    = $_REQUEST['description'];
$signature      = $_REQUEST['signature'];
$secret_key     = TRUEMONEY_SECRET_KEY;

$data = "access_key=$access_key&order_id=$order_id&trans_ref=$trans_ref
&payment_type=$payment_type&result_code=$result_code";
$sign = hash_hmac("sha256", $data, $secret);

if ($sign == $signature) {
    if ($result_code == "00") {
        echo "$result_code-$order_id";
    }
} else {
    echo "Wrong Signature!";
}