<?php 
require_once "truemoney.constants.php";

$access_key     = $_POST['access_key'];
$order_id       = $_POST['order_id'];
$trans_ref      = $_POST['trans_ref'];
$payment_type   = $_POST['payment_type'];
$result_code    = $_POST['result_code'];
$description    = $_POST['description'];
$signature      = $_POST['signature'];
$secret_key     = TRUEMONEY_SECRET_KEY;

$data = "access_key=$access_key&order_id=$order_id&trans_ref=$trans_ref
&payment_type=$payment_type&result_code=$result_code";
$sign = hash_hmac("sha256", $data, $secret);
header('Content-Type: application/json');
if ($sign == $signature) {
    if ($result_code == "00") {
        echo "{response_code:00, response_message:\"Success\"}";
    }
} else {
    echo "{response_code: 99, response_message:\"Wrong Signature\"}";
}
exit();