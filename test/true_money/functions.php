<?php
require_once "truemoney.constants.php";

function execPostRequest($url, $data)
{
    try {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    } catch (Exception $e) {
        return false;
    }
}

/**
 * Get Payment URL from order information.
 *
 * @param $access_key  The access_key provided by payment system.
 * @param $secret      The secret_key provided by payment system.
 * @param $order_info  The short description for order.
 * @param $amount      Total amount of order.
 * @param $order_id    The Id of order, you will check it on payment system.
 * @param $method      The method you registered on payment system. Current support bank/wallet/visa.
 * @param $bankcode    The code of bank provided in documents at developers.pay.truemoney.com.vn
 * 
 * @return null/object  Object response include error/url payment after request.
 * */
function getPayUrl($access_key, $secret, $order_info, $amount, $order_id, $method = '', $bankcode = '')
{
    $return_url = TRUEMONEY_RETURN_URL;
    $amount = intval(round($amount));
    $data = "access_key=".$access_key.
          "&amount=".$amount.
          "&order_id=".$order_id.
          "&order_info=".$order_info.
          "&return_url=".$return_url;
    $signature = hash_hmac("sha256", $data, $secret);
    $send_data = [
        "access_key"   => $access_key,
        "amount"       => $amount,
        "order_id"     => $order_id,
        "order_info"   => $order_info,
        "return_url"   => $return_url,
        "language"     => 'vi',
        "signature"    => $signature
    ];
    if ($method == 'atm') {
        $send_data['payment_type'] = 0;
        $send_data['bank_code'] = $bankcode;
    }
    else if ($method == 'visa') {
        $send_data['payment_type'] = 1;
    }
    else if ($method == 'wallet') {
        $send_data['payment_type'] = 2;
        $send_data['wallet_code'] = 'TRUEMONEY';
    } else {
        return null;
    }
    $data1 = json_encode($send_data);
    $json_bankCharging = execPostRequest(TRUEMONEY_DO_PAYMENT_URL, $data1);
    if ($json_bankCharging) {
        $decode_bankCharging = json_decode($json_bankCharging, true);  // decode json
        return $decode_bankCharging;
    }
    return null;
}

/**
 * Get Detail of transaction. Use for check information when order in processing.
 *
 * @param $access_key  The access_key provided by payment system.
 * @param $secret      The secret_key provided by payment system.
 * @param $trans_ref   The reference of Transaction after response of request of pay URL method.
 * 
 * @return null/object Object response include error/detail of transaction after request.
 * */
function getTransactionDetail($access_key, $secret, $trans_ref)
{
    $data = [
        'access_key' => $access_key,
        'trans_ref'  => $trans_ref
    ];
    $signature = hash_hmac("sha256", $data, $secret);
    $send_data = [
        "access_key"   => $access_key,
        "trans_ref"    => $trans_ref,
        "language"     => 'vi',
        "signature"    => $signature
    ];
    $data1 = json_encode($send_data);
    $json_trans_detail = execPostRequest(TRUEMONEY_TRANS_DETAIL_URL, $data1);
    if ($json_trans_detail) {
        $decode_trans_detail = json_decode($json_trans_detail, true);  // decode json
        return $decode_trans_detail;
    }
    return null;
}