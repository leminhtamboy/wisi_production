<?php 
require "functions.php";

$order_id = $_REQUEST['order_id'];
$order_info = $_REQUEST['order_info'];
$order_amount = $_REQUEST['order_amount'];
$method = $_REQUEST['method'];
$result = getPayUrl(TRUEMONEY_ACCESS_KEY, TRUEMONEY_SECRET_KEY, $order_info, $order_amount, $order_id, $method);
if ($result->response_code == '00') {
    return $result->data->payment_url;
}
return null;