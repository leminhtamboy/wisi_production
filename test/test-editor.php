<!DOCTYPE html>
<html>
<head>
  <!--<script src="http://dev.wisiproduction.com/script/tinymce.min.js"></script>
  <script src="http://dev.wisiproduction.com/script/jquery.tinymce.min.js"></script>-->
  <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=xy4fto9ytmd6zfeilzf2r1uuqdugp0yqg0bmkzejkr7k5n7s"></script>
  <script>
  tinymce.init({
    selector: '#mytextarea',
    images_upload_url: 'upload_image.php',
    height:550,
    images_upload_credentials: true,
    images_upload_base_path: '/test',
    images_reuse_filename: true,
    automatic_uploads: true,
    plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker imagetools powerpaste',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality emoticons template paste textcolor'
    ],
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons | image',
    images_upload_handler: function (blobInfo, success, failure) {
      var xhr, formData;

      xhr = new XMLHttpRequest();
      xhr.withCredentials = false;
      xhr.open('POST', 'upload_image.php');

      xhr.onload = function() {
        var json;

        if (xhr.status != 200) {
          failure('HTTP Error: ' + xhr.status);
          return;
        }

        json = JSON.parse(xhr.responseText);
        console.log(json);
        if (!json || typeof json.location != 'string') {
          failure('Invalid JSON: ' + xhr.responseText);
          return;
        }

        success(json.location);
      };

      formData = new FormData();
      formData.append('file', blobInfo.blob(), blobInfo.filename());

      xhr.send(formData);
    }
  });
  </script>
</head>
<body>
<style>
#mceu_47{display:none}
</style>
  <textarea id="mytextarea"></textarea>
</body>
</html>