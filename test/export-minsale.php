<?php
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\Product\Visibility;
use Magento\Catalog\Setup\CategorySetup;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Eav\Api\Data\AttributeOptionInterface;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$object_Manager = $bootstraps->getObjectManager();
$state = $object_Manager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
/** Include PHPExcel */
require_once("Classes/PHPExcel.php");
//Create a PHPExcel object
$objPHPExcel = new PHPExcel();

//Set document properties
$objPHPExcel->getProperties()->setCreator("Wisi")
							 ->setLastModifiedBy("Wisi")
							 ->setTitle("User's Information")
							 ->setSubject("User's Personal Data")
							 ->setDescription("Description of User's")
							 ->setKeywords("")
							 ->setCategory("");

// Set default font
$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
                                          ->setSize(10);

//Set the first row as the header row
$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Sku')
                              ->setCellValue('B1', 'Tên Sản Phẩm')
                              ->setCellValue('C1', 'Tên Nhà Cung Cấp')
                              ->setCellValue('D1', 'Số lượng bán tối thiểu')
                              ->setCellValue('E1', 'Giá Bán Lẻ (Đã bao gồm VAT)')
                              ->setCellValue('F1', 'Giá Khuyến Mãi (Đã bao gồm VAT)')
                              ->setCellValue('G1', 'VAT (%)');
//Rename the worksheet
$objPHPExcel->getActiveSheet()->setTitle('Giá bán thấp nhất');

//Set active worksheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
//Create List Sheet For Category,Tax,Free Shipment,type product,Color,Size
//End Of Comment Column

//Category List
$modelCagegory = $object_Manager->get("\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory");
$categorylist = $modelCagegory->create();
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$taxClassObj = $objectManager->create('Magento\Tax\Model\TaxClass\Source\Product');
$taxClassess = $taxClassObj->getAllOptions();
$string_category = "";
$c = 2;
$ctax = 2;
$c_color = 2;
$c_size  = 2;

$start = 2;
$start_tax = 2;
$start_color = 2;
$start_size = 2;

$end = 0;
$end_tax = 0;
$end_color = 0;
$end_size = 0;

$code_color = 93;
$code_size = 141;
foreach($categorylist as $_category){
    $path = $_category->getData("path");
    $array_path = explode("/",$path);
    $item = "";
    $children_count = $_category->getData("children_count");
    if($children_count == 0 || $children_count == "0"){
        foreach($array_path as $id_category){
            if($id_category == 1){
                continue;
            }
            if($id_category == 2){
                $item.="Wisi Category";
                continue;
            }
            $modelCate = $object_Manager->create("\Magento\Catalog\Model\Category");
            $cate = $modelCate->load($id_category);
            $name_cate = $cate->getName();
            $item.="/".$name_cate;
        }
        if($item != ""){
            $item = str_replace("'","",$item);
            $string_category.=$item.",";
        }
        
        $c++;
    }
}
$end = $c;
foreach($taxClassess as $_tax){
    $title = $_tax["label"];
    $value = $_tax["value"];
    $ctax++;
}
$end_tax = $ctax;
//////
//Color
$eavConfig = $object_Manager->create(\Magento\Eav\Model\Config::class);
$eavConfig->clear();
$attribute = $eavConfig->getAttribute('catalog_product', $code_color);
$installer = $object_Manager->create(\Magento\Catalog\Setup\CategorySetup::class);
$eavConfig->clear();
$options = $attribute->getOptions();
$attribute_id = $attribute->getData("attribute_id");
foreach($options as $_option){
    $c_color++;
}
$end_color = $c_color;
//Size
$eavConfig = $object_Manager->create(\Magento\Eav\Model\Config::class);
$eavConfig->clear();
$attribute = $eavConfig->getAttribute('catalog_product', $code_size);
$installer = $object_Manager->create(\Magento\Catalog\Setup\CategorySetup::class);
$eavConfig->clear();
$options = $attribute->getOptions();
$attribute_id = $attribute->getData("attribute_id");
foreach($options as $_option){
    $c_size++;
}
$end_size = $c_size;
//////
$string_category = substr($string_category,0,strlen($string_category) - 1);
$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
$productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface');
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$connection = $resource->getConnection();
//Get Products
$modelProducts = $object_Manager->get("\Magento\Catalog\Model\Product");
$products = null;
if(isset($_GET["id_provider"])){
    $provider = $_GET["id_provider"];
    $products = $modelProducts->getCollection()
    ->addAttributeToSelect("*")
    ->addAttributeToFilter("id_seller",array("eq" => $provider))
    ->setOrder("entity_id","desc");
}else{
    $products = $modelProducts->getCollection()->addAttributeToSelect("*")->setOrder("entity_id","desc");
}
$i = 1;
$id_parent = "";
foreach($products as $_product){
    $id_product = $_product->getId();
    $name_product = $_product->getName();
    $sku = $_product->getSku();
    $type_product = $_product->getTypeId();
    $visibility = $_product->getVisibility();
    if($type_product == "simple"){ //simple thuần chủng
        $category = $_product->getCategoryIds();
        $string_category_product =  "";
        if(count($category) > 0 ){
            $id_product_category = $category[0];
            $modelCate = $object_Manager->create("\Magento\Catalog\Model\Category");
            $cate = $modelCate->load($id_product_category);
            $path = $cate->getPath();
            $array_path = explode("/",$path);
            $item = "";
            foreach($array_path as $id_category){
                if($id_category == 1){
                    continue;
                }
                if($id_category == 2){
                    $item.="Wisi Category";
                    continue;
                }
                $modelCate = $object_Manager->create("\Magento\Catalog\Model\Category");
                $cate = $modelCate->load($id_category);
                $name_cate = $cate->getName();
                $item.="/".$name_cate;
            }
            $string_category_product = $item;
        }
        $weight = $_product->getWeight();
        $length = $_product->getData("length");
        $width = $_product->getData("width");
        $height = $_product->getData("height");
        $show_free_ship = "Yes";
        $color = $_product->getColor();
        $size = $_product->getSize();
        $show_color = "";
        $show_size = "";
        if($color != ""){
            $sql_getoption_label = "SELECT * FROM `eav_attribute_option_value` WHERE option_id = $color";
            $data_option_color = $connection->fetchAll($sql_getoption_label);
            if(count($data_option_color) > 0){
                $show_color = $data_option_color[0]["value"];
            }
        }
        if($size != ""){
            $sql_getoption_label = "SELECT * FROM `eav_attribute_option_value` WHERE option_id = $size";
            $data_option_size = $connection->fetchAll($sql_getoption_label);
            if(count($data_option_size) > 0){
                $show_size = $data_option_size[0]["value"];
            }
        }
        $free_shipment = $_product->getFreeShipment();
        if($free_shipment == 0){
            $show_free_ship = "No";
        }
        $tax = $_product->getTaxClassId();
        $show_tax = "None";
        if($tax == 4){
            $show_tax = "VAT 10%";
        }
        if($tax == 5){
            $show_tax = "VAT 5%";
        }
        $stock = $productStockObj->getStockItem($id_product);
        $qty = round($stock->getData("qty"),0);
        $status = $_product->getStatus();
        $price = $_product->getPrice();
        $special_price = $_product->getSpecialPrice();
        $description = $_product->getDescription();
        $short_description = $_product->getShortDescription();
        $url_key = $_product->getData("url_key");
        $meta_title = $_product->getData("meta_title");
        $meta_keyword = $_product->getData("meta_keyword");
        $meta_description = $_product->getData("meta_description");
        $json_quick_detail = $_product->getData("quick_detail");
        $array_quick_detail = json_decode($json_quick_detail,true);
        $array_value_quick_detail = array();
        if(count($array_quick_detail) > 0){
            $array_value_quick_detail = $array_quick_detail["data"];
        }
        $string_quick_detail = "";
        $min_qty = round($stock->getData("min_sale_qty"),0);
        foreach($array_value_quick_detail as $_detail){
            $title = $_detail["title"];
            $content = $_detail["content"];
            $string_quick_detail.= $title.":".$content.";";
        } 
        $string_quick_detail = substr($string_quick_detail,0,strlen($string_quick_detail) - 1);
        //Region Image
		
        $imageHelperFactory = $objectManager->get("\Magento\Catalog\Helper\ImageFactory");
        $base_img = $imageHelperFactory->create()->init($_product, 'product_page_image_large')->getUrl();
		$base_name_img = basename($imageHelperFactory->create()->init($_product, 'product_page_image_large')->getUrl());
		if($base_name_img != ".jpg" && $base_name_img != ".png"){
			$base_img = "temp_image_product/".basename($imageHelperFactory->create()->init($_product, 'product_page_image_large')->getUrl());
		}else{
			$json_image = json_decode($_product->getData("image_data"),true);
			$url_main_img = $json_image["main"][$id_product]["base_img"];
			$base_img = $url_main_img;
		}
        $arrayImages = $_product->getMediaGalleryImages();
        if(!$arrayImages){
            $arrayImages = array();
        }
        $array_thumbnail_img = array();
        $string_url_thumnbnail_img = "";
        foreach($arrayImages as $_child){ 
            $array_thumbnail_img[] = $_child->getUrl();
            $url_thumb = basename($_child->getUrl());
            $path_final = "temp_image_product/".$url_thumb;
            $string_url_thumnbnail_img .= $path_final.",";
        }
        if($string_url_thumnbnail_img != ""){
            $string_url_thumnbnail_img = substr($string_url_thumnbnail_img,0,strlen($string_url_thumnbnail_img) - 1);
        }
        $id_seller =  $_product->getData("id_seller");
        $modelProvider = $object_Manager->create("\Libero\Customer\Model\Seller");
        $provider = $modelProvider->load($id_seller);
        $store_name = $provider->getData("store_name");
        $vat = $_product->getData("Vat");
        //End Region
        $objPHPExcel->getActiveSheet()
                    ->setCellValue('A'.($i+1), $sku)
                    ->setCellValue('B'.($i+1), $name_product)
                    ->setCellValue('C'.($i+1), $store_name)
                    ->setCellValue('D'.($i+1), $min_qty)
                    ->setCellValue('E'.($i+1), $price)
                    ->setCellValue('F'.($i+1), $special_price)
                    ->setCellValue('G'.($i+1), $vat);
        $i++;
    }
    $id_parent = $id_product;
    /*if($type_product == "configurable"){
        $_children = $_product->getTypeInstance()->getUsedProducts($_product);
        foreach($_children as $_variant){
            $_product = $object_Manager->create("\Magento\Catalog\Model\Product")->load($_variant->getId());
            $id_product = $_product->getId();
            $name_product = $_product->getName();
            $sku = $_product->getSku();
            $type_product = "configurable";
            $category = $_product->getCategoryIds();
            $string_category_product =  "";
            if(count($category) > 0 ){
                $id_product_category = $category[0];
                $modelCate = $object_Manager->create("\Magento\Catalog\Model\Category");
                $cate = $modelCate->load($id_product_category);
                $path = $cate->getPath();
                $array_path = explode("/",$path);
                $item = "";
                foreach($array_path as $id_category){
                    if($id_category == 1){
                        continue;
                    }
                    if($id_category == 2){
                        $item.="Wisi Category";
                        continue;
                    }
                    $modelCate = $object_Manager->create("\Magento\Catalog\Model\Category");
                    $cate = $modelCate->load($id_category);
                    $name_cate = $cate->getName();
                    $item.="/".$name_cate;
                }
                $string_category_product = $item;
            }
            $weight = $_product->getWeight();
            $length = $_product->getData("length");
            $width = $_product->getData("width");
            $height = $_product->getData("height");
            $color = $_product->getColor();
            $size = $_product->getSize();
            $show_color = "";
            $show_size = "";
            $sql_getoption_label = "SELECT * FROM `eav_attribute_option_value` WHERE option_id = $color";
            $data_option_color = $connection->fetchAll($sql_getoption_label);
            if(count($data_option_color) > 0){
                $show_color = $data_option_color[0]["value"];
            }
            $sql_getoption_label = "SELECT * FROM `eav_attribute_option_value` WHERE option_id = $size";
            $data_option_size = $connection->fetchAll($sql_getoption_label);
            if(count($data_option_size) > 0){
                $show_size = $data_option_size[0]["value"];
            }
            $show_free_ship = "Yes";
            $free_shipment = $_product->getFreeShipment();
            if($free_shipment == 0){
                $show_free_ship = "No";
            }
            $tax = $_product->getTaxClassId();
            $show_tax = "None";
            if($tax == 4){
                $show_tax = "VAT 10%";
            }
            if($tax == 5){
                $show_tax = "VAT 5%";
            }
            $stock = $productStockObj->getStockItem($id_product);
            $qty = round($stock->getData("qty"),0);
            $status = $_product->getStatus();
            $price = $_product->getPrice();
            $special_price = $_product->getSpecialPrice();
            $description = $_product->getDescription();
            $short_description = $_product->getShortDescription();
            $url_key = $_product->getData("url_key");
            $meta_title = $_product->getData("meta_title");
            $meta_keyword = $_product->getData("meta_keyword");
            $meta_description = $_product->getData("meta_description");
            $json_quick_detail = $_product->getData("quick_detail");
            $array_quick_detail = json_decode($json_quick_detail,true);
            $array_value_quick_detail = array();
            if(count($array_quick_detail) > 0){
                $array_value_quick_detail = $array_quick_detail["data"];
            }
            $string_quick_detail = "";
            foreach($array_value_quick_detail as $_detail){
                $title = $_detail["title"];
                $content = $_detail["content"];
                $string_quick_detail.= $title.":".$content.";";
            } 
            $string_quick_detail = substr($string_quick_detail,0,strlen($string_quick_detail) - 1);
            //Region Image
            $imageHelperFactory = $objectManager->get("\Magento\Catalog\Helper\ImageFactory");
            $base_img = $imageHelperFactory->create()->init($_product, 'product_page_image_large')->getUrl();
            $base_img = "temp_image_product/".basename($imageHelperFactory->create()->init($_product, 'product_page_image_large')->getUrl());
            $arrayImages = $_product->getMediaGalleryImages();
            if(!$arrayImages){
                $arrayImages = array();
            }
            $array_thumbnail_img = array();
            $string_url_thumnbnail_img = "";
            foreach($arrayImages as $_child){ 
                $array_thumbnail_img[] = $_child->getUrl();
                $url_thumb = basename($_child->getUrl());
                $path_final = "temp_image_product/".$url_thumb;
                $string_url_thumnbnail_img .= $path_final.",";
            }
            if($string_url_thumnbnail_img != ""){
                $string_url_thumnbnail_img = substr($string_url_thumnbnail_img,0,strlen($string_url_thumnbnail_img) - 1);
            }
            //End Region
            $validation = $objPHPExcel->getActiveSheet()->getCell('D'.($i+1))->getDataValidation();
            $validation->setType("list");
            $validation->setErrorStyle("information");
            $validation->setAllowBlank(false);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setShowDropDown(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Value is not in list.');
            $validation->setPromptTitle('Select category');
            $validation->setPrompt('Categories.');
            $validation->setFormula1('=\'Reference\'!$A$'.$start.':$A$'.$end.'');
            
            $validation_tax = $objPHPExcel->getActiveSheet()->getCell('J'.($i+1))->getDataValidation();
            $validation_tax->setType("list");
            $validation_tax->setErrorStyle("information");
            $validation_tax->setAllowBlank(false);
            $validation_tax->setShowInputMessage(true);
            $validation_tax->setShowErrorMessage(true);
            $validation_tax->setShowDropDown(true);
            $validation_tax->setErrorTitle('Input error');
            $validation_tax->setError('Value is not in list.');
            $validation_tax->setPromptTitle('Select tax');
            $validation_tax->setPrompt('Tax.');
            $validation_tax->setFormula1('=\'Reference\'!$B$'.$start_tax.':$B$'.$end_tax.'');
            
            $validation_type_product = $objPHPExcel->getActiveSheet()->getCell('B'.($i+1))
            ->getDataValidation();
            $validation_type_product->setType("list");
            $validation_type_product->setErrorStyle("information");
            $validation_type_product->setAllowBlank(false);
            $validation_type_product->setShowInputMessage(true);
            $validation_type_product->setShowErrorMessage(true);
            $validation_type_product->setShowDropDown(true);
            $validation_type_product->setErrorTitle('Input error');
            $validation_type_product->setError('Type product is not in list.');
            $validation_type_product->setPromptTitle('Select Type Product');
            $validation_type_product->setPrompt('Type Product.');
            $validation_type_product->setFormula1('"simple,configurable"');

            $validation_freeshipment = $objPHPExcel->getActiveSheet()->getCell('I'.($i+1))->getDataValidation();
            $validation_freeshipment->setType("list");
            $validation_freeshipment->setErrorStyle("information");
            $validation_freeshipment->setAllowBlank(false);
            $validation_freeshipment->setShowInputMessage(true);
            $validation_freeshipment->setShowErrorMessage(true);
            $validation_freeshipment->setShowDropDown(true);
            $validation_freeshipment->setErrorTitle('Input error');
            $validation_freeshipment->setError('Type product is not in list.');
            $validation_freeshipment->setPromptTitle('Lựa chọn sản phẩm miễn phí ship');
            $validation_freeshipment->setPrompt('Miên phí ship.');
            $validation_freeshipment->setFormula1('"Yes,No"');

            $validation_color = $objPHPExcel->getActiveSheet()->getCell('E'.($i+1))->getDataValidation();
            $validation_color->setType("list");
            $validation_color->setErrorStyle("information");
            $validation_color->setAllowBlank(false);
            $validation_color->setShowInputMessage(true);
            $validation_color->setShowErrorMessage(true);
            $validation_color->setShowDropDown(true);
            $validation_color->setErrorTitle('Input error');
            $validation_color->setError('Value is not in list.');
            $validation_color->setPromptTitle('Lựa Chọn Màu Sắc');
            $validation_color->setPrompt('Màu Sắc.');
            $validation_color->setFormula1('=\'Reference\'!$C$'.$start_color.':$C$'.$end_color.'');

            $validation_color = $objPHPExcel->getActiveSheet()->getCell('F'.($i+1))->getDataValidation();
            $validation_color->setType("list");
            $validation_color->setErrorStyle("information");
            $validation_color->setAllowBlank(false);
            $validation_color->setShowInputMessage(true);
            $validation_color->setShowErrorMessage(true);
            $validation_color->setShowDropDown(true);
            $validation_color->setErrorTitle('Input error');
            $validation_color->setError('Value is not in list.');
            $validation_color->setPromptTitle('Lựa Chọn Kích Thước');
            $validation_color->setPrompt('Kích Thước.');
            $validation_color->setFormula1('=\'Reference\'!$D$'.$start_size.':$D$'.$end_size.'');

            $objPHPExcel->getActiveSheet()
                    ->setCellValue('A'.($i+1), $sku)
                    ->setCellValue('B'.($i+1), $type_product)
                    ->setCellValue('C'.($i+1), $name_product)
                    ->setCellValue('D'.($i+1), $string_category_product)
                    ->setCellValue('E'.($i+1), $show_color)
                    ->setCellValue('F'.($i+1), $show_size)
                    ->setCellValue('G'.($i+1), $price)
                    ->setCellValue('H'.($i+1), $special_price)
                    ->setCellValue('I'.($i+1), $show_free_ship)
                    ->setCellValue('J'.($i+1), $show_tax)
                    ->setCellValue('K'.($i+1), $qty)
                    ->setCellValue('L'.($i+1), $weight)
                    ->setCellValue('M'.($i+1), $length)
                    ->setCellValue('N'.($i+1), $height)
                    ->setCellValue('O'.($i+1),$width)
                    ->setCellValue('P'.($i+1), $short_description)
                    ->setCellValue('Q'.($i+1), "")
                    ->setCellValue('R'.($i+1), "")
                    ->setCellValue('S'.($i+1), "")
                    ->setCellValue('T'.($i+1), "")
                    ->setCellValue('U'.($i+1), "")
                    ->setCellValue('V'.($i+1), "")
                    ->setCellValue('W'.($i+1), $string_quick_detail)
                    ->setCellValue('X'.($i+1), $meta_title)
                    ->setCellValue('Y'.($i+1), $meta_keyword)
                    ->setCellValue('Z'.($i+1), $meta_description)
                    ->setCellValue('AA'.($i+1), $base_img)
                    ->setCellValue('AB'.($i+1), $string_url_thumnbnail_img);
            $i++;
        }
    }*/
}
/*************** Fetching data from database ***************/
/*$sql = "select * from `user_details`";
$res = mysqli_query($con, $sql);
if(mysqli_num_rows($res)>0)
{
	 $i = 1;
	 while($row = mysqli_fetch_object($res)) {
		$objPHPExcel->getActiveSheet()->setCellValue('A'.($i+1), $row->id)
									  ->setCellValue('B'.($i+1), $row->name)
									  ->setCellValue('C'.($i+1), $row->mobile)
									  ->setCellValue('D'.($i+1), $row->country);					  
	    $i++;
	 }
}
							  
$objPHPExcel->getActiveSheet()->setCellValue("B10","Item B");*/
//Dynamic name, the combination of date and time
$filename = "export_min_sale_".date('d-m-Y_H-i-s').".xlsx";

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

//if you want to save the file on the server instead of downloading, 
//comment the last 3 lines and remove the comment from the next line
//$objWriter->save(str_replace('.php', '.xlsx', $filename));
//header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename='.$filename);
$objWriter->save("php://output");
