<?php 
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$object_Manager = $bootstraps->getObjectManager();
$state = $object_Manager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$attribute_ids = $_GET["attribute_ids"];
$attribute_ids = substr($attribute_ids,0,strlen($attribute_ids) - 1);
$array_attribute_ids = explode("|",$attribute_ids);
?><div class="ui-sortable"><?php
foreach($array_attribute_ids as $_attribute){
    $eavConfig = $object_Manager->create(\Magento\Eav\Model\Config::class);
    $eavConfig->clear();
    $attribute = $eavConfig->getAttribute('catalog_product', $_attribute);
    /** @var $installer \Magento\Catalog\Setup\CategorySetup */
    $installer = $object_Manager->create(\Magento\Catalog\Setup\CategorySetup::class);
    $eavConfig->clear();
    $options = $attribute->getOptions();
    $attribute_id = $attribute->getData("attribute_id");
        ?>
        <div class="attribute-entity" id="attribute-id-<?php echo $attribute_id; ?>" data-attribute-id="<?php echo $attribute_id; ?>" data-attribute-title="<?php echo $attribute->getStoreLabel(); ?>">
            <div class="attribute-entity-top">
                <div class="attribute-entity-title-block">
                    <span class="draggable-handle" data-role="draggable" title="Sort Variations">
                    </span>
                    <div class="attribute-entity-title"><?php echo $attribute->getStoreLabel(); ?></div>
                    <div class="attribute-options-block">
                        (<span class="attribute-length"><?php echo count($options); ?></span> Options)
                    </div>
                </div>
            </div>

            <fieldset class="admin__fieldset admin__fieldset-options">
                <ul class="attribute-options">
                <?php foreach($options as $_option){ ?>
                    <?php if($_option->getValue() == "") continue; ?>
                    <li class="attribute-option">
                        <div class="admin__field admin__field-option">
                            <input type="checkbox" style="width: 10% !important;vertical-align: text-top;" class="option_array customopt-<?php echo $_option->getValue(); ?>" value="<?php echo $_option->getValue(); ?>" id="<?php echo $_option->getValue(); ?>">
                            <label for="<?php echo $_option->getValue(); ?>"><?php echo $_option->getLabel() ?></label>
                        </div>
                    </li>
                <?php } ?>
                </ul>
            </fieldset>
        </div>
<?php } ?>
</div>
<p></p>