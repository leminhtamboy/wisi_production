<?php
include_once("database.php");
$database = new database();
use \Magento\Framework\App\Bootstrap;
use Magento\Eav\Api\AttributeRepositoryInterface;
include_once('../app/bootstrap.php');
$bootstraps = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstraps->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

//Clear Reseller and Provider
echo "=============Begin Of Clear Customer=================".PHP_EOL;
//Get Collection Customer
/*$modelCustomer = $objectManager->create("Magento\Customer\Model\Customer");
$collectionCustomer = $modelCustomer->getCollection();
$objectManager->get('Magento\Framework\Registry')->register('isSecureArea', true);
echo "DELETING CUSTOMER....".PHP_EOL;
foreach($collectionCustomer as $_customer){
    $id_customer = $_customer->getId();
    $tempCustomer = $objectManager->create("Magento\Customer\Model\Customer")->load($id_customer);
    $tempCustomer->delete();
    
}*/
//Clear Provider And Reseller
/*$sql_delete_provider = "DELETE FROM `libero_customer_seller_company`;";
$sql_delete_provider .= "DELETE FROM `libero_reseller`;DELETE FROM `libero_reseller_bank`;DELETE FROM `libero_reseller_products`;DELETE FROM `libero_reseller_ware_house`";
$database->ExcuteNonQuery($sql_delete_provider);*/
echo "=============End Of Clear Customer==========".PHP_EOL;
//Clear Product

echo "=============Begin Of Clear Product=================".PHP_EOL;
$modelProduct = $objectManager->get('Magento\Catalog\Model\Product');
$collectionProduct = $modelProduct->getCollection();
echo "DELETING PRODUCT....".PHP_EOL;
foreach($collectionProduct as $_product){
    $id_product = $_product->getId();
    //load model product
    $tempProduct = $objectManager->create("Magento\Catalog\Model\Product")->load($id_product);
    $tempProduct->delete();
}
echo "=============End Of Clear Product==========".PHP_EOL;
//Clear Order And Quote 
/*$checkoutSession = $objectManager->get('Magento\Checkout\Model\Session');          
$cart = $objectManager->create('Magento\Checkout\Model\Cart');
$allItems = $checkoutSession->getQuote()->getItemsCollection(); //returns all teh items in session     
foreach ($allItems as $item) {
    $itemId = $item->getItemId(); //item id of particular item
    $cart->removeItem($itemId); //deletes the item
    $cart->save();
}
$sql_delete_order = "SET FOREIGN_KEY_CHECKS=0;
# Clean order history
TRUNCATE TABLE `sales_bestsellers_aggregated_daily`;
TRUNCATE TABLE `sales_bestsellers_aggregated_monthly`;
TRUNCATE TABLE `sales_bestsellers_aggregated_yearly`;

# Clean order infos
TRUNCATE TABLE `sales_creditmemo`;
TRUNCATE TABLE `sales_creditmemo_comment`;
TRUNCATE TABLE `sales_creditmemo_grid`;
TRUNCATE TABLE `sales_creditmemo_item`;
TRUNCATE TABLE `sales_invoice`;
TRUNCATE TABLE `sales_invoiced_aggregated`;
TRUNCATE TABLE `sales_invoiced_aggregated_order`;
TRUNCATE TABLE `sales_invoice_comment`;
TRUNCATE TABLE `sales_invoice_grid`;
TRUNCATE TABLE `sales_invoice_item`;
TRUNCATE TABLE `sales_order`;
TRUNCATE TABLE `sales_order_address`;
TRUNCATE TABLE `sales_order_aggregated_created`;
TRUNCATE TABLE `sales_order_aggregated_updated`;
TRUNCATE TABLE `sales_order_grid`;
TRUNCATE TABLE `sales_order_item`;
TRUNCATE TABLE `sales_order_payment`;
TRUNCATE TABLE `sales_order_status_history`;
TRUNCATE TABLE `sales_order_tax`;
TRUNCATE TABLE `sales_order_tax_item`;
TRUNCATE TABLE `sales_payment_transaction`;
TRUNCATE TABLE `sales_refunded_aggregated`;
TRUNCATE TABLE `sales_refunded_aggregated_order`;
TRUNCATE TABLE `sales_shipment`;
TRUNCATE TABLE `sales_shipment_comment`;
TRUNCATE TABLE `sales_shipment_grid`;
TRUNCATE TABLE `sales_shipment_item`;
TRUNCATE TABLE `sales_shipment_track`;
TRUNCATE TABLE `sales_shipping_aggregated`;
TRUNCATE TABLE `sales_shipping_aggregated_order`;

# Clean cart infos
TRUNCATE TABLE `quote`;
TRUNCATE TABLE `quote_address`;
TRUNCATE TABLE `quote_address_item`;
TRUNCATE TABLE `quote_id_mask`;
TRUNCATE TABLE `quote_item`;
TRUNCATE TABLE `quote_item_option`;
TRUNCATE TABLE `quote_payment`;
TRUNCATE TABLE `quote_shipping_rate`;

# Reset indexes (if you want your orders number start back to 1
TRUNCATE TABLE sequence_invoice_1;
TRUNCATE TABLE sequence_order_1;
TRUNCATE TABLE sequence_shipment_1;
TRUNCATE TABLE sequence_creditmemo_1;

TRUNCATE TABLE sales_order_seller_logs;
TRUNCATE TABLE seller_review;
TRUNCATE TABLE seller_review_summary;
TRUNCATE TABLE seller_sales_invoice;
TRUNCATE TABLE seller_sales_invoice_comment;
TRUNCATE TABLE seller_sales_invoice_grid;
TRUNCATE TABLE seller_sales_invoice_item;
TRUNCATE TABLE seller_sales_order;
TRUNCATE TABLE seller_sales_order_grid;
TRUNCATE TABLE seller_sales_order_item;
TRUNCATE TABLE seller_sales_shipment;
TRUNCATE TABLE seller_sales_shipment_grid;
TRUNCATE TABLE seller_sales_shipment_item;
TRUNCATE TABLE seller_sales_shipment_track;
TRUNCATE TABLE `report_compared_product_index`;
TRUNCATE TABLE `report_viewed_product_aggregated_daily`;
TRUNCATE TABLE `report_viewed_product_aggregated_monthly`;
TRUNCATE TABLE `report_viewed_product_aggregated_yearly`;
TRUNCATE TABLE `report_viewed_product_index`;
TRUNCATE TABLE `libero_quote_shipping_price`;
TRUNCATE TABLE `libero_dowload_products`;
SET FOREIGN_KEY_CHECKS=1;";
$database->ExcuteNonQuery($sql_delete_order);*/
//Clear Log