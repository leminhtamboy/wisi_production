/* sticky menu */
var offTop, msgOffTop, sellerCateOffTop;
/*setTimeout(() => {
	if(jQuery(".header-middle").length) {

		msgOffTop = offTop + 50;
	}
}, 1000);*/

setTimeout(() => {
    if (jQuery("#seller-category-list").length) {
        sellerCateOffTop = jQuery("#seller-category-list").offset().top;
        sellerCateOffTop -= 100;
    }
},1000);


jQuery(function(){
    showMegamenuOnHomepage();
    scrollofftop();

    /* hover megamenu */
    hoverMegamenu();

    /* dropdown menu */
    hoverDropdown();
    clickDropdown();
    clickDropdownMobile();

    /* popup */
    fnClosePopupOnEven();

    /* tabs */
    fnChangeTab();

    /* collapse & expand */
    fnCollapseExpand();
    fnShowSubmenuSellerDashboard();
    fnShowDropdown();

    /* change qty input on checkout page */
    fnQtyInputCheckout();

    /* active slider */
    activeSlider();
    activeSellerSlider();

    /* set full screen width */
    setFullScreenWidth();

    /* upload file */
    fnUploadFileBtn();
    fnGetUploadFileName();
    fnRemoveUploadFile();

    /* set content width for seller dashboard */
    fnSetContentWidthForSellerDashboard();

    /* show submenu on seller dashboard  */
    showSubmenuSellerDashboard();

    /* show admin seller sign-out menu   */
    showAdminSellerMenu();

    /* check nubmer format on key up */
    numFormat();

    /* Checkbox all */
    fnCheckboxAll();

    /* Set height for sub mega - menu */
    setHeightSubMegaMenu();

    /* Submit form */
    submitOnEnter();

});

function showMegamenuOnHomepage() {
    jQuery(".cms-index-index #libe-menu-ver").addClass("show");
}

function scrollofftop() {
    jQuery(document).ready(function () {
        var offTop = jQuery('.header-middle');
        if (offTop.length) {
            var HeaderMiddle = offTop.offset().top + 100;
        }
        // var offTop = jQuery('.header-middle').offset().top;
        jQuery(window).on("scroll", function () {
            var scrollTop = jQuery(window).scrollTop();
            var header = jQuery(".header-middle");
            var msg = jQuery(".page.messages");
            var sellerCateList = jQuery("#seller-category-list");
            var limitScrollTop = 58;          //58 là height của #wrap_logo_header
            if (jQuery(".cms-index-index").length > 0) {            //nếu là trang chủ
                limitScrollTop = 401;
            }
            if (scrollTop > limitScrollTop) {
                header.addClass("scrolled-down");
                jQuery("#wrap_logo_header").slideUp();
                jQuery(".cms-index-index #libe-menu-ver").removeClass("show");          //mặc định hiển thị
                jQuery(".cms-index-index #libe-menu-ver").addClass("fixed");            //fix khi scroll xuống
                jQuery("#libe-menu-ver").css({ top: 83 });          //cách top 83px
            } else {                //chưa scroll xuống
                header.removeClass("scrolled-down");
                jQuery("#wrap_logo_header").slideDown();
                jQuery(".cms-index-index #libe-menu-ver").addClass("show");
                jQuery(".cms-index-index #libe-menu-ver").removeClass("fixed");
                jQuery("#libe-menu-ver").css({ top: 133 });          //cách top 83px
            }
        });
    });
}
/**************** dropdown menu ****************/
function hoverDropdown(){
    jQuery(".dropdown-cus-title, .dropdown-cus, .tooltip-cus").hover(
        function(){
            if(jQuery(this).parent().find(".tooltip-cus").length > 0){
                jQuery(this).parent().find(".tooltip-cus").show();
            } else {
                jQuery(this).parent().find(".dropdown-cus").show();
            }

        }, function(){
            if(jQuery(this).parent().find(".tooltip-cus").length > 0){
                jQuery(this).parent().find(".tooltip-cus").hide();
            } else {
                jQuery(this).parent().find(".dropdown-cus").hide();
            }
        }
    );
}

/* click for dropdown menu */
function clickDropdown() {
    jQuery(document).on("click", ".dropdown-cus-title-click", function () {
        if (jQuery(this).parent().find(".tooltip-cus").length > 0) {
            jQuery(this).toggleClass("active");
            jQuery(this).parent().find(".tooltip-cus").slideToggle();
        }
    });
}

/* combox menu for mobile */
function clickDropdownMobile() {
    jQuery(document).on("click", ".dropdown-cus-title-mobile", function () {
        if (jQuery(this).parent().find(".tooltip-cus-mobile").length > 0) {
            jQuery(this).parent().find(".tooltip-cus-mobile").slideToggle();
        }
    });
}


/**************** show/hide mega menu ****************/

function hoverMegamenu() {
    jQuery("#show-cate-btn, #libe-menu-ver").hover(function () {
        jQuery("#libe-menu-ver").show();
        jQuery("#black_background_cover").show();
    }, function () {
        jQuery("#libe-menu-ver").hide();
        jQuery("#black_background_cover").hide();
    });
}
    









/**************** popup ****************/
function fnShowPopup(popId){
    jQuery("#" + popId ).show();
}

function fnClosePopup(){
    jQuery(".layer-popup").hide();
}

function fnClosePopupOnEven(){
    jQuery(document).on("click",function(event){
        if(event.target.className.indexOf("layer-popup")>-1){
            fnClosePopup();
        }
    });

    jQuery(document).on("keydown",function(event){
        if(event.keyCode == 27){
            fnClosePopup();
        }
    });
}

/**************** tabs ****************/
function fnChangeTab(){
    jQuery(document).on("click",".tabs-cus .tab-cus",function(){
        var tabCntId = jQuery(this).parent().attr("tab-content-id");
        jQuery(this).parent().find(".tab-cus").removeClass("on");
        jQuery(this).addClass("on");
        var idx = jQuery(this).index();
        jQuery("#"+tabCntId).eq(0).find(".tab-cus-cnt").removeClass("on");
        jQuery("#"+tabCntId).eq(0).find(".tab-cus-cnt").eq(idx).addClass("on");
    });
}

/**************** collapse & expand ****************/
function fnCollapseExpand() {
    jQuery(document).on("click",".collapse-expands .collapse-expands-title",function(){
        jQuery(this).toggleClass("active");
        jQuery(this).parent().find(".collapse-expands-cnt").slideToggle();
    });
}

function fnShowSubmenuSellerDashboard(){
    jQuery(document).on("click",".active-child-menu",function(){
        var isActive = false;
        if (jQuery(this).parent().hasClass("active")){
            isActive = true;
        }
        jQuery(".active-child-menu").parent().removeClass("active");
        jQuery(".active-child-menu").next().slideUp();
        if (!isActive){
            jQuery(this).parent().addClass("active");
            if (jQuery(this).next().is(".nav.child_menu")) {
                jQuery(this).next().slideDown();
            }
        }
    });
}

function fnShowDropdown() {
    jQuery(document).on("click", ".dropdown-toggle", function () {
        var isActive = false;
        if (jQuery(this).hasClass("active")) {
            isActive = true;
        }
        jQuery(".dropdown-toggle").removeClass("active");
        jQuery(".dropdown-toggle").next().slideUp();
        if (!isActive) {
            jQuery(this).addClass("active");
            if (jQuery(this).next().is(".dropdown-menu")) {
                jQuery(this).next().slideDown();
            }
        }
    });
}

/**************** change qty input on checkout page ****************/
function fnQtyInputCheckout(){
    jQuery(document).on("click",'.quantity-minus-shoppingcart',function () {
        var input = jQuery(this).parent().find('.input-text.qty');
        var value = Number(input.val())-1;
        if(value > 0){
            input.val(value);
        }
    });

    jQuery(document).on("click",'.quantity-plus-shoppingcart',function () {
        var input = jQuery(this).parent().find('.input-text.qty');
        var value = Number(input.val())+1;
        if(value > 0){
            input.val(value);
        }
    });
}

/**************** slider ****************/
function activeSlider(){
    var clients_slider = jQuery(".clients-say-slider");
    clients_slider.owlCarousel({
        nav: true,
        autoplay: true,
        autoplayHoverPause: true,
        loop: true,
        responsive: {
            0: {items:1},
            480: {items:1},
            768: {items:1},
            991: {items:1},
            1200: {items:1}
        }
    });
}

function activeSellerSlider() {
    var clients_slider = jQuery(".seller-slider");
    clients_slider.owlCarousel({
        nav: true,
        dots: false,
        autoplay: true,
        autoplayHoverPause: true,
        loop: true,
        responsive: {
            0: { items: 1 },
            480: { items: 1 },
            768: { items: 1 },
            991: { items: 1 },
            1200: { items: 1 }
        }
    });
}

/**************** set full screen width ****************/
function setFullScreenWidth(){
    var w = jQuery(window).width();
    jQuery(".fullwidth").width(w);
    jQuery(".fullwidth-cnt").width(w);

    if(jQuery(".fullwidth").length>0){
        var marLeft = jQuery(".fullwidth > .container").css("margin-left");
        if(marLeft != "undifined" && marLeft.indexOf("px")>-1){
            marLeft = marLeft.replace(/px/g,"")*(-1);
            jQuery(".fullwidth").css({"margin-left":marLeft});
            jQuery(".fullwidth-cnt").css({"margin-left":marLeft});
        }
    }
}


/**************** upload file ****************/
function fnUploadFileBtn() {
    jQuery(document).on("click",".upload-file-btn",function(){
        jQuery(this).parent().find("input[type='file']").click();
    });
}

function fnGetUploadFileName() {
    jQuery(document).on("change","input[type='file']",function(){
        if (this.files[0]) {
            jQuery(this).parent().find(".file-name").html(this.files[0].name);
            jQuery(this).parent().find(".remove-file").remove();
            jQuery(this).parent().append("<button type='button' class='remove-file'>x</button>");
            jQuery(this).parent().find("div.mage-error").remove();
        } else {
            jQuery(this).parent().find(".file-name").text("");
        }

    });
}

function fnValidateUploadFile() {
    var isRightExtension;
    var isRightSize;
    jQuery("input[type='file']").each(function(i,e){
        var extensionArr = jQuery(this).attr("file-extension").toLowerCase().split(",");
        var file = this.files[0];
        var extensionChosen = file.name.split(".");
        extensionChosen = extensionChosen[extensionChosen.length-1].toLowerCase();
        isRightExtension = false;
        jQuery(extensionArr).each(function(i,e){
            if(extensionChosen.toLowerCase().trim() == this.trim()){
                isRightExtension = true;
                return false;
            }
        })

        if(!isRightExtension) {
            alert("File [" +file.name+ "] is not one of these extension: " + extensionArr);
            return false;
        }

        isRightSize = true;
        if (file.size > 2097152) {
            alert("File [" +file.name+ "] is over 2MB. Please choose another file");
            isRightSize = false;
            return false;
        }
    });
    return (isRightExtension && isRightSize);
}

function fnRemoveUploadFile() {
    jQuery(document).on("click", ".remove-file", function () {
        var file = jQuery(this).parent().find("input[type='file']");
        file.replaceWith(file.val(''));
        jQuery(this).parent().find(".file-name").empty();
        jQuery(this).remove();
    });
}


/**************** sticky menu ****************/
function fnStickyMenu(){

}

/**************** check show message ****************/


/**************** set content width for seller dashboard ****************/
function fnSetContentWidthForSellerDashboard(){
    var cnt = jQuery(".customer-seller-dashboard #maincontent .page-title-wrapper .wrap-seller-dashboard");
    if (cnt){
        var document_w = jQuery(document).width();
        var left_w = jQuery(".left-dashboard").width();
        jQuery(".right-dashboard").width(document_w - left_w);
    }
}

/**************** set active menu for seller dashboard ****************/
function fnSetActiveSellerMenu(active,current){
    if (current!= null) {
        jQuery(".main_menu_side .nav.side-menu > li").eq(active).addClass("active");
        jQuery(".main_menu_side .nav.side-menu > li").eq(active).find(".child_menu").show();
        jQuery(".main_menu_side .nav.side-menu > li").eq(active).find(".child_menu li").eq(current).addClass("current-page");
    } else {
        jQuery(".main_menu_side .nav.side-menu > li").eq(active).addClass("current-page");
    }
}

/* show submenu on seller dashboard  */
function showSubmenuSellerDashboard(){
    jQuery(document).on("click","#nav_seller li > a", function(){
        jQuery("#nav_seller li").removeClass("_active").removeClass("_show");
        jQuery(this).parent().addClass("_active").addClass("_show");
    });

    jQuery(document).on("click", "#nav_seller ._close", function() {
        jQuery("#nav_seller li").removeClass("_active").removeClass("_show");
    });
}


/* show admin seller sign-out menu   */
function showAdminSellerMenu() {
    jQuery(document).on("click", ".seller-admin-drop .admin__action-dropdown", function () {
        jQuery(this).toggleClass("active");
        if (jQuery(this).next().is(".admin__action-dropdown-menu")) {
            jQuery(this).next().toggle();
        }
    });
}

/**************** number input ****************/
/* check nubmer format on key up */
function numFormat() {
    jQuery(document).on("keyup",'input.number-check', function (event) {
        //jQuery('input.number-check').keyup(function (event) {
        // skip for arrow keys
        if (event.which >= 37 && event.which <= 40) return;

        // format number
        jQuery(this).val(function (index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                ;
        });
    });
}

/* check nubmer format on key up */
function numFormatOnKeyUp(event) {
    // skip for arrow keys
    if (event.which >= 37 && event.which <= 40) return;

    // format number
    jQuery(this).val(function (index, value) {
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            ;
    });
}

function numberOnly(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 47 && charCode < 58)
        return true;
    return false;
};

function numberFormat(value) {
    return value.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".") ;
};


function removeNumFormat(number) {
    while (number.toString().indexOf(".") > 0) { number = number.replace(".", ""); }
    while (number.toString().indexOf(",") > 0) { number = number.replace(",", ""); }
    return number;
}



/**************** Checkbox all ****************/
function fnCheckboxAll(){
    jQuery(".chkAll").click(function(){
        var isChecked = jQuery(this).is(":checked");
        if (!isChecked) {
            jQuery("input.chkChild").attr("checked",false);
        } else {
            jQuery("input.chkChild").attr("checked", true);
        }
    });
}


/**************** Set height for sub mega-menu ****************/
function setHeightSubMegaMenu(){
    //var height = jQuery("#libe-menu-ver").height();
    var height = 384.203;
    jQuery("#libe-menu-ver .sm-megamenu-child").css({"height":height});
}

/**************** Wishlist ****************/
function addToWishlist(url, product_id, qty=1) {
    jQuery("#cartpro_process").addClass("cartpro-show");
    var param = { "product_id": product_id, "qty":qty };
    jQuery.ajax({
        url: url,
        data: param,
        method: "post",
        dataType: 'json',
        success: function (res) {
            var json_result = JSON.parse(res);
            var msg = json_result.message;
            if (!json_result.error) {
                jQuery("#cartpro_process").removeClass("cartpro-show");
                //var new_qty = parseInt(jQuery("#wishlist_qty").text()) + parseInt(qty);
                jQuery("#wishlist_qty").text(json_result.qty);
            }
        },
        error: function (res) {
            var msg = 'Please Login to add wishlist';
            alert(msg);
            jQuery("#cartpro_process").removeClass("cartpro-show");
        }
    });
}


/**************** Submit form ****************/
function submitOnEnter() {
    jQuery(document).on("keydown", ".enter-submit", (event) => {
        if (event.keyCode == 13) {
            $(this).closest("form").submit();
        }
    });
}
