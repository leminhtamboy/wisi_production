/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
define([
    'jquery',
    'uiComponent',
    'underscore',
    'Magento_Customer/js/customer-data',
    'Magento_Ui/js/modal/alert',
    'jquery/jquery-storageapi'
], function ($, Component, _, customerData,confirmation,alert) {
    'use strict';
    return Component.extend({
        defaults: {
            cookieMessages: [],
            messages: []
        },
        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.cookieMessages = $.cookieStorage.get('mage-messages');
            this.messages = customerData.get('messages').extend({
                disposableCustomerData: 'messages'
            });
            var message_alert = this.cookieMessages;
            if(message_alert != null) {
                var length_message = message_alert.length;
                var i = 0;
                for (i = 0; i < length_message; i++) {
                    var messObject = message_alert[i];
                    if (typeof messObject === 'object') {
                        var type = messObject.type;
                        var text = messObject.text;
                        console.log(type);
                        if (text == "") {
                            continue;
                        }
                        this.alertCustomMethod(type, text);
                        //break;
                    }
                }
            }
            if (!_.isEmpty(this.messages().messages)) {
                customerData.set('messages', {});
            }
            $.cookieStorage.set('mage-messages', '');
        },
        alertCustomMethod:function(type_text,text_message){
            $('#mes').alert({
                title: type_text,
                content: text_message,
                actions: {
                    always: function(){}
                }
            });
            if(type_text == "success"){
                $(".modal-inner-wrap").addClass("success_custom");
            }else if (type_text == "error") {
                $(".modal-inner-wrap").addClass("error_custom");
            }else {
                $(".modal-inner-wrap").addClass("warrning_custom");
            }
        }
    });
});
