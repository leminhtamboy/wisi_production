<?php
namespace Libero\Api\Controller\Provider;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

class Get extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    //Manufacture


    public function getTextValueByIdAttribute($value,$attribute_code,$_product)
    {
        $optionId = $value;   
        $attr = $_product->getResource()->getAttribute($attribute_code);
        $optionText = '';
        if ($attr->usesSource()) {
            $optionText = $attr->getSource()->getOptionText($optionId);
        }
        return $optionText;
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();

        try {

            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $result_json = array();
            $modelProvider = $this->_objectManager->create('\Libero\Customer\Model\Seller');
            $collectionProvider = $modelProvider->getCollection();
            $result_items = array();
            foreach($collectionProvider as $_provider){
                $result_items[] = $_provider->getData();
            }
			$result_json["success"] = true;
			$result_json["code"] = 200;
			$result_json["message"] = "Have ".count($result_items)." Provider Found";
            $result_json["items"] = $result_items;
            $resultJson = $jsonResultFactory->setData($result_json);
            return $resultJson;
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
