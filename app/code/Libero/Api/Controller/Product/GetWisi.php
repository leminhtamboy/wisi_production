<?php
namespace Libero\Api\Controller\Product;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

class GetWisi extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    //Manufacture


    public function getTextValueByIdAttribute($value,$attribute_code,$_product)
    {
        $optionId = $value;   
        $attr = $_product->getResource()->getAttribute($attribute_code);
        $optionText = '';
        if ($attr->usesSource()) {
            $optionText = $attr->getSource()->getOptionText($optionId);
        }
        return $optionText;
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();

        try {

            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            //$id_provider = $this->getRequest()->getParam("id_provider");
            $id_reseller = $this->getRequest()->getParam('id_reseller');
            $modelProduct = $this->_objectManager->create("\Magento\Catalog\Model\Product");
            $modelCategory = $this->_objectManager->create('\Magento\Catalog\Model\Category');
            $stockItemRepository = $this->_objectManager->create('\Magento\CatalogInventory\Model\Stock\StockItemRepository');
            $stockStateInterface = $this->_objectManager->create('\Magento\CatalogInventory\Api\StockStateInterface');
			$result_json = array();
			if($id_reseller == "" || $id_reseller == null){
				$result_json["success"] = false;
				$result_json["code"] = 400;
				$result_json["items"] = array();
				$result_json["message"] = "Bad Request Missing Attribute";
				$resultJson = $jsonResultFactory->setData($result_json);
				return $resultJson;
			}
            //Get Detail product
            //$product = $modelProduct->load($id_seller,"id_seller");
            $sql_reseller_product = "SELECT * FROM `libero_reseller_products` WHERE id_reseller = '$id_reseller'";
            $resource = $this->_objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $data_reseller_product = $connection->fetchAll($sql_reseller_product);
            //Get Data
            $result_items = array();
            foreach($data_reseller_product as  $product){
                $id_product = $product["id_product"];
                $_product = $this->_objectManager->create("Magento\Catalog\Model\Product")->load($id_product);
                $value_manufacture = $_product->getData("manufacturer");
				$result = array();
				$result = $_product->getData();
				$result["category"] = "tempo";
				if ($value_manufacture) {
					$result["manufacturer"] = $this->getTextValueByIdAttribute($value_manufacture, "manufacturer", $_product);
				}
				$image = $_product->getData("image");
				if ($image) {
					$baseUrl = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface')
						->getStore()->getBaseUrl();
					$result["image"] = $baseUrl . 'media/catalog/product' . $image;
				}
				$result["quantity"] = $stockStateInterface->getStockQty($_product->getId());
				$result_items[] = $result;
            }
			$result_json["success"] = true;
			$result_json["code"] = 200;
			$result_json["message"] = "Have ".count($result_items)." Product Found";
            $result_json["items"] = $result_items;
            $resultJson = $jsonResultFactory->setData($result_json);
            return $resultJson;
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
