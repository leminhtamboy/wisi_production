<?php
namespace Libero\Api\Controller\Product;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

class TmsTakeProduct extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function getTextValueByIdAttribute($value,$attribute_code,$_product)
    {
        $optionId = $value;   
        $attr = $_product->getResource()->getAttribute($attribute_code);
        $optionText = '';
        if ($attr->usesSource()) {
            $optionText = $attr->getSource()->getOptionText($optionId);
        }
        return $optionText;
    }
    public function mysql_escape_mimic($inp)
	{
		if (is_array($inp))
			return array_map(__METHOD__, $inp);

		if (!empty($inp) && is_string($inp)) {
			return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
		} else {
			return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
		}
		return $inp;
	}
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        
        try {
            $request_body = file_get_contents('php://input');
            $post_data = json_decode($request_body,true);
            $products = $post_data['products'];
            $qtys = $post_data['qty'];
            $length = count($products);
            $result_json = array(
                "productsSuccess" => [],
                "productsNotEnough" => [],
                "productsNotExist" => []
            );
            for ($i = 0; $i < $length; $i++) {
                $updateResult = $this->updateProductStock($products[$i], $qtys[$i]);
                if ($updateResult["success"] == true) {
                    array_push($result_json["productsSuccess"], $updateResult["resultProduct"]);
                } else {
                    if ($updateResult["code"] == 1) {               //Remaining product is not enough
                        array_push($result_json["productsNotEnough"], $updateResult["resultProduct"]);
                    } else if ($updateResult["code"] == 2) {        //Product does not exist
                        array_push($result_json["productsNotExist"], $updateResult["resultProduct"]);
                    }
                }
            }
            $result_json["success"] = true;
            $result_json["code"] = 200;
            $resultJson = $jsonResultFactory->setData($result_json);
            return $resultJson;
			
        }catch (Exception $e){
            $result_json["success"] = false;
            $result_json["code"] = 400;
            $result_json["message"] = "Error Catch";
            $resultJson = $jsonResultFactory->setData($result_json);
            return $resultJson;
        }
    }

    /**
     * For Update stock of product
     * @param int $productId which stock you want to update
     * @param array $stockData your updated data
     * @return void 
    */
    public function updateProductStock($product,$qty) {
        $productId = $product["id"];
        $productName = $product["name"];
        $idx = $product["index"];
        $result_json["resultProduct"] = array(
            "id" => $productId,
            "name" => $productName,
            "index" => $idx
        );

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productModel = $objectManager->get('\Magento\Catalog\Model\Product');
        $stockRegistry = $objectManager->get('\Magento\CatalogInventory\Api\StockRegistryInterface');
        $stockItemRepository = $objectManager->create('\Magento\CatalogInventory\Model\Stock\StockItemRepository');
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();

        $sql = "SELECT * FROM wisi_production.catalog_product_entity WHERE entity_id = $productId;";
        $sql_result = $connection->fetchAll($sql);
        
        $isExisted = false;
        if (count($sql_result) > 0) {
            $isExisted = true;
        }

        if ($isExisted) {
            $product = $productModel->load($productId); //load product which you want to update stock
            $stockItem = $stockRegistry->getStockItem($productId); // load stock of that product
            $curQty = $stockItem->getQty();
            $newQty = $curQty - $qty;
            
            if ($newQty < 0) {
                $result_json["success"] = false;
                $result_json["code"] = 1;       //Remaining product is not enough
                return $result_json;
            }
            $is_in_stock = 1;
            if ($newQty == 0) {
                $is_in_stock = 0;
            }
            
            $stockItem->setData('is_in_stock',$is_in_stock); //set updated data as your requirement
            $stockItem->setData('qty',$newQty); //set updated quantity 
            $stockItem->setData('manage_stock',1);
            $stockItem->setData('use_config_notify_stock_qty',1);
            $stockItem->save(); //save stock of item
            $product->save(); //  also save product
            $result_json["success"] = true;
            $result_json["code"] = 0;       //Get product successfully
            return $result_json;
        } else {
            $result_json["success"] = false;
            $result_json["code"] = 2;               //Product does not exist
            return $result_json;
        }
    }
}


