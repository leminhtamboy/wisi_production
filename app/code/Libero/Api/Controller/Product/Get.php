<?php
namespace Libero\Api\Controller\Product;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

class Get extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    //Manufacture


    public function getTextValueByIdAttribute($value,$attribute_code,$_product)
    {
        $optionId = $value;   
        $attr = $_product->getResource()->getAttribute($attribute_code);
        $optionText = '';
        if ($attr->usesSource()) {
            $optionText = $attr->getSource()->getOptionText($optionId);
        }
        return $optionText;
    }
    public function mysql_escape_mimic($inp)
	{
		if (is_array($inp))
			return array_map(__METHOD__, $inp);

		if (!empty($inp) && is_string($inp)) {
			return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
		} else {
			return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
		}
		return $inp;
	}
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        try {
            //$id_provider = $this->getRequest()->getParam("id_provider");
            //$post_data = $this->getRequest()->getPostValue();
            $request_body = file_get_contents('php://input');
            $post_data = json_decode($request_body,true);
            $list_catIds = $post_data['fil_cat'];
            $id_provider = $post_data['id_provider'];
            $name_search = $post_data["name_search"];
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $modelProduct = $this->_objectManager->create("\Magento\Catalog\Model\Product");
            $modelCategory = $this->_objectManager->create('\Magento\Catalog\Model\Category');
            $stockItemRepository = $this->_objectManager->create('\Magento\CatalogInventory\Model\Stock\StockItemRepository');
            $stockStateInterface = $this->_objectManager->create('\Magento\CatalogInventory\Api\StockStateInterface');
			if($list_catIds == "" && $id_provider == "" && $name_search == ""){
				$result_json["success"] = false;
				$result_json["code"] = 400;
				$result_json["items"] = array();
				$result_json["message"] = "Bad Request Missing Attribute";
				$resultJson = $jsonResultFactory->setData($result_json);
				return $resultJson;
            }
            //Get Detail product
            //$product = $modelProduct->load($id_seller,"id_seller");
            $products = null;
            if($name_search != ""){
                $products = $modelProduct->getCollection()
                ->addAttributeToSelect("*")
                ->addAttributeToFilter("status",array("eq" => 1))
                ->addAttributeToFilter("visibility",array("eq" => 4))
                ->addAttributeToFilter("name",array("like" => "%".$name_search."%"));
            }else{
                $products = $modelProduct->getCollection()->addAttributeToSelect("*")->addAttributeToFilter("status",array("eq" => 1))
                ->addAttributeToFilter("visibility",array("eq" => 4));
            }
            if($list_catIds != ""){
                $joinConditions = "e.entity_id  = catalog_category_product.product_id";    
                $products->getSelect()->join(
                    ['catalog_category_product'],
                    $joinConditions,
                    []
                    )->columns("catalog_category_product.category_id")->where("catalog_category_product.category_id  IN (".$list_catIds.")");
            }
            if($id_provider != ""){
                $joinConditionsAttribute = "e.entity_id  = catalog_product_entity_varchar.entity_id";
                $products->getSelect()->join(
                    ['catalog_product_entity_varchar'],
                    $joinConditionsAttribute,
                    []
                    )->columns("catalog_product_entity_varchar.value")->where("catalog_product_entity_varchar.attribute_id = '156' AND catalog_product_entity_varchar.value IN (".$id_provider.")");
            }
            /*if($name_search != ""){
                $name_search = $this->mysql_escape_mimic($name_search);
                $joinConditionsAttribute = "e.entity_id  = catalog_product_entity_varchar.entity_id";
                $products->getSelect()->join(
                    ['catalog_product_entity_varchar'],
                    $joinConditionsAttribute,
                    []
                    )->columns("catalog_product_entity_varchar.value")->where("catalog_product_entity_varchar.attribute_id = '73' AND catalog_product_entity_varchar.value LIKE '%".$name_search."%'");
            }*/          
            /*if($list_catIds != "" && $id_provider != ""){
                $products = $modelProduct->getCollection()->addAttributeToSelect("*");
                

                //$products = $products->addAttributeToFilter("id_seller",array("eq" => $id_provider));
            }
            if($list_catIds != "" && $id_provider == ""){
                $products->getSelect()->join(
                    ['catalog_category_product'],
                    $joinConditions,
                    []
                    )->columns("catalog_category_product.category_id")->where("catalog_category_product.category_id  IN (".$list_catIds.")"); 
            }
            if($id_provider != "" && $list_catIds == ""){
                $products = $products->addAttributeToFilter("id_seller",array("eq" => $id_provider));
            }*/
            //Get Data
            $result_items = array();
            foreach($products as $index => $_product){
                $stockRegistry = $objectManager->create("\Magento\CatalogInventory\Api\StockRegistryInterface");
                $stockItem = $stockRegistry->getStockItem($_product->getId());
                $value_manufacture = $_product->getData("manufacturer");
                $seller_id = $_product->getData("id_seller");
                $sql = "select store_name from libero_customer_seller_company where `id_seller_company` = '$seller_id'";
                $data_seller = $connection->fetchAll($sql);
                $seller_name = "No Provider";
                if(count($data_seller) > 0){
                    $seller_name = $data_seller[0]["store_name"];
                }
				$result = array();
				$result = $_product->getData();
				$result["category"] = "tempo";
				if ($value_manufacture) {
					$result["manufacturer"] = $this->getTextValueByIdAttribute($value_manufacture, "manufacturer", $_product);
				}
				$image = $_product->getData("image");
				if ($image) {
					$baseUrl = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface')
						->getStore()->getBaseUrl();
					$result["image"] = $baseUrl . 'media/catalog/product' . $image;
                }
                $sepcial_price = str_replace(".","",$_product->getSpecialPrice());
                $sepcial_price = str_replace(",","",$_product->getSpecialPrice());
                $result["special_price"] = $sepcial_price;
                $result["quantity"] = $stockStateInterface->getStockQty($_product->getId());
                $min_sale_qty = $stockItem->getData("min_sale_qty");
                $result["min_quantity"] = $min_sale_qty;
                $result["provider_name"] = $seller_name;
				$result["product_url"] = $_product->getProductUrl();
				$result_items[] = $result;
            }
			$result_json["success"] = true;
			$result_json["code"] = 200;
			$result_json["message"] = "Have ".count($result_items)." Product Found";
            $result_json["items"] = $result_items;
            $resultJson = $jsonResultFactory->setData($result_json);
            return $resultJson;
        }catch (Exception $e){
            $result_json["success"] = false;
            $result_json["code"] = 400;
            $result_json["items"] = array();
            $result_json["message"] = "Error Catch";
            $resultJson = $jsonResultFactory->setData($result_json);
            return $resultJson;
        }
    }
}
