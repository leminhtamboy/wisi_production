<?php
namespace Libero\Cron\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
class InstallSchema implements  InstallSchemaInterface{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        //Do something
        $setup->getConnection();
        $setup->run("CREATE TABLE `wisi_production`.`libero_cron_execute_background` ( `id_cron` INT NOT NULL AUTO_INCREMENT , `type_cron` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `cron_code` VARCHAR(550) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `content_excute` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `created_at` TIMESTAMP NOT NULL , `status` INT(10) NOT NULL , `etc` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_cron`)) ENGINE = InnoDB;");
        $setup->endSetup();
    }
}
