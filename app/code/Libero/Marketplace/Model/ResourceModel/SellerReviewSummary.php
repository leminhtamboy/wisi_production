<?php
namespace Libero\Marketplace\Model\ResourceModel;


class SellerReviewSummary extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
	
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('seller_review_summary', 'id');
	}
	
}
