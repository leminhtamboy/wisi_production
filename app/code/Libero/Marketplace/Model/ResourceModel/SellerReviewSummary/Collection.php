<?php
namespace Libero\Marketplace\Model\ResourceModel\SellerReviewSummary;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'kd_seller_review_sellerreviewsummary_collection';
	protected $_eventObject = 'sellerreviewsummary_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Libero\Marketplace\Model\SellerReviewSummary', 'Libero\Marketplace\Model\ResourceModel\SellerReviewSummary');
	}

}
