<?php
namespace Libero\Marketplace\Model\ResourceModel\SellerReview;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
	protected $_idFieldName = 'id';
	protected $_eventPrefix = 'kd_seller_review_sellerreview_collection';
	protected $_eventObject = 'sellerreview_collection';

	/**
	 * Define resource model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Libero\Marketplace\Model\SellerReview', 'Libero\Marketplace\Model\ResourceModel\SellerReview');
	}

}
