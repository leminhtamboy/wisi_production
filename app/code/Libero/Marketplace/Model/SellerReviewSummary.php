<?php
namespace Libero\Marketplace\Model;
class SellerReviewSummary extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'kd_seller_review_post';

	protected $_cacheTag = 'kd_seller_review_post';

	protected $_eventPrefix = 'kd_seller_review_post';

	protected function _construct()
	{
		$this->_init('Libero\Marketplace\Model\ResourceModel\SellerReviewSummary');
	}

	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}

	public function getDefaultValues()
	{
		$values = [];

		return $values;
	}
}
