<?php
namespace Libero\Marketplace\Block;

use Magento\Framework\View\Element\Template;

class StoreProduct extends \Sm\BasicProducts\Block\BasicProducts{
    protected $_defaultToolbarBlock = 'Magento\Catalog\Block\Product\ProductList\Toolbar';
    
    public function getCategoryIds($id_seller){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resourceConnection = $objectManager->create("\Magento\Framework\App\ResourceConnection");    
        $connection = $resourceConnection->getConnection();
        /*
        $id_seller_code_sql = "SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'id_seller';";
        $id_seller_code = $connection->fetchAll($id_seller_code_sql)[0]["attribute_id"];

        $category_of_seller_code_sql = "SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'category_of_seller';";
        $category_of_seller_code = $connection->fetchAll($category_of_seller_code_sql)[0]["attribute_id"];

        $sql = "
            SELECT 
                value as category_id
            FROM
                (SELECT 
                    entity_id
                FROM
                    catalog_product_entity_varchar
                WHERE
                    attribute_id = $id_seller_code AND value = $id_seller
                GROUP BY entity_id) AS a
                    JOIN
                catalog_product_entity_varchar AS b ON b.entity_id IN (a.entity_id)
            WHERE
                b.attribute_id = $category_of_seller_code
            GROUP BY category_id
            ;
        ";
        $result = $connection->fetchAll($sql);*/
        $products = $objectManager->get("\Magento\Catalog\Model\Product")
                    ->getCollection()
                    ->addAttributeToFilter("id_seller",array("eq" => $id_seller));
        $arrayProducts = "";
        foreach($products as $_pro){
            $arrayProducts .= $_pro->getId().",";
        }
        $arrayProducts = substr($arrayProducts,0,strlen($arrayProducts) - 1);
        $sql_product_category = "";
        if($arrayProducts != "") {
            $sql_product_category = "SELECT DISTINCT category_id FROM catalog_category_product WHERE product_id in ($arrayProducts)";
            $result_category = $connection->fetchAll($sql_product_category);
            $categoryIds = array();
            foreach ($result_category as $_cate) {
                $id_cate = $_cate["category_id"];
                $category = $this->getDetailCategory($id_cate);
                if (count($category->getChildrenCategories()) < 1) {
                    $categoryIds[] = $id_cate;
                }
            }
            return $categoryIds;
        }else{
            $sql_product_category = "SELECT  id_category FROM libero_customer_seller_company WHERE id_seller_company = $id_seller";
            $result_category = $connection->fetchAll($sql_product_category);
            $string_id_category = $result_category[0]["id_category"];
            $categoryIds = explode("|",$string_id_category);
            return $categoryIds;
        }
    }

    public function getCategoryIdsIgnoreProduct($id_seller){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resourceConnection = $objectManager->create("\Magento\Framework\App\ResourceConnection");    
        $connection = $resourceConnection->getConnection();
        $sql_category = "SELECT id_category FROM libero_customer_seller_company WHERE id_seller_company = $id_seller;";
        $result_category = $connection->fetchAll($sql_category);
        if(count($result_category) > 0){
            $categoryArr = explode("|", $result_category[0]["id_category"]);
            $categoryIds = array();
            foreach($categoryArr as $id) {
                array_push($categoryIds, $id);
            }
            return $categoryIds;
        }else{
            return array();
        }
    }

    public function getProviderProducts($id_seller){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resourceConnection = $objectManager->create("\Magento\Framework\App\ResourceConnection"); 
        $requestHttp = $objectManager->create("\Magento\Framework\App\Request\Http");
        $util = $objectManager->create("\Libero\Customer\Block\Seller\Admin\Util");

        $pageSize = 30;
        $curPage= 1;
        $product_list_mode = "grid";
        $order_by = "e.position";
        $desc = "ASC";
        if($requestHttp->getParam("product_list_mode")) {
            $product_list_mode = $requestHttp->getParam("product_list_mode");
        }
        
        if($requestHttp->getParam("product_list_limit")) {
            $pageSize = $requestHttp->getParam("product_list_limit");
        }

        if($requestHttp->getParam("page")) {
            $curPage = $requestHttp->getParam("page");
        }
        if($requestHttp->getParam("product_list_order")){
            $order_by  = $requestHttp->getParam("product_list_order");
        }
        if($requestHttp->getParam("product_list_dir")){
            $desc  = $requestHttp->getParam("product_list_dir");
        }
        $productDependency = $objectManager->create("Magento\Catalog\Model\Product");
        $connection = $resourceConnection->getConnection();
        $products = $objectManager->get("\Magento\Catalog\Model\Product")
                    ->getCollection()
                    ->addAttributeToSelect("*")
                    ->addAttributeToFilter("id_seller",array("eq" => $id_seller))
                    ->addAttributeToFilter("visibility",array("eq" => 4))
                    ->addAttributeToFilter("status",array("eq" => 1));
        $total = count($products->getData());
        $lastPage = $util->getLastPageByTotal($total,$pageSize);
        $firstNumber = $util->getFirstNumArray();
        $curPage = ($util->getCurPageArray() < $lastPage) ? $util->getCurPageArray() : $lastPage;
        $curPage = ($curPage > 0) ? $curPage : 1;
        $products = $products->setPageSize($pageSize)->setCurPage($curPage);
        $products->setOrder($order_by,$desc);
        $result = array(
            "id_seller" => $id_seller,
            "total" => $total,
            "pageSize" => $pageSize,
            "curPage" => $curPage,
            "lastPage" => $lastPage,
            "listRecords" => $products,
            "firstNumber" => $firstNumber,
            "product_list_mode" => $product_list_mode
        );
        return $result;
    }

    /**
     * Get product reviews summary
     *
     * @param \Magento\Catalog\Model\Product $product
     * @param bool $templateType
     * @param bool $displayIfNoReviews
     * @return string
     */
    public function getReviewsSummaryHtml(
        \Magento\Catalog\Model\Product $product,
        $templateType = false,
        $displayIfNoReviews = false
    ) {
        return $this->reviewRenderer->getReviewsSummaryHtml($product, $templateType, $displayIfNoReviews);
    }

    /**
     * Specifies that price rendering should be done for the list of products
     * i.e. rendering happens in the scope of product list, but not single product
     *
     * @return \Magento\Framework\Pricing\Render
     */
    protected function getPriceRender(){
        return $this->getLayout()
                    ->getBlock('product.price.render.default')
                    ->setData('is_product_list', true);
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return string
     */
    public function getProductPrice(\Magento\Catalog\Model\Product $product){
        $priceRender = $this->getPriceRender();
        $price = '';
        if ($priceRender) {
            $price = $priceRender->render(
                \Magento\Catalog\Pricing\Price\FinalPrice::PRICE_CODE,
                $product,
                [
                    'include_container' => true,
                    'display_minimal_price' => true,
                    'zone' => \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
                    'list_category_page' => true
                ]
            );
        }

        return $price;
    }

    /**
     * Retrieve product details html
     *
     * @param \Magento\Catalog\Model\Product $product
     * @return mixed
     */
    public function getProductDetailsHtml(\Magento\Catalog\Model\Product $product)
    {
        $renderer = $this->getDetailsRenderer($product->getTypeId());
        if ($renderer) {
            $renderer->setProduct($product);
            return $renderer->toHtml();
        }
        return '';
    }
};