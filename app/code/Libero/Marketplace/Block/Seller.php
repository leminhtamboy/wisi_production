<?php
namespace Libero\Marketplace\Block;

use Magento\Framework\View\Element\Template;

class Seller extends \Magento\Framework\View\Element\Template{


    //Using dependen injecttion class

    protected $layoutProcessors;

    protected $_breadcrums;

    public function __construct(
        Template\Context $context,
        array $layoutProcessors = [],
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->jsLayout = isset($data['jsLayout']) && is_array($data['jsLayout']) ? $data['jsLayout'] : [];
        $this->layoutProcessors = $layoutProcessors;
        
    }

    public function getJsLayout()
    {
        foreach ($this->layoutProcessors as $processor) {
            $this->jsLayout = $processor->process($this->jsLayout);
        }
        return \Zend_Json::encode($this->jsLayout);
    }
    public function getBreadcrums(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $blockBreadcrums = $objectManager->get('\Magento\Theme\Block\Html\Breadcrumbs');
        $arrayCrumsHome = ["link" => $this->getUrl() , "label" => __("Home")];
        $arrayCrumsMarketplace = ["link" => "" , "label" => __("Marketplace") , 'last' => true];
        $blockBreadcrums->addCrumb("home-page",$arrayCrumsHome)->addCrumb("market-place",$arrayCrumsMarketplace);
        return $blockBreadcrums->toHtml();
    }
    public function getSellerCollection()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelCustomer = $objectManager->create('Libero\Customer\Model\CustomerSellerStore');
        $collection = $modelCustomer->getCollection();
        return $collection;
    }

    public function filterSellerByCategory($id_category){

    }

}