<?php
namespace Libero\Marketplace\Block;

use Magento\Framework\View\Element\Template;
class TopInfo extends \Magento\Theme\Block\Html\Header\Logo{

    public function getBreadcrums(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $blockBreadcrums = $objectManager->get('\Magento\Theme\Block\Html\Breadcrumbs');
        $arrayCrumsHome = ["link" => $this->getUrl() , "label" => __("Home")];
        $arrayCrumsMarketplace = ["link" => "" , "label" => __("Marketplace") , 'last' => true];
        $blockBreadcrums->addCrumb("home-page",$arrayCrumsHome)->addCrumb("market-place",$arrayCrumsMarketplace);
        return $blockBreadcrums->toHtml();
    }
    public function getSellerCollection()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelCustomer = $objectManager->create('Libero\Customer\Model\CustomerSellerStore');
        $collection = $modelCustomer->getCollection();
        return $collection;
    }
    public function getSellerDetail()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $this->_objectManager->get("\Magento\Framework\App\Request\Http");
        $id_seller = $injectHTTP->getParam("id_seller");
        $injectSeller = $this->_objectManager->create("Libero\Customer\Model\CustomerSeller");
        $dataSeller = $injectSeller->load($id_seller);
        return $dataSeller; 
    }
    public function getSellerDetailByIdSeller($id_seller)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectSeller = $this->_objectManager->create("Libero\Customer\Model\CustomerSeller");
        $dataSeller = $injectSeller->load($id_seller);
        return $dataSeller; 
    }

    public function getReviewAction(){
        return $this->getUrl('marketplace/store/review');
    }

    public function getSellerReviewSummary() {
        $result = array('review_count' => 0, 'rating_count' => 0,'percent' => 0);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $objectManager->get("\Magento\Framework\App\Request\Http");
        $id_seller = $injectHTTP->getParam("id_seller");

        $sellerSummaryModel = $objectManager->create("Libero\Marketplace\Model\SellerReviewSummary");
        $sellerSummary = $sellerSummaryModel->load($id_seller,"seller_id");
        
        $review_count = $sellerSummary->getData("review_count");
        $rating_count = $sellerSummary->getData("rating_count");
        
        $sellerReviewModel = $objectManager->create("Libero\Marketplace\Model\SellerReview");
        $sellerReview = $sellerReviewModel->getCollection()->addFieldToFilter("seller_id",array("eq" => $id_seller));
        $rating_1 = 0;
        $rating_2 = 0;
        $rating_3 = 0;
        $rating_4 = 0;
        $rating_5 = 0;
        foreach($sellerReview as $_review) {
            switch ($_review["rating"]) {
                case "1": $rating_1++; break;
                case "2": $rating_2++; break;
                case "3": $rating_3++; break;
                case "4": $rating_4++; break;
                case "5": $rating_5++; break;
                default: break;
            }
        }

        $percent = $rating_count/($review_count*5)*100;

        if ($review_count != "" && $rating_count != "") {
            $result['review_count'] = $review_count;
            $result['rating_count'] = $rating_count;
            $result['percent'] = $percent;
            $result["1"] = $rating_1;
            $result["2"] = $rating_2;
            $result["3"] = $rating_3;
            $result["4"] = $rating_4;
            $result["5"] = $rating_5;
            $result["avg"] = round($percent/20,1);
        }
        return $result;
    }
    public function getSellerReviewSummaryByProviderId($provider_id) {
        $result = array('review_count' => 0, 'rating_count' => 0,'percent' => 0);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $id_seller = $provider_id;
        $sellerSummaryModel = $objectManager->create("Libero\Marketplace\Model\SellerReviewSummary");
        $sellerSummary = $sellerSummaryModel->load($id_seller,"seller_id");
        $review_count = $sellerSummary->getData("review_count");
        $rating_count = $sellerSummary->getData("rating_count");
        
        if ($review_count != "" && $rating_count != "") {
            $result['review_count'] = $review_count;
            $result['rating_count'] = $rating_count;
            $result['percent'] = $rating_count/($review_count*5)*100;
        }
        return $result;
    }
    public function getSellerReviewList(){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $objectManager->get("\Magento\Framework\App\Request\Http");
        $id_seller = $injectHTTP->getParam("id_seller");
        $sellerReviewModel = $objectManager->create("Libero\Marketplace\Model\SellerReview");
        $sellerReviewList = $sellerReviewModel->getCollection()
                                            ->addFieldToFilter("seller_id",array("eq" => $id_seller));

        return $sellerReviewList;
    }   
};