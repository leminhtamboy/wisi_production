<?php 

namespace Libero\Marketplace\Setup;


use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade( SchemaSetupInterface $setup, ModuleContextInterface $context )
    {
        $installer = $setup;
        $installer->startSetup();

        if(version_compare($context->getVersion(), '2.0.1', '<')) {

            if (!$installer->tableExists('seller_review')) {

                $table = $installer->getConnection()->newTable(
                    $installer->getTable('seller_review')
                )->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity'=>true,'unsigned'=>true,'nullable'=>false,'primary'=>true]
                )
                ->addColumn(
                    'customer_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    11,
                    ['nullbale'=>true,'default'=>null]
                )
                ->addColumn(
                    'seller_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    11,
                    ['nullbale'=>false]
                )
                ->addColumn(
                    'nickname',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable'=>false,'default'=>'']
                )
                ->addColumn(
                    'title',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullbale'=>false,'default'=>'']
                )
                ->addColumn(
                    'detail',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    '2M',
                    ['nullbale'=>false,'default'=>'']
                )
                ->addColumn(
                    'rating',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    '4',
                    ['nullbale'=>false]
                )
                ->addColumn(
                    'approved',
                    \Magento\Framework\DB\Ddl\Table::TYPE_BOOLEAN,
                    null,
                    ['nullbale'=>false,'default'=>false]
                )
                ->addColumn(
                    'created_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false, 
                        'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                    ]
                )
                ->addColumn(
                    'updated_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false, 
                        'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                    ]
                )                            
                ->setComment('review of customer for seller')
                ->setOption('charset','utf8');

                $installer->getConnection()->createTable($table);
            }
        }

        if(version_compare($context->getVersion(), '2.0.2', '<')) {

            if (!$installer->tableExists('seller_review_summary')) {

                $table = $installer->getConnection()->newTable(
                    $installer->getTable('seller_review_summary')
                )->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity'=>true,'unsigned'=>true,'nullable'=>false,'primary'=>true]
                )
                ->addColumn(
                    'seller_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    11,
                    ['nullbale'=>false]
                )
                ->addColumn(
                    'review_count',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    11,
                    ['nullbale'=>false]
                )
                ->addColumn(
                    'rating_count',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    11,
                    ['nullable'=>false]
                )
                ->addColumn(
                    'created_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false, 
                        'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                    ]
                )
                ->addColumn(
                    'updated_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false, 
                        'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
                    ]
                )                            
                ->setComment('review summary of customer for seller')
                ->setOption('charset','utf8');

                $installer->getConnection()->createTable($table);
            }
        }

        if(version_compare($context->getVersion(),'2.0.3') < 0)
        {
            $setup->run("ALTER TABLE `seller_review` DROP COLUMN `updated_at`, DROP COLUMN `created_at`;");
            $setup->run("ALTER TABLE `seller_review` ADD COLUMN `created_at` VARCHAR(255) NOT NULL AFTER `approved`, ADD COLUMN `updated_at` VARCHAR(255) NOT NULL AFTER `created_at`;");
        }

        $installer->endSetup();
    }
}
