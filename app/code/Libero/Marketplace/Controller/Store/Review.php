<?php
namespace Libero\Marketplace\Controller\Store;

class Review extends \Magento\Framework\App\Action\Action
{
    protected $_session;
    protected $_sellerReviewFactory;
    protected $_sellerReviewSummaryFactory;
    protected $_resultJsonFactory;
    protected $_saveTransaction;
    protected $_logger;
    protected $_objectManager;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $session,
        \Libero\Marketplace\Model\SellerReviewFactory $sellerReviewFactory,
        \Libero\Marketplace\Model\SellerReviewSummaryFactory $sellerReviewSummaryFactory,
        \Magento\Framework\DB\TransactionFactory $transactionFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    )
    {
        $this->_session = $session;
        $this->_sellerReviewFactory = $sellerReviewFactory;
        $this->_sellerReviewSummaryFactory = $sellerReviewSummaryFactory;
        $this->_saveTransaction = $transactionFactory;
        $this->_logger = $logger;
        $this->_resultJsonFactory = $resultJsonFactory;
        return parent::__construct($context);
    }
 
    /*
    * save customer review for seller
    * return Json object
    */
    public function execute()
    {
        $result = array('status' => true);
        if ($this->_session->isLoggedIn()) {
             // Customer is logged in 
            if($this->getRequest()->isAjax()) {
                $request = $this->getRequest()->getPostValue();
                $validData = $this->validateDataReview($request);
                if ($validData) {
                    $sellerReviewModel = $this->_sellerReviewFactory->create();
                    
                    date_default_timezone_set("Asia/Bangkok");
                    $curDateTime = date("Y-m-d H:i:s");

                    $customer_id = $this->_session->getCustomerId();
                    if($customer_id != NULL){
                        $sellerReviewModel->setData('customer_id',$customer_id);
                    }
                    $review_rating = isset($request['rating'])? $request['rating'] : 1;
                    $sellerReviewModel->setData('seller_id',$request['seller_id']);
                    $sellerReviewModel->setData('nickname',$request['nickname']);
                    $sellerReviewModel->setData('title',$request['title']);
                    $sellerReviewModel->setData('detail',$request['detail']);
                    $sellerReviewModel->setData('rating', $review_rating);
                    $sellerReviewModel->setData('created_at', $curDateTime);
                    $sellerReviewModel->setData('updated_at', $curDateTime);

                    $sellerReviewSummaryModel = $this->_sellerReviewSummaryFactory->create();
                    $dataSellerReviewSummary = $sellerReviewSummaryModel->getCollection()->addFieldToFilter("seller_id",array("eq" => $request['seller_id']))->getFirstItem();

                    if($dataSellerReviewSummary->getId()){

                        //update summary
                        $sellerReviewSummaryModel = $this->_sellerReviewSummaryFactory->create()->load($dataSellerReviewSummary->getId());
                        $cur_review_count = $dataSellerReviewSummary->getReviewCount();
                        $cur_rating_count = $dataSellerReviewSummary->getRatingCount();
                        $sellerReviewSummaryModel->setData('seller_id',$request['seller_id']);
                        $sellerReviewSummaryModel->setData('review_count',($cur_review_count+1));
                        $sellerReviewSummaryModel->setData('rating_count',($cur_rating_count+$review_rating));

                    }else{

                        //create new summary
                        $sellerReviewSummaryModel->setData('seller_id',$request['seller_id']);
                        $sellerReviewSummaryModel->setData('review_count',1);
                        $sellerReviewSummaryModel->setData('rating_count',$review_rating);
                    }

                    //save data
                    $transaction = $this->_saveTransaction->create();
                    
                    $transaction->addObject($sellerReviewModel);
                    $transaction->addObject($sellerReviewSummaryModel);
                    
                    $transaction->save();

                    $sellerSummaryModel = $this->_objectManager->create("Libero\Marketplace\Model\SellerReviewSummary");
                    $sellerSummary = $sellerSummaryModel->load($request['seller_id'],"seller_id");
                    $review_count = $sellerSummary->getData("review_count");
                    $rating_count = $sellerSummary->getData("rating_count");
                    $percent = 0;
                    if ($review_count != "" && $rating_count != "") {
                        $percent = round($rating_count/($review_count*5)*100);
                    }

                    //get model seller obejct

                    $modelSeller = $this->_objectManager->create("\Libero\Customer\Model\CustomerSeller")->load($request['seller_id']);
                    $modelSeller->setData("rating_sumary",$percent);
                    $modelSeller->save();
                    
                    $result['msg'] = __('Review successful');
                }else{
                    $result['status'] = false;
                    $result['msg'] = __('Review error, You cannot review this seller this time');
                }
            }
        } else {
            // Customer is not logged in, return error
            $result['status'] = false;
            $result['msg'] = __('Please login to write review');
        }
       
        $resultJson = $this->_resultJsonFactory->create();
        $resultJson->setData($result);
        return $resultJson;
       
    }

    public function validateDataReview($request = array())
    {
        $result = true;

        if($request['seller_id'] == NULL || $request['seller_id'] == ''){
            $result = false;
            $this->_logger->debug('Save review seller ID not valid, refuse save'); 
        }
        return $result;
    }
}
