<?php
namespace Libero\Marketplace\Controller\Store;
use Magento\Framework\App\Action\Context;

class Store extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $topInfo = $objectManager->create("\Libero\Marketplace\Block\TopInfo");
        $detail_seller = $topInfo->getSellerDetail();
        $resultPage->getConfig()->getTitle()->set(__($detail_seller["store_name"]));
        

        return $resultPage;
    }
}
