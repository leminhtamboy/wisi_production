<?php
namespace Libero\Marketplace\Controller\Store;

use Magento\Framework\App\Action\Context;

class Get extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    protected $_urlBuilder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
        $this->_urlBuilder = $context->getUrl();
    }
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelCustomer = $objectManager->create('Libero\Customer\Model\CustomerSeller');
        $collection = $modelCustomer->getCollection()->addFieldToFilter("status", array("eq" => "1"));
        $array_store = array();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $url = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $front_url = $url."marketplace/store/store/id_seller/";
        $media_url = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        $logo_url = "";
        foreach($collection as $_store){
            $url = $front_url."".$_store->getData("id_seller_company");
            $logo_url = $_store->getData("logo_url");
            $banner_url = $_store->getData("banner_url1");
            $array_item = array();
            $array_item["store_url"] = $url;
            $array_item["logo_url"] = $media_url.$logo_url;
            $array_item["provider_name"] = $_store->getData("store_name");
            $array_item["banner_url"] = $media_url.$banner_url;
            $array_store[] = $array_item;
        }
        //$stores = array("stores_seller" => $array_store);
        echo json_encode($array_store);
        die;
    }
}
