define([
    'ko',
    'uiComponent',
    'mage/url',
    'mage/storage'
], function (ko, Component, urlBuilder,storage,jquery) {
    'use strict';
    return Component.extend({
        initialize: function () {
                this._super();
                this.getStoreSellers();
                return this;
            },
        defaults: {
            template: 'Libero_Marketplace/seller_store',
        },

        stores: ko.observableArray([]),
/*
        callSeller: function(){
            alert("dasdsadas");
        },
*/
        getStoreSellers: function(dataToPass){
            var self = this;
            var serviceUrl = urlBuilder.build('marketplace/store/get');
            return storage.post(
                serviceUrl,
                JSON.stringify(dataToPass),
                true
            ).done(
                function (response) {
                    var result  = JSON.parse(response);
                    for(var i = 0 ; i < result.length ; i ++){
                        var item = {
                            link:result[i].link,
                            link_image:result[i].link_image,
                            biz_type:result[i].biz_type 
                        };
                        self.stores.push(item);
                    }
                }
            ).fail(
                function (response) {
                    alert(response);
                }
            );
        },

    });
});