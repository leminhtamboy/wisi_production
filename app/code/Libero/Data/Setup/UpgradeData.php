<?php 

namespace Libero\Data\Setup;

use Magento\Catalog\Api\Data\ProductAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Model\Category;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

class UpgradeData implements UpgradeDataInterface
{
    private $eavSetupFactory;
    private $attributeSetFactory;
    private $attributeSet;
    private $categorySetupFactory;

    public function __construct(EavSetupFactory $eavSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory; 
        /* assign object to class global variable for use in other class methods */
    }
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$this->eavSetupFactory = $objectManager->get("\Magento\Eav\Setup\EavSetupFactory");
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
                $eavSetup->addAttribute(
                        \Magento\Catalog\Model\Product::ENTITY,
                    'supply_ability',
                    [
                        'type' => 'text',
                        'label' => '',
                        'backend' => '',
                        'input' => 'text',
                        'sort_order' => 131,
                        'label' => 'Supply Ability',
                        'class' => 'supply-ability-class',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to'=>'simple,configurable,bundle,grouped'
                ]
            );  
        }
        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
                $eavSetup->addAttribute(
                        \Magento\Catalog\Model\Product::ENTITY,
                    'packaging_delivery',
                    [
                        'type' => 'text',
                        'label' => '',
                        'backend' => '',
                        'input' => 'text',
                        'sort_order' => 132,
                        'label' => 'Packing Delivery',
                        'class' => 'supply-ability-class',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to'=>'simple,configurable,bundle,grouped'
                ]
            );  
        }
        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'packaging_img',
                    [
                        'type' => 'text',
                        'label' => '',
                        'backend' => '',
                        'input' => 'text',
                        'sort_order' => 132,
                        'label' => 'Packing Image',
                        'class' => 'packaging-img-class',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to'=>'simple,configurable,bundle,grouped'
                    ]
            );  
        }
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'product_workflow',
                    [
                        'type' => 'text',
                        'label' => '',
                        'backend' => '',
                        'input' => 'textarea',
                        'wysiwyg_enabled'   => true,
                        'sort_order' => 133,
                        'label' => 'Product Workflow',
                        'class' => 'product-workflow-class',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to'=>'simple,configurable,bundle,grouped'
                    ]
            );  
        }
        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'coupon_code',
                    [
                        'type' => 'text',
                        'label' => '',
                        'backend' => '',
                        'input' => 'text',
                        'sort_order' => 135,
                        'label' => 'Coupon Code',
                        'class' => 'coupon-code-class',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to'=>'simple,configurable,bundle,grouped'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.6') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'hot_trend',
                    [
                        'type' => 'int',
                        'label' => '',
                        'backend' => '',
                        'input' => 'select',
                        'sort_order' => 135,
                        'label' => 'Hot Trend',
                        'class' => 'hot-trend-class',
                        'source' => 'Libero\Data\Model\Config\Source\Options',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to'=>'simple,configurable,bundle,grouped'
                    ]
            );

        }
        if (version_compare($context->getVersion(), '1.0.7') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'rma_policy',
                    [
                        'type' => 'text',
                        'label' => '',
                        'backend' => '',
                        'input' => 'text',
                        'sort_order' => 135,
                        'label' => 'RMA Policy',
                        'class' => 'rma-policy-class',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to'=>'simple,configurable,bundle,grouped'
                    ]
            );
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    'buyer_warranty',
                    [
                        'type' => 'text',
                        'label' => '',
                        'backend' => '',
                        'input' => 'text',
                        'sort_order' => 135,
                        'label' => 'Buyer Warranty',
                        'class' => 'buyer-warranty-class',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to'=>'simple,configurable,bundle,grouped'
                    ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.8') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'unit_item',
                [
                    'type' => 'text',
                    'label' => '',
                    'backend' => '',
                    'input' => 'text',
                    'sort_order' => 135,
                    'label' => 'Unit Item',
                    'class' => 'unit-item-class',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to'=>'simple,configurable,bundle,grouped'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.9') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'message_deny',
                [
                    'type' => 'text',
                    'label' => '',
                    'backend' => '',
                    'input' => 'text',
                    'sort_order' => 135,
                    'label' => 'Message Deny',
                    'class' => 'message-deny-class',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to'=>'simple,configurable,bundle,grouped'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.10') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'length',
                [
                    'type' => 'text',
                    'label' => '',
                    'backend' => '',
                    'input' => 'text',
                    'sort_order' => 135,
                    'label' => 'Length',
                    'class' => 'length-class',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to'=>'simple,configurable,bundle,grouped'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.11') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'height',
                [
                    'type' => 'text',
                    'label' => '',
                    'backend' => '',
                    'input' => 'text',
                    'sort_order' => 135,
                    'label' => 'Height',
                    'class' => 'height-class',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to'=>'simple,configurable,bundle,grouped'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.12') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'width',
                [
                    'type' => 'text',
                    'label' => '',
                    'backend' => '',
                    'input' => 'text',
                    'sort_order' => 135,
                    'label' => 'Width',
                    'class' => 'width-class',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to'=>'simple,configurable,bundle,grouped'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.13') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'image_data',
                [
                    'type' => 'text',
                    'label' => '',
                    'backend' => '',
                    'input' => 'text',
                    'sort_order' => 135,
                    'label' => 'Image Data',
                    'class' => 'image-data-class',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => false,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'unique' => false,
                    'apply_to'=>'simple,configurable,bundle,grouped'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.14') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                Category::ENTITY,
                'commission',
                [
                    'type' => 'varchar',
                    'label' => 'Commission',
                    'input' => 'text',
                    'required' => true,
                    'sort_order' => 100,
                    'global' => ScopedAttributeInterface::SCOPE_STORE,
                    'group' => 'General Information',
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.15') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'last_category_name',
                [
                    'type' => 'text',
                    'label' => '',
                    'backend' => '',
                    'input' => 'text',
                    'sort_order' => 135,
                    'label' => 'Last Category Name',
                    'class' => 'last-category-name-class',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => false,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'apply_to'=>'simple,configurable,bundle,grouped'
                ]
            );
        }
        if (version_compare($context->getVersion(), '1.0.16') < 0) {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'Vat',
                [
                    'type' => 'text',
                    'label' => '',
                    'backend' => '',
                    'input' => 'text',
                    'sort_order' => 135,
                    'label' => 'VAR',
                    'class' => 'vat-class',
                    'source' => '',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                    'visible' => false,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => false,
                    'unique' => false,
                    'apply_to'=>'simple,configurable,bundle,grouped'
                ]
            );
        }
       $setup->endSetup();
    }
}
