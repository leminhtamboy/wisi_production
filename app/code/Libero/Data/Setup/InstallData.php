<?php
namespace Libero\Data\Setup;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
 
class InstallData implements InstallDataInterface
{
   private $eavSetupFactory;
   private $attributeSetFactory;
   private $attributeSet;
   private $categorySetupFactory;
 
   public function __construct(EavSetupFactory $eavSetupFactory, AttributeSetFactory $attributeSetFactory, CategorySetupFactory $categorySetupFactory )
       {
           $this->eavSetupFactory = $eavSetupFactory;
           $this->attributeSetFactory = $attributeSetFactory;
           $this->categorySetupFactory = $categorySetupFactory;
       }
 
   public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
   {
       $setup->startSetup();
 
               // TO CREATE ATTRIBUTE SET
       /*$categorySetup = $this->categorySetupFactory->create(['setup' => $setup]);
 
       $attributeSet = $this->attributeSetFactory->create();
       $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
       $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
       $data = [
           'attribute_set_name' => 'MyCustomAttribute',
           'entity_type_id' => $entityTypeId,
           'sort_order' => 200,
       ];
       $attributeSet->setData($data);
       $attributeSet->validate();
       $attributeSet->save();
       $attributeSet->initFromSkeleton($attributeSetId);
       $attributeSet->save();*/
 
               // TO CREATE PRODUCT ATTRIBUTE
       $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
           $eavSetup->addAttribute(
                   \Magento\Catalog\Model\Product::ENTITY,
               'quick_detail',
               [
                   'type' => 'text',
                   'label' => '',
                   'backend' => '',
                   'input' => 'textarea',
                   'wysiwyg_enabled'   => false,
                   'sort_order' => 130,
                   'label' => 'Quick Detail',
                   'class' => 'quick-detail-class',
                   'source' => '',
                   'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                   'visible' => true,
                   'required' => false,
                   'user_defined' => false,
                   'default' => '',
                   'searchable' => false,
                   'filterable' => false,
                   'comparable' => false,
                   'visible_on_front' => false,
                   'used_in_product_listing' => true,
                   'unique' => false,
                   'apply_to'=>'simple,configurable,bundle,grouped'
           ]
       );  
       $setup->endSetup();
   }
}