<?php
namespace Libero\Mywishlist\Block;
//use \Magento\Customer\Api\CustomerRepositoryInterface;
class MyWishlist extends \Magento\Framework\View\Element\Template {
    
    public function getWishlistCount(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        
        $customer_id = $util->getCustomerId();
        if($customer_id){
            $store_id = $objectManager->get("\Magento\Store\Model\StoreManagerInterface")->getStore()->getId();

            $resource = $objectManager->get('\Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();

            $qty_sql = "
                SELECT 
                    SUM(qty) as qty
                FROM
                    wishlist_item AS wli
                        JOIN
                    wishlist AS wl ON wli.wishlist_id = wl.wishlist_id
                WHERE
                    customer_id = $customer_id AND store_id = $store_id
                ;
            ";
            $result = $connection->fetchAll($qty_sql);
            $qty = 0;
            if(count($result) > 0) {
                $qty = $connection->fetchAll($qty_sql)[0]['qty'];
            }
            
            
            return $qty;
        }else{
            return 0;
        }

    }

}