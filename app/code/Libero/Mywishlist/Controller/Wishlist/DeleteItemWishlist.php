<?php
namespace Libero\Mywishlist\Controller\Wishlist;

class DeleteItemWishlist extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkCustomerLogin();

        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();

        try {
            $postData = $this->getRequest()->getPostValue();
            $product_id = $postData["product_id"];
            $customer_id = $util->getCustomerId();

            $store_id = $objectManager->get("\Magento\Store\Model\StoreManagerInterface")->getStore()->getId();
            $arrayResult = array(
                "error" => false,
                "message"=> "success"
            );

            $resource = $objectManager->get('\Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();

            $wishlist_id_sql = "SELECT wishlist_id FROM wishlist where customer_id = $customer_id;";
            $wishlist_id = $connection->fetchAll($wishlist_id_sql)[0]['wishlist_id'];

            $delete_sql = "DELETE FROM wishlist_item WHERE wishlist_id='$wishlist_id' AND product_id='$product_id';";
            $delete_result = $connection->query($delete_sql);

            $wishlist_total_qty_sql = "SELECT sum(qty) as qty FROM wishlist_item WHERE wishlist_id = $wishlist_id;";
            $wishlist_total_qty = $connection->fetchAll($wishlist_total_qty_sql)[0]['qty'];
            $wishlist_total_qty = $wishlist_total_qty ? $wishlist_total_qty : 0;
            $arrayResult['qty'] = $util->formatPrice($wishlist_total_qty);
            $jsonData = json_encode($arrayResult);

            $jsonResultFactory->setData($jsonData);

            return $jsonResultFactory;

        }catch (Exception $e){
            $arrayResult = array(
                "error" => true,
                "message"=> "fail"
            );
            $jsonData = json_encode($arrayResult);

            $jsonResultFactory->setData($jsonData);

            return $jsonResultFactory;
        }
    }
}
