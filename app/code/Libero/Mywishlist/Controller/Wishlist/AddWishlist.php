<?php
namespace Libero\Mywishlist\Controller\Wishlist;

class AddWishlist extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkCustomerLogin();

        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();

        try {
            $postData = $this->getRequest()->getPostValue();
            $product_id = $postData["product_id"];
            $add_qty = $postData["qty"];
            $customer_id = $util->getCustomerId();

            $store_id = $objectManager->get("\Magento\Store\Model\StoreManagerInterface")->getStore()->getId();

            $arrayResult = array(
                "error" => false,
                "message"=> "success",
                "qty" => $add_qty
            );

            $resource = $objectManager->get('\Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            
            $isExisted = false;
            $sqlCheck = "SELECT * FROM wishlist where customer_id = $customer_id;";
            if(count($connection->fetchAll($sqlCheck))){
                $isExisted = true;
            }

            $sql = "";
            if(!$isExisted){
                $sql = "INSERT INTO `wishlist` (`customer_id`, `shared`) VALUES ('$customer_id', '0');";
                $connection->query($sql);
            }

            $wishlist_id_sql = "SELECT wishlist_id FROM wishlist where customer_id = $customer_id;";
            $wishlist_id = $connection->fetchAll($wishlist_id_sql)[0]['wishlist_id'];

            $wishlist_qty_sql = "SELECT * FROM wishlist_item WHERE wishlist_id = $wishlist_id AND product_id = $product_id;";
            $qty_result = $connection->fetchAll($wishlist_qty_sql);

            $updateWishlist = "";
            $qty = 0;
            if(count($qty_result) > 0) {
                $qty = $qty_result[0]['qty'] + $add_qty;
                $wishlist_item_id = $qty_result[0]['wishlist_item_id'];
                $updateWishlist = "UPDATE wishlist_item SET `qty`='$qty' WHERE `wishlist_item_id`= $wishlist_item_id;";
            } else {
                $qty = $add_qty;
                $updateWishlist = "INSERT INTO wishlist_item (`wishlist_id`, `product_id`, `store_id`, `qty`) VALUES ('$wishlist_id', '$product_id', '$store_id' , '$qty');";
            }
            
            $result = $connection->query($updateWishlist);

            $wishlist_total_qty_sql = "SELECT sum(qty) as qty FROM wishlist_item WHERE wishlist_id = $wishlist_id;";
            $wishlist_total_qty = $connection->fetchAll($wishlist_total_qty_sql)[0]['qty'];

            $arrayResult['qty'] = $util->formatPrice($wishlist_total_qty);
            
            $jsonData = json_encode($arrayResult);

            $jsonResultFactory->setData($jsonData);

            return $jsonResultFactory;

        }catch (Exception $e){
            $arrayResult = array(
                "error" => true,
                "message"=> "fail"
            );
            $jsonData = json_encode($arrayResult);

            $jsonResultFactory->setData($jsonData);

            return $jsonResultFactory;
        }
    }
}
