<?php
namespace Libero\ComparePrice\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $setup->run("CREATE TABLE `libero_compareprice_product` ( `id_compare` INT NOT NULL AUTO_INCREMENT , `product_id` VARCHAR(20) NOT NULL COMMENT 'Product id of Wisi' , `product_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `price` VARCHAR(50) NOT NULL , `rating` INT NOT NULL , `img_path` VARCHAR(500) NOT NULL , `sku` VARCHAR(100) NOT NULL , `type_ecom` VARCHAR(50) NOT NULL , `link_url` VARCHAR(500) NOT NULL , `name_search` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `etc` TEXT NOT NULL , PRIMARY KEY (`id_compare`)) ENGINE = InnoDB;");
        }
        
        $setup->endSetup();
    }
}
