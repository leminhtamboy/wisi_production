<?php
namespace Libero\Onestepcheckout\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $cartRepositoryInterface;
    protected $_storeManager;
    protected $cartManagementInterface;
    protected $customerFactory;
    protected $customerRepository;
    protected $_product;
    protected $order;
    /**
     * @param Magento\Framework\App\Helper\Context $context
     * @param Magento\Store\Model\StoreManagerInterface $storeManager
     * @param Magento\Catalog\Model\Product $product,
     * @param Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
     * @param Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
     * @param Magento\Customer\Model\CustomerFactory $customerFactory,
     * @param Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
     * @param Magento\Sales\Model\Order $order
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Sales\Model\Order $order
    ) {
        $this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->order = $order;
        parent::__construct($context);
    }
    // giao hang nhanh api data
    protected $_token_key_giao_hang_nhanh = "5bff599094c06b2dc11d08f5";

    protected $_token_test_key_giao_hang_nhanh =  "TokenTest";

    protected $_url_api_giao_hang_nhanh =  "https://console.ghn.vn/api/v1/apiv3/GetDistricts";

    protected $_url_test_api_giao_hang_nhanh =  "http://api.serverapi.host/api/v1/apiv3/GetDistricts";

    protected $_url_api_get_ward_giao_hang_nhanh = "https://console.ghn.vn/api/v1/apiv3/GetWards";

    protected $_url_api_calculated_fee_giao_hang_nhanh = "https://console.ghn.vn/api/v1/apiv3/CalculateFee";

    protected $_url_api_find_service_available = "https://console.ghn.vn/api/v1/apiv3/FindAvailableServices";

    protected $_url_api_place_order = "https://console.ghn.vn/api/v1/apiv3/CreateOrder";

    protected $_url_api_get_order = "https://console.ghn.vn/api/v1/apiv3/OrderInfo";

    //Giao hàng tiết kiệm API

    //protected $_token_key_giao_hang_tiet_kiem = "56368B8A1D8aBAC91c1F8fDc079Fa89787BFAd75";
    protected $_token_key_giao_hang_tiet_kiem = "2181547E3df02EBE2250D8954c3cf5E15d9bCe41";
    
    protected $_url_giao_hang_tiet_kiem_tinh_phi_van_chuyen = "https://services.giaohangtietkiem.vn/services/shipment/fee?";

    protected $_url_giao_hang_tiet_kiem_tao_don_hang = "";

    public $_CODE_GIAO_HANG_TIET_KIEM = "giao_hang_tiet_kiem";

    public $_CODE_GIAO_HANG_MIEN_PHI = "giao_hang_mien_phi";

    public $_CODE_GIAO_HANG_NHANH = "giao_hang_nhanh";

    public $_CODE_GIAO_HANG_SG247 = "giao_hang_sg247";

    public $_NAME_GIAO_HANG_TIET_KIEM = "Giao Hàng Tiết Kiệm";

    public $_NAME_GIAO_HANG_MIEN_PHI = "Giao Hàng Miễn Phí";

    public $_NAME_GIAO_HANG_NHANH = "Giao Hàng Nhanh";

    public $_NAME_GIAO_HANG_SG247 = "Giao Hàng SG247";
    
    public $_CODE_TYPE_MY_DELIVERY = "my_delivery";

    public $_CODE_TYPE_WISI_DELIVERY = "wisi_delivery";

    public $_CODE_TYPE_TMS_REQUEST = "tms_delivery";

    //Giao Hàng SG247
    protected $_token_key_sg_247 = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJFMDAwNzcyIiwiYXV0aCI6IlJPTEVfQ1VTVE9NRVIiLCJjb21wYW55SWQiOjEsImN1c3RvbWVySWQiOjEwMDMwNiwicmVtZW1iZXJNZSI6ZmFsc2UsImV4cCI6MTU0NDc3MDM0Mn0.OAENLIrh6MfOMP0-eimgF8JwZ7FlD18DlTp0w7hDuYHbMXhnJ5dqI5inNQNISpUCzTTv5b5aAMNiF6h8Cw6-SQ";
    
    /* Giao hàng nhanh Information */
    public function getTokenKeyApiGiaoHangNhanh()
    {
        return $this->_token_key_giao_hang_nhanh;
    }

    public function getUrlApiGiaoHangNhanh()
    {
        return $this->_url_api_giao_hang_nhanh;
    }

    public function getUrlGetWardGiaoHangNhanh()
    {
        return $this->_url_api_get_ward_giao_hang_nhanh;
    }
    public function getUrlCalculatedFee()
    {
        return $this->_url_api_calculated_fee_giao_hang_nhanh;
    }
    public function getUrlFindServiceAvailable()
    {
        return $this->_url_api_find_service_available;
    }
    public function getUrlPlaceOrder()
    {
        return $this->_url_api_place_order;
    }
    public function getUrlInforOrder()
    {
        return $this->_url_api_get_order;
    }
    /**
     * Function get data from API Giao Hang Nhanh
     *
     * @param [type] $data
     * @return void
     */
    public function getDataCalculatedFee($data)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $curlMagento_2 = $objectManager->get("\Magento\Framework\HTTP\Client\Curl");
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $url_website = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $post_data_to_send["weight"] = $data["weight"];
        $post_data_to_send["length"] = $data["length"];
        $post_data_to_send["width"] = $data["width"];
        $post_data_to_send["height"] = $data["height"];
        $post_data_to_send["from_district_id"] = $data["from_district_id"];
        $post_data_to_send["to_district_id"] = $data["to_district_id"];
        $post_data_to_send["service_id"] = $data["service_id"];
        $post_data_to_send["coupon_code"] = "";
        $post_data_to_send["insurance_free"] = 0;
        $url_shipping = $url_website."onestepcheckout/api/calculatedshipping";
        $curlMagento_2->post($url_shipping,$post_data_to_send);
        //response will contain the output in form of JSON string
        return $curlMagento_2->getBody();
    }
    /**
     * Function get Service Available From GiaoHangNhanh
     *
     * @param [type] $data
     * @return void
     */
    public function getDataServiceAvailable($data)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $curlMagento_2 = $objectManager->get("\Magento\Framework\HTTP\Client\Curl");
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $url_website = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $post_data_to_send["weight"] = $data["weight"];
        $post_data_to_send["length"] = $data["length"];
        $post_data_to_send["width"] = $data["width"];
        $post_data_to_send["height"] = $data["height"];
        $post_data_to_send["from_district_id"] = $data["from_district_id"];
        $post_data_to_send["to_district_id"] = $data["to_district_id"];
        $url = $this->getUrlFindServiceAvailable();
        $url = $url_website."onestepcheckout/api/findservice";
        $curlMagento_2->post($url,$post_data_to_send);
        //response will contain the output in form of JSON string
        return $curlMagento_2->getBody();
    }
    public function pushOrderToGiaoHangNhanh($data){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $curlMagento_2 = $objectManager->get("\Magento\Framework\HTTP\Client\Curl");
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $url_website = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $url = $this->getUrlPlaceOrder();
        $url = $url_website."onestepcheckout/api/placeorder";
        $curlMagento_2->post($url,$data);
        //response will contain the output in form of JSON string
        return $curlMagento_2->getBody();
    }
    public function pushOrderToGiaoHangTietKiem($data)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $curlMagento_2 = $objectManager->get("\Magento\Framework\HTTP\Client\Curl");
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $url_website = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $url = $this->getUrlPlaceOrder();
        $url = $url_website."onestepcheckout/api/placeordertietkiem";
        $curlMagento_2->post($url,$data);
        //response will contain the output in form of JSON string
        return $curlMagento_2->getBody();
    }
    /**
     * Undocumented function
     *
     * @param [type] $data
     * @return void
     */
    public function getOrderInformation($data)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $curlMagento_2 = $objectManager->get("\Magento\Framework\HTTP\Client\Curl");
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $url_website = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $url = $this->getUrlPlaceOrder();
        $url = $url_website."onestepcheckout/api/getorder";
        $curlMagento_2->post($url,$data);
        //response will contain the output in form of JSON string
        return $curlMagento_2->getBody();
    }
    public function getOrderGHTK($data){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $curlMagento_2 = $objectManager->get("\Magento\Framework\HTTP\Client\Curl");
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $url_website = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        $url = $this->getUrlPlaceOrder();
        $url = $url_website."onestepcheckout/api/getordertietkiem";
        $curlMagento_2->post($url,$data);
        //response will contain the output in form of JSON string
        return $curlMagento_2->getBody();
    }
    /**
     * Undocumented function
     *
     * @param [type] $data
     * @return void
     */
    public function getDataCalculatedFeeGiaoHangTietKiem($data)
    {
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://services.giaohangtietkiem.vn/services/shipment/fee?" . http_build_query($data),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                //"Token:56368B8A1D8aBAC91c1F8fDc079Fa89787BFAd75",
                "Token:2181547E3df02EBE2250D8954c3cf5E15d9bCe41",
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        
        return $response;
        //$url = $this->_url_giao_hang_tiet_kiem_tinh_phi_van_chuyen;
        //return $this->callUrlGiaoHangTietKiem($url,$data);
    }
    /**
     * Undocumented function
     *
     * @param [type] $id_district
     * @return void
     */
    public function getNameProvince($id_district){
        $connect = $this->getConnectionDatabaseDirect();
        $sql_province = "SELECT * FROM `libero_province` inner join `libero_district` on  libero_province.province_id = libero_district.province_id WHERE libero_district.district_id = '$id_district'";
        $data_province = $connect->fetchAll($sql_province);
        return $data_province[0]["province_name"];
    }
    public function getIdProvinceSg247($name){
        $connect = $this->getConnectionDatabaseDirect();
        $sql_province = "SELECT * FROM `libero_province` WHERE `name_province` LIKE '%$name%'";
        $data_province = $connect->fetchAll($sql_province);
        return $data_province[0]["code_province"];
    }
    public function getIdDistrictSg247($name,$id_province){
        $connect = $this->getConnectionDatabaseDirect();
        $sql_province = "SELECT * FROM `libero_district_sg247` WHERE `name_district` LIKE '%$name%' AND `id_province` = '$id_province'";
        $data_province = $connect->fetchAll($sql_province);
        return $data_province[0]["code_district"];
    }
    /**
     * Undocumented function
     *
     * @param [type] $id_district
     * @return void
     */
    public function getNameDistrict($id_district){
        $connect = $this->getConnectionDatabaseDirect();
        $sql_district = "SELECT * FROM  `libero_district` WHERE libero_district.district_id = '$id_district'";
        $data_district = $connect->fetchAll($sql_district);
        return $data_district[0]["district_name"];
    }
    /**
     * Function get all shipping price for api shipping code
     *
     * @param [type] $data
     * @return void
     */
    public function getFeeShipingAll($data){
        try{
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $helperCheckout = $objectManager->get("\Libero\Onestepcheckout\Helper\Data");
            $shipping_code = $data["shipping_code"];
            $weight = $data["weight"];
            $price = $data["price"];
            $from_district_id = $data["from_district"];
            $to_district_id = $data["to_district"];
            $array_ret_json = array();
            $weight_save = "";
            $length_save = "";
            $width_save = "";
            $height_save = "";
            $from_district_save = "";
            $from_province_save = "";
            $to_district_save = "";
            $to_province_save = "";
            $service_id_save = "";
            $code_delivery_save = "";
            $order_id_save = "";
            if($weight == ""){
                $weight = 0;
            }
            $weight_check = round($weight,0);
            $array_ret_json["error"] = true;
            if($shipping_code == $helperCheckout->_CODE_GIAO_HANG_MIEN_PHI){
                $array_ret_json["error"] = false;
                $array_ret_json["fee"] = 0;
            }
            if($shipping_code == $helperCheckout->_CODE_GIAO_HANG_NHANH){
                $post_data_to_send  = array();
                $post_data_to_send["weight"] = $weight;
                $post_data_to_send["length"] = 5;
                $post_data_to_send["width"] = 5;
                $post_data_to_send["height"] = 5;
                $post_data_to_send["from_district_id"] = $from_district_id;
                if($to_district_id == ""){
                    $to_district_id = $from_district_id;
                }
                $post_data_to_send["to_district_id"] = $to_district_id;
                $post_data_to_send["service_id"] = 53320;
                $post_data_to_send["coupon_code"] = "";
                $post_data_to_send["insurance_free"] = 0;
                $service_id = 53320;
                if($weight_check > 0){
                    $responseService = $helperCheckout->getDataServiceAvailable($post_data_to_send);
                    $json_service = json_decode($responseService,true);
                    foreach($json_service["data"] as $_service){
                        if($_service["Name"] == "Chuẩn"){
                            $service_id = $_service["ServiceID"]; // dang lat service chuan
                        }
                    }
                    $post_data_to_send["service_id"] = $service_id;
                    $response = $helperCheckout->getDataCalculatedFee($post_data_to_send);
                    $json_data = json_decode($response);
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = $json_data->data->CalculatedFee;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_NHANH;
                }else{
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = 0;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_MIEN_PHI;
                    
                }
                $from_district_save = $from_district_id;
                $from_province_save = "";
                $to_district_save = $to_district_id;
                $to_province_save = "";
                $service_id_save = $service_id;
                $weight_save =$weight;
            }
            if($shipping_code == $helperCheckout->_CODE_GIAO_HANG_TIET_KIEM){
                $post_data_to_send = array();
                $pick_province = $helperCheckout->getNameProvince($from_district_id);
                $pick_district = $helperCheckout->getNameDistrict($from_district_id);
                if($to_district_id == ""){
                    $to_district_id = $from_district_id;
                }
                $to_province = $helperCheckout->getNameProvince($to_district_id);
                $to_district = $helperCheckout->getNameDistrict($to_district_id);
                $post_data_to_send["pick_province"] = $pick_province;
                $post_data_to_send["pick_district"] = $pick_district;
                $post_data_to_send["province"] = $to_province;
                $post_data_to_send["district"] = $to_district;
                $post_data_to_send["weight"] = $weight;
                $post_data_to_send = array(
                    "pick_province" => $pick_province,
                    "pick_district" => $pick_district,
                    "province" => $to_province,
                    "district" => $to_district,
                    "weight" => $weight
                );
                if($weight_check > 0){
                    $response = $helperCheckout->getDataCalculatedFeeGiaoHangTietKiem($post_data_to_send);
                    $json_data = json_decode($response);
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = $json_data->fee->fee;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_TIET_KIEM;
                }else{
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = 0;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_MIEN_PHI;
                }
                $from_district_save = $pick_district;
                $from_province_save = $pick_province;
                $to_district_save = $to_district;
                $to_province_save = $to_province;
                $service_id_save = "";
                $weight_save =$weight;
            }
            if($shipping_code == $helperCheckout->_CODE_GIAO_HANG_SG247){

                $post_data_to_send = array();
                $pick_province = $helperCheckout->getNameProvince($from_district_id);
                $pick_district = $helperCheckout->getNameDistrict($from_district_id);
                if($to_district_id == ""){
                    $to_district_id = $from_district_id;
                }
                $to_province = $helperCheckout->getNameProvince($to_district_id);
                $to_district = $helperCheckout->getNameDistrict($to_district_id);
                $post_data_to_send["pick_province"] = $pick_province;
                $post_data_to_send["pick_district"] = $pick_district;
                $post_data_to_send["province"] = $to_province;
                $post_data_to_send["district"] = $to_district;
                $post_data_to_send["weight"] = $weight;
                $post_data_to_send = array(
                    "pick_province" => $pick_province,
                    "pick_district" => $pick_district,
                    "province" => $to_province,
                    "district" => $to_district,
                    "weight" => $weight
                );
                if($weight_check > 0){
                    $response = $helperCheckout->getDataCalculatedFeeGiaoHangTietKiem($post_data_to_send);
                    $json_data = json_decode($response);
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = $json_data->fee->fee;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_TIET_KIEM;
                }else{
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = 0;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_MIEN_PHI;
                }
                $from_district_save = $pick_district;
                $from_province_save = $pick_province;
                $to_district_save = $to_district;
                $to_province_save = $to_province;
                $service_id_save = "";
                $weight_save =$weight;
            }
            if($array_ret_json["error"] == false){
                $length_save = 5;
                $width_save = 5;
                $height_save = 5;
                $array_ret_json["weight"] = $weight_save;
                $array_ret_json["length"] = $length_save;
                $array_ret_json["width"] = $width_save;
                $array_ret_json["height"] = $height_save;
                $array_ret_json["from_district"] = $from_district_save;
                $array_ret_json["from_province"] = $from_province_save;
                $array_ret_json["to_district"] = $to_district_save;
                $array_ret_json["to_province"] = $to_province_save;
                $array_ret_json["service_id"] = $service_id_save;
                $array_ret_json["code_delivery"] = $code_delivery_save;
                $array_ret_json["order_id"] = $order_id_save;
            }else{
                $array_ret_json["error"] = true;
                $array_ret_json["fee"] = 0;
                $array_ret_json["msg"] = "ERROR_SHIPPING_PRICE";
            }
            return $array_ret_json;
        }catch(\Exception $e){
            $array_null = array("error" => true,"fee"=> 0, "msg" => $e->getMessage());
            return $array_null;
        }
    }
    /**
     * Function save information shipping from quote to table for get ting after reloading page
     *
     * @param [type] $quote_id
     * @param [type] $shipping_name
     * @param [type] $shipping_fee
     * @param [type] $seller_id
     * @param [type] $weight
     * @param [type] $length
     * @param [type] $width
     * @param [type] $height
     * @param [type] $from_district
     * @param [type] $from_province
     * @param [type] $to_district
     * @param [type] $to_province
     * @param [type] $service_id
     * @param [type] $code_delivery
     * @param [type] $order_id
     * @return void
     */
    public function saveShippingInformationToQuote($quote_id,$shipping_name,$shipping_fee,$seller_id,$weight,$length,$width,$height,$from_district,$from_province,$to_district,$to_province,$service_id,$code_delivery,$order_id)
    {
        $connect = $this->getConnectionDatabaseDirect();
        $sql_checking_esxit_shipping = "SELECT * FROM libero_quote_shipping_price where id_seller = '$seller_id' and quote_id = '$quote_id'";
        $data_checking = $connect->fetchAll($sql_checking_esxit_shipping);
        $sql_insert_shiping_to_quote = "";
        if(count($data_checking) > 0){
            if($shipping_name != $data_checking[0]["name_shipping"]){
                $sql_insert_shiping_to_quote = "UPDATE `libero_quote_shipping_price` SET `fee_shipping` = '$shipping_fee',name_shipping = '$shipping_name', `weight` = '$weight',`length` = '$length',`width` = '$width',`height` = '$height',`from_district` = '$from_district',`from_province` = '$from_province',`to_district` = '$to_district',`to_province` = '$to_province',`service_id` = '$service_id',`code_delivery` = '$code_delivery',`order_id` = '$order_id' WHERE id_seller = '$seller_id' and quote_id = '$quote_id'";
                $connect->query($sql_insert_shiping_to_quote);
            }else{
                $sql_insert_shiping_to_quote = "UPDATE `libero_quote_shipping_price` SET `fee_shipping` = '$shipping_fee',name_shipping = '$shipping_name', `weight` = '$weight',`length` = '$length',`width` = '$width',`height` = '$height',`from_district` = '$from_district',`from_province` = '$from_province',`to_district` = '$to_district',`to_province` = '$to_province',`service_id` = '$service_id',`code_delivery` = '$code_delivery',`order_id` = '$order_id' WHERE id_seller = '$seller_id' and quote_id = '$quote_id'";
                $connect->query($sql_insert_shiping_to_quote);
            }
        }else{
            $sql_insert_shiping_to_quote = "INSERT INTO `libero_quote_shipping_price` (`id_quote_price`, `quote_id`, `name_shipping`, `fee_shipping`, `id_seller`,`weight`, `length`, `width`, `height`, `from_district`, `from_province`, `to_district`, `to_province`, `service_id`, `code_delivery`, `order_id`) 
            VALUES (NULL, '$quote_id', '$shipping_name', '$shipping_fee', '$seller_id','$weight','$length','$width','$height','$from_district','$from_province','$to_district','$to_province','$service_id','$code_delivery','$order_id')";
            $connect->query($sql_insert_shiping_to_quote);
        }
    }
    /**
     * Update Shipping Information
     *
     * @param [type] $quote_id
     * @param [type] $shipping_name
     * @param [type] $shipping_fee
     * @param [type] $seller_id
     * @param [type] $weight
     * @param [type] $length
     * @param [type] $width
     * @param [type] $height
     * @param [type] $from_district
     * @param [type] $from_province
     * @param [type] $to_district
     * @param [type] $to_province
     * @param [type] $service_id
     * @param [type] $code_delivery
     * @param [type] $order_id
     * @return void
     */
    public function saveShippingInformation($quote_id,$to_district)
    {
        $connect = $this->getConnectionDatabaseDirect();
        $sql_checking_esxit_shipping = "SELECT * FROM libero_quote_shipping_price where quote_id = '$quote_id'";
        $data_checking = $connect->fetchAll($sql_checking_esxit_shipping);
        $sql_insert_shiping_to_quote = "";
        if(count($data_checking) > 0){
            $sql_insert_shiping_to_quote = "UPDATE `libero_quote_shipping_price` SET `to_district` = '$to_district' WHERE `quote_id` = '$quote_id'";
            $connect->query($sql_insert_shiping_to_quote);
        }
    }
    /**
     * Get List of Shipping from Quote
     *
     * @return void
     */
    public function getListShipingFromQuote($quote_id)
    {
        //echo "quote id".$quote_id;
        //die;
        $connect = $this->getConnectionDatabaseDirect();
        $sql_get_shipping = "SELECT *, sum(fee_shipping) as sum_fee_shipping FROM libero_quote_shipping_price where `quote_id` = '$quote_id' GROUP BY code_delivery;";
        $data = $connect->fetchAll($sql_get_shipping);
        return $data;
    }
    /**
     * Function get one item shipping price in quote shiping by seller id
     *
     * @param [type] $seller_id
     * @return void
     */
    public function getShippingBySellerAndQuote($seller_id,$quote_id)
    {
        $connect = $this->getConnectionDatabaseDirect();
        $sql_get_shipping = "SELECT * FROM libero_quote_shipping_price where `quote_id` = '$quote_id' and id_seller = '$seller_id'";
        $data = $connect->fetchAll($sql_get_shipping);
        return $data;
    }
    /**
     * Function get one item shipping price in quote shiping by seller id
     *
     * @param [type] $seller_id
     * @return void
     */
    public function getShippingSpecificFromSeller($seller_id)
    {
        $connect = $this->getConnectionDatabaseDirect();
        $sql_get_shipping = "SELECT * FROM libero_quote_shipping_price where `quote_id` = '$quote_id' and seller_id = '$seller_id'";
        $data = $connect->fetchAll($sql_get_shipping);
        return $data;
    }
    /**
     * @param $url
     * @param $postData
     * @return json data
     */
    public function callUrl($url,$postData){
        $postData=json_encode($postData);
        $header=array(
            "Accept:application/json",
            "Content-Type:application/json"
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 3000);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$postData);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    /**
     * Call Url Accompany Giao Hang Tiet Kiem
     * @param [type] $url
     * @param [type] $data
     * @return void
     */
    public function callUrlGiaoHangTietKiem($url,$data){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "$url?" . http_build_query($data),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_HTTPHEADER => array(
                "Token:$this->_token_key_giao_hang_tiet_kiem",
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
    /**
     * Function get connectio database directly for case lazy create model
     * @return mixed
     */
    public function getConnectionDatabaseDirect()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        return $connection;
    }

    /**
     * Function get list product of seller
     *
     * @return void
     */
    public function getProductOfSeller($quote)
    {
        $quote_id = $quote->getData("entity_id");
        $connect = $this->getConnectionDatabaseDirect();
        $sql_quote = "SELECT * FROM `quote_item` WHERE `quote_id` = $quote_id order by id_seller ASC";
        $data_quote_item = $connect->fetchAll($sql_quote);
        return $data_quote_item;
    }
    public function getDetailProvider($id_provider)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model_provider = $objectManager->create("\Libero\Customer\Model\Seller");
        $provider = $model_provider->load($id_provider);
        return $provider;
    }
    /**
     * Function get options for congfigruable product
     *
     * @return void
     */
    public function getProductOptions($item_id,$product_id = null)
    {
        $connect = $this->getConnectionDatabaseDirect();
        $sql_options = "SELECT * FROM `quote_item_option` WHERE `item_id` = '$item_id' and code = 'info_buyRequest' and `product_id` = '$product_id'";
        if($product_id  == null){
            $sql_options = "SELECT * FROM `quote_item_option` WHERE `item_id` = '$item_id' and code = 'info_buyRequest' order by `option_id` desc";
        }
        $data_options = $connect->fetchAll($sql_options);
        return $data_options;

    }
    /**
     * Function get option value
     * 
     * @param [type] $option_id
     * @return void
     */
    public function getOptionValue($option_id)
    {
        $connect = $this->getConnectionDatabaseDirect();
        $sql_option_value = "SELECT * FROM `eav_attribute_option_value` WHERE `option_id` = '$option_id'";
        $data_option_value = $connect->fetchAll($sql_option_value);
        return $data_option_value;
    }
    /**
     * Function get Label Option
     *
     * @param [type] $attribute_id
     * @return void
     */
    public function getLabelOption($attribute_id)
    {
        $connect = $this->getConnectionDatabaseDirect();
        $sql_attribute = "SELECT * FROM `eav_attribute` WHERE `attribute_id` = '$attribute_id'";
        $data_attribute = $connect->fetchAll($sql_attribute);
        return $data_attribute;
    }
    /**
     * Function get Shipping Of Seller
     *
     * @param [type] $id_seller
     * @return void
     */
    public function getListShippingOfSeller($id_seller){
        $connect = $this->getConnectionDatabaseDirect();
        $sql_shipping = "SELECT * FROM `libero_onestepcheckout_shipping_method` WHERE `id_seller` = '82'";
        $data_shipping = $connect->fetchAll($sql_shipping);
        return $data_shipping;
    }
    public function getListShipping($id_seller){
        $connect = $this->getConnectionDatabaseDirect();
        $sql_shipping = "SELECT * FROM `libero_delivery_company`";
        $data_shipping = $connect->fetchAll($sql_shipping);
        return $data_shipping;
    }
    public function getAddressOfSeller($id_seller){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelSeller = $objectManager->create('\Libero\Customer\Model\Seller');
        $modelCustomer = $objectManager->create('Magento\Customer\Model\Customer');
        $connectionDirect = $this->getConnectionDatabaseDirect();
        $seller_information = $modelSeller->load($id_seller);
        $seller_of_customer = $modelCustomer->load($seller_information->getData("id_customer"));
        $id_customer_address = $seller_information->getData("id_customer");
        //echo "id seller".$id_seller;
        $sql = "select postcode from customer_address_entity where parent_id = '$id_customer_address'";
        //echo $sql;
        $data_address =  $connectionDirect->fetchAll($sql);
        //echo "<pre>";
        //var_dump($data_address);
        //echo "</pre>";
        //die;
        $postcode  = $data_address[0]["postcode"];
        $from_district_id = explode("-",$postcode)[0];
        return $from_district_id;
    }
    /** Product Detail Order Count */
    public function getCountOfOrder($product_sku,$product_id)
    {
        $connect = $this->getConnectionDatabaseDirect();
        $sql_sum_order = "SELECT SUM(`qty_ordered`) as sum_count_order FROM `sales_order_item` WHERE `sku` = '$product_sku'";
        $data_sum_order = $connect->fetchAll($sql_sum_order);
        if($data_sum_order[0]['sum_count_order'])
        {
            return round($data_sum_order[0]['sum_count_order'],0);
        }
        $sql_sum_order = "SELECT SUM(`qty_ordered`) as sum_count_order FROM `sales_order_item` WHERE `product_id` = '$product_id'";
        $data_sum_order = $connect->fetchAll($sql_sum_order);
        if($data_sum_order[0]['sum_count_order'])
        {
            return round($data_sum_order[0]['sum_count_order'],0);
        }
        return 0;
    }
    /*******/
    /**
     * Function formart Qtgy
     *
     * @param [type] $qty
     * @return void
     */
    public function formartQty($qty)
    {
        return number_format($qty);
    }
    /**
     * Create Order On Your Store
     *
     * @param array $orderData
     * @return array
     *
     */
    public function createMageOrder($payment_code) {
        /*$orderData=[
            'currency_id'  => 'USD',
            'email'        => 'test@webkul.com', //buyer email id
            'shipping_address' =>[
                'firstname'    => 'jhon', //address Details
                'lastname'     => 'Deo',
                'street' => '123',
                'city' => 'Ho Chi Minh',
                'country_id' => 'VN',
                'region' => 'Binh Thanh',
                'postcode' => '70000',
                'telephone' => '52332',
                'fax' => '32423',
                'save_in_address_book' => 1
            ],
            'items'=> [ //array of product which order you want to create
                ['product_id'=>'2','qty'=>1]
            ]
        ];
        $store=$this->_storeManager->getStore();
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $customer=$this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($orderData['email']);// load customet by email address
        if(!$customer->getEntityId()){
            //If not avilable then create this customer
            $customer->setWebsiteId($websiteId)
                ->setStore($store)
                ->setFirstname($orderData['shipping_address']['firstname'])
                ->setLastname($orderData['shipping_address']['lastname'])
                ->setEmail($orderData['email'])
                ->setPassword($orderData['email']);
            $customer->save();
        }

        $cartId = $this->cartManagementInterface->createEmptyCart(); //Create empty cart
        $quote = $this->cartRepositoryInterface->get($cartId); // load empty cart quote
        $quote->setStore($store);
        // if you have allready buyer id then you can load customer directly
        $customer= $this->customerRepository->getById($customer->getEntityId());
        $quote->setCurrency();
        $quote->assignCustomer($customer); //Assign quote to customer

        //add items in quote
        foreach($orderData['items'] as $item){
            $product=$this->_product->load($item['product_id']);
            //$product->setPrice($item['price']);
            //var_dump($product->getData());
            $quote->addProduct($product, intval($item['qty']));
        }

        //Set Address to quote
        $quote->getBillingAddress()->addData($orderData['shipping_address']);
        $quote->getShippingAddress()->addData($orderData['shipping_address']);

        // Collect Rates and Set Shipping & Payment Method

        $shippingAddress=$quote->getShippingAddress();
        $shippingAddress->setCollectShippingRates(true)
            ->collectShippingRates()
            ->setShippingMethod('magepsycho_customshipping_magepsycho_customshipping'); //shipping method
        $quote->setPaymentMethod('checkmo'); //payment method
        $quote->setInventoryProcessed(false); //not effetc inventory

        // Set Sales Order Payment
        $quote->getPayment()->importData(['method' => 'checkmo']);
        $quote->save(); //Now Save quote and your quote is ready

        // Collect Totals
        $quote->collectTotals();

        // Create Order From Quote
        $quote = $this->cartRepositoryInterface->get($quote->getId());
        $orderId = $this->cartManagementInterface->placeOrder($quote->getId());
        $order = $this->order->load($orderId);

        $order->setEmailSent(0);
        $increment_id = $order->getRealOrderId();
        if($order->getEntityId()){
            $result['order_id']= $order->getRealOrderId();
        }else{
            $result=['error'=>1,'msg'=>'Your custom message'];
        }
        return $result;
        /*$store=$this->_storeManager->getStore();
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $customer=$this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($orderData['email']);// load customet by email address
        if(!$customer->getEntityId()){
            //If not avilable then create this customer
            $customer->setWebsiteId($websiteId)
                ->setStore($store)
                ->setFirstname($orderData['shipping_address']['firstname'])
                ->setLastname($orderData['shipping_address']['lastname'])
                ->setEmail($orderData['email'])
                ->setPassword($orderData['email']);
            $customer->save();
        }

        $cartId = $this->cartManagementInterface->createEmptyCart(); //Create empty cart
        $quote = $this->cartRepositoryInterface->get($cartId); // load empty cart quote
        $quote->setStore($store);
        // if you have allready buyer id then you can load customer directly
        $customer= $this->customerRepository->getById($customer->getEntityId());
        $quote->setCurrency();
        $quote->assignCustomer($customer); //Assign quote to customer

        //add items in quote
        foreach($orderData['items'] as $item){
            $product=$this->_product->load($item['product_id']);
            $quote->addProduct($product, intval($item['qty']));
        }

        //Set Address to quote
        $quote->getBillingAddress()->addData($orderData['shipping_address']);
        $quote->getShippingAddress()->addData($orderData['shipping_address']);

        // Collect Rates and Set Shipping & Payment Method

        $shippingAddress=$quote->getShippingAddress();
        $shippingAddress->setCollectShippingRates(true)
            ->collectShippingRates()
            ->setShippingMethod('shipchung_viettel'); //shipping method

        $quote->setPaymentMethod('custompayment'); //payment method
        $quote->setInventoryProcessed(false); //not effetc inventory
        */
        try{
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $block = $objectManager->get("\Libero\Onestepcheckout\Block\AbstractCheckOut");
            $quote = $block->getCheckoutSession()->getQuote();
            $quote->setPaymentMethod($payment_code);
            // Set Sales Order Payment
            $quote->getPayment()->importData(['method' => $payment_code]);
            $quote->save(); //Now Save quote and your quote is ready
            // Collect Totals
            $quote->collectTotals();
            // Create Order From Quote
            $quote = $this->cartRepositoryInterface->get($quote->getId());
            $orderId = $this->cartManagementInterface->placeOrder($quote->getId());
            $order = $this->order->load($orderId);
            $order->setEmailSent(0);
            $increment_id = $order->getRealOrderId();
            if($order->getEntityId()){
                $result = ['error'=>0,'msg'=>'success','order_id' => $orderId,'increment_id' => $increment_id];
            }else{
                $result = ['error'=>1,'msg'=>'fail'];
            }
            return $result;
        }catch(\Exception $e){
            $result = ['error'=>1,'msg'=>'fail','msg_error' => $e->getMessage()];
            return $result;
        }
        
    }
    public function resizeImage($product, $imageId, $width, $height = null)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $imageHelper = $objectManager->create("\Magento\Catalog\Helper\Image");
        $resizedImage = $imageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }
}