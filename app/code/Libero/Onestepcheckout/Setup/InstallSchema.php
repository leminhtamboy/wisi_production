<?php
namespace Libero\Onestepcheckout\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements  InstallSchemaInterface{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        //Do something

        $setup->getConnection();
        $setup->run("CREATE TABLE `libero_onestepcheckout_shipping_method` ( `id_shipping_method` INT NOT NULL AUTO_INCREMENT , `name_shipping_method` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `title_shipping_method` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `status_shipping_method` INT NOT NULL , PRIMARY KEY (`id_shipping_method`)) ENGINE = InnoDB;");
        $setup->endSetup();
    }
}
