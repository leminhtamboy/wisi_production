<?php
namespace Libero\Onestepcheckout\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $server = "localhost";
        $database = "wisi_production";
        $username = "root";
        $password = "root";
        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            //code to upgrade to 1.0.1
            
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();
            $conn = new \PDO("mysql:host=$server; dbname=$database", $username, $password);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $conn->exec("SET CHARACTER SET utf8");
            $conn->exec("SET FOREIGN_KEY_CHECKS=0");
            $conn->exec("CREATE TABLE `seller_sales_order` (
              `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
              `state` varchar(32) DEFAULT NULL COMMENT 'State',
              `status` varchar(32) DEFAULT NULL COMMENT 'Status',
              `coupon_code` varchar(255) DEFAULT NULL COMMENT 'Coupon Code',
              `protect_code` varchar(255) DEFAULT NULL COMMENT 'Protect Code',
              `shipping_description` varchar(255) DEFAULT NULL COMMENT 'Shipping Description',
              `is_virtual` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Is Virtual',
              `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
              `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Customer Id',
              `seller_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Seller Id',
              `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
              `base_discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Canceled',
              `base_discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Invoiced',
              `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
              `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
              `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
              `base_shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Canceled',
              `base_shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Invoiced',
              `base_shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Refunded',
              `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
              `base_shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Refunded',
              `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
              `base_subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Canceled',
              `base_subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Invoiced',
              `base_subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Refunded',
              `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
              `base_tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Canceled',
              `base_tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Invoiced',
              `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
              `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
              `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
              `base_total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Canceled',
              `base_total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced',
              `base_total_invoiced_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Invoiced Cost',
              `base_total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Offline Refunded',
              `base_total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Online Refunded',
              `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
              `base_total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Qty Ordered',
              `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
              `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
              `discount_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Canceled',
              `discount_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Invoiced',
              `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
              `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
              `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
              `shipping_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Canceled',
              `shipping_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Invoiced',
              `shipping_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Refunded',
              `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
              `shipping_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Refunded',
              `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
              `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
              `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
              `subtotal_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Canceled',
              `subtotal_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Invoiced',
              `subtotal_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Refunded',
              `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
              `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
              `tax_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Tax Invoiced',
              `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
              `total_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Total Canceled',
              `total_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Total Invoiced',
              `total_offline_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Offline Refunded',
              `total_online_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Online Refunded',
              `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
              `total_qty_ordered` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty Ordered',
              `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded',
              `can_ship_partially` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Can Ship Partially',
              `can_ship_partially_item` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Can Ship Partially Item',
              `customer_is_guest` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Customer Is Guest',
              `customer_note_notify` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Customer Note Notify',
              `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
              `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
              `edit_increment` int(11) DEFAULT NULL COMMENT 'Edit Increment',
              `email_sent` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Email Sent',
              `send_email` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Send Email',
              `forced_shipment_with_invoice` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Forced Do Shipment With Invoice',
              `payment_auth_expiration` int(11) DEFAULT NULL COMMENT 'Payment Authorization Expiration',
              `quote_address_id` int(11) DEFAULT NULL COMMENT 'Quote Address Id',
              `quote_id` int(11) DEFAULT NULL COMMENT 'Quote Id',
              `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
              `adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Negative',
              `adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Adjustment Positive',
              `base_adjustment_negative` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Negative',
              `base_adjustment_positive` decimal(12,4) DEFAULT NULL COMMENT 'Base Adjustment Positive',
              `base_shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Amount',
              `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
              `base_total_due` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Due',
              `payment_authorization_amount` decimal(12,4) DEFAULT NULL COMMENT 'Payment Authorization Amount',
              `shipping_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Amount',
              `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
              `total_due` decimal(12,4) DEFAULT NULL COMMENT 'Total Due',
              `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
              `customer_dob` datetime DEFAULT NULL COMMENT 'Customer Dob',
              `increment_id` varchar(32) DEFAULT NULL COMMENT 'Increment Id',
              `applied_rule_ids` varchar(128) DEFAULT NULL COMMENT 'Applied Rule Ids',
              `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
              `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
              `customer_firstname` varchar(128) DEFAULT NULL COMMENT 'Customer Firstname',
              `customer_lastname` varchar(128) DEFAULT NULL COMMENT 'Customer Lastname',
              `customer_middlename` varchar(128) DEFAULT NULL COMMENT 'Customer Middlename',
              `customer_prefix` varchar(32) DEFAULT NULL COMMENT 'Customer Prefix',
              `customer_suffix` varchar(32) DEFAULT NULL COMMENT 'Customer Suffix',
              `customer_taxvat` varchar(32) DEFAULT NULL COMMENT 'Customer Taxvat',
              `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
              `ext_customer_id` varchar(32) DEFAULT NULL COMMENT 'Ext Customer Id',
              `ext_order_id` varchar(32) DEFAULT NULL COMMENT 'Ext Order Id',
              `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
              `hold_before_state` varchar(32) DEFAULT NULL COMMENT 'Hold Before State',
              `hold_before_status` varchar(32) DEFAULT NULL COMMENT 'Hold Before Status',
              `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
              `original_increment_id` varchar(32) DEFAULT NULL COMMENT 'Original Increment Id',
              `relation_child_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Id',
              `relation_child_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Child Real Id',
              `relation_parent_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Id',
              `relation_parent_real_id` varchar(32) DEFAULT NULL COMMENT 'Relation Parent Real Id',
              `remote_ip` varchar(32) DEFAULT NULL COMMENT 'Remote Ip',
              `shipping_method` varchar(32) DEFAULT NULL COMMENT 'Shipping Method',
              `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
              `store_name` varchar(32) DEFAULT NULL COMMENT 'Store Name',
              `x_forwarded_for` varchar(32) DEFAULT NULL COMMENT 'X Forwarded For',
              `customer_note` text COMMENT 'Customer Note',
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
              `total_item_count` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Total Item Count',
              `customer_gender` int(11) DEFAULT NULL COMMENT 'Customer Gender',
              `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
              `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
              `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
              `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
              `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
              `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
              `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
              `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
              `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
              `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
              `coupon_rule_name` varchar(255) DEFAULT NULL COMMENT 'Coupon Sales Rule Name',
              `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
              `paypal_ipn_customer_notified` int(11) DEFAULT '0' COMMENT 'Paypal Ipn Customer Notified'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order';");

              $conn->exec("ALTER TABLE `seller_sales_order`
                ADD PRIMARY KEY (`entity_id`),
                ADD KEY `SALES_ORDER_STATUS` (`status`),
                ADD KEY `SALES_ORDER_STATE` (`state`),
                ADD KEY `SALES_ORDER_STORE_ID` (`store_id`),
                ADD KEY `SALES_ORDER_CREATED_AT` (`created_at`),
                ADD KEY `SALES_ORDER_CUSTOMER_ID` (`customer_id`),
                ADD KEY `SALES_ORDER_EXT_ORDER_ID` (`ext_order_id`),
                ADD KEY `SALES_ORDER_QUOTE_ID` (`quote_id`),
                ADD KEY `SALES_ORDER_UPDATED_AT` (`updated_at`),
                ADD KEY `SALES_ORDER_SEND_EMAIL` (`send_email`),
                ADD KEY `SALES_ORDER_EMAIL_SENT` (`email_sent`);");

              $conn->exec("ALTER TABLE `seller_sales_order`MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=1;");  

              $conn->exec("CREATE TABLE `seller_sales_order_item` (
              `item_id` int(10) UNSIGNED NOT NULL COMMENT 'Item Id',
              `order_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Order Id',
              `parent_item_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Parent Item Id',
              `quote_item_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Quote Item Id',
              `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
              `product_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Product Id',
              `product_type` varchar(255) DEFAULT NULL COMMENT 'Product Type',
              `product_options` text COMMENT 'Product Options',
              `weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Weight',
              `is_virtual` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Is Virtual',
              `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
              `name` varchar(255) DEFAULT NULL COMMENT 'Name',
              `description` text COMMENT 'Description',
              `applied_rule_ids` text COMMENT 'Applied Rule Ids',
              `additional_data` text COMMENT 'Additional Data',
              `is_qty_decimal` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Is Qty Decimal',
              `no_discount` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'No Discount',
              `qty_backordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Backordered',
              `qty_canceled` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Canceled',
              `qty_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Invoiced',
              `qty_ordered` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Ordered',
              `qty_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Refunded',
              `qty_shipped` decimal(12,4) DEFAULT '0.0000' COMMENT 'Qty Shipped',
              `base_cost` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Cost',
              `price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Price',
              `base_price` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Price',
              `original_price` decimal(12,4) DEFAULT NULL COMMENT 'Original Price',
              `base_original_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Original Price',
              `tax_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Percent',
              `tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Amount',
              `base_tax_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Amount',
              `tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Tax Invoiced',
              `base_tax_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Tax Invoiced',
              `discount_percent` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Percent',
              `discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Amount',
              `base_discount_amount` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Amount',
              `discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Discount Invoiced',
              `base_discount_invoiced` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Discount Invoiced',
              `amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Amount Refunded',
              `base_amount_refunded` decimal(12,4) DEFAULT '0.0000' COMMENT 'Base Amount Refunded',
              `row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Total',
              `base_row_total` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Total',
              `row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Row Invoiced',
              `base_row_invoiced` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Base Row Invoiced',
              `row_weight` decimal(12,4) DEFAULT '0.0000' COMMENT 'Row Weight',
              `base_tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Before Discount',
              `tax_before_discount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Before Discount',
              `ext_order_item_id` varchar(255) DEFAULT NULL COMMENT 'Ext Order Item Id',
              `locked_do_invoice` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Locked Do Invoice',
              `locked_do_ship` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Locked Do Ship',
              `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
              `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
              `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
              `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
              `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
              `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
              `discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Invoiced',
              `base_discount_tax_compensation_invoiced` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Invoiced',
              `discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Refunded',
              `base_discount_tax_compensation_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Refunded',
              `tax_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Tax Canceled',
              `discount_tax_compensation_canceled` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Canceled',
              `tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Tax Refunded',
              `base_tax_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Refunded',
              `discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Discount Refunded',
              `base_discount_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Refunded',
              `free_shipping` smallint(6) DEFAULT NULL,
              `gift_message_id` int(11) DEFAULT NULL COMMENT 'Gift Message Id',
              `gift_message_available` int(11) DEFAULT NULL COMMENT 'Gift Message Available',
              `weee_tax_applied` text COMMENT 'Weee Tax Applied',
              `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
              `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
              `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
              `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
              `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
              `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
              `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
              `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition'
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Item';");
  
            $conn->exec("ALTER TABLE `seller_sales_order_item`
              ADD PRIMARY KEY (`item_id`),
              ADD KEY `SALES_ORDER_ITEM_ORDER_ID` (`order_id`),
              ADD KEY `SALES_ORDER_ITEM_STORE_ID` (`store_id`);");

            $conn->exec("ALTER TABLE `seller_sales_order_item` MODIFY `item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Item Id', AUTO_INCREMENT=1;");  

            $conn->exec("CREATE TABLE `seller_sales_order_grid` (
            `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
            `status` varchar(32) DEFAULT NULL COMMENT 'Status',
            `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
            `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
            `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Customer Id',
            `seller_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'Seller Id',
            `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
            `base_total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Paid',
            `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
            `total_paid` decimal(12,4) DEFAULT NULL COMMENT 'Total Paid',
            `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
            `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
            `order_currency_code` varchar(255) DEFAULT NULL COMMENT 'Order Currency Code',
            `shipping_name` varchar(255) DEFAULT NULL COMMENT 'Shipping Name',
            `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
            `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
            `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
            `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
            `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
            `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
            `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
            `customer_group` varchar(255) DEFAULT NULL COMMENT 'Customer Group',
            `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
            `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
            `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
            `payment_method` varchar(255) DEFAULT NULL COMMENT 'Payment Method',
            `total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Total Refunded'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Order Grid';");
  
          $conn->exec("ALTER TABLE `seller_sales_order_grid`
            ADD PRIMARY KEY (`entity_id`),
            ADD KEY `SALES_ORDER_GRID_STATUS` (`status`),
            ADD KEY `SALES_ORDER_GRID_STORE_ID` (`store_id`),
            ADD KEY `SALES_ORDER_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
            ADD KEY `SALES_ORDER_GRID_BASE_TOTAL_PAID` (`base_total_paid`),
            ADD KEY `SALES_ORDER_GRID_GRAND_TOTAL` (`grand_total`),
            ADD KEY `SALES_ORDER_GRID_TOTAL_PAID` (`total_paid`),
            ADD KEY `SALES_ORDER_GRID_SHIPPING_NAME` (`shipping_name`),
            ADD KEY `SALES_ORDER_GRID_BILLING_NAME` (`billing_name`),
            ADD KEY `SALES_ORDER_GRID_CREATED_AT` (`created_at`),
            ADD KEY `SALES_ORDER_GRID_CUSTOMER_ID` (`customer_id`),
            ADD KEY `SALES_ORDER_GRID_UPDATED_AT` (`updated_at`);");
          $conn->exec("ALTER TABLE `seller_sales_order_grid` ADD FULLTEXT KEY `FTI_65B9E2925EC58F0C7C2E2F6379C233E7` (`increment_id`,`billing_name`,`shipping_name`,`shipping_address`,`billing_address`,`customer_name`,`customer_email`);");
          $conn->exec("ALTER TABLE `seller_sales_order_grid`MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=1;");  
          $conn->exec("SET FOREIGN_KEY_CHECKS=1");
        }
        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order_grid` ADD `order_id` INT(20) NOT NULL AFTER `seller_id`;");
        }


        if(version_compare($context->getVersion(),'1.0.3') < 0)
        {
          /* table seller_sales_invoice*/
          $seller_sales_invoice = "
          CREATE TABLE `seller_sales_invoice` (
              `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
              `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
              `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
              `seller_id` int(10) unsigned NOT NULL COMMENT 'Seller Id',
              `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
              `shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Tax Amount',
              `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
              `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
              `store_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Order Rate',
              `base_shipping_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Tax Amount',
              `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
              `base_to_order_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Order Rate',
              `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
              `shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Amount',
              `subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal Incl Tax',
              `base_subtotal_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal Incl Tax',
              `store_to_base_rate` decimal(12,4) DEFAULT NULL COMMENT 'Store To Base Rate',
              `base_shipping_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Amount',
              `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
              `base_to_global_rate` decimal(12,4) DEFAULT NULL COMMENT 'Base To Global Rate',
              `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
              `base_subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Base Subtotal',
              `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
              `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
              `is_used_for_refund` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Used For Refund',
              `email_sent` smallint(5) unsigned DEFAULT NULL COMMENT 'Email Sent',
              `send_email` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email',
              `can_void_flag` smallint(5) unsigned DEFAULT NULL COMMENT 'Can Void Flag',
              `state` varchar(11) DEFAULT NULL COMMENT 'State',
              `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
              `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
              `transaction_id` varchar(255) DEFAULT NULL COMMENT 'Transaction Id',
              `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
              `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
              `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
              `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
              `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
              `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
              `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
              `shipping_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Discount Tax Compensation Amount',
              `base_shipping_discount_tax_compensation_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Discount Tax Compensation Amount',
              `shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Shipping Incl Tax',
              `base_shipping_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Shipping Incl Tax',
              `base_total_refunded` decimal(12,4) DEFAULT NULL COMMENT 'Base Total Refunded',
              `discount_description` varchar(255) DEFAULT NULL COMMENT 'Discount Description',
              `customer_note` text COMMENT 'Customer Note',
              `customer_note_notify` smallint(5) unsigned DEFAULT NULL COMMENT 'Customer Note Notify',
              PRIMARY KEY (`entity_id`),
              KEY `SALES_INVOICE_STORE_ID` (`store_id`),
              KEY `SALES_INVOICE_GRAND_TOTAL` (`grand_total`),
              KEY `SALES_INVOICE_ORDER_ID` (`order_id`),
              KEY `SALES_INVOICE_STATE` (`state`),
              KEY `SALES_INVOICE_CREATED_AT` (`created_at`),
              KEY `SALES_INVOICE_UPDATED_AT` (`updated_at`),
              KEY `SALES_INVOICE_SEND_EMAIL` (`send_email`),
              KEY `SALES_INVOICE_EMAIL_SENT` (`email_sent`)
              ) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='Seller Sales Flat Invoice';
          ";
          
          $setup->run($seller_sales_invoice);

          /* table seller_sales_invoice_comment */
          $seller_sales_invoice_comment = "
          CREATE TABLE `seller_sales_invoice_comment` (
              `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
              `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
              `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
              `seller_id` int(10) unsigned NOT NULL COMMENT 'Seller Id',
              `is_customer_notified` smallint(5) unsigned DEFAULT NULL COMMENT 'Is Customer Notified',
              `send_email_customer` smallint(5) unsigned DEFAULT NULL COMMENT 'Send Email Customer',
              `is_visible_on_front` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Is Visible On Front',
              `comment` text COMMENT 'Comment',
              `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
              PRIMARY KEY (`entity_id`),
              KEY `SALES_INVOICE_COMMENT_CREATED_AT` (`created_at`),
              KEY `SALES_INVOICE_COMMENT_PARENT_ID` (`parent_id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='Seller Sales Flat Invoice Comment';
          ";
          $setup->run($seller_sales_invoice_comment);

          /* table seller_sales_invoice_grid */
          $seller_sales_invoice_grid = "
          CREATE TABLE `seller_sales_invoice_grid` (
              `entity_id` int(10) unsigned NOT NULL COMMENT 'Entity Id',
              `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
              `state` int(11) DEFAULT NULL COMMENT 'State',
              `store_id` smallint(5) unsigned DEFAULT NULL COMMENT 'Store Id',
              `store_name` varchar(255) DEFAULT NULL COMMENT 'Store Name',
              `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
              `seller_id` int(10) unsigned NOT NULL COMMENT 'Seller Id',
              `order_increment_id` varchar(50) DEFAULT NULL COMMENT 'Order Increment Id',
              `order_created_at` timestamp NULL DEFAULT NULL COMMENT 'Order Created At',
              `customer_name` varchar(255) DEFAULT NULL COMMENT 'Customer Name',
              `customer_email` varchar(255) DEFAULT NULL COMMENT 'Customer Email',
              `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
              `payment_method` varchar(128) DEFAULT NULL COMMENT 'Payment Method',
              `store_currency_code` varchar(3) DEFAULT NULL COMMENT 'Store Currency Code',
              `order_currency_code` varchar(3) DEFAULT NULL COMMENT 'Order Currency Code',
              `base_currency_code` varchar(3) DEFAULT NULL COMMENT 'Base Currency Code',
              `global_currency_code` varchar(3) DEFAULT NULL COMMENT 'Global Currency Code',
              `billing_name` varchar(255) DEFAULT NULL COMMENT 'Billing Name',
              `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
              `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
              `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
              `subtotal` decimal(12,4) DEFAULT NULL COMMENT 'Subtotal',
              `shipping_and_handling` decimal(12,4) DEFAULT NULL COMMENT 'Shipping and handling amount',
              `grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Grand Total',
              `base_grand_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Grand Total',
              `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
              `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
              PRIMARY KEY (`entity_id`),
              KEY `SALES_INVOICE_GRID_STORE_ID` (`store_id`),
              KEY `SALES_INVOICE_GRID_GRAND_TOTAL` (`grand_total`),
              KEY `SALES_INVOICE_GRID_ORDER_ID` (`order_id`),
              KEY `SALES_INVOICE_GRID_STATE` (`state`),
              KEY `SALES_INVOICE_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
              KEY `SALES_INVOICE_GRID_CREATED_AT` (`created_at`),
              KEY `SALES_INVOICE_GRID_UPDATED_AT` (`updated_at`),
              KEY `SALES_INVOICE_GRID_ORDER_CREATED_AT` (`order_created_at`),
              KEY `SALES_INVOICE_GRID_BILLING_NAME` (`billing_name`),
              KEY `SALES_INVOICE_GRID_BASE_GRAND_TOTAL` (`base_grand_total`),
              FULLTEXT KEY `FTI_95D9C924DD6A8734EB8B5D01D60F90DE` (`increment_id`,`order_increment_id`,`billing_name`,`billing_address`,`shipping_address`,`customer_name`,`customer_email`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Seller Sales Flat Invoice Grid';
          ";
          $setup->run($seller_sales_invoice_grid);

          /* table seller_sales_invoice_item */
          $seller_sales_invoice_item = "
          CREATE TABLE `seller_sales_invoice_item` (
              `entity_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Entity Id',
              `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
              `order_id` int(10) unsigned NOT NULL COMMENT 'Order Id',
              `seller_id` int(10) unsigned NOT NULL COMMENT 'Seller Id',
              `base_price` decimal(12,4) DEFAULT NULL COMMENT 'Base Price',
              `tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Tax Amount',
              `base_row_total` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total',
              `discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Amount',
              `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
              `base_discount_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Amount',
              `price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Price Incl Tax',
              `base_tax_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Tax Amount',
              `base_price_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Price Incl Tax',
              `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
              `base_cost` decimal(12,4) DEFAULT NULL COMMENT 'Base Cost',
              `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
              `base_row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Base Row Total Incl Tax',
              `row_total_incl_tax` decimal(12,4) DEFAULT NULL COMMENT 'Row Total Incl Tax',
              `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
              `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
              `additional_data` text COMMENT 'Additional Data',
              `description` text COMMENT 'Description',
              `sku` varchar(255) DEFAULT NULL COMMENT 'Sku',
              `name` varchar(255) DEFAULT NULL COMMENT 'Name',
              `discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Discount Tax Compensation Amount',
              `base_discount_tax_compensation_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Discount Tax Compensation Amount',
              `tax_ratio` text COMMENT 'Ratio of tax invoiced over tax of the order item',
              `weee_tax_applied` text COMMENT 'Weee Tax Applied',
              `weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Amount',
              `weee_tax_applied_row_amount` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Applied Row Amount',
              `weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Disposition',
              `weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Weee Tax Row Disposition',
              `base_weee_tax_applied_amount` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Amount',
              `base_weee_tax_applied_row_amnt` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Applied Row Amnt',
              `base_weee_tax_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Disposition',
              `base_weee_tax_row_disposition` decimal(12,4) DEFAULT NULL COMMENT 'Base Weee Tax Row Disposition',
              PRIMARY KEY (`entity_id`),
              KEY `SALES_INVOICE_ITEM_PARENT_ID` (`parent_id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COMMENT='Seller Sales Flat Invoice Item';
          ";

      }

        if(version_compare($context->getVersion(),'1.0.4') < 0)
        {
            $setup->run("ALTER TABLE `wisi_production`.`seller_sales_invoice_grid` CHANGE COLUMN `entity_id` `entity_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id' , CHANGE COLUMN `state` `state` VARCHAR(11) NULL DEFAULT NULL COMMENT 'State' ; ");
        }
        if(version_compare($context->getVersion(),'1.0.5') < 0)
        {
            $setup->run("ALTER TABLE `seller_sales_invoice_grid` DROP INDEX `SALES_INVOICE_GRID_INCREMENT_ID_STORE_ID`;");
        }
        if(version_compare($context->getVersion(),'1.0.6') < 0)
        {
            $setup->run("ALTER TABLE `seller_sales_invoice` DROP INDEX `SALES_INVOICE_INCREMENT_ID_STORE_ID`;");
        }
        if(version_compare($context->getVersion(),'1.0.7') < 0)
        {
            $setup->run("SET FOREIGN_KEY_CHECKS=0");
            $setup->run("CREATE TABLE `seller_sales_shipment` (
                `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
                `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
                `total_weight` decimal(12,4) DEFAULT NULL COMMENT 'Total Weight',
                `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
                `email_sent` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Email Sent',
                `send_email` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Send Email',
                `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order Id',
                `customer_id` int(11) DEFAULT NULL COMMENT 'Customer Id',
                `shipping_address_id` int(11) DEFAULT NULL COMMENT 'Shipping Address Id',
                `billing_address_id` int(11) DEFAULT NULL COMMENT 'Billing Address Id',
                `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
                `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
                `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
                `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At',
                `packages` text COMMENT 'Packed Products in Packages',
                `shipping_label` mediumblob COMMENT 'Shipping Label Content',
                `customer_note` text COMMENT 'Customer Note',
                `customer_note_notify` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Customer Note Notify'
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment';");
            $setup->run("ALTER TABLE `seller_sales_shipment`
            ADD PRIMARY KEY (`entity_id`),
            ADD KEY `SALES_SHIPMENT_STORE_ID` (`store_id`),
            ADD KEY `SALES_SHIPMENT_TOTAL_QTY` (`total_qty`),
            ADD KEY `SALES_SHIPMENT_ORDER_ID` (`order_id`),
            ADD KEY `SALES_SHIPMENT_CREATED_AT` (`created_at`),
            ADD KEY `SALES_SHIPMENT_UPDATED_AT` (`updated_at`),
            ADD KEY `SALES_SHIPMENT_SEND_EMAIL` (`send_email`),
            ADD KEY `SALES_SHIPMENT_EMAIL_SENT` (`email_sent`);");
            $setup->run("ALTER TABLE `seller_sales_shipment` MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=1;");
            $setup->run("CREATE TABLE `seller_sales_shipment_grid` (
                `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
                `increment_id` varchar(50) DEFAULT NULL COMMENT 'Increment Id',
                `store_id` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'Store Id',
                `order_increment_id` varchar(32) NOT NULL COMMENT 'Order Increment Id',
                `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order Id',
                `order_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Order Increment Id',
                `customer_name` varchar(128) NOT NULL COMMENT 'Customer Name',
                `total_qty` decimal(12,4) DEFAULT NULL COMMENT 'Total Qty',
                `shipment_status` int(11) DEFAULT NULL COMMENT 'Shipment Status',
                `order_status` varchar(32) DEFAULT NULL COMMENT 'Order',
                `billing_address` varchar(255) DEFAULT NULL COMMENT 'Billing Address',
                `shipping_address` varchar(255) DEFAULT NULL COMMENT 'Shipping Address',
                `billing_name` varchar(128) DEFAULT NULL COMMENT 'Billing Name',
                `shipping_name` varchar(128) DEFAULT NULL COMMENT 'Shipping Name',
                `customer_email` varchar(128) DEFAULT NULL COMMENT 'Customer Email',
                `customer_group_id` smallint(6) DEFAULT NULL COMMENT 'Customer Group Id',
                `payment_method` varchar(32) DEFAULT NULL COMMENT 'Payment Method',
                `shipping_information` varchar(255) DEFAULT NULL COMMENT 'Shipping Method Name',
                `created_at` timestamp NULL DEFAULT NULL COMMENT 'Created At',
                `updated_at` timestamp NULL DEFAULT NULL COMMENT 'Updated At',
                `seller_id` int(10) unsigned NOT NULL COMMENT 'Seller Id'
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Grid';");
            $setup->run("
            ALTER TABLE `seller_sales_shipment_grid`
              ADD PRIMARY KEY (`entity_id`),
              ADD KEY `SALES_SHIPMENT_GRID_STORE_ID` (`store_id`),
              ADD KEY `SALES_SHIPMENT_GRID_TOTAL_QTY` (`total_qty`),
              ADD KEY `SALES_SHIPMENT_GRID_ORDER_INCREMENT_ID` (`order_increment_id`),
              ADD KEY `SALES_SHIPMENT_GRID_SHIPMENT_STATUS` (`shipment_status`),
              ADD KEY `SALES_SHIPMENT_GRID_ORDER_STATUS` (`order_status`),
              ADD KEY `SALES_SHIPMENT_GRID_CREATED_AT` (`created_at`),
              ADD KEY `SALES_SHIPMENT_GRID_UPDATED_AT` (`updated_at`),
              ADD KEY `SALES_SHIPMENT_GRID_ORDER_CREATED_AT` (`order_created_at`),
              ADD KEY `SALES_SHIPMENT_GRID_SHIPPING_NAME` (`shipping_name`),
              ADD KEY `SALES_SHIPMENT_GRID_BILLING_NAME` (`billing_name`);");

            $setup->run("ALTER TABLE `seller_sales_shipment_grid` ADD FULLTEXT KEY `FTI_086B40C8955F167B8EA76653437879B4` (`increment_id`,`order_increment_id`,`shipping_name`,`customer_name`,`customer_email`,`billing_address`,`shipping_address`);");

            $setup->run("ALTER TABLE `seller_sales_shipment_grid` CHANGE `entity_id` `entity_id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id';");

            $setup->run("CREATE TABLE `seller_sales_shipment_item` (
                `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
                `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Parent Id',
                `row_total` decimal(12,4) DEFAULT NULL COMMENT 'Row Total',
                `price` decimal(12,4) DEFAULT NULL COMMENT 'Price',
                `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
                `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
                `product_id` int(11) DEFAULT NULL COMMENT 'Product Id',
                `order_item_id` int(11) DEFAULT NULL COMMENT 'Order Item Id',
                `additional_data` text COMMENT 'Additional Data',
                `description` text COMMENT 'Description',
                `name` varchar(255) DEFAULT NULL COMMENT 'Name',
                `sku` varchar(255) DEFAULT NULL COMMENT 'Sku'
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Item';");

            $setup->run("ALTER TABLE `seller_sales_shipment_item`
            ADD PRIMARY KEY (`entity_id`),
            ADD KEY `SALES_SHIPMENT_ITEM_PARENT_ID` (`parent_id`);");

            $setup->run("ALTER TABLE `seller_sales_shipment_item` MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=1;");

            $setup->run("CREATE TABLE `seller_sales_shipment_track` (
                `entity_id` int(10) UNSIGNED NOT NULL COMMENT 'Entity Id',
                `parent_id` int(10) UNSIGNED NOT NULL COMMENT 'Parent Id',
                `weight` decimal(12,4) DEFAULT NULL COMMENT 'Weight',
                `qty` decimal(12,4) DEFAULT NULL COMMENT 'Qty',
                `order_id` int(10) UNSIGNED NOT NULL COMMENT 'Order Id',
                `track_number` text COMMENT 'Number',
                `description` text COMMENT 'Description',
                `title` varchar(255) DEFAULT NULL COMMENT 'Title',
                `carrier_code` varchar(32) DEFAULT NULL COMMENT 'Carrier Code',
                `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created At',
                `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Updated At'
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Sales Flat Shipment Track';");

            $setup->run("ALTER TABLE `seller_sales_shipment_track`
            ADD PRIMARY KEY (`entity_id`),
            ADD KEY `SALES_SHIPMENT_TRACK_PARENT_ID` (`parent_id`),
            ADD KEY `SALES_SHIPMENT_TRACK_ORDER_ID` (`order_id`),
            ADD KEY `SALES_SHIPMENT_TRACK_CREATED_AT` (`created_at`);");

            $setup->run("ALTER TABLE `seller_sales_shipment_track` MODIFY `entity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Entity Id', AUTO_INCREMENT=2;");

            $setup->run("SET FOREIGN_KEY_CHECKS=1");
        }
        if(version_compare($context->getVersion(),'1.0.8') < 0)
        {
            $setup->run("CREATE TABLE `libero_province` ( `id` INT NOT NULL AUTO_INCREMENT , `province_id` INT NOT NULL , `province_name` VARCHAR(150) NOT NULL , `etc` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");

            $setup->run("CREATE TABLE  `libero_district` ( `id` INT NOT NULL AUTO_INCREMENT , `district_id` INT(20) NOT NULL , `district_name` VARCHAR(150) NOT NULL , `position` INT(20) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");

            $setup->run("ALTER TABLE `libero_province` CHANGE `province_name` `province_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");

            $setup->run("ALTER TABLE `libero_district` CHANGE `district_name` `district_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");

            $setup->run("ALTER TABLE `libero_district` ADD `province_id` INT(20) NOT NULL AFTER `position`;");
        }
        if(version_compare($context->getVersion(),'1.0.9') < 0)
        {
            $setup->run("ALTER TABLE `seller_sales_order` ADD `order_code_api` VARCHAR(150) NOT NULL AFTER `paypal_ipn_customer_notified`;");
        }
        if(version_compare($context->getVersion(),'1.0.10') < 0){
            $setup->run("ALTER TABLE `seller_sales_order` ADD `weight_api` INT(150) NOT NULL AFTER `order_code_api`, ADD `length` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `weight`, ADD `width` VARCHAR(150) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL AFTER `length`, ADD `height` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `width`, ADD `from_district_id` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `height`, ADD `to_district_id` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `from_district_id`, ADD `service_id` VARCHAR(150) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL AFTER `to_district_id`, ADD `coupon_code_api` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `service_id`, ADD `insurance_free` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `coupon_code`, ADD `note` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `insurance_free`, ADD `client_contact_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `note`, ADD `client_contact_phone` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `client_contact_name`, ADD `client_address` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `client_contact_phone`, ADD `customer_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `client_address`, ADD `customer_phone` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `customer_name`, ADD `shipping_address` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `customer_phone`, ADD `cod_amount` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `shipping_address`;");
        }
        if(version_compare($context->getVersion(),'1.0.11') < 0){
            $setup->run("CREATE TABLE `sales_order_seller_logs` ( `id_logs` INT NOT NULL AUTO_INCREMENT , `seller_id` VARCHAR(150) NOT NULL , `order_id` VARCHAR(150) NOT NULL , `content` TEXT NOT NULL , `etc` TEXT NOT NULL , PRIMARY KEY (`id_logs`)) ENGINE = InnoDB;");
        }
        if(version_compare($context->getVersion(),'1.0.12') < 0){
            $setup->run("ALTER TABLE `seller_sales_order` CHANGE COLUMN `created_at` `created_at` VARCHAR(255) NOT NULL COMMENT 'Created At' , CHANGE COLUMN `updated_at` `updated_at` VARCHAR(255) NOT NULL COMMENT 'Updated At' ;");
            $setup->run("ALTER TABLE `seller_sales_order_grid` CHANGE COLUMN `created_at` `created_at` VARCHAR(255) NULL COMMENT 'Created At' , CHANGE COLUMN `updated_at` `updated_at` VARCHAR(255) NULL COMMENT 'Updated At' ;");
            $setup->run("ALTER TABLE `seller_sales_order_item` CHANGE COLUMN `created_at` `created_at` VARCHAR(255) NOT NULL COMMENT 'Created At' , CHANGE COLUMN `updated_at` `updated_at` VARCHAR(255) NOT NULL COMMENT 'Updated At' ;");
            $setup->run("ALTER TABLE `seller_sales_invoice` CHANGE COLUMN `created_at` `created_at` VARCHAR(255) NOT NULL COMMENT 'Created At' , CHANGE COLUMN `updated_at` `updated_at` VARCHAR(255) NOT NULL COMMENT 'Updated At' ;");
            $setup->run("ALTER TABLE `seller_sales_invoice_comment` CHANGE COLUMN `created_at` `created_at` VARCHAR(255) NOT NULL COMMENT 'Created At' ;");
            $setup->run("ALTER TABLE `seller_sales_invoice_grid` CHANGE COLUMN `created_at` `created_at` VARCHAR(255) NULL COMMENT 'Created At' , CHANGE COLUMN `updated_at` `updated_at` VARCHAR(255) NULL COMMENT 'Updated At' ;");
            $setup->run("ALTER TABLE `seller_sales_invoice_item` ADD COLUMN `created_at` VARCHAR(255) NULL AFTER `base_weee_tax_row_disposition`, ADD COLUMN `updated_at` VARCHAR(255) NULL AFTER `created_at`;");
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD COLUMN `created_at` VARCHAR(255) NULL AFTER `hot_line`, ADD COLUMN `updated_at` VARCHAR(255) NULL AFTER `created_at`;");
            $setup->run("ALTER TABLE `sales_order_seller_logs` ADD COLUMN `created_at` VARCHAR(255) NULL AFTER `etc`;");
            $setup->run("ALTER TABLE `seller_sales_order` ADD COLUMN `status_code` VARCHAR(5) NULL COMMENT '1: pending\n2: processing\n3: prepared to ship' AFTER `status`;");
            $setup->run("ALTER TABLE `seller_sales_order_grid` ADD COLUMN `status_code` VARCHAR(5) NULL COMMENT '1: pending\n2: processing\n3: prepared to ship' AFTER `status`;");
            $setup->run("ALTER TABLE `libero_onestepcheckout_shipping_method` ADD COLUMN `created_at` VARCHAR(255) NULL AFTER `price`, ADD COLUMN `updated_at` VARCHAR(255) NULL AFTER `created_at`;");
        }
        if(version_compare($context->getVersion(),'1.0.13') < 0){
            $setup->run("ALTER TABLE `seller_sales_invoice_grid` CHANGE COLUMN `order_created_at` `order_created_at` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Order Created At' ;");
        }
        if(version_compare($context->getVersion(),'1.0.14') < 0){
            $setup->run("CREATE TABLE `libero_delivery_company` ( `id_delivery` INT NOT NULL AUTO_INCREMENT , `name_delivery` VARCHAR(150) CHARACTER SET utf16 COLLATE utf16_unicode_ci NOT NULL , `img_delivery` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `etc` TEXT NOT NULL , PRIMARY KEY (`id_delivery`)) ENGINE = InnoDB;");
        }
        if(version_compare($context->getVersion(),'1.0.15') < 0){
            $setup->run("INSERT INTO `libero_delivery_company` (`id_delivery`, `name_delivery`, `img_delivery`, `etc`) VALUES (NULL, 'Giao Hàng Nhanh', '', ''), (NULL, 'Giao Hàng Tiết Kiệm', '', '');");
        }

        if(version_compare($context->getVersion(),'1.0.16') < 0){
            $setup->run("INSERT INTO `libero_delivery_company` (`name_delivery`, `img_delivery`) VALUES ('Giao Hàng Tiết Kiệm', 'delivery_company/giao-hang-tiet-kiem.png');");
            $setup->run("UPDATE `libero_delivery_company` SET `name_delivery`='Giao Hàng Nhanh', `img_delivery`='delivery_company/ghn-logo.png' WHERE `id_delivery`='2';");
            $setup->run("UPDATE `libero_delivery_company` SET `name_delivery`='Giao Hàng Miễn Phí', `img_delivery`='delivery_company/free-ship.png' WHERE `id_delivery`='1';");
        }
        if(version_compare($context->getVersion(),'1.0.17') < 0){
            $setup->run("ALTER TABLE `libero_onestepcheckout_shipping_method` ADD COLUMN `shipping_code` VARCHAR(45) NOT NULL AFTER `id_shipping_method`;");
        }
        if (version_compare($context->getVersion(), '1.0.18') < 0) {
            $setup->run("ALTER TABLE `quote_item` ADD `id_seller` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `base_weee_tax_row_disposition`;");
        }
        if (version_compare($context->getVersion(), '1.0.19') < 0) {
            //$setup->run("ALTER TABLE `libero_delivery_company` DROP PRIMARY KEY;");
            $setup->run("ALTER TABLE `libero_delivery_company` CHANGE COLUMN `id_delivery` `id_delivery` VARCHAR(20) NOT NULL , ADD COLUMN `entity_id` INT NOT NULL AUTO_INCREMENT FIRST, DROP PRIMARY KEY, ADD PRIMARY KEY (`entity_id`);");
        }
        if (version_compare($context->getVersion(), '1.0.20') < 0) {
            //$setup->run("ALTER TABLE `libero_delivery_company` CHANGE `id_delivery` `id_delivery` VARCHAR(20) NOT NULL;");
        }
        if (version_compare($context->getVersion(), '1.0.21') < 0) {
            //$setup->run("ALTER TABLE `libero_delivery_company` ADD `entity_id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`entity_id`);");
        }
        if (version_compare($context->getVersion(), '1.0.22') < 0) {
            $setup->run("CREATE TABLE `libero_quote_shipping_price` ( `id_quote_price` INT NOT NULL AUTO_INCREMENT , `quote_id` VARCHAR(20) NOT NULL , `name_shipping` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `fee_shipping` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `id_seller` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_quote_price`)) ENGINE = InnoDB;");
        }
        if (version_compare($context->getVersion(), '1.0.23') < 0) {
            $setup->run("CREATE TABLE `libero_ward` ( `id` INT NOT NULL AUTO_INCREMENT, `ward_id` VARCHAR(255) NOT NULL, `ward_name` VARCHAR(255) NOT NULL, `district_id` VARCHAR(255) NOT NULL, `province_id` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`));");
        }
        if(version_compare($context->getVersion(), '1.0.24') < 0){
            $setup->run("ALTER TABLE `libero_delivery_company` CHANGE `id_delivery` `id_delivery` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
            $setup->run("UPDATE `libero_delivery_company` SET `id_delivery` = 'giao_hang_tiet_kiem' WHERE `libero_delivery_company`.`name_delivery` like '%Giao Hàng Tiết Kiệm%'");
            $setup->run("UPDATE `libero_delivery_company` SET `id_delivery` = 'giao_hang_mien_phi' WHERE `libero_delivery_company`.`name_delivery` like '%Giao Hàng Miễn Phí%'");
            $setup->run("UPDATE `libero_delivery_company` SET `id_delivery` = 'giao_hang_nhanh' WHERE `libero_delivery_company`.`name_delivery` like '%Giao Hàng Nhanh%'");
        }
        if(version_compare($context->getVersion(), '1.0.25') < 0){
            /*$setup->run("ALTER TABLE `libero_delivery_company` CHANGE `id_delivery` `id_delivery` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
            $setup->run("UPDATE `libero_delivery_company` SET `id_delivery` = 'giao_hang_tiet_kiem' WHERE `libero_delivery_company`.`name_delivery` like '%Giao Hàng Tiết Kiệm%'");
            $setup->run("UPDATE `libero_delivery_company` SET `id_delivery` = 'giao_hang_mien_phi' WHERE `libero_delivery_company`.`name_delivery` like '%Giao Hàng Miễn Phí%'");
            $setup->run("UPDATE `libero_delivery_company` SET `id_delivery` = 'giao_hang_nhanh' WHERE `libero_delivery_company`.`name_delivery` like '%Giao Hàng Nhanh%'");*/
        }
        if(version_compare($context->getVersion(), '1.0.26') < 0){
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order_grid'),
                'request_type',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'comment' => 'Request Type',
                    'length' => 255
                ]
            );
            $setup->getConnection()->addColumn(
                $setup->getTable('sales_order_grid'),
                'status_request',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'comment' => 'Status Request',
                    'length' => 30
                ]
            );
        }
        if(version_compare($context->getVersion(), '1.0.27') < 0){
            $setup->run("ALTER TABLE `libero_quote_shipping_price` ADD `weight` VARCHAR(50) NOT NULL AFTER `id_seller`, ADD `length` VARCHAR(50) NOT NULL AFTER `weight`, ADD `width` VARCHAR(50) NOT NULL AFTER `length`, ADD `height` VARCHAR(50) NOT NULL AFTER `width`, ADD `from_district` VARCHAR(250) NOT NULL AFTER `height`, ADD `from_province` VARCHAR(250) NOT NULL AFTER `from_district`, ADD `to_district` VARCHAR(250) NOT NULL AFTER `from_province`, ADD `to_province` VARCHAR(250) NOT NULL AFTER `to_district`, ADD `service_id` VARCHAR(50) NOT NULL AFTER `to_province`, ADD `code_delivery` VARCHAR(50) NOT NULL AFTER `service_id`, ADD `order_id` VARCHAR(150) NOT NULL AFTER `code_delivery`;");
        }
        if(version_compare($context->getVersion(), '1.0.28') < 0){
            $setup->run("ALTER TABLE `libero_quote_shipping_price` CHANGE `from_district` `from_district` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL, CHANGE `from_province` `from_province` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL, CHANGE `to_district` `to_district` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL, CHANGE `to_province` `to_province` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        }
        if(version_compare($context->getVersion(), '1.0.29') < 0){
            $setup->run("ALTER TABLE `sales_order` ADD `request_type` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `paypal_ipn_customer_notified`, ADD `status_request` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `request_type`;");
        }
        if(version_compare($context->getVersion(), '1.0.30') < 0){
            $setup->run("ALTER TABLE `seller_sales_order` ADD `provider_name` VARCHAR(750) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `customer_name`;");
        }
        if (version_compare($context->getVersion(), '1.0.31') < 0) {
            $setup->run("CREATE TABLE `libero_order_status` ( `id_status` INT NOT NULL AUTO_INCREMENT , `code_status` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `label_status` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_status`)) ENGINE = InnoDB;");
        }
        if (version_compare($context->getVersion(), '1.0.32') < 0) {
            $setup->run("CREATE TABLE `libero_order_mapping_status_shipping` ( `id_mapping` INT NOT NULL AUTO_INCREMENT , `shipping_company` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `code_shipping_status` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `label_shipping_status` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `id_wisi_status` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_mapping`)) ENGINE = InnoDB;");
        }
        if (version_compare($context->getVersion(), '1.0.33') < 0) {
            $setup->run("CREATE TABLE `libero_order_log_change_order` ( `id_change_status` INT NOT NULL AUTO_INCREMENT , `order_id` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `increament_id` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `provider_id` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `provider_name` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `from_code_status` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `from_label_status` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `to_code_status` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `to_label_status` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `change_time` DATETIME NOT NULL , `infor_change` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_change_status`)) ENGINE = InnoDB;");
        }
        if (version_compare($context->getVersion(), '1.0.34') < 0) {
            $setup->run("CREATE TABLE `libero_order_cron_status_shipping` ( `id_cron` INT NOT NULL AUTO_INCREMENT , `order_id` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `shipping_id` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `shipping_company` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `status_run` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `from_status` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `to_status` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_cron`)) ENGINE = InnoDB;");
        }
        if (version_compare($context->getVersion(), '1.0.35') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order` ADD `phi_bao_hiem` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '0' AFTER `type_of_order`;");
        }
        $setup->endSetup();
    }
}





