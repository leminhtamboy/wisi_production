<?php

namespace Libero\Onestepcheckout\Observer;

class SaveBefore implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        //$text = __("");
        $order->setData("request_type","Direct Shipping");
        $order->setData("status_request","unconfirmed");
        return $this;
    }
}