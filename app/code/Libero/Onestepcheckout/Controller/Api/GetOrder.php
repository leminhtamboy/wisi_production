<?php
namespace Libero\Onestepcheckout\Controller\Api;

class GetOrder extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            //get Params
            $post_data = $this->getRequest()->getPostValue();
            $order_code = $post_data["order_code"];
            //get helper
            #Libero\Onestepcheckout\Helper\Data
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
            $post_data_to_send = array();
            $post_data_to_send["token"] = $helper->getTokenKeyApiGiaoHangNhanh();
            $post_data_to_send["OrderCode"] = $order_code;
            $url = $helper->getUrlInforOrder();
            $data = $helper->callUrl($url,$post_data_to_send);
            $data = \Zend_Json::decode($data);
            $result = $jsonResultFactory->setData($data);
            return $result;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
