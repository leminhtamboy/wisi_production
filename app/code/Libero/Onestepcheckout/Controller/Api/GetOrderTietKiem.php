<?php
namespace Libero\Onestepcheckout\Controller\Api;

class GetOrderTietKiem extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            //get Params
            $post_data = $this->getRequest()->getPostValue();
            $order_code = $post_data["order_code"];
            //get helper
            #Libero\Onestepcheckout\Helper\Data
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
            $post_data_to_send = array();
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://services.giaohangtietkiem.vn/services/shipment/v2/".$order_code,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_HTTPHEADER => array(
                    "Token: 2181547E3df02EBE2250D8954c3cf5E15d9bCe41",
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $data = $response;
            $data = \Zend_Json::decode($data);
            $result = $jsonResultFactory->setData($data);
            return $result;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
