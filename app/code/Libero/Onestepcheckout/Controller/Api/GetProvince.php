<?php
namespace Libero\Onestepcheckout\Controller\Api;

class GetProvince extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            //get helper
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
            $connectionLiberoProvince = $helper->getConnectionDatabaseDirect();
            $sql = "SELECT * FROM libero_province";
            $data_province = $connectionLiberoProvince->fetchAll($sql);
            //consider
            //$data_json = json_encode($data_province);
            $result = $jsonResultFactory->setData($data_province);
            return $result;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
