<?php
namespace Libero\Onestepcheckout\Controller\Api;

class GetWard extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            //get Params
            $post_data = $this->getRequest()->getParams();
            $district_id = $post_data["district"];
            //get helper
            #Libero\Onestepcheckout\Helper\Data
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
            $post_data_to_send = array();
            $post_data_to_send["token"] = $helper->getTokenKeyApiGiaoHangNhanh();
            $post_data_to_send["DistrictID"] = intval($district_id);
            $url = $helper->getUrlGetWardGiaoHangNhanh();
            $data_wards = $helper->callUrl($url,$post_data_to_send);
            $data_wards = \Zend_Json::decode($data_wards);
            $result = $jsonResultFactory->setData($data_wards);
            return $result;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
