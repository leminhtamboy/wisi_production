<?php
namespace Libero\Onestepcheckout\Controller\Api;

class PlaceOrder extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            //get Params
            $post_data = $this->getRequest()->getPostValue();
            $weight = $post_data["weight"];
            $length = $post_data["length"];
            $width = $post_data["width"];
            $height = $post_data["height"];
            $from_distric = $post_data["from_district_id"];
            $to_distric = $post_data["to_district_id"];
            $service_id = $post_data["service_id"];
            $coupon_code = $post_data["coupon_code"];
            $insurance_free = $post_data["insurance_free"];
            $note = $post_data["note"];
            $name_seller = $post_data["client_contact_name"];
            $phone_seller = $post_data["client_contact_phone"];
            $address_seller = $post_data["client_address"];
            $customer_name = $post_data["customer_name"];
            $customer_phone = $post_data["customer_phone"];
            $shipping_address = $post_data["shipping_address"];
            $grantotal_order = $post_data["cod_amount"];
            //get helper
            #Libero\Onestepcheckout\Helper\Data
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
            $post_data_to_send = array();
            $post_data_to_send["token"] = $helper->getTokenKeyApiGiaoHangNhanh();
            $post_data_to_send["PaymentTypeID"] = 2;
            $post_data_to_send["FromDistrictID"] = intval($from_distric);
            $post_data_to_send["ToDistrictID"] = intval($to_distric);
            $post_data_to_send["Note"] = $note;
            $post_data_to_send["SealCode"] = "tem niêm phong";
            $post_data_to_send["ExternalCode"] = "";
            $post_data_to_send["ClientContactName"] = $name_seller;
            $post_data_to_send["ClientContactPhone"] = $phone_seller;
            $post_data_to_send["ClientAddress"] = $address_seller;
            $post_data_to_send["CustomerName"] = $customer_name;
            $post_data_to_send["CustomerPhone"] = $customer_phone;
            $post_data_to_send["ShippingAddress"] = $shipping_address;
            $post_data_to_send["CoDAmount"] = floatval($grantotal_order);
            $post_data_to_send["NoteCode"] = "CHOXEMHANGKHONGTHU";
            $post_data_to_send["InsuranceFee"] = 0;
            $post_data_to_send["ClientHubID"] = 0;
            $post_data_to_send["ServiceID"] = intval($service_id);
            $post_data_to_send["Content"] = "";
            $post_data_to_send["Weight"] = intval($weight);
            $post_data_to_send["Length"] = intval($length);
            $post_data_to_send["Width"] = intval($width);
            $post_data_to_send["Height"] = intval($height);
            $post_data_to_send["CheckMainBankAccount"] = false;
            $post_data_to_send["ReturnContactName"] = "";
            $post_data_to_send["ReturnContactPhone"] = "";
            $post_data_to_send["ReturnAddress"] = "";
            $post_data_to_send["ReturnDistrictCode"] = "";
            $post_data_to_send["ExternalReturnCode"] = "";
            $post_data_to_send["IsCreditCreate"] = true;
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info(print_r($post_data_to_send, true));
            $url = $helper->getUrlPlaceOrder();
            $data = $helper->callUrl($url,$post_data_to_send);
            $data = \Zend_Json::decode($data);
            $logger->info(print_r($data, true));
            $logger->info(print_r(json_encode($post_data_to_send), true));
            $result = $jsonResultFactory->setData($data);
            return $result;

        }catch (\Exception $e){
			$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
            $logger = new \Zend\Log\Logger();
            $this->messageManager->addError($e->getMessage());
        }
    }
}
