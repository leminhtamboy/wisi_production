<?php
namespace Libero\Onestepcheckout\Controller\Api;

class CalculatedShipping extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            //get Params
            $post_data = $this->getRequest()->getPostValue();
            $weight = $post_data["weight"];
            $length = $post_data["length"];
            $width = $post_data["width"];
            $height = $post_data["height"];
            $from_distric = $post_data["from_district_id"];
            $to_distric = $post_data["to_district_id"];
            $service_id = $post_data["service_id"];
            $coupon_code = $post_data["coupon_code"];
            $insurance_free = $post_data["insurance_free"];
            //get helper
            #Libero\Onestepcheckout\Helper\Data
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
            $post_data_to_send = array();
            $post_data_to_send["token"] = $helper->getTokenKeyApiGiaoHangNhanh();
            $post_data_to_send["Weight"] = intval($weight);
            $post_data_to_send["Length"] = intval($length);
            $post_data_to_send["Width"] = intval($width);
            $post_data_to_send["Height"] = intval($height);
            $post_data_to_send["FromDistrictID"] = intval($from_distric);
            $post_data_to_send["ToDistrictID"] = intval($to_distric);
            $post_data_to_send["ServiceID"] = intval($service_id);
            $arrayAdditionalService = array();
            $post_data_to_send["OrderCosts"] = $arrayAdditionalService;
            $post_data_to_send["CouponCode"] = $coupon_code;
            $post_data_to_send["InsuranceFee"] = intval($insurance_free);
            $url = $helper->getUrlCalculatedFee();
            $data_wards = $helper->callUrl($url,$post_data_to_send);
            $data_wards = \Zend_Json::decode($data_wards);
            $result = $jsonResultFactory->setData($data_wards);
            return $result;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
