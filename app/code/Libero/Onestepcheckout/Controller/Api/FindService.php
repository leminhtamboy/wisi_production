<?php
namespace Libero\Onestepcheckout\Controller\Api;

class FindService extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            //get Params
            $post_data = $this->getRequest()->getPostValue();
            $weight = $post_data["weight"];
            $length = $post_data["length"];
            $width = $post_data["width"];
            $height = $post_data["height"];
            $from_distric = $post_data["from_district_id"];
            $to_distric = $post_data["to_district_id"];
            //get helper
            #Libero\Onestepcheckout\Helper\Data
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
            $post_data_to_send = array();
            $post_data_to_send["token"] = $helper->getTokenKeyApiGiaoHangNhanh();
            $post_data_to_send["Weight"] = intval($weight);
            $post_data_to_send["Length"] = intval($length);
            $post_data_to_send["Width"] = intval($width);
            $post_data_to_send["Height"] = intval($height);
            $post_data_to_send["FromDistrictID"] = intval($from_distric);
            $post_data_to_send["ToDistrictID"] = intval($to_distric);
            $url = $helper->getUrlFindServiceAvailable();
            $data = $helper->callUrl($url,$post_data_to_send);
            $data = \Zend_Json::decode($data);
            $result = $jsonResultFactory->setData($data);
            return $result;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
