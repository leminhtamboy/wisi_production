<?php
namespace Libero\Onestepcheckout\Controller\Api;

class PlaceOrderTietKiem extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            //get Params
            $post_data = $this->getRequest()->getPostValue();
            $order_id = $post_data["order_id"];
            $weight = $post_data["weight"];
            $length = $post_data["length"];
            $width = $post_data["width"];
            $height = $post_data["height"];
            $from_distric = $post_data["from_district_id"];
            $to_distric = $post_data["to_district_id"];
            $from_province = $post_data["from_province"];
            $to_province = $post_data["to_province"];
            $service_id = $post_data["service_id"];
            $coupon_code = $post_data["coupon_code"];
            $insurance_free = $post_data["insurance_free"];
            $note = $post_data["note"];
            $name_seller = $post_data["client_contact_name"];
            $phone_seller = $post_data["client_contact_phone"];
            $address_seller = $post_data["client_address"];
            $customer_name = $post_data["customer_name"];
            $customer_phone = $post_data["customer_phone"];
            $shipping_address = $post_data["shipping_address"];
            $grantotal_order = $post_data["cod_amount"];
            $comment = $post_data["comment_text"];
            //get helper
            #Libero\Onestepcheckout\Helper\Data
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
            /*$order = array();
            $item_to_post = array();
            $item_to_post["id"] = $order_id;
            $item_to_post["pick_name"] = $name_seller;
            $item_to_post["pick_address"] = $address_seller;
            $item_to_post["pick_province"] = $from_province;
            $item_to_post["pick_district"] = $from_distric;
            $item_to_post["pick_tel"] = $phone_seller;
            $item_to_post["tel"] = $customer_phone;
            $item_to_post["name"] = $customer_name;
            $item_to_post["address"] = $shipping_address;
            $item_to_post["province"] = $to_province;
            $item_to_post["district"] = $to_distric;
            $item_to_post["is_freeship"] = "0";
            $item_to_post["pick_money"] =  $grantotal_order;
            $item_to_post["note"] = "Khối lượng tính cước tối đa: 450 gram";
            $item_to_post["value"] = 0;
            $order["order"] = $item_to_post;*/
            //Region
            if($insurance_free == 0){
                $order = 
                '{
                    "order": {
                        "id": "'.$order_id.'",
                        "pick_name": "'.$name_seller.'",
                        "pick_address": "'.$address_seller.'",
                        "pick_province": "'.$from_province.'",
                        "pick_district": "'.$from_distric.'",
                        "pick_tel": "'.$phone_seller.'",
                        "tel": "'.$customer_phone.'",
                        "name": "'.$customer_name.'",
                        "address": "'.$shipping_address.'",
                        "province": "'.$to_province.'",
                        "district": "'.$to_distric.'",
                        "is_freeship": "1",
                        "pick_money": '.$grantotal_order.',
                        "note": "'.$comment.'",
                        "value": 0
                    }
                }'
                ;
            }else{
                $order = 
                '{
                    "order": {
                        "id": "'.$order_id.'",
                        "pick_name": "'.$name_seller.'",
                        "pick_address": "'.$address_seller.'",
                        "pick_province": "'.$from_province.'",
                        "pick_district": "'.$from_distric.'",
                        "pick_tel": "'.$phone_seller.'",
                        "tel": "'.$customer_phone.'",
                        "name": "'.$customer_name.'",
                        "address": "'.$shipping_address.'",
                        "province": "'.$to_province.'",
                        "district": "'.$to_distric.'",
                        "is_freeship": "1",
                        "pick_money": '.$grantotal_order.',
                        "note": "'.$comment.'"
                    }
                }'
                ;
            }
            //End region
            //$json_order = json_encode($order);
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://services.giaohangtietkiem.vn/services/shipment/order",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $order,
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "Token: 2181547E3df02EBE2250D8954c3cf5E15d9bCe41",
                    "Content-Length: " . strlen($order),
                ),
            ));            
            $response = curl_exec($curl);
            curl_close($curl);
            //echo $response;
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info(print_r($response, true));
            $logger->info(print_r($post_data, true));
            $data = \Zend_Json::decode($response);
            //$logger->info(print_r($data, true));
            //$logger->info(print_r(json_encode($post_data_to_send), true));
            $result = $jsonResultFactory->setData($data);
            return $result;

        }catch (\Exception $e){
            $this->messageManager->addError($e->getMessage());
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info(print_r($e->getMessage(), true));
        }
    }
}
