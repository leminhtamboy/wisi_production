<?php
namespace Libero\Onestepcheckout\Controller\Api;

class CreateOrder extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
	protected $cartRepositoryInterface;
    protected $_storeManager;
    protected $cartManagementInterface;
    protected $customerFactory;
    protected $customerRepository;
    protected $_product;
    protected $order;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Product $product,
        \Magento\Quote\Api\CartRepositoryInterface $cartRepositoryInterface,
        \Magento\Quote\Api\CartManagementInterface $cartManagementInterface,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Sales\Model\Order $order
    ){
        $this->resultPageFactory = $resultPageFactory;
		$this->_storeManager = $storeManager;
        $this->_product = $product;
        $this->cartRepositoryInterface = $cartRepositoryInterface;
        $this->cartManagementInterface = $cartManagementInterface;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->order = $order;
        parent::__construct($context);
    }
    public function execute(){
        try {
            //get Params
			$raw_data = file_get_contents("php://input");
            //$post_data = $this->getRequest()->getParams();
			$post_data = json_decode($raw_data,true);
			//var_dump($post_data);
			//die;
            //get helper
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            
            $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
            $orderData = $post_data;
            $orderData=[
                'currency_id'  => $post_data["currency_id"],
                'email'        => $post_data["email"], //buyer email id
                'shipping_address' =>[
                    'firstname'    => $post_data["shipping_address"][0]["firstname"], //address Details
                    'lastname'     => $post_data["shipping_address"][0]["lastname"],
                    'street' => $post_data["shipping_address"][0]["street"],
                    'city' => $post_data["shipping_address"][0]["city"],
                    'country_id' => $post_data["shipping_address"][0]["country_id"],
                    'region' => $post_data["shipping_address"][0]["region"],
                    'postcode' => $post_data["shipping_address"][0]["postcode"],
                    'telephone' => $post_data["shipping_address"][0]["telephone"],
                    'fax' => $post_data["shipping_address"][0]["fax"],
                    'save_in_address_book' => 1
                ],
                'items'=> [ //array of product which order you want to create
                    ['product_id'=>'2','qty'=>1]
                ]
            ];    
            $items = $post_data["items"];
            $arrayAll = array();
            foreach($items as $item){
                $arrayItem = array();
                $arrayItem["product_id"] = $item["product_id"];
                $arrayItem["qty"] = $item["qty"];
				$arrayItem["price"] = $item["price"];
                $arrayAll[]  = $arrayItem;
            }
            $orderData["items"] = $arrayAll;
            $result['error'] = 0;
            $result['order_id']= json_encode($orderData);
            /*$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($orderData);
            $logger->info(print_r($orderData));*/
            $result = $this->createMageOrder($orderData);
            //consider
            $result = $jsonResultFactory->setData($result);
            return $result;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
    /**
     * Create Order On Your Store
     * 
     * @param array $orderData
     * @return array
     * 
    */
    public function createMageOrder($orderData) {
        $store=$this->_storeManager->getStore();
        $websiteId = $this->_storeManager->getStore()->getWebsiteId();
        $customer=$this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($orderData['email']);// load customet by email address
        if(!$customer->getEntityId()){
            //If not avilable then create this customer 
            $customer->setWebsiteId($websiteId)
                    ->setStore($store)
                    ->setFirstname($orderData['shipping_address']['firstname'])
                    ->setLastname($orderData['shipping_address']['lastname'])
                    ->setEmail($orderData['email']) 
                    ->setPassword("ZXCasdqwe123");
            $customer->save();
        }
         
        $cartId = $this->cartManagementInterface->createEmptyCart(); //Create empty cart
        $quote = $this->cartRepositoryInterface->get($cartId); // load empty cart quote
        $quote->setStore($store);
        // if you have allready buyer id then you can load customer directly 
        $customer= $this->customerRepository->getById($customer->getEntityId());
        $quote->setCurrency();
        $quote->assignCustomer($customer); //Assign quote to customer
 
        //add items in quote
		
        foreach($orderData['items'] as $item){
			$modelProduct = $this->_objectManager->create("\Magento\Catalog\Model\Product");
            $product=$modelProduct->load($item['product_id']);
            $product->setPrice($item['price']);
            $quote->addProduct($product, intval($item['qty']));
        }
 
        //Set Address to quote
        $quote->getBillingAddress()->addData($orderData['shipping_address']);
        $quote->getShippingAddress()->addData($orderData['shipping_address']);
 
        // Collect Rates and Set Shipping & Payment Method
 
        $shippingAddress=$quote->getShippingAddress();
        $shippingAddress->setCollectShippingRates(true)
                        ->collectShippingRates()
                        ->setShippingMethod('freeshipping_freeshipping'); //shipping method
        $quote->setPaymentMethod('checkmo'); //payment method
        $quote->setInventoryProcessed(false); //not effetc inventory
 
        // Set Sales Order Payment
        $quote->getPayment()->importData(['method' => 'checkmo']);
        $quote->save(); //Now Save quote and your quote is ready
 
        // Collect Totals
        $quote->collectTotals();
 
        // Create Order From Quote
        $quote = $this->cartRepositoryInterface->get($quote->getId());
        $orderId = $this->cartManagementInterface->placeOrder($quote->getId());
        $order = $this->order->load($orderId);
        $order->setData("request_type","Request From TMS");
        $order->setData("status_request","unconfirmed");
        $order->save();
        $order->setEmailSent(0);
        $increment_id = $order->getRealOrderId();
        if($order->getEntityId()){
            $result['error'] = 0;
            $result['order_id']= $order->getRealOrderId();
        }else{
            $result=['error'=>1,'msg'=>'Fail To Create Order'];
        }
        return $result;
    }
}
