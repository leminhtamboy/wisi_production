<?php
namespace Libero\Onestepcheckout\Controller\Ajax;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class GetFee extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $helperCheckout = $objectManager->get("\Libero\Onestepcheckout\Helper\Data");
            $postData = $this->getRequest()->getPostValue();
            $shipping_code = $postData["shipping_code"];
            $weight = $postData["weight"];
            $price = $postData["price"];
            $from_district_id = $postData["from_district"];
            $to_district_id = $postData["to_district"];
            $quote_id = $postData["quote_id"];
            $shipping_name = $postData["shiping_name"];
            $seller_id = $postData["seller_id"];
            $array_ret_json = array();
            $weight_save = "";
            $length_save = "";
            $width_save = "";
            $height_save = "";
            $from_district_save = "";
            $from_province_save = "";
            $to_district_save = "";
            $to_province_save = "";
            $service_id_save = "";
            $code_delivery_save = "";
            $order_id_save = "";
            if($weight == ""){
                $weight = 0;
            }
            $weight_check = round($weight,0);
            if($shipping_code == $helperCheckout->_CODE_GIAO_HANG_MIEN_PHI){
                $array_ret_json["error"] = false;
                $array_ret_json["fee"] = 0;
            }
            if($shipping_code == $helperCheckout->_CODE_GIAO_HANG_NHANH){
                $post_data_to_send  = array();
                $post_data_to_send["weight"] = $weight;
                $post_data_to_send["length"] = 5;
                $post_data_to_send["width"] = 5;
                $post_data_to_send["height"] = 5;
                $post_data_to_send["from_district_id"] = $from_district_id;
                if($to_district_id == ""){
                    $to_district_id = $from_district_id;
                }
                $post_data_to_send["to_district_id"] = $to_district_id;
                $post_data_to_send["service_id"] = 53320;
                $post_data_to_send["coupon_code"] = "";
                $post_data_to_send["insurance_free"] = 0;
                $service_id = 53320;
                if($weight_check > 0){
                    $responseService = $helperCheckout->getDataServiceAvailable($post_data_to_send);
                    $json_service = json_decode($responseService,true);
                    foreach($json_service["data"] as $_service){
                        if($_service["Name"] == "Chuẩn"){
                            $service_id = $_service["ServiceID"]; // dang lat service chuan
                        }
                    }
                    $post_data_to_send["service_id"] = $service_id;
                    $response = $helperCheckout->getDataCalculatedFee($post_data_to_send);
                    $json_data = json_decode($response);
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = $json_data->data->CalculatedFee;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_NHANH;
                }else{
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = 0;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_MIEN_PHI;
                    $shipping_name = $helperCheckout->_NAME_GIAO_HANG_MIEN_PHI;
                }
                $from_district_save = $from_district_id;
                $from_province_save = "";
                $to_district_save = $to_district_id;
                $to_province_save = "";
                $service_id_save = $service_id;
            }
            if($shipping_code == $helperCheckout->_CODE_GIAO_HANG_TIET_KIEM){
                $post_data_to_send = array();
                $pick_province = $helperCheckout->getNameProvince($from_district_id);
                $pick_district = $helperCheckout->getNameDistrict($from_district_id);
                if($to_district_id == ""){
                    $to_district_id = $from_district_id;
                }
                $to_province = $helperCheckout->getNameProvince($to_district_id);
                $to_district = $helperCheckout->getNameDistrict($to_district_id);
                $post_data_to_send["pick_province"] = $pick_province;
                $post_data_to_send["pick_district"] = $pick_district;
                $post_data_to_send["province"] = $to_province;
                $post_data_to_send["district"] = $to_district;
                $post_data_to_send["weight"] = $weight;
                $post_data_to_send = array(
                    "pick_province" => $pick_province,
                    "pick_district" => $pick_district,
                    "province" => $to_province,
                    "district" => $to_district,
                    "weight" => $weight
                );
                if($weight_check > 0){
                    $response = $helperCheckout->getDataCalculatedFeeGiaoHangTietKiem($post_data_to_send);
                    $json_data = json_decode($response);
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = $json_data->fee->fee;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_TIET_KIEM;
                }else{
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = 0;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_MIEN_PHI;
                    $shipping_name = $helperCheckout->_NAME_GIAO_HANG_MIEN_PHI;
                }
                $from_district_save = $pick_district;
                $from_province_save = $pick_province;
                $to_district_save = $to_district;
                $to_province_save = $to_province;
                $service_id_save = "";   
            }
            if($shipping_code == $helperCheckout->_CODE_GIAO_HANG_SG247){
                $post_data_to_send = array();
                $pick_province = $helperCheckout->getNameProvince($from_district_id);
                $pick_district = $helperCheckout->getNameDistrict($from_district_id);
                if($to_district_id == ""){
                    $to_district_id = $from_district_id;
                }
                $to_province = $helperCheckout->getNameProvince($to_district_id);
                $to_district = $helperCheckout->getNameDistrict($to_district_id);
                $post_data_to_send["pick_province"] = $pick_province;
                $post_data_to_send["pick_district"] = $pick_district;
                $post_data_to_send["province"] = $to_province;
                $post_data_to_send["district"] = $to_district;
                $post_data_to_send["weight"] = $weight;
                $post_data_to_send = array(
                    "pick_province" => $pick_province,
                    "pick_district" => $pick_district,
                    "province" => $to_province,
                    "district" => $to_district,
                    "weight" => $weight
                );
                if($weight_check > 0){
                    $response = $helperCheckout->getDataCalculatedFeeGiaoHangTietKiem($post_data_to_send);
                    $json_data = json_decode($response);
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = $json_data->fee->fee;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_TIET_KIEM;
                }else{
                    $array_ret_json["error"] = false;
                    $array_ret_json["fee"] = 0;
                    $code_delivery_save = $helperCheckout->_CODE_GIAO_HANG_MIEN_PHI;
                    $shipping_name = $helperCheckout->_NAME_GIAO_HANG_MIEN_PHI;
                }
                $from_district_save = $pick_district;
                $from_province_save = $pick_province;
                $to_district_save = $to_district;
                $to_province_save = $to_province;
                $service_id_save = "";   
            }
            $shipping_fee = $array_ret_json["fee"];
            $util = $this->_objectManager->get('\Libero\Customer\Block\Seller\Admin\Util');
            $price_format = $util->formatPriceOneStepCheckout($shipping_fee);
            $array_ret_json["fee_format"] = $price_format;
            $weight_save = $weight;
            $length_save = 5;
            $width_save = 5;
            $height_save = 5;
            $helperCheckout->saveShippingInformationToQuote($quote_id,$shipping_name,$shipping_fee,$seller_id,$weight_save,$length_save,$width_save,$height_save,$from_district_save,$from_province_save,$to_district_save,$to_province_save,$service_id_save,$code_delivery_save,$order_id_save);
            $result = $jsonResultFactory->setData($array_ret_json);
            return $result;
        }catch (\Exception $e){
            $this->messageManager->addError("Can not connect to Shipping company.Please waiting 3 seconds to reconnect");
            $array_ret_json = array();
            $array_ret_json["error"] = true;
            $array_ret_json["fee"] = "Can not connect to Shipping company.Please waiting 3 seconds to reconnect";
            $result = $jsonResultFactory->setData($array_ret_json);
            return $result;
        }
    }
}
