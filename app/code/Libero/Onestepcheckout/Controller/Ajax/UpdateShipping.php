<?php
namespace Libero\Onestepcheckout\Controller\Ajax;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class UpdateShipping extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $resultRedirect = $this->resultRedirectFactory->create();
            $helperCheckout = $objectManager->get("\Libero\Onestepcheckout\Helper\Data");
            $postData = $this->getRequest()->getPostValue();
            $to_district_id = $postData["to_district"];
            $quote_id = $postData["quote_id"];
            $helperCheckout->saveShippingInformation($quote_id,$to_district_id);
            //$list_shipping_quote = $helperCheckout->getListShipingFromQuote($quote_id);
            $html_update_shipping  = "";
            /*foreach($list_shipping_quote as $_shiping){
                $fee =  $_shiping["fee_shipping"];
                $name_shipping = $_shiping['name_shipping'];
                $html_update_shipping .= '<tr class="totals shipping excl"><th class="mark" scope="row">
                <span class="label">'.$name_shipping.'</span></th><td class="amount">
                <span class="price">'.$objectManager->get('Magento\Framework\Pricing\Helper\Data')->currency($fee,true,false).'></span></td></tr>';
            }*/
            $array_ret_json = array ("error" => false,"html" => $html_update_shipping);
            $result = $jsonResultFactory->setData($array_ret_json);
            return $result;
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
