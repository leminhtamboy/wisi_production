<?php
namespace Libero\Onestepcheckout\Controller\Ajax;
use Magento\Framework\App\Action\Context;

class UpdateSumary extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $blockAddress = $this->_objectManager->get("\Libero\Onestepcheckout\Block\AbstractCheckOut");
        $quote = $blockAddress->getCheckoutSession()->getQuote();
        $addressShippingQuote = $quote->getShippingAddress();
        $addressShippingQuote->setCollectShippingRates(true)->collectShippingRates()->setShippingMethod('lr_shipping_lr_shipping'); //shipping method
        $quote->collectTotals();
        $quote->save();
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;
    }
}
