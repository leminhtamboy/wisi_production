<?php
namespace Libero\Onestepcheckout\Controller\Ajax;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class Get extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    /*public function execute(){
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $resultRedirect = $this->resultRedirectFactory->create();
            $postData = $this->getRequest()->getPostValue();
            $toCity = $postData["toCity"];
            $toProvince = $postData["toProvince"];
            $priceProduct = $postData["price_product"];
            $cartPrice = intval($priceProduct);
            $weight = $postData["weight"];
            $arrayPostData  = array(
                "From" => array("City"  =>  52,"Province"   => 569),
                "To" => array("City"    =>  $toCity,"Province" => $toProvince),
                "Order" =>  array("Amount"   =>  $cartPrice,"Weight" =>  $weight),
                "Config"    =>  array("Service" =>  2,"CoD" =>  1,"Protected"   =>  1,"Checking"    =>  1,"Payment" =>  2,"Fragile" =>  2),
                "Domain" =>  "haravan.com",
                "MerchantKey" =>"b7dfd7457c04b8fd547068bfda886681"
            );
            //Use CURL to get
            $resultJson = $this->callUrl("http://prod.boxme.vn/api/public/api/rest/courier/calculate",$arrayPostData);
            //End
            $resultJson = json_decode($resultJson);
            //$result = $this->resultPageFactory->create();
            $result = $jsonResultFactory->setData($resultJson);
            return $result;
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }*/
    public function execute()
    {
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $resultRedirect = $this->resultRedirectFactory->create();
            $blockCheckout = $objectManager->get('\Libero\Onestepcheckout\Block\AbstractCheckOut');
            $modelShippingSeller  = $objectManager->get('\Libero\Onestepcheckout\Model\Shipping');
            $modelProduct = $objectManager->get('Magento\Catalog\Model\Product');
            $quote = $blockCheckout->getCheckoutSession()->getQuote();
            $items = $quote->getAllItems();
            $weight = 0;
            $arrayJson = array();
            $arrayAllCourier = array();
            foreach($items as $item) {

                $id_product = $item->getData("product_id");
                //get seller từ đây
                $dataProduct = $modelProduct->load($id_product);
                $id_seller = $dataProduct->getData("id_seller");
                $shipping = $modelShippingSeller->load($id_seller,"id_seller"); 
                $name = $shipping->getData("name_shipping_method");
                $price = $shipping->getData("price");
                $url_logo = "";
                if($name == "Viettel Post"){
                    $url_logo = "http://cloud.shipchung.vn//uploads/images/cards/2c20b3ca36e4dcd11b52f2247566d453.png";
                }
                if($name == "Ninjavan"){
                    $url_logo = "http://cloud.shipchung.vn//uploads/images/cards/8712b0b57eec9362beb8273986dcd2ce.png";
                }
                if($name == "Giao Hàng Tiết Kiệm"){
                    $url_logo = "http://cloud.shipchung.vn//uploads/images/cards/f1983aaf8c292a05236fabd0478020ee.png";
                }
                $id_shipping = $shipping->getData("id_shipping_method");
                $arrayCourier = array();
                $arrayCourier["courier_id"] = $id_shipping;
                $arrayCourier["courier_name"] = $name;
                $arrayCourier["price"] = $price;
                $arrayCourier["courier_logo"] = $url_logo;
                $arrayAllCourier[$id_seller] = $arrayCourier;
                
            }
            $arrayJson["couriers"] = $arrayAllCourier;
            $postData = $this->getRequest()->getPostValue();
            //Use CURL to get
            //End
            //$resultJson = json_encode($arrayJson);
            //$result = $this->resultPageFactory->create();
            $result = $jsonResultFactory->setData($arrayJson);
            return $result;
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
    public function callUrl($url,$postData){
        $postData=json_encode($postData);
        $header=array(
            "Accept:application/json",
            "Content-Type:application/json"
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 3000);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$postData);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
