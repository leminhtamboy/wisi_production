<?php
namespace Libero\Onestepcheckout\Controller\Success;
use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Success Page'));
        $blockCheckout = $this->_objectManager->get("Libero\Onestepcheckout\Block\AbstractCheckOut");
        $quote = $blockCheckout->getCheckoutSession()->getQuote();
        $resultRedirect = $this->resultRedirectFactory->create();
        if(!$quote->getId()){
            //$resultRedirect->setPath('/');
            //return $resultRedirect;
        }
        $allItems = $this->_objectManager->get("\Magento\Checkout\Model\Session")->getQuote()->getAllVisibleItems();
        foreach ($allItems as $item) {
            $itemId = $item->getItemId();
            $this->_objectManager->get("\Magento\Checkout\Model\Cart")->removeItem($itemId)->save();
        }
        $cart = $blockCheckout->getCart();
        $cart->truncate();
        $allItems = $quote->getAllVisibleItems();//returns all teh items in session
        foreach ($allItems as $item) {
            $quote->removeItem($item->getId());
            $quote-save();
        }
        $session = $this->_objectManager->get("Magento\Checkout\Model\Type\Onepage")->getCheckout();
        $session->clearQuote();
        foreach($allItems as $item) {
            $item->isDeleted(true);
        }
        return $resultPage;
    }
}
