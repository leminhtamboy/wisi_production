<?php
namespace Libero\Onestepcheckout\Controller\Import;


class District extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        //get Json in here
        //Resolve Json
        $jsonEncode = \Zend_Json::decode($this->getProvinceList());
        $data_province = $jsonEncode["data"];
        $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
        $connectionLiberoProvince = $helper->getConnectionDatabaseDirect();
        foreach($data_province as $province){
            $province_id = $province["ProvinceID"];
            $district_id = $province["DistrictID"];
            $district_name = $province["DistrictName"];
            $position = $province["Priority"];
            //check before
            $sql_insert_province = "INSERT INTO `libero_district` (`id`, `district_id`, `district_name`, `position`, `province_id`) VALUES (NULL, '$district_id', N'$district_name', '$position', '$province_id');";
            $connectionLiberoProvince->query($sql_insert_province);
        }
        $connectionLiberoProvince->query("DELETE FROM libero_district WHERE district_name = 'Tp HCM';");

        $connectionLiberoProvince->query("DELETE FROM libero_district WHERE district_name LIKE '%Ha Noi%';");

        echo "IMPORT DISTRICT SUCCESS";
        echo "<pre>";
        var_dump($jsonEncode["data"]);
        echo "</pre>";
        die;
        //$resultRedirect = $this->resultRedirectFactory->create();
        //return $resultRedirect->setPath('onestepcheckout/index/index');
    }
    public function getProvinceList()
    {
        $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
        $token = $helper->getTokenKeyApiGiaoHangNhanh();
        //using CURL
        $post_data = array();
        $post_data["token"] = $helper->getTokenKeyApiGiaoHangNhanh();

        $url = $helper->getUrlApiGiaoHangNhanh();
        $dataJsonProvince = $helper->callUrl($url,$post_data);
        return $dataJsonProvince;
        //End using CURL
    }

}
