<?php
namespace Libero\Onestepcheckout\Controller\Import;


class Province extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        //get Json in here
        //Resolve Json
        $jsonEncode = \Zend_Json::decode($this->getProvinceList());
        $data_province = $jsonEncode["data"];
        $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
        $connectionLiberoProvince = $helper->getConnectionDatabaseDirect();
        foreach($data_province as $province){
            $province_id = $province["ProvinceID"];
            $province_name = $province["ProvinceName"];
            //check before
            $sql_check = "SELECT `province_id` FROM libero_province WHERE province_id = '$province_id'";
            $data_province_in_sql = $connectionLiberoProvince->fetchAll($sql_check);
            if(count($data_province_in_sql) > 0){
                continue;
            }else {
                $sql_insert_province = "INSERT INTO `libero_province` (`id`, `province_id`, `province_name`, `etc`) VALUES (NULL, '$province_id',N'$province_name', '');";
                $connectionLiberoProvince->query($sql_insert_province);
            }
        }
        echo "IMPORT SUCCESS";
        echo "<pre>";
        var_dump($jsonEncode["data"]);
        echo "</pre>";
        die;
        //$resultRedirect = $this->resultRedirectFactory->create();
        //return $resultRedirect->setPath('onestepcheckout/index/index');
    }
    public function getProvinceList()
    {
        $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
        $token = $helper->getTokenKeyApiGiaoHangNhanh();
        //using CURL
        $post_data = array();
        $post_data["token"] = $helper->getTokenKeyApiGiaoHangNhanh();

        $url = $helper->getUrlApiGiaoHangNhanh();
        $dataJsonProvince = $helper->callUrl($url,$post_data);
        return $dataJsonProvince;
        //End using CURL
    }

}
