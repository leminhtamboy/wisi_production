<?php
namespace Libero\Onestepcheckout\Controller\Import;


class Ward extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        //get Json in here
        //Resolve Json

        $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
        $util = $this->_objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $connectionLiberoProvince = $helper->getConnectionDatabaseDirect();
        $districts_sql ="SELECT DISTINCT * FROM libero_district;";
        $districts = $connectionLiberoProvince->fetchAll($districts_sql);
        $count = 0;
        foreach($districts as $_district){
            $jsonEncode = \Zend_Json::decode($this->getDistrictList($_district["district_id"]));
            $data_district = $jsonEncode["data"]["Wards"];

            if(count($data_district)>0){
                foreach($data_district as $province){
                    $ward_id = $province["WardCode"];
                    $ward_name = $province["WardName"];
                    $ward_name = $util->removeQuoteInString($ward_name);
                
                    $district_id = $province["DistrictID"];
                    $province_id = $province["ProvinceID"];

                    //check before
                    $sql_insert_ward = "INSERT INTO `libero_ward` ( `ward_id`, `ward_name`, `district_id`, `province_id`) VALUES ('$ward_id', N'$ward_name', '$district_id', '$province_id');";
                    $connectionLiberoProvince->query($sql_insert_ward);
                }

                echo "IMPORT district ". $_district['district_name'] . " successfully<br/>";
                $count++;
            }
            
        }
        echo "IMPORT wards for $count districts";
        die;
        //$resultRedirect = $this->resultRedirectFactory->create();
        //return $resultRedirect->setPath('onestepcheckout/index/index');
    }
    public function getDistrictList($district_id)
    {
        $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
        $token = $helper->getTokenKeyApiGiaoHangNhanh();
        //using CURL
        $post_data = array();
        $post_data["token"] = $helper->getTokenKeyApiGiaoHangNhanh();
        $post_data["DistrictID"] = +$district_id;

        $url = $helper->getUrlGetWardGiaoHangNhanh();
        $dataJsonProvince = $helper->callUrl($url,$post_data);
        return $dataJsonProvince;
        //End using CURL
    }

}
