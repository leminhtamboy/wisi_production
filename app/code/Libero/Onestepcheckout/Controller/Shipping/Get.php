<?php
namespace Libero\Onestepcheckout\Controller\Shipping;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class Get extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $postData = $this->getRequest()->getPostValue();
            $firstName = $postData["firstname"];
            $lastName = $postData["lastname"];
            $company = $postData["company"];
            $street = $postData["street"];
            $city = $postData["city"];
            $region = $postData["region"];
            $ward = $postData["phuong"];
            $street = $street." ".$ward;
            $telephone = $postData["telephone"];
            $zipCode = $postData["zipcode"];
            $country = $postData["country"];
            $customerId = $postData["customer_id"];
            //Save address customer
            $addresssModel = $this->_objectManager->get('\Magento\Customer\Model\AddressFactory');
            $addressModel = $addresssModel->create();
            $addressModel->setCustomerId($customerId)
                ->setFirstname($firstName)
                ->setLastname($lastName)
                ->setCountryId('VN')
                ->setPostcode($zipCode)
                ->setRegion($region)
                ->setCity($city)
                ->setTelephone($telephone)
                ->setFax($telephone)
                ->setCompany($company)
                ->setStreet($street)
                ->setSaveInAddressBook('1');
            $addressModel->save();
            $arrayMessage = array("mess"=>"sucessfully");
            //echo Zend_Json::encode($arrayMessage);
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
