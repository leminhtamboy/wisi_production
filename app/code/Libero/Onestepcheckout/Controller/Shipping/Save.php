<?php
namespace Libero\Onestepcheckout\Controller\Shipping;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class Save extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $postData = $this->getRequest()->getPostValue();
            $name = $postData["name"];
            $price =  $postData["price"];
            $registry = $this->_objectManager->create("\Magento\Checkout\Model\Session");
            $registry->setName($name);
            $registry->setPrice($price);
            $blockAddress = $this->_objectManager->get("\Libero\Onestepcheckout\Block\AbstractCheckOut");
            $quote = $blockAddress->getCheckoutSession()->getQuote();
            $addressShippingQuote = $quote->getShippingAddress();
            $addressShippingQuote->setCollectShippingRates(true)->collectShippingRates()->setShippingMethod('lr_shipping_lr_shipping'); //shipping method
            //Save address customer
            $quote->collectTotals();
            $quote->save();
            $arrayMessage = array("mess"=>"sucessfully");
            //echo Zend_Json::encode($arrayMessage);
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
