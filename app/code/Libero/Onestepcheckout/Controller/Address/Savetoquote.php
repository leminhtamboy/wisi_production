<?php
namespace Libero\Onestepcheckout\Controller\Address;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class Savetoquote extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $postData = $this->getRequest()->getPostValue();
            $addressData = $postData["valueAdress"];
            $arrayAdressData = explode("|",$addressData);
            $firstName = $arrayAdressData[0];
            $lastName = $arrayAdressData[1];
            $street = $arrayAdressData[2];
            $city = $arrayAdressData[3];
            $region = $arrayAdressData[5];
            //$street = $street." ".$ward;
            $telephone = $arrayAdressData[7];
            $zipCode = $arrayAdressData[6];
            $country = $arrayAdressData[4];
            $fax = $arrayAdressData[8];
            //Save address to quote address
            $blockAddress = $this->_objectManager->get("\Libero\Onestepcheckout\Block\AbstractCheckOut");
            $quote = $blockAddress->getCheckoutSession()->getQuote();
            $addressData=[
                'shipping_address' =>[
                    'firstname'    => $firstName, //address Details
                    'lastname'     => $lastName,
                    'street' => $street,
                    'city' => $city,
                    'country_id' => $country,
                    'region' => $region,
                    'postcode' => $zipCode,
                    'telephone' => $telephone,
                    'fax' => $fax,
                    "same_as_billing" => 1
                ],
                'billing_address' =>[
                    'firstname'    => $firstName, //address Details
                    'lastname'     => $lastName,
                    'street' => $street,
                    'city' => $city,
                    'country_id' => $country,
                    'region' => $region,
                    'postcode' => $zipCode,
                    'telephone' => $telephone,
                    'fax' => $fax,
                ]
            ];
            $addressBillingQuote = $quote->getBillingAddress();

            $addressBillingQuote->addData($addressData['billing_address']);

            $addressShippingQuote = $quote->getShippingAddress();

            $addressShippingQuote->addData($addressData['shipping_address']);
            $quote->save();
            $quote->collectTotals();
            /*$quote->save();
            $addressShippingQuote->setCollectShippingRates(true)->collectShippingRates()->setShippingMethod('magepsycho_customshipping_magepsycho_customshipping'); //shipping method
            //Save address customer
            $quote->collectTotals();
            $quote->save();*/
            $arrayMessage = array("mess"=>"sucessfully");
            //echo Zend_Json::encode($arrayMessage);
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
