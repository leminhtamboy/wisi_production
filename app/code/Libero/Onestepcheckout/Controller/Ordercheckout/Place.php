<?php
namespace Libero\Onestepcheckout\Controller\Ordercheckout;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class Place extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    protected $_orderSender;

    protected $_order;

    protected $_helperOneStep;

    public $nganluong_url = 'https://www.nganluong.vn/checkout.php';
    // Mã website của bạn đăng ký trong chức năng tích hợp thanh toán của NgânLượng.vn.
    public $merchant_site_code = '57326'; //100001 chỉ là ví dụ, bạn hãy thay bằng mã của bạn
    // Mật khẩu giao tiếp giữa website của bạn và NgânLượng.vn.
    public $secure_pass= '0a15138a64cd20ff89317a5237be3bd3'; //d685739bf1 chỉ là ví dụ, bạn hãy thay bằng mật khẩu của bạn
    // Nếu bạn thay đổi mật khẩu giao tiếp trong quản trị website của chức năng tích hợp thanh toán trên NgânLượng.vn, vui lòng update lại mật khẩu này trên website của bạn
    public $affiliate_code = ''; //Mã đối tác tham gia chương trình liên kết của NgânLượng.vn



    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Libero\Onestepcheckout\Helper\Data $helperOneStep,
        \Magento\Sales\Model\Order\Email\Sender\OrderSender $orderSender,
        \Magento\Sales\Model\Order $order
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->_helperOneStep = $helperOneStep;
        $this->_orderSender = $orderSender;
        $this->_order = $order;
        parent::__construct($context);
    }
    public function buildCheckoutUrlExpand($return_url, $receiver, $transaction_info, $order_code, $price, $currency = 'vnd', $quantity = 1, $tax = 0, $discount = 0, $fee_cal = 0, $fee_shipping = 0, $order_description = '', $buyer_info = '', $affiliate_code = '')
    {
        if ($affiliate_code == "") $affiliate_code = $this->affiliate_code;
        $arr_param = array(
            'merchant_site_code'=>	strval($this->merchant_site_code),
            'return_url'		=>	strval(strtolower($return_url)),
            'receiver'			=>	strval($receiver),
            'transaction_info'	=>	strval($transaction_info),
            'order_code'		=>	strval($order_code),
            'price'				=>	strval($price),
            'currency'			=>	strval($currency),
            'quantity'			=>	strval($quantity),
            'tax'				=>	strval($tax),
            'discount'			=>	strval($discount),
            'fee_cal'			=>	strval($fee_cal),
            'fee_shipping'		=>	strval($fee_shipping),
            'order_description'	=>	strval($order_description),
            'buyer_info'		=>	strval($buyer_info), //"Họ tên người mua *|* Địa chỉ Email *|* Điện thoại *|* Địa chỉ nhận hàng"
            'affiliate_code'	=>	strval($affiliate_code)
        );

        $secure_code ='';
        $secure_code = implode(' ', $arr_param) . ' ' . $this->secure_pass;
        $arr_param['secure_code'] = md5($secure_code);
        /* */
        $redirect_url = $this->nganluong_url;
        if (strpos($redirect_url, '?') === false) {
            $redirect_url .= '?';
        } else if (substr($redirect_url, strlen($redirect_url)-1, 1) != '?' && strpos($redirect_url, '&') === false) {
            $redirect_url .= '&';
        }

        /* */
        $url = '';
        foreach ($arr_param as $key=>$value) {
            $value = urlencode($value);
            if ($url == '') {
                $url .= $key . '=' . $value;
            } else {
                $url .= '&' . $key . '=' . $value;
            }
        }
        //echo $url;
        // die;
        return $redirect_url.$url;
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        date_default_timezone_set("Asia/Bangkok");
        $curDateTime = date("Y-m-d H:i:s");

        try {
            $helperCheckout = $objectManager->get("\Libero\Onestepcheckout\Helper\Data");
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $postData = $this->getRequest()->getPostValue();
            $payment_code = $postData["code_payment"];
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerModel = $objectManager->get('Magento\Customer\Helper\Session\CurrentCustomer');
            $customer = $customerModel->getCustomer();
            $customer_id  = $customer->getId();
            $blockCheckout = $objectManager->get('\Libero\Onestepcheckout\Block\AbstractCheckOut');
            $quote = $blockCheckout->getCheckoutSession()->getQuote();
            $quote_id = $quote->getId();
            if($payment_code == "custompayment"){
                $block = $objectManager->get("\Libero\Onestepcheckout\Block\AbstractCheckOut");
                $quote = $block->getCheckoutSession()->getQuote();
                $customerObj = $objectManager->create('Magento\Customer\Model\Customer');
                $modelCustomer = $customerObj->load($customer_id);
                $balance = $modelCustomer->getData("virtual_money");
                $grand_total = floor($quote->getData("grand_total"));
                if($grand_total >  $balance){
                    $this->messageManager->addError(__("Your credit money does not enough to pay."));
                    $result = json_encode(['error'=>1,'msg'=>'Your credit money does not enough to pay']);
                    $resultJson = $jsonResultFactory->setData($result);
                    return $resultJson;
                }else {
                    $price = $grand_total;
                    $money = $balance - $price;
                    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                    $connection = $resource->getConnection();
                    $sql = "UPDATE `customer_entity` SET `first_failure` = NULL, `lock_expires` = NULL, `updated_at` = '$curDateTime', `virtual_money` = '" . $money . "' WHERE `customer_entity`.`entity_id` = $customer_id;";
                    $connection->query($sql);
                }
            }
            if($payment_code == "onlinebankpayment"){
                $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
                $store = $storeManager->getStore();
                $return_url = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB)."onestepcheckout/payment/purchase";
                $receiver = "info@wisi.vn";
                $transaction_info = "Online Bank With Ngan Luong";
                $order_code = $quote->getId();
                $base_grand_total = $quote->getData("base_grand_total");
                $base_grand_total = floor($base_grand_total);
                $price = $base_grand_total;
                //$buyer_info = $quote->getData("customer_firstname") . "*|*" . $quote->getData("customer_email") . "*|*" . $order->getBillingAddress()->getTelephone();
                $buyer_info = $quote->getData("customer_firstname") . "*|*" . $quote->getData("customer_email");
                $url = $this->buildCheckoutUrlExpand($return_url, $receiver, $transaction_info, $order_code, $price, $currency = 'vnd', $quantity = 1, $tax = 0, $discount = 0, $fee_cal = 0, $fee_shipping = 0, $order_description = '', $buyer_info, $affiliate_code = '');
                $result = json_encode(['error'=>2,'msg'=>'Your credit money does not enough to pay','url' => $url]);
                $resultJson = $jsonResultFactory->setData($result);
                return $resultJson;
            }
            $blockCheckout = $objectManager->get('\Libero\Onestepcheckout\Block\AbstractCheckOut');
            $result = $this->_helperOneStep->createMageOrder($payment_code);
            $has_error = $result["error"];
            if($has_error == 0){

                $order_id = $result["order_id"];
                $increment_id = $result["increment_id"];
                $registry = $this->_objectManager->create("\Magento\Checkout\Model\Session");
                $registry->setOrderIncreaId($increment_id);
                /*
                Region Update Seller
                */
                //Update seller order
                $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();
                //Get order detail
                $modelProduct = $objectManager->get('Magento\Catalog\Model\Product');
                $modelShippingSeller  = $objectManager->get('\Libero\Onestepcheckout\Model\Shipping');
                $helperCheckout = $objectManager->get("\Libero\Onestepcheckout\Helper\Data");
                $items = $quote->getAllItems();
                $arrayProductSeller = array();
                $arrayProductPrice = array();
                
                foreach($items as $item) {
                    $id_product = $item->getData("product_id");
                    $dataProduct = $modelProduct->load($id_product);
                    $id_seller = $dataProduct->getData("id_seller");
                    $row_total = $item->getData("base_row_total_incl_tax");
                    $sum_all = 0;
                    if(array_key_exists($id_seller,$arrayProductPrice)){
                        $sum = $arrayProductPrice[$id_seller];
                        $sum_all = $sum + $row_total;
                        $arrayProductPrice[$id_seller] = $sum_all;
                    }else{
                        $arrayProductPrice[$id_seller] = $row_total;
                    }
                    $arrayProductSeller[$id_seller] = $id_seller;
                }
                //////
                $sql_sale_order = "SELECT * FROM sales_order WHERE entity_id = '$order_id'";
                $data_sale_order = $connection->fetchAll($sql_sale_order)[0];
				$increment_id = $data_sale_order["increment_id"];
				
				$sql_update_time_stamp_order_grid = "UPDATE `sales_order_grid` SET `created_at` = '$curDateTime' where `increment_id` = '$increment_id'";
				$connection->query($sql_update_time_stamp_order_grid);
				
				$sql_update_time_stamp_order = "UPDATE `sales_order` SET `created_at` = '$curDateTime' where `entity_id` = '$order_id'";
				$connection->query($sql_update_time_stamp_order);
				
                $arrayLastInsertId = array();
                $arrayOrderIds = array();
                $array_shipping = null;
                $shipping_description = "";
                $shipping_price = "";
                $shipping_weight = "";
                $shipping_seller_info = "";
                $seller_from_district = "";
                $customer_to_district = "";
                $shipping_service_id = "";
                $coupon_code = "";
                $insurance_free = 0;
                $note = "";
                $customer_name = "";
                $customer_phone = "";
                $customer_address = "";
                $all_subtotal = 0;
                $all_grandtotal = 0;
                $all_shipping_fee = 0;
                foreach($arrayProductSeller as $_productSeller){
                    $sql_order = "INSERT INTO `seller_sales_order` (";
                    foreach($data_sale_order as $key => $value){
						if($key == "request_type" || $key == "status_request"){
                            continue;
                        }
                        $sql_order .= "`$key`,";
                    }
                    $sql_order.="|";
                    $sql_order = str_replace(",|",")",$sql_order);
                    $sql_order.= "VALUES (NULL,";
                    $skip = 0;
                    foreach($data_sale_order as $key => $value){
                        if($skip == 0){$skip++; continue;};
						if($key == "request_type" || $key == "status_request"){
                            continue;
                        }
                        if ($key == "created_at" || $key == "updated_at"){
                            $sql_order .= "'$curDateTime',";
                        } else {
                            $sql_order .= "'$value',";
                        }
                    }
                    $sql_order.= "|";
                    $sql_order = str_replace(",|",")",$sql_order);
                    $connection->query($sql_order);
                    $lastInsert = $connection->lastInsertId();
                    $arrayOrderIds[] = $lastInsert;
                    $arrayLastInsertId[$lastInsert] = $_productSeller;
                    $shipping = $modelShippingSeller->load($_productSeller,"id_seller"); 
                    //lay gia va method cho nay
                    $provider = $helperCheckout->getDetailProvider($_productSeller);
                    $data_shipping_quote = $helperCheckout->getShippingBySellerAndQuote($_productSeller,$quote_id);
                    $name_shipping = $data_shipping_quote[0]["name_shipping"];
                    $save_shipping_method = $name_shipping;
                    $price_shipping = $data_shipping_quote[0]["fee_shipping"];
                    $price = $arrayProductPrice[$_productSeller];
                    $base_grand_total = $price + $price_shipping;
                    $all_subtotal += $price;
                    $all_grandtotal += $base_grand_total;
                    $all_shipping_fee += $price_shipping;
                    $sql_update = "UPDATE `seller_sales_order` SET `seller_id` = '$_productSeller',`shipping_description` = '$save_shipping_method',`base_shipping_amount` = '$price_shipping',`base_grand_total` = '$base_grand_total',`base_subtotal` = '$price',`grand_total` = '$base_grand_total', `updated_at` = '$curDateTime'  WHERE entity_id = $lastInsert";
                    $connection->query($sql_update);
                    $data_post = array();
                    $weight = "";
                    $length = 5;
                    $width = 5;
                    $height = 5;
                    $from_district = "";
                    $to_distric = "";
                    $service_id = "";
                    $name_seller = "";
                    $phone_seller = "";
                    $address_seller = "";
                    $store_name = $provider->getData("store_name");
                    $sql_update = "UPDATE `seller_sales_order` SET 	`weight_api` = '$weight',
                    `length` = '$length',
                    `width` = $width,
                    `height` = '$height',
                    `from_district_id` = '$from_district',
                    `to_district_id` = '$to_distric',
                    `service_id` = '$service_id',
                    `coupon_code_api` = '$coupon_code',
                    `insurance_free` = '$insurance_free',
                    `note` = '$note',
                    `client_contact_name` = '$name_seller',
                    `client_contact_phone` = '$phone_seller',
                    `client_address` = '$address_seller',
                    `customer_name` = '$customer_name',
                    `customer_phone` = '$customer_phone',
                    `shipping_address` = '$customer_address',
                    `cod_amount` = '$base_grand_total',
                    `updated_at` = '$curDateTime'
                    WHERE entity_id = $lastInsert";
                    $connection->query($sql_update);
                    //Update lai quote cho cap nhat order id
                    $sql_update_order_id_for_quote_id = "UPDATE `libero_quote_shipping_price` SET `order_id` = '$lastInsert' WHERE `id_seller` = '$_productSeller' and quote_id = '$quote_id'";
                    $connection->query($sql_update_order_id_for_quote_id);
                    $modelSeller = $objectManager->create('\Libero\Customer\Model\Seller');
                    $seller = $modelSeller->load($_productSeller);
                    $type_of_order = $seller->getData("delivery_type");
                    $sql_update_type_of_order = "UPDATE `seller_sales_order` SET `type_of_order` = '$type_of_order',`provider_name` = '$store_name' WHERE `entity_id` = $lastInsert";
                    $connection->query($sql_update_type_of_order);
                }
                ////Update for Giao Hang Nhanh
                ///////
                //////
                $sql_sale_order = "SELECT * FROM sales_order_grid WHERE entity_id = $order_id";
                $data_sale_order = $connection->fetchAll($sql_sale_order)[0];
                $j = 0;
                foreach($arrayProductSeller as $_productSeller){
                    $sql_order = "INSERT INTO `seller_sales_order_grid` (";
                    foreach($data_sale_order as $key => $value){
                        if($key == "request_type" || $key == "status_request"){
                            continue;
                        }
                        $sql_order .= "`$key`,";
                    }
                    $sql_order.="|";
                    $sql_order = str_replace(",|",")",$sql_order);
                    $sql_order.= "VALUES (NULL,";
                    $skip = 0;
                    foreach($data_sale_order as $key => $value){
                        if($skip == 0){$skip++; continue;};
                        if($key == "request_type" || $key == "status_request"){
                            continue;
                        }
                        if ($key == "created_at" || $key == "updated_at"){
                            $sql_order .= "'$curDateTime',";
                        } else {
                            $sql_order .= "'$value',";
                        }
                    }
                    $sql_order.= "|";
                    $sql_order = str_replace(",|",")",$sql_order);
                    $connection->query($sql_order);
                    $lastInsertGrid = $connection->lastInsertId();
                    $order_id_grid = $arrayOrderIds[$j];
                    $shipping = $modelShippingSeller->load($_productSeller,"id_seller");
                    //lat gia va method cho nay
                    $data_shipping_quote = $helperCheckout->getShippingBySellerAndQuote($_productSeller,$quote_id);
                    $price_shipping = $data_shipping_quote[0]["fee_shipping"];
                    $price = $arrayProductPrice[$_productSeller];
                    $base_grand_total = $price + $price_shipping;
                    $sql_update = "UPDATE `seller_sales_order_grid` SET `seller_id` = '$_productSeller', `order_id` = '$order_id_grid' ,`base_grand_total` = '$base_grand_total',`subtotal` = '$price',`grand_total` = '$base_grand_total',`shipping_and_handling` =  '$price_shipping', `updated_at` = '$curDateTime' WHERE entity_id = $lastInsertGrid";
                    $connection->query($sql_update);
                    $j++;
                }
                //////
                $sql_sale_items = "SELECT * FROM sales_order_item WHERE order_id = $order_id";
                $data_sale_items = $connection->fetchAll($sql_sale_items);
                $arrayLastInsertIdSaleItems = array();
                $arrayProductIdsSaleItems = array();
                foreach($data_sale_items as $_items){
                    $sql_order = "INSERT INTO `seller_sales_order_item` (";
                    foreach($_items as $key => $value)
                    {
                        $sql_order .= "`$key`,";
                    }
                    $sql_order.="|";
                    $sql_order = str_replace(",|",")",$sql_order);
                    $sql_order.= "VALUES (NULL,";
                    $skip = 0;
                    foreach($_items as $key => $value){
                        if($skip == 0){$skip++; continue;};
                        if ($key == "created_at" || $key == "updated_at"){
                            $sql_order .= "'$curDateTime',";
                        } else {
                            $sql_order .= "'$value',";
                        }
                    }
                    $sql_order.= "|";
                    $sql_order = str_replace(",|",")",$sql_order);
                    $connection->query($sql_order);
                    $lastInsertItem = $connection->lastInsertId();
                    $id_product = $_items["product_id"];
                    $arrayLastInsertIdSaleItems[] = $lastInsertItem;
                    $arrayProductIdsSaleItems[] = $id_product;
                }
                $i = 0;
                $array_vat_save = array();
                foreach($arrayLastInsertIdSaleItems as $_insertItem)
                {
                    $id_insert_item = $_insertItem;
                    $product_id = $arrayProductIdsSaleItems[$i];
                    $dataProduct = $modelProduct->load($product_id);
                    $id_seller = $dataProduct->getData("id_seller");
                    $array_vat = array();
                    if(in_array($id_seller,$arrayLastInsertId)){
                        //update lại khúc này
                        $vat_subtotal = $dataProduct->getData("Vat");
                        if($vat_subtotal == ""){
                            $vat_subtotal = 0; 
                        }
                        $array_vat["id"] = $product_id;
                        $array_vat["sku"] =  $dataProduct->getSku();
                        $array_vat["vat"] = $vat_subtotal;

                        $order_id_seller = array_search($id_seller,$arrayLastInsertId);
                        $sql_update = "UPDATE `seller_sales_order_item` SET `order_id` = '$order_id_seller', `updated_at` = '$curDateTime'  WHERE `item_id` = $id_insert_item;";
                        $connection->query($sql_update);
                        $array_vat_save[] = $array_vat;
                    }
                    $i++;
                }
                /*
                End Region
                */
                $checked_vat = $postData["checked_vat"];
                if($checked_vat == "request"){
                    $company_name = $postData["company_name"];
                    $company_code = $postData["company_code"];
                    $company_address = $postData["address_company"];
                    $order_number = $order_id;
                    $array_etc = array();
                    $array_etc["total"] = $all_grandtotal;
                    $array_etc["sub_total"] = $all_subtotal;
                    $array_etc["shipping_fee"] = $all_shipping_fee;
                    $array_etc["infor_product"] = $array_vat_save;
                    $json_save = json_encode($array_etc);
                    $sql_insert_table_vat = "INSERT INTO `libero_vat` 
                    (`id_vat`, `order_number`, `request_at`, `export_at`, `company_name`, `company_number`, `company_address`, `export_status`, `etc`) 
                    VALUES (NULL, '$increment_id', '$curDateTime', '$curDateTime', '$company_name', '$company_code', '$company_address', '1', '$json_save')";
                    $connection->query($sql_insert_table_vat);
                }
                $result = json_encode(['error'=>0,'msg'=>'Thank you for your purchasing !!!']);
                $resultJson = $jsonResultFactory->setData($result);
                $blockCheckout = $this->_objectManager->create("Libero\Onestepcheckout\Block\AbstractCheckOut");
                $cart = $blockCheckout->getCart();
                $cart->truncate();
                $allItems = $quote->getAllVisibleItems();//returns all teh items in session
                foreach ($allItems as $item) {
                    $quote->removeItem($item->getId())->save();
                }
                $session = $objectManager->get("Magento\Checkout\Model\Type\Onepage")->getCheckout();
                $session->clearQuote();
                $this->_orderSender->send($this->_order->load($order_id));
                return $resultJson;
            }else{
                $message_error = $result["msg_error"];
                $result = json_encode(['error'=>1,'msg'=> $message_error]);
                $resultJson = $jsonResultFactory->setData($result);
                return $resultJson;    
            }
        }catch (\Exception $e){
            $this->messageManager->addError($e->getMessage());
            $result = json_encode(['error'=>1,'msg'=>$e->getMessage()]);
            $resultJson = $jsonResultFactory->setData($result);
            return $resultJson;
        }
    }
}
