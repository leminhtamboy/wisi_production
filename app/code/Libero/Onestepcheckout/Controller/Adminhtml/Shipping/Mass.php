<?php
namespace Libero\Onestepcheckout\Controller\Adminhtml\Shipping;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Webapi\Exception;

class Mass extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $registry;
    public function __construct(\Magento\Backend\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        try {
            $resultPage = $this->resultPageFactory->create();
            $post = $this->getRequest()->getPostValue();
            $name_shipping = $post["checked-box"];
            $type = $post["type-mass"];
            $arrayShippingToMass = explode("|",$name_shipping);
            $modelShipping = $this->_objectManager->create("Libero\Onestepcheckout\Model\Shipping");
            foreach($arrayShippingToMass as $_item){
                $shipping = $modelShipping->load($_item);
                if($type == "enable") {
                    $shipping->setData("status_shipping_method", 1);
                }else{
                    $shipping->setData("status_shipping_method", 0);
                }
                $shipping->save();
            }
            //Create
            return $resultPage;
        }catch (Exception $e){
            echo $e->getMessage();
        }
    }
}