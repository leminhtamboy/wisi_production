<?php
namespace Libero\Onestepcheckout\Block;

use Magento\Framework\View\Element\Template;

class Cart extends \Libero\Onestepcheckout\Block\AbstractCheckOut{

    protected $_customer = null;

    protected $_billingAddress = null;

    protected $_shippingAddress = null;

    protected $_productImageHelper = null;
    public function __construct(Template\Context $context,
                                \Magento\Checkout\Model\Cart $cart,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                \Magento\Catalog\Helper\Image $productImageHelper,
                                array $data = []){
        $this->_productImageHelper = $productImageHelper;
        parent::__construct($context,$cart,$checkoutSession,$data);
        if(parent::getCustomerIfLogin()) {
            $customerAbstract = parent::getCustomer();
            $this->_customer = $customerAbstract;
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($this->_customer->getId());
            $this->_billingAddress = $customerObj->getDefaultBillingAddress();
            $this->_shippingAddress = $customerObj->getDefaultShippingAddress();
        }
    }

    public function getCustomerData(){

        return $this->_customer;
    }
    public function getBillingAddress(){

        return $this->_billingAddress;
    }
    public function getShippingAddress(){

        return $this->_shippingAddress;
    }
    public function getArrayAddress($customerId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $customerAddress = array();
        foreach ($customerObj->getAddresses() as $address)
        {
            $customerAddress[] = $address->toArray();
        }
        //testing
        /*foreach ($customerAddress as $customerAddres) {
            echo $customerAddres['street'];
            echo $customerAddres['city'];
        }*/
        return $customerAddress;
    }
    public function getAddressOfCustomer(){

    }
    public function getPostUrlSaveAddress(){
        return $this->getUrl("onestepcheckout/address/save");
    }
    /**
     * 
     */
    public function getPostUrlGetShippingMethod(){
        return $this->getUrl("onestepcheckout/ajax/get");
    }
    /**
     * Undocumented function
     *
     * @return void
     */
    public function getPostUrlGetFeeShipping(){
        return $this->getUrl("onestepcheckout/ajax/getfee");
    }
    /**
     * Undocumented function
     *
     * @return void
     */
    public function getPostUrlUpdaeFeeShipping(){
        return $this->getUrl("onestepcheckout/ajax/updateshippingfee");
    }
    /**
     * Undocumented function
     *
     * @return void
     */
    public function getPostUrlUpdateShipping(){
        return $this->getUrl("onestepcheckout/ajax/updateshipping");
    }
    /**
     * Function get ajax update summary
     *
     * @return void
     */
    public function getPostUrlUpdateSummary(){
        return $this->getUrl("onestepcheckout/ajax/updatesumary");
    }
    public function resizeImage($product, $imageId, $width, $height = null)
    {
        $resizedImage = $this->_productImageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }

    public function getCustomCheckOutUrl(){
        return $this->getUrl("onestepcheckout/index/index");
    }

    public function getQuoteInfoById($quoteId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $ruleModel = $objectManager->create('\Magento\SalesRule\Model\Rule');
        $quote = $ruleModel->load($quoteId);
        
        $conditions_serialized = unserialize($quote->getConditionsSerialized());

        /* $conditions = [
            "attribute" => [],
            "operator" => [],
            "value" => [],
        ];
        foreach($conditions_serialized["conditions"] as $_depth1) {
            if ($_depth1["conditions"]) {
                foreach($_depth1["conditions"] as $_depth2) {
                    var_dump($_depth2);
                    array_push($conditions["attribute"],$_depth2["attribute"]);
                    array_push($conditions["operator"],$_depth2["operator"]);
                    array_push($conditions["value"],$_depth2["value"]);
                }
            }
        } */
        //  ==	is
        //  !=	is not
        //  >=	equals or greater than
        //  <=	equals or less than
        //  >	greater than
        //  <	less than
        //  ()	is one of
        //  !()	is not one of
        $result = [
            "quote" => $quote,
            "conditions_serialized" => $conditions_serialized
        ];
        return $result;
    }

    private function isSerialized($value)
    {
        return (boolean) preg_match('/^((s|i|d|b|a|O|C):|N;)/', $value);
    }
    
}