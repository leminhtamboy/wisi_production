<?php

namespace Libero\Onestepcheckout\Block;

class Sidebar extends \Magento\Checkout\Block\Cart\Sidebar
{
    /**
     * Get one page checkout page url.
     *
     * @codeCoverageIgnore
     * @return string
     */
    public function getCheckoutUrl()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $authorizationLink = $objectManager->get("Override\Customer\Block\Account\AuthorizationLink");
        $checkoutUrl = "";
        if ($authorizationLink->isLoggedIn()){
            $checkoutUrl = $this->getUrl("onestepcheckout");
        } else {
            $checkoutUrl = $this->getUrl("onestepcheckout");
            //$checkoutUrl = $this->getUrl("customer/account/login/");
        }

        return $checkoutUrl;
    }
}