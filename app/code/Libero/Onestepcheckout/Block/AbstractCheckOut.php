<?php
namespace Libero\Onestepcheckout\Block;

use Magento\Framework\View\Element\Template;

class AbstractCheckOut extends \Magento\Framework\View\Element\Template{

    protected $_cart;
    protected $_checkoutSession;

    public function __construct(
        Template\Context $context,
        \Magento\Checkout\Model\Cart $cart,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = [])
    {
        $this->_cart = $cart;
        $this->_checkoutSession = $checkoutSession;
        parent::__construct($context, $data);
    }
    public function getCustomerIfLogin(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if($customerSession->isLoggedIn()) {
            // customer login action
            return true;
        }
        return false;
    }
    public function getCustomer(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerModel = $objectManager->get('Magento\Customer\Helper\Session\CurrentCustomer');
        return $customerModel->getCustomer();
    }
    
    public function getCart()
    {
        return $this->_cart;
    }
    public function getQuoteCheckout(){
        return $this->_cart->getQuote();
    }
    public function getCheckoutSession(){

        return $this->_checkoutSession;
    }
    public function getOrderByIncreasementId($increaseId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderModel = $objectManager->get('Magento\Sales\Model\Order');
        $orderData = $orderModel->loadByIncrementId($increaseId);
        return $orderData;
    }
    public function getHomeUrl(){
        return $this->getBaseUrl();
    }
    public function getOrderDetail($orderId){
        return $this->getUrl("sales/order/view/order_id/$orderId/");
    }
    public function getPostActionUrl(){
        return $this->getUrl("customer/seller/save");
    }
    /**
     * @param  [array] id product
     * @return [type] collection products
     */
    public function getRelatedProducts($order){

        $items = $order->getAllItems();
        $arrayRelatedProducts = array();
        die("something");
        foreach($items as $_item){
            $parent_id = $_item->getData("parent_item_id");
            if($parent_id)
                continue;
            //$id = $_item->getData("product_id");
            $product = $_item->getProduct()->getRelatedProducts();
            $arrayRelatedProducts[] = $product;
        }

        return $arrayRelatedProducts;
    }

    /**
     * Ham lay danh sach shipping theo seller
     */
    public function getShippingBySeller()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelShippingSeller  = $objectManager->get('\Libero\Onestepcheckout\Model\Shipping');
        $helperCheckout = $objectManager->get("\Libero\Onestepcheckout\Helper\Data");
        $modelCustomer = $objectManager->create('Magento\Customer\Model\Customer');
        $modelSeller = $objectManager->create('\Libero\Customer\Model\Seller');
        $connectionDirect = $helperCheckout->getConnectionDatabaseDirect();
        $valueWeight = 0;
        $quote = $this->getCheckoutSession()->getQuote();
        $addressShippingQuote = $quote->getShippingAddress();
        /*$zip_code_customer = $addressShippingQuote->getData("postcode");
        $to_district_id = explode("-",$zip_code_customer)[0];*/
        $customer = $this->getCustomer();
        $id_of_customer = $customer->getId();
        $sql = "select postcode from customer_address_entity where parent_id = '$id_of_customer'";
        $data_address_customer =  $connectionDirect->fetchAll($sql);
        $to_district_id = "";
        if(count($data_address_customer) > 0){
            $zip_code_customer = $data_address_customer[0]["postcode"];
            $to_district_id = explode("-",$zip_code_customer)[0];
        }
        $quote = $this->getCheckoutSession()->getQuote();
        $items = $quote->getAllItems();
        $arrayFilter = array();
        $arrayQty = array();
        $arrayPrice = array();
        $arrayWeight = array();
        $arrayShipping = array();
        $arrayNameShipping = array();
        foreach($items as $item) {
            $id = $item->getProductId();
            $_product = $objectManager->create('Magento\Catalog\Model\Product')->load($id);
            $id_seller = $_product->getData("id_seller");
            $id_shipping = $_product->getData("id_shipping");
            $parent = $item->getData("parent_item_id");
            if($parent){
                continue;
            }
            $shipping = $modelShippingSeller->load($id_seller,"id_seller");
            $name_shipping = $shipping->getData("name_shipping_method");
            $price_shipping = $shipping->getData("price");
            if(array_key_exists($id_seller,$arrayFilter)){
                $value = $arrayFilter[$id_seller];
                $value.= "|".$id;
                $valueQty = $arrayQty[$id_seller];
                $valueQty.= "|".$item->getQty();
                $valuePrice = $arrayPrice[$id_seller];
                $valuePrice.= "|".$item->getPrice();
                $valueWeight.= "|".$item->getWeight();
                $arrayFilter[$id_seller] = $value;
                $arrayQty[$id_seller] = $valueQty;
                $arrayPrice[$id_seller] = $valuePrice;
                $arrayWeight[$id_seller] = $valueWeight;

                $valueShipping = $arrayShipping[$id_seller];
                $valueShipping .= "|".$price_shipping;
                $arrayShipping[$id_seller] = $valueShipping;

                $value_name_shipping = $arrayNameShipping[$id_seller];
                $value_name_shipping .= "|".$name_shipping;
                $arrayNameShipping[$id_seller] = $value_name_shipping;

            }else{
                $arrayFilter[$id_seller] = $id;
                $arrayQty[$id_seller] = $item->getQty();
                $arrayPrice[$id_seller] = $item->getPrice();
                $arrayWeight[$id_seller] = $item->getWeight();
                $arrayShipping[$id_seller] = $price_shipping;
                $arrayNameShipping[$id_seller] = $name_shipping;
            }
        }
        $arrayFilterShipping = array();
        $arrayFilterShippingName = array();
        $arrayFilterShippingWeight = array();
        $arrayFilterShippingService = array();
        $arrayFilterShippingFromDistrict = array();
        $arrayFilterShippingSellerInfor = array();
        foreach($arrayShipping as $key => $value){
            $explodeShipping = explode("|",$value);
            $value_name = $arrayNameShipping[$key];
            $explodeShipingName = explode("|",$value_name);
            $sum_ship = 0;
            $s = 0;
            $arrayChildShipping = array();
            $arrayChildShippingName = array();
            $arrayChildShippingWeight = array();
            $arrayChildShippingService = array();
            $arrayChildShippingFromDistrict = array();
            $arrayChildShippingSellerInfor = array();
            $name_change = "";
            $seller_information = $modelSeller->load($key);
            $weigh_products_of_seller = $arrayWeight[$key];
            foreach($explodeShipping as $ship)
            {
                if($ship == 0 || $ship == "0"){
                    unset($arrayChildShipping);
                    unset($arrayChildShippingName);
                    unset($arrayChildShippingWeight);
                    $arrayChildShipping = array();
                    $arrayChildShippingName = array();
                    $arrayChildShippingWeight = array();
                    $arrayChildShipping[] = 0;
                    $arrayChildShippingName[] = "Giao hàng miễn phí";
                    $arrayChildShippingWeight[] = 0;
                    break;
                }else {

                    $name_shipping = $explodeShipingName[$s];
                    if($name_change != $name_shipping){
                        //call api to calculated price
                        $postcode = "";
                        $seller_of_customer = $modelCustomer->load($seller_information->getData("id_customer"));
                        /*$customerAddress = array();
                        $address = $seller_of_customer->getAddresses();*/
                        $id_customer_address = $seller_information->getData("id_customer");
                        $sql = "select postcode from customer_address_entity where parent_id = '$id_customer_address'";
                        $data_address =  $connectionDirect->fetchAll($sql);
                        if(count($data_address_customer) > 0){
                            $postcode  = $data_address[0]["postcode"];
                            $arrayWeightOfProduct = explode("|",$weigh_products_of_seller);
                            $total_weight = 0; //gram
                            foreach($arrayWeightOfProduct as $_weight){
                                $total_weight+=$_weight;
                            }
                            $from_district_id = explode("-",$postcode)[0];
                            $post_data_to_send  = array();
                            $post_data_to_send["weight"] = $total_weight;
                            $post_data_to_send["length"] = 5;
                            $post_data_to_send["width"] = 5;
                            $post_data_to_send["height"] = 5;
                            $post_data_to_send["from_district_id"] = $from_district_id;
                            $post_data_to_send["to_district_id"] = $to_district_id;
                            $post_data_to_send["service_id"] = 53319;
                            $post_data_to_send["coupon_code"] = "";
                            $post_data_to_send["insurance_free"] = 0;
                            $responseService = $helperCheckout->getDataServiceAvailable($post_data_to_send);
                            $json_service = json_decode($responseService,true);
                            $service_id = $json_service["data"][1]["ServiceID"]; // dang lat service chuan
                            $post_data_to_send["service_id"] = $service_id;
                            $response = $helperCheckout->getDataCalculatedFee($post_data_to_send);
                            $json_data = json_decode($response);
                            $name_change = $name_shipping;
                            $arrayChildShipping[] = $json_data->data->CalculatedFee;
                            $arrayChildShippingName[] = $name_shipping;
                            $arrayChildShippingWeight[] = $total_weight;
                            $arrayChildShippingFromDistrict[] = $from_district_id;
                            $arrayChildShippingService[] = $service_id;
                            $arrayChildShippingSellerInfor["client_contact_name"] = $seller_of_customer->getData("firstname");
                            $arrayChildShippingSellerInfor["client_contact_phone"] = $seller_information->getData("telephone");
                            $arrayChildShippingSellerInfor["client_shipping_address"] = $seller_information->getData("full_address");
                        }else{
                            $name_change = $name_shipping;
                            $arrayChildShipping[] = 25000;
                            $arrayChildShippingName[] = "Need Address";
                            $arrayChildShippingWeight[] = 0;
                            $arrayChildShippingFromDistrict[] = 0;
                            $arrayChildShippingService[] = 0;
                            $arrayChildShippingSellerInfor["client_contact_name"] = "";
                            $arrayChildShippingSellerInfor["client_contact_phone"] = "";
                            $arrayChildShippingSellerInfor["client_shipping_address"] = "";
                        }
                    }
                }
                $s++;
            }
            $arrayFilterShipping[$key] = $arrayChildShipping;
            $arrayFilterShippingName[$key] = $arrayChildShippingName;
            $arrayFilterShippingWeight[$key] = $arrayChildShippingWeight;
            $arrayFilterShippingSellerInfor[$key] = $arrayChildShippingSellerInfor;
            $arrayFilterShippingService[$key] = $arrayChildShippingService;
            $arrayFilterShippingFromDistrict[$key] = $arrayChildShippingFromDistrict;
        }
        $arrayRetShippingForSeller = array();
        $arrayRetShippingForSeller["shipping_price"] = $arrayFilterShipping;
        $arrayRetShippingForSeller["shipping_name"] = $arrayFilterShippingName;
        $arrayRetShippingForSeller["shipping_weight"] = $arrayFilterShippingWeight;
        $arrayRetShippingForSeller["seller_infor"]  = $arrayFilterShippingSellerInfor;
        $arrayRetShippingForSeller["from_district_id"] = $arrayFilterShippingFromDistrict;
        $arrayRetShippingForSeller["to_district_id"] = $to_district_id;
        $arrayRetShippingForSeller["service_id"] = $arrayFilterShippingService;
        $arrayRetShippingForSeller["customer_name"] = $addressShippingQuote->getData("lastname")." ".$addressShippingQuote->getData("firstname");
        $arrayRetShippingForSeller["customer_phone"] = $addressShippingQuote->getData("telephone");
        $arrayRetShippingForSeller["customer_shipping_address"] = $addressShippingQuote->getData("street")." ".$addressShippingQuote->getData("city");
        
        return $arrayRetShippingForSeller;
    }
}