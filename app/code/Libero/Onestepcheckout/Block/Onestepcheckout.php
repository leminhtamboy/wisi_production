<?php
namespace Libero\Onestepcheckout\Block;

use Magento\Framework\View\Element\Template;

class Onestepcheckout extends \Libero\Onestepcheckout\Block\AbstractCheckOut{

    public function __construct(Template\Context $context,
                                \Magento\Checkout\Model\Cart $cart,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                array $data = [])
    {
        parent::__construct($context,$cart,$checkoutSession,$data);
    }
    public function sendSms(){

    }
    public function sendEmail(){

    }
    public function getPostActionUrl(){
        return $this->getUrl("customer/seller/save");
    }
}