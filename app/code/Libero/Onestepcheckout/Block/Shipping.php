<?php
namespace Libero\Onestepcheckout\Block;

use Magento\Framework\View\Element\Template;

class Shipping extends \Libero\Onestepcheckout\Block\AbstractCheckOut{

    protected $_customer = null;

    protected $_billingAddress = null;

    protected $_shippingAddress = null;

    public function __construct(Template\Context $context,
                                \Magento\Checkout\Model\Cart $cart,
                                \Magento\Checkout\Model\Session $checkoutSession,
                                array $data = []){

        parent::__construct($context,$cart,$checkoutSession,$data);
        if(parent::getCustomerIfLogin()) {
            $customerAbstract = parent::getCustomer();
            $this->_customer = $customerAbstract;
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($this->_customer->getId());
            $this->_billingAddress = $customerObj->getDefaultBillingAddress();
            $this->_shippingAddress = $customerObj->getDefaultShippingAddress();
        }
    }

    public function getCustomerData(){

        return $this->_customer;
    }
    public function getBillingAddress(){

        return $this->_billingAddress;
    }
    public function getShippingAddress(){

        return $this->_shippingAddress;
    }
    public function getArrayAddress($customerId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customerId);
        $customerAddress = array();
        foreach ($customerObj->getAddresses() as $address)
        {
            $customerAddress[] = $address->toArray();
        }
        //testing
        /*foreach ($customerAddress as $customerAddres) {
            echo $customerAddres['street'];
            echo $customerAddres['city'];
        }*/
        return $customerAddress;
    }
    public function getCustomCheckOutUrl(){
        return $this->getUrl("onestepcheckout/index/index");
    }
    public function getPostUrlSaveAddress(){
        return $this->getUrl("onestepcheckout/address/save");
    }
    public function getPostUrlGetShippingMethod(){
        return $this->getUrl("onestepcheckout/ajax/get");
    }
}