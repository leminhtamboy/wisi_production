<?php
namespace Libero\Onestepcheckout\Block\Adminhtml\Shipping;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Edit extends \Magento\Framework\View\Element\Template{
    protected $_coreRegistry = null;

    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    public function getDataSeller(){
        $model = $this->_coreRegistry->registry('libero_customer_seller');
        $dataRet = null;
        $dataRet = $model->getData();
        return $dataRet;
    }
    public function getSaveUrl()
    {
        $route = "shippingcustom/shipping/save/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getShippingData(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelShipping = $objectManager->create("Libero\Onestepcheckout\Model\Shipping");
        return $modelShipping->getCollection();
    }
    public function getMassActionUrl(){
        $route = "shippingcustom/shipping/mass/";
        $params = [];
        return $this->getUrl($route, $params);
    }
}