<?php

namespace Libero\Onestepcheckout\Model\Carrier;

use Magento\Quote\Model\Quote\Address\RateRequest;

/**
 * @category   MagePsycho
 * @package    MagePsycho_Customshipping
 * @author     magepsycho@gmail.com a
 * @website    http://www.magepsycho.com
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Customshipping extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'lr_shipping';

    /**
     * @var bool
     */
    protected $_isFixed = true;

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $_rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $_rateMethodFactory;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * @param RateRequest $request
     * @return \Magento\Shipping\Model\Rate\Result
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */
    /*public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $block = $objectManager->get('\Libero\Onestepcheckout\Block\Address');
        //echo $shippingAddress->getData("postcode");
        //get shipping method ngay tai day ne
        $quote = $block->getCheckoutSession()->getQuote();
        $items = $quote->getAllItems();
        $weight = 0;
        foreach($items as $item) {
            $weight += ($item->getWeight() * $item->getQty()) ;
        }
        $baseTotal =  $quote->getSubtotal();
        $grandTotal = $quote->getGrandTotal();
        $shippingAddress = $quote->getShippingAddress();
        $postCode = $shippingAddress->getData("postcode");
        $arrayPostCode = explode("-",$postCode);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $coreRegistry = $objectManager->create("\Magento\Checkout\Model\Session");
        $carierTitle = "Giao Hàng Nhanh";
        $name = $coreRegistry->getName();
        $price = $coreRegistry->getPrice();
        $result = $this->_rateResultFactory->create();
        $shippingPrice = $this->getConfigData('price');
        $method = $this->_rateMethodFactory->create();
        $method->setCarrier($this->_code);
        $method->setCarrierTitle($carierTitle);
        $method->setMethod($this->_code);
        $method->setMethodTitle($name);
        $method->setPrice($price);
        $method->setCost($price);
        $result->append($method);

        return $result;
    }
    */
    public function collectRates(RateRequest $request)
    {
        if (!$this->isActive()) {
            return false;
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $blockCheckout = $objectManager->get('\Libero\Onestepcheckout\Block\AbstractCheckOut');
        $quote = $blockCheckout->getCheckoutSession()->getQuote();
        $helperCheckout = $objectManager->get("\Libero\Onestepcheckout\Helper\Data");
        //get List Quote Shipping
        $list_shipping_quote = $helperCheckout->getListShipingFromQuote($quote->getId());
        $sumPrice = 0;
        $nameSum = "";
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        
        foreach($list_shipping_quote as $_shipping)
        {
            $price = $_shipping["fee_shipping"];
            $name_shipping = $_shipping["name_shipping"];
            $sumPrice += $price;
            $nameSum.=$name_shipping.",";
        }
        
        $nameSum = substr($nameSum,0,strlen($nameSum) - 1);
        $name = $nameSum;
        $price = $sumPrice;
        $carierTitle = __("Delivery Company");
        $result = $this->_rateResultFactory->create();
        $shippingPrice = $this->getConfigData('price');
        $method = $this->_rateMethodFactory->create();
        $method->setCarrier($this->_code);
        $method->setCarrierTitle($carierTitle);
        $method->setMethod($this->_code);
        $method->setMethodTitle($name);
        $method->setPrice($price);
        $method->setCost($price);
        $result->append($method);
        return $result;
    }
    /*public function collectRates(RateRequest $request)
    {
        if (!$this->isActive()) {
            return false;
        }
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $blockCheckout = $objectManager->get('\Libero\Onestepcheckout\Block\AbstractCheckOut');
        $modelShippingSeller  = $objectManager->get('\Libero\Onestepcheckout\Model\Shipping');
        $modelProduct = $objectManager->get('Magento\Catalog\Model\Product');
        $quote = $blockCheckout->getCheckoutSession()->getQuote();
        $items = $quote->getAllItems();
        $weight = 0;
        $priceSum = "";
        $nameSum = "";
        $arraySumPricebySeller = array();
        $arrayNameSeller = array();
        foreach($items as $item) {
            $id_product = $item->getData("product_id");
            //get seller từ đây
            $dataProduct = $modelProduct->load($id_product);
            $id_seller = $dataProduct->getData("id_seller");
            $shipping = $modelShippingSeller->load($id_seller,"id_seller"); 
            $name = $shipping->getData("name_shipping_method");
            $price = $shipping->getData("price");
            //$priceSum += $price;
            //$nameSum .= $name.", ";
            $arraySumPricebySeller[$id_seller] = $price;
            $arrayNameSeller[$id_seller] = $name;
        }
        foreach($arraySumPricebySeller as $_price){
            $priceSum+= $_price;
        }
        foreach($arrayNameSeller as $_name){
            $nameSum .= $_name.",";
        }
        $nameSum = substr($nameSum,0,strlen($nameSum) - 1);
        $block = $objectManager->get('\Libero\Onestepcheckout\Block\Address');
        //echo $shippingAddress->getData("postcode");
        //get shipping method ngay tai day ne
        $quote = $block->getCheckoutSession()->getQuote();
        $items = $quote->getAllItems();
        $weight = 0;
        foreach($items as $item) {
            $weight += ($item->getWeight() * $item->getQty()) ;
        }
        $baseTotal =  $quote->getSubtotal();
        $grandTotal = $quote->getGrandTotal();
        $shippingAddress = $quote->getShippingAddress();
        $postCode = $shippingAddress->getData("postcode");
        $arrayPostCode = explode("-",$postCode);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $coreRegistry = $objectManager->create("\Magento\Checkout\Model\Session");
        $carierTitle = "Giao Hàng Nhanh";
        $name = $nameSum;
        $price = $priceSum;
        $result = $this->_rateResultFactory->create();
        $shippingPrice = $this->getConfigData('price');
        $method = $this->_rateMethodFactory->create();
        $method->setCarrier($this->_code);
        $method->setCarrierTitle($carierTitle);
        $method->setMethod($this->_code);
        $method->setMethodTitle($name);
        $method->setPrice($price);
        $method->setCost($price);
        $result->append($method);

        return $result;
    }*/
    public function callUrl($url,$postData){
        $postData=json_encode($postData);
        $header=array(
            "Accept:application/json",
            "Content-Type:application/json"
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_NOSIGNAL, true);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 3000);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$postData);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }
}