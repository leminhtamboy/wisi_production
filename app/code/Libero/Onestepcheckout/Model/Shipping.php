<?php
namespace Libero\Onestepcheckout\Model;
class Shipping extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Libero\Onestepcheckout\Model\ResourceModel\Shipping');
    }
}