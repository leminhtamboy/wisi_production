<?php
namespace Libero\Onestepcheckout\Model\ResourceModel;

class Shipping extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        // TODO: Implement _construct() method.
        $this->_init("libero_onestepcheckout_shipping_method","id_shipping_method");
    }
}