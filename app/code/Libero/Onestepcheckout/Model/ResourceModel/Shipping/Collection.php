<?php
namespace Libero\Onestepcheckout\Model\ResourceModel\Shipping;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Libero\Onestepcheckout\Model\Shipping',
            'Libero\Onestepcheckout\Model\ResourceModel\Shipping'
        );
    }
}