<?php

namespace Libero\Product\Observer;

use Magento\Framework\Event\ObserverInterface;

class Productsaveafter implements ObserverInterface
{    
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $_product = $observer->getProduct();  // you will get product object
        $_sku=$_product->getSku(); // for sku
        $status = $_product->getStatus(); // for sku
        $product_id = $_product->getId();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $base_url = $storeManager->getStore()->getBaseUrl();
        //Put save image to cron job
        /*$writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info($status);*/
        /*$temp_product = $objectManager->create("\Magento\Catalog\Model\Product")->load($product_id);
        $temp_product->setStatus($status);
        $temp_product->save();*/
        $type_cron = "CRON_PRODUCT_IMAGE";
        $cron_code = "CRON_EX15G79_PRODUCT_".$product_id."_UPDATE";
        $content_cron_excute = $_product->getData("image_data");
        $content_cron_excute_status = json_encode(array("product_id" => $product_id,"status" =>$status));
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $time_update = date("Y-m-d H:i:s");
        $created_at = $time_update;
        $status = 0; // Processsing;
        $etc = "";
        //Using CRUL Webhook to update status
            /*$postData = array();
            $postData["product_id"] = $product_id;
            $postData["status"] = $status;
            $postData=json_encode($postData);
            $url = $base_url."test/update-cron-status.php";
            $header=array(
                "Accept:application/json",
                "Content-Type:application/json"
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_NOSIGNAL, true);
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 3000);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$postData);
            curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
            $result = curl_exec($ch);
            curl_close($ch);*/
        //End Using
        $sql_insert_cron = "INSERT INTO `libero_cron_execute_background` (`id_cron`, `type_cron`, `cron_code`, `content_excute`, `created_at`, `status`, `etc`) 
        VALUES (NULL, '$type_cron', '$cron_code', '$content_cron_excute', '$created_at', '0', '$etc');";
        $connection->query($sql_insert_cron);
        $sql_insert_cron = "INSERT INTO `libero_cron_execute_background` (`id_cron`, `type_cron`, `cron_code`, `content_excute`, `created_at`, `status`, `etc`) 
        VALUES (NULL, 'CRON_PRODUCT_STATUS', '$cron_code', '$content_cron_excute_status', '$created_at', '0', '$etc')";
        $connection->query($sql_insert_cron);
    }   
}