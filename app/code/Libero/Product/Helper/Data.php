<?php
namespace Libero\Product\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * Function get object manager directly . Not good practice
     *
     * @return void
     */
    public function getObjectManager()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        return $objectManager;
    }
    //Categories Region

    /**
     * Function get list category children include id ,name and link
     *
     * @param [type] $parent_id
     * @return arrayjson
     */
    public function getChildrenCategoryByParenId($parent_id)
    {
        $array_ret = array();
        $category = $this->getObjectManager()->get("\Magento\Catalog\Model\Category")->load($parent_id);
        $sub_category = $category->getChildrenCategories();
        foreach($sub_category as $_subcate){
            $array_item = array();
            $array_item["id"] = $_subcate->getId();
            $array_item["name"] = $_subcate->getName();
            $array_item["url"] = $_subcate->getUrl();
            $array_ret [] = $array_item;
        }
        return $array_ret;
    }
    
}
