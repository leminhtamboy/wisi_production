<?php
namespace Libero\Excel\Block\Adminhtml;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Excel extends \Magento\Framework\View\Element\Template{
    protected $_coreRegistry = null;

    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }
    public function getDataSeller(){
        $model = $this->_coreRegistry->registry('libero_customer_seller');
        $dataRet = null;
        $dataRet = $model->getData();
        return $dataRet;
    }
    public function getUrlExport()
    {
        return $this->getBaseUrl()."test/export-minsale.php";
    }
    public function getApproveUrl()
    {
        $route = "manager/seller/approve/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getViewSeller()
    {
        $route = "manager/seller/view/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getSaveUrl(){
        $route = "manager/seller/save/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getBackUrl(){
        $route = "manager/seller/index/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getCancelUrl(){
        $route = "manager/seller/cancel/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getFormKey(){

    }
}