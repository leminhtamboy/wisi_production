<?php
namespace Libero\SearchProduct\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_connection;

    protected $_table_search_output = 'libero_search_product_output';

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->_connection = $resource->getConnection();
        parent::__construct($context);
    }
    
    public function getHotkeywords(){
        $sql = "SELECT *, COUNT(keyword) AS count FROM wisi_production.libero_search_keyword GROUP BY keyword ORDER BY count DESC LIMIT 5;";
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }

    public function getRelatedKeyword($keyword){
        $sql = "SELECT *, COUNT(keyword) AS count FROM libero_search_keyword WHERE keyword LIKE '%$keyword%' GROUP BY keyword ORDER BY count DESC LIMIT 3;";
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }

    public function getTopSearchProductByKeyword($keyword){
        $sql = "SELECT *, COUNT(product_id) AS count FROM libero_search_keyword WHERE keyword LIKE '%$keyword%' GROUP BY product_id ORDER BY count DESC LIMIT 5;";
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }

    public function getCategoryIdsByKeyword($keyword){
        $sql = "SELECT * FROM libero_search_keyword WHERE keyword LIKE '%$keyword%' GROUP BY product_category;";
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }

    public function getCategoryNameById($id){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $category = $objectManager->create('Magento\Catalog\Model\Category')->load($id);
        return $category->getName();
    }

    public function getCategoryUrlById($id){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $category = $objectManager->create('Magento\Catalog\Model\Category')->load($id);
        return $category->getUrlPath();
    }
    function removeSign($str){
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }
    public function getKeyWordFromCache($key_word)
    {
        $sql = "SELECT * FROM libero_search_product_output WHERE keyword LIKE '$key_word'";
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }
    /**
     * Khoang cach Levenstein trong 2 chuoi 
     *
     * @param [type] $string_1
     * @param [type] $string_2
     * @return void
     */

    public function slimilarString($s1,$s2)
    {
        $l1 = strlen($s1);
        $l2 = strlen($s2);
        $dis = range(0,$l2);
        for($x=1;$x<=$l1;$x++){        
            $dis_new[0]=$x;
            for($y=1;$y<=$l2;$y++){
                $c = ($s1[$x-1] == $s2[$y-1])?0:1;
                $dis_new[$y] = min($dis[$y]+1,$dis_new[$y-1]+1,$dis[$y-1]+$c);	 
            }
            $dis = $dis_new;              
        }	

        return $dis[$l2];
    }

    /**
     * Giải thuật so sánh theo từ
     *
     * @param [type] $s
     * @return void
     */
    function wordsof($s) {
        $a = [];foreach(explode(" ",$s)as $w) $a[$w]++;
        return $a;
    }
    
    function compare($s1,$s2) {
    
        $w1 = $this->wordsof($s1);if(!$w1) return 0;
        $w2 = $this->wordsof($s2);if(!$w2) return 0;
    
        $totalLength = strlen(join("",$w1).join("",$w2)) || 1;
    
        $chDiff = 0;
        foreach($w1 as $word=>$x) if(!$w2[$word]) $chDiff+=strlen($word);
        foreach($w2 as $word=>$x) if(!$w1[$word]) $chDiff+=strlen($word);
    
        return $chDiff/$totalLength;
    
    }
    function lev($s,$t) {
        $m = strlen($s);
        $n = strlen($t);
        
        for($i=0;$i<=$m;$i++) $d[$i][0] = $i;
        for($j=0;$j<=$n;$j++) $d[0][$j] = $j;
        
        for($i=1;$i<=$m;$i++) {
            for($j=1;$j<=$n;$j++) {
                $c = ($s[$i-1] == $t[$j-1])?0:1;
                $d[$i][$j] = min($d[$i-1][$j]+1,$d[$i][$j-1]+1,$d[$i-1][$j-1]+$c);
            }
        }
    
        return $d[$m][$n];
    }
    /**
     * Function processing Keyword have sign to no sign 
     *
     * @param [type] $key_word have sign
     * @return void
     */
    public function getBestKeyWord($key_word)
    {
        $arrayBestKeyWord = array();
        $flagIsBelongtoDatabase = false;
        //Truong hop Giay Nice -> Giay Nike
        //Truong Hop Tui xack -> Tui Xach
        //Buoc 1 : Xu ly trong bang ketword search de lay ra danh sach cac tu khoa gan giong
        //Neu co thi show
        //Nguoc lai thi khong
            //Lay bang sql thu truoc vi trường hợp có âm "đ" thì search rất khó khắn mà khi quy về không dấu thì sẽ không đúng cho nên lúc này phải lưu lại trường etc luôn
        $sql = "SELECT keyword from `$this->_table_search_output` WHERE keyword like '$key_word'";
        $result = $this->_connection->fetchAll($sql);
        
        if(count($result) > 0){
            $flagIsBelongtoDatabase =  true;
            foreach ($result  as $_key){
                $arrayBestKeyWord[]  = $_key;
            }
        }else{
            $sql = "SELECT keyword from `$this->_table_search_output`";
            $result_list = $this->_connection->fetchAll($sql);
            foreach($result_list as  $_key){
                //Dung giai thuat similar text de tim kiem
                //Cho nay dot performance rat la nhieu nen phai tim kiem nhanh va danh dau keyword la index
                //Ngay tai giai thuat nay tuc la so lan de bien doi chuoi 1 thanh chuoi 2
                //Tuc la tu keyword seach thanh keyword goc trong database
                $step = $this->slimilarString($key_word,$_key);
                if($step < 4){
                    $arrayBestKeyWord[]  = $_key;
                    $flagIsBelongtoDatabase =  true;
                }
            }
        }
        //Con trong truong hop khong co trong list nay thi sao 
        if($flagIsBelongtoDatabase == false){
            //Thi return ra luon chu sao h vi khong tim duoc best keyword trong he thong database
            $arrayBestKeyWord[] = $key_word;
        }
        return $arrayBestKeyWord;
    }

    //Vung danh cho API

    /**
     * Ham lay product list 
     *
     * @return void
     */
    public function getProductList($key_word)
    {
        //Giá trị entity_id
        $array_result = array();
        $array_entity_id = array();
        $array_entity_error = array();
        //Scan toan vung sau do tu phan lop
        $sql = "SELECT * FROM `catalog_product_entity_varchar` where `attribute_id` = '73' and store_id = 0";
        //73 la attribute id cua bang eav_attribute voi attribute code la name
        $data_name = $this->_connection->fetchAll($sql);
        //trong nay neu keyword la tui xach thi ok , con keyword ko phai la tui xach ma la tui xak
        //Machine learning phai tu hoc de toi phan lop keyword do thuoc ve lop nao de lam dc điều này thì chỉ có cách là dùng keyword và tìm trên name product
        //Tách cuỗi ra thanh từng word vi key word minh se tach va product name minh se tach
        //Output mong muon la nguyen chuoi keyword sau khi tach phai du diem 10 vi minh chi co 0 va 10 neu nhu trong chuoi tim kiem chi can 1 tu khon similar dong ngia voi viec khong co product
        //Khi output khong co product viec do se luu lai toan bo keyword da tra ve 404 va tu lay cai tu khong co de tim kiem tren product name neu khong co tren product name thi con nguoi se can thiep
        //Neu nhu con nguoi cam thay khong co loai san pham nay thi keyword nay se được điều chỉnh lại tức là keyword 404 se có trường etc_1 là keyword similar
        $array_split_key = explode(" ",$key_word);
        //Ở day la tieng viet nen thong nhat la se tim sau khi bo dau hoac la co dau vi co dau similar text gan nhu la kho xac dinh
        //Minh thu 2 truong hop 1 truong la co dau
        foreach($data_name as $_product){
            //truong hop co dau
            $name = $_product["value"];
            $entity_id = $_product["entity_id"];
            $point = 0;
            $countKeyWord = 0;
            foreach($array_split_key as $_key_input){
                //Truoc het la phai dung indexOf
                if($this->indexOfString($_key_input,$name)){
                    $point+=10;
                }else{
                    //Bước nay rất khó chịu vì dùng levenstein va phai cat chuoi name product ra
                    //echo "input product name".$name;
                    //echo "<br/>";
                    $name = trim($name);
                    $array_name = explode(" ",$name);
                    $shortest = -1;
                    foreach($array_name as $_sub_name_product){
                        $step = $this->slimilarString($_key_input,$_sub_name_product);
                        //echo "string 1 ".$_key_input;
                        //echo "<br/>";
                        //echo "step  leven ".$step;
                        //echo "<br/>";
                        //echo "string 2 ".$_sub_name_product;
                        //echo "<br/>";
                        //check for an exact match
                        if ($step == 0) {
                            // closest word is this one (exact match)
                            $point+=10;
                            // break out of the loop; we've found an exact match
                            break;
                        }
                        // if this distance is less than the next found shortest
                        // distance, OR if a next shortest word has not yet been found
                        if ($step <= $shortest || $shortest < 0) {
                            // set the closest match, and shortest distance
                            //Cap nhat lai position
                            $shortest = $step;
                        }
                    }
                    if($shortest < 2){
                        //Ok
                        $point+=10;
                    }
                    if($shortest  == 3){
                        //echo "result";
                        //echo "<br/>";
                        $array_entity_error [] = $entity_id;
                    }
                }
                $countKeyWord++;
            }
            //echo "point".$point;
            //echo "<br/>";
            if($point >= $countKeyWord * 10){
                //Tìm thấy product matching
                //Lấy product entity do ra lưu lại vào mảng
                $array_entity_id [] = $entity_id;
                $point = 0;
            }
            
        }
        //Truong hop khong dau
        foreach($data_name as $_product){
            //truong hop khong dau
            $name = $this->removeSign($_product["value"]);
            $entity_id = $_product["entity_id"];
            $point = 0;
            $countKeyWord = 0;
            foreach($array_split_key as $_key_input){
                //Truoc het la phai dung indexOf
                if($this->indexOfString($_key_input,$name)){
                    $point+=10;
                }else{
                    //Bước nay rất khó chịu vì dùng levenstein va phai cat chuoi name product ra
                    //echo "input product name".$name;
                    //echo "<br/>";
                    $name = trim($name);
                    $array_name = explode(" ",$name);
                    $shortest = -1;
                    foreach($array_name as $_sub_name_product){
                        $step = $this->slimilarString($_key_input,$_sub_name_product);
                        //echo "string 1 ".$_key_input;
                        //echo "<br/>";
                        //echo "step  leven ".$step;
                        //echo "<br/>";
                        //echo "string 2 ".$_sub_name_product;
                        //echo "<br/>";
                        //check for an exact match
                        if ($step == 0) {
                            // closest word is this one (exact match)
                            $point+=10;
                            // break out of the loop; we've found an exact match
                            break;
                        }
                        // if this distance is less than the next found shortest
                        // distance, OR if a next shortest word has not yet been found
                        if ($step <= $shortest || $shortest < 0) {
                            // set the closest match, and shortest distance
                            //Cap nhat lai position
                            $shortest = $step;
                        }
                    }
                    if($shortest < 2){
                        //Ok
                        $point+=10;
                    }
                    if($shortest  == 3){
                        //echo "result";
                        //echo "<br/>";
                        $array_entity_error [] = $entity_id;
                    }
                }
                $countKeyWord++;
            }
            //echo "point".$point;
            //echo "<br/>";
            if($point >= $countKeyWord * 10){
                //Tìm thấy product matching
                //Lấy product entity do ra lưu lại vào mảng
                $array_entity_id [] = $entity_id;
                $point = 0;
            }
            
        }
        $array_result ["correct_product"] = array_unique($array_entity_id);
        $array_result ["error_product"] = array_unique($array_entity_error);
        if(count($array_entity_error) > count($array_entity_id)){
            $array_result ["error_product"] = [];
        }
        return $array_result;
    }
    
    /**
     * Function Check if s1 was belong to s2
     *
     * @param [type] $sub // searching
     * @param [type] $parent // input string
     * @return void
     */
    public function indexOfString($sub,$parent)
    {
        //trim sach 2 cai
        $sub = trim($sub);
        $parent = trim($parent);
        //cách hay nhát là dung explode
        $array_check = explode($sub,$parent);
        if(count($array_check) > 1){
            return true; //10 point
        }else{
            return false; //0 point
        }
    }
}