<?php
namespace Libero\SearchProduct\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    protected $_connection;

    protected $_table_search_output = 'libero_search_product_output';

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->_connection = $resource->getConnection();
        parent::__construct($context);
    }
    
    public function getHotkeywords(){
        $sql = "SELECT *, COUNT(keyword) AS count FROM wisi_production.libero_search_keyword GROUP BY keyword ORDER BY count DESC LIMIT 5;";
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }

    public function getRelatedKeyword($keyword){
        $sql = "SELECT *, COUNT(keyword) AS count FROM libero_search_keyword WHERE keyword LIKE '%$keyword%' GROUP BY keyword ORDER BY count DESC LIMIT 3;";
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }

    public function getTopSearchProductByKeyword($keyword){
        $sql = "SELECT *, COUNT(product_id) AS count FROM libero_search_keyword WHERE keyword LIKE '%$keyword%' GROUP BY product_id ORDER BY count DESC LIMIT 5;";
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }

    public function getCategoryIdsByKeyword($keyword){
        $sql = "SELECT * FROM libero_search_keyword WHERE keyword LIKE '%$keyword%' GROUP BY product_category;";
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }

    public function getCategoryNameById($id){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $category = $objectManager->create('Magento\Catalog\Model\Category')->load($id);
        return $category->getName();
    }

    public function getCategoryUrlById($id){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $category = $objectManager->create('Magento\Catalog\Model\Category')->load($id);
        return $category->getUrlPath();
    }
    function removeSign($str){
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }
    public function getKeyWordFromCache($key_word)
    {
        $sql = "SELECT * FROM libero_search_product_output WHERE keyword LIKE '$key_word'";
        $result = $this->_connection->fetchAll($sql);
        return $result;
    }
    /**
     * Khoang cach Levenstein trong 2 chuoi 
     *
     * @param [type] $string_1
     * @param [type] $string_2
     * @return void
     */

    public function slimilarString($s1,$s2)
    {
        $l1 = strlen($s1);
        $l2 = strlen($s2);
        $dis = range(0,$l2);
        for($x=1;$x<=$l1;$x++){        
            $dis_new[0]=$x;
            for($y=1;$y<=$l2;$y++){
                $c = ($s1[$x-1] == $s2[$y-1])?0:1;
                $dis_new[$y] = min($dis[$y]+1,$dis_new[$y-1]+1,$dis[$y-1]+$c);	 
            }
            $dis = $dis_new;              
        }	

        return $dis[$l2];
    }

    /**
     * Giải thuật so sánh theo từ
     *
     * @param [type] $s
     * @return void
     */
    function wordsof($s) {
        $a = [];foreach(explode(" ",$s)as $w) $a[$w]++;
        return $a;
    }
    
    function compare($s1,$s2) {
    
        $w1 = $this->wordsof($s1);if(!$w1) return 0;
        $w2 = $this->wordsof($s2);if(!$w2) return 0;
    
        $totalLength = strlen(join("",$w1).join("",$w2)) || 1;
    
        $chDiff = 0;
        foreach($w1 as $word=>$x) if(!$w2[$word]) $chDiff+=strlen($word);
        foreach($w2 as $word=>$x) if(!$w1[$word]) $chDiff+=strlen($word);
    
        return $chDiff/$totalLength;
    
    }
    function lev($s,$t) {
        $m = strlen($s);
        $n = strlen($t);
        
        for($i=0;$i<=$m;$i++) $d[$i][0] = $i;
        for($j=0;$j<=$n;$j++) $d[0][$j] = $j;
        
        for($i=1;$i<=$m;$i++) {
            for($j=1;$j<=$n;$j++) {
                $c = ($s[$i-1] == $t[$j-1])?0:1;
                $d[$i][$j] = min($d[$i-1][$j]+1,$d[$i][$j-1]+1,$d[$i-1][$j-1]+$c);
            }
        }
    
        return $d[$m][$n];
    }
    /**
     * Function processing Keyword have sign to no sign 
     *
     * @param [type] $key_word have sign
     * @return void
     */
    public function getBestKeyWord($key_word)
    {
        $arrayBestKeyWord = array();
        $flagIsBelongtoDatabase = false;
        //Truong hop Giay Nice -> Giay Nike
        //Truong Hop Tui xack -> Tui Xach
        //Buoc 1 : Xu ly trong bang ketword search de lay ra danh sach cac tu khoa gan giong
        //Neu co thi show
        //Nguoc lai thi khong
            //Lay bang sql thu truoc vi trường hợp có âm "đ" thì search rất khó khắn mà khi quy về không dấu thì sẽ không đúng cho nên lúc này phải lưu lại trường etc luôn
        $sql = "SELECT keyword from `$this->_table_search_output` WHERE keyword like '$key_word'";
        $result = $this->_connection->fetchAll($sql);
        
        if(count($result) > 0){
            $flagIsBelongtoDatabase =  true;
            foreach ($result  as $_key){
                $arrayBestKeyWord[]  = $_key;
            }
        }else{
            $sql = "SELECT keyword from `$this->_table_search_output`";
            $result_list = $this->_connection->fetchAll($sql);
            foreach($result_list as  $_key){
                //Dung giai thuat similar text de tim kiem
                //Cho nay dot performance rat la nhieu nen phai tim kiem nhanh va danh dau keyword la index
                //Ngay tai giai thuat nay tuc la so lan de bien doi chuoi 1 thanh chuoi 2
                //Tuc la tu keyword seach thanh keyword goc trong database
                $step = $this->slimilarString($key_word,$_key);
                if($step < 4){
                    $arrayBestKeyWord[]  = $_key;
                    $flagIsBelongtoDatabase =  true;
                }
            }
        }
        //Con trong truong hop khong co trong list nay thi sao 
        if($flagIsBelongtoDatabase == false){
            //Thi return ra luon chu sao h vi khong tim duoc best keyword trong he thong database
            $arrayBestKeyWord[] = $key_word;
        }
        return $arrayBestKeyWord;
    }

    //Vung danh cho API
    function mbStringToArray($string, $encoding = 'UTF-8') {
        $arrayResult = array();
        while ($iLen = mb_strlen($string, $encoding)) {
            array_push($arrayResult, mb_substr($string, 0, 1, $encoding));
            $string = mb_substr($string, 1, $iLen, $encoding);
        }
        return $arrayResult;
    }
    /**
     * 編集距離（レーベンシュタイン距離）を求める（マルチバイト文字対応）
     * @param $str1
     * @param $str2
     * @param $encoding
     * @param $costReplace
     * @return 数値（距離）,かぶっていた文字の数
     */
    function LevenshteinDistance($str1, $str2, $costReplace = 2, $encoding = 'UTF-8') {
        $count_same_letter = 0;
        $d = array();
        $mb_len1 = mb_strlen($str1, $encoding);
        $mb_len2 = mb_strlen($str2, $encoding);
        $mb_str1 = $this->mbStringToArray($str1, $encoding);
        $mb_str2 = $this->mbStringToArray($str2, $encoding);
        for ($i1 = 0; $i1 <= $mb_len1; $i1++) {
            $d[$i1] = array();
            $d[$i1][0] = $i1;
        }
        for ($i2 = 0; $i2 <= $mb_len2; $i2++) {
            $d[0][$i2] = $i2;
        }
        for ($i1 = 1; $i1 <= $mb_len1; $i1++) {
            for ($i2 = 1; $i2 <= $mb_len2; $i2++) {
                //			$cost = ($str1[$i1 - 1] == $str2[$i2 - 1]) ? 0 : 1;
                if ($mb_str1[$i1 - 1] === $mb_str2[$i2 - 1]) {
                    $cost = 0;
                    $count_same_letter++;
                } else {
                    $cost = $costReplace; //置換
                    }
                $d[$i1][$i2] = min($d[$i1 - 1][$i2] + 1, //挿入
                $d[$i1][$i2 - 1] + 1, //削除
                $d[$i1 - 1][$i2 - 1] + $cost);
            }
        }
        //return $d[$mb_len1][$mb_len2];
        return array('distance' => $d[$mb_len1][$mb_len2], 'count_same_letter' => $count_same_letter);
    }
    function levenshtein_php($str1, $str2){
        $length1 = mb_strlen( $str1, 'UTF-8');
        $length2 = mb_strlen( $str2, 'UTF-8');
        if( $length1 < $length2) return $this->levenshtein_php($str2, $str1);
        if( $length1 == 0 ) return $length2;
        if( $str1 === $str2) return 0;
        $prevRow = range( 0, $length2);
        $currentRow = array();
        for ( $i = 0; $i < $length1; $i++ ) {
            $currentRow=array();
            $currentRow[0] = $i + 1;
            $c1 = mb_substr( $str1, $i, 1, 'UTF-8') ;
            for ( $j = 0; $j < $length2; $j++ ) {
                $c2 = mb_substr( $str2, $j, 1, 'UTF-8' );
                $insertions = $prevRow[$j+1] + 1;
                $deletions = $currentRow[$j] + 1;
                $substitutions = $prevRow[$j] + (($c1 != $c2)?1:0);
                $currentRow[] = min($insertions, $deletions, $substitutions);
            }
            $prevRow = $currentRow;
        }
        return $prevRow[$length2];
    }
    /**
     * Ham lay product list 
     *
     * @return void
     */
    public function getProductList($key_word,$provider_id = null,$id_seller_attribute_code = null)
    {
        //die;
        //Giá trị entity_id
        $array_result = array();
        $array_entity_id = array();
        $array_entity_error = array();
        //Scan toan vung sau do tu phan lop
        if($provider_id == null){
            $sql = "SELECT  entity_id as product_id, `value` as product_name FROM `catalog_product_entity_varchar` where `attribute_id` = '73' and store_id = 0";
        }else{
            $sql = "SELECT a.entity_id as product_id, a.value as product_name FROM 
            (SELECT * FROM `catalog_product_entity_varchar` where `attribute_id` = '73' and store_id = 0) as a
            JOIN (SELECT * FROM `catalog_product_entity_varchar` where (`attribute_id` = '$id_seller_attribute_code' and `value` = '$provider_id' ) and store_id = 0) as b
            ON a.entity_id = b.entity_id";
        }

        //73 la attribute id cua bang eav_attribute voi attribute code la name
        $data_name = $this->_connection->fetchAll($sql);
        //$key_word = $this->removeSign($key_word);
        //$key_word = $this->removeSign($key_word);
        //DEBUG MODE
        /*$key_word = "bình xịt";
        $name = "Sở cảnh sát - 12345";
        $data_name  = array(array("product_name" => $name , "product_id" => "1234"));*/
        //END DEBUG MODE
        //trong nay neu keyword la tui xach thi ok , con keyword ko phai la tui xach ma la tui xak
        //Machine learning phai tu hoc de toi phan lop keyword do thuoc ve lop nao de lam dc điều này thì chỉ có cách là dùng keyword và tìm trên name product
        //Tách cuỗi ra thanh từng word vi key word minh se tach va product name minh se tach
        //Output mong muon la nguyen chuoi keyword sau khi tach phai du diem 10 vi minh chi co 0 va 10 neu nhu trong chuoi tim kiem chi can 1 tu khon similar dong ngia voi viec khong co product
        //Khi output khong co product viec do se luu lai toan bo keyword da tra ve 404 va tu lay cai tu khong co de tim kiem tren product name neu khong co tren product name thi con nguoi se can thiep
        //Neu nhu con nguoi cam thay khong co loai san pham nay thi keyword nay se được điều chỉnh lại tức là keyword 404 se có trường etc_1 là keyword similar
        $array_split_key = explode(" ",$key_word);
        //Ở day la tieng viet nen thong nhat la se tim sau khi bo dau hoac la co dau vi co dau similar text gan nhu la kho xac dinh
        //Minh thu 2 truong hop 1 truong la co dau
        foreach($data_name as $_product){
            //truong hop co dau
            //$name = $this->removeSign($_product["product_name"]);
            $name = $_product["product_name"];
            $entity_id = $_product["product_id"];
            $point = 0;
            $countKeyWord = 0;
            $array_name_product = explode(" ",$name);
            $coun_correct = 0;
            foreach($array_split_key as $_key){
                foreach($array_name_product as $_name){
                    //dùng khoảng cách leven check nó , số bước chuyển
                    $step = $this->levenshtein_php($_key,$_name);
                    if($step < 2){
                        //Ok
                        $coun_correct++;
                        break;
                    }
                }
            }
            //After count correct sum count keyword show the entity
            if($coun_correct >= (count($array_split_key) * 0.4)){
                $array_entity_id [] = $entity_id;
            }            
        }
        //Truong hop khong dau
        $array_result ["correct_product"] = array_unique($array_entity_id);
        $array_result ["error_product"] = array_unique($array_entity_error);
        /*if(count($array_entity_error) > count($array_entity_id)){
            $array_result ["error_product"] = [];
        }*/
        return $array_result;
    }
    
    /**
     * Function Check if s1 was belong to s2
     *
     * @param [type] $sub // searching
     * @param [type] $parent // input string
     * @return void
     */
    public function indexOfString($sub,$parent)
    {
        //trim sach 2 cai
        $sub = trim($sub);
        $parent = trim($parent);
        //cách hay nhát là dung explode
        $array_check = explode($sub,$parent);
        if(count($array_check) > 1){
            return true; //10 point
        }else{
            return false; //0 point
        }
    }
}