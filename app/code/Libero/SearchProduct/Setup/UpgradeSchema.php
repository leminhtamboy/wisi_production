<?php
namespace Libero\SearchProduct\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $setup->run("CREATE TABLE `libero_search_keyword` ( `entity_id` INT NOT NULL AUTO_INCREMENT, `keyword` VARCHAR(255) NOT NULL, `product_id` VARCHAR(255) NOT NULL, `product_category` VARCHAR(255) NULL, `access_ip` VARCHAR(255) NOT NULL, `user_agent` VARCHAR(255) NOT NULL, `created_at` VARCHAR(255) NOT NULL, PRIMARY KEY (`entity_id`));");
        }
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $setup->run("CREATE TABLE `libero_search_product_output` (`id_output` INT NOT NULL AUTO_INCREMENT, `text_search` VARCHAR(45) NULL, `id_customer` VARCHAR(45) NULL, `ip` VARCHAR(45) NULL, `list_product` TEXT(1500) NULL, `category_id` VARCHAR(45) NULL, `suggested_product` VARCHAR(45) NULL, `suggested_key` VARCHAR(45) NULL, `etc_1` VARCHAR(45) NULL, `etc_2` VARCHAR(45) NULL, PRIMARY KEY (`id_output`)) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8; ");
        }

        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $setup->run("ALTER TABLE `libero_search_product_output` CHANGE COLUMN `text_search` `keyword` VARCHAR(45) NOT NULL , CHANGE COLUMN `id_customer` `customer_id` VARCHAR(45) NULL DEFAULT NULL , CHANGE COLUMN `ip` `access_ip` VARCHAR(45) NULL DEFAULT NULL , CHANGE COLUMN `list_product` `list_product` TEXT NOT NULL , CHANGE COLUMN `category_id` `product_category` VARCHAR(45) NULL DEFAULT NULL ;");
        }

        $setup->endSetup();
    }
}
