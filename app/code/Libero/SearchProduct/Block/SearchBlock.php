<?php
namespace Libero\SearchProduct\Block;

use \Magento\Framework\View\Element\Template\Context;
class SearchBlock extends \Magento\Framework\View\Element\Template {

    protected $_connection;

    /**
     * Constructor
     *
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resource,
        Context $context,
        array $data = []
    )
    {
        $this->_connection = $resource->getConnection();
        parent::__construct($context, $data);
    }
    
    public function getSearchResults(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->create("\Libero\Customer\Block\Seller\Admin\Util");
        $requestHttp = $objectManager->create("\Magento\Framework\App\Request\Http");

        $product_list_mode = "";
        if($requestHttp->getParam("product_list_mode")) {
            $product_list_mode = $requestHttp->getParam("product_list_mode");
        }
        
        $pageSize = 20;
        $curPage= 1;

        if($requestHttp->getParam("pageSize")) {
            $pageSize = $requestHttp->getParam("pageSize");
        }

        if($requestHttp->getParam("page")) {
            $curPage = $requestHttp->getParam("page");
        }

        $search_cate = $requestHttp->getParam("cat");
        
        $helperData = $objectManager->create("\Libero\Customer\Helper\Data");
        $search_key = $helperData->RemoveSign(trim(str_replace("+"," ",$requestHttp->getParam("q"))));

        $id_seller = "";
        if($requestHttp->getParam("id_seller")){
            $id_seller = $requestHttp->getParam("id_seller");
        }

        $id_seller_attribute= ($id_seller != "") ? $this->getIdSellerAttribute() : "";
        $ids_render = array();
        $ids_by_description = array();


        /* search by meta keyword */
        $meta_key_attribute = $this->getMetaKeywordsAttribute();
        $meta_key_sql = "";
        if($id_seller != ""){
            $meta_key_sql = $this->getSearchSqlWithIdSeller("catalog_product_entity_text", "catalog_product_entity_varchar", $id_seller_attribute, $id_seller, $meta_key_attribute, array($search_key));
        } else {
            if($search_cate!=""){
                $meta_key_sql = $this->getSearchSqlWithCate("catalog_product_entity_text","catalog_category_product",$meta_key_attribute,array($search_key),$search_cate);
            } else {
                $meta_key_sql = $this->getSearchSqlWithoutCate("catalog_product_entity_text",$meta_key_attribute,array($search_key));
            }
        }
        $meta_key_total = $this->_connection->fetchAll($meta_key_sql);
        $total = $util->getTotalArray($meta_key_total);
        $total_ids = array();
        foreach( $meta_key_total as $_id) {
            $total_ids[] = $_id["product_id"];
        }
        $meta_key_sql .= " LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";
        $meta_key_result = $this->_connection->fetchAll($meta_key_sql);
        foreach( $meta_key_result as $_id) {
            $ids_render[] = $_id["product_id"];
        }


        /* search by name */
        $product_name_attribute = $this->getProductNameAttribute();
        $name_sql = "";
        if($id_seller != ""){
            $name_sql = $this->getSearchSqlWithIdSeller("catalog_product_entity_varchar", "catalog_product_entity_varchar", $id_seller_attribute, $id_seller, $product_name_attribute, array($search_key));
        } else {
            if($search_cate!=""){
                $name_sql = $this->getSearchSqlWithCate("catalog_product_entity_varchar","catalog_category_product",$product_name_attribute,array($search_key),$search_cate);
            } else {
                $name_sql = $this->getSearchSqlWithoutCate("catalog_product_entity_varchar",$product_name_attribute,array($search_key));
            }
        }
        $name_result = $this->_connection->fetchAll($name_sql);
        foreach( $name_result as $_id) {
            if(!in_array($_id["product_id"],$total_ids)){
                $total++;
                $total_ids[] = $_id["product_id"];
            }
        }
        if(count($ids_render) < $pageSize){
            $count_ids_remaining = $pageSize - count($ids_render);
            $name_sql .= " LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";
            $name_result = $this->_connection->fetchAll($name_sql);
            foreach( $name_result as $_id) {
                if($count_ids_remaining > 0) {
                    if(!in_array($_id["product_id"],$ids_render)){
                        $ids_render[] = $_id["product_id"];
                    }
                }
                $count_ids_remaining--;
            }
        }


        /* search by description */
        $description_attribute = $this->getDescriptionAttribute();
        $description_sql = "";
        if($id_seller != ""){
            $description_sql = $this->getSearchSqlWithIdSeller("catalog_product_entity_text", "catalog_product_entity_varchar", $id_seller_attribute, $id_seller, $description_attribute, array($search_key));
        } else {
            if($search_cate!=""){
                $description_sql = $this->getSearchSqlWithCate("catalog_product_entity_text","catalog_category_product",$description_attribute,array($search_key),$search_cate);
            } else {
                $description_sql = $this->getSearchSqlWithoutCate("catalog_product_entity_text",$description_attribute,array($search_key));
            }
        }
        $description_result = $this->_connection->fetchAll($description_sql);
        foreach( $description_result as $_id) {
            if(!in_array($_id["product_id"],$total_ids)){
                $total++;
                $total_ids[] = $_id["product_id"];
            }
        }
        if(count($ids_render) < $pageSize){
            $count_ids_remaining = $pageSize - count($ids_render);
            $description_sql .= " LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";
            $description_result = $this->_connection->fetchAll($description_sql);
            foreach( $description_result as $_id) {
                if($count_ids_remaining > 0) {
                    if(!in_array($_id["product_id"],$ids_render)){
                        $ids_render[] = $_id["product_id"];
                    }
                }
                $count_ids_remaining--;
            }
        }
        $lastPage = $util->getLastPageByTotal($total,$pageSize);
        $productDependency = $objectManager->create("Magento\Catalog\Model\Product");
        $productListingBlock = $objectManager->create("Libero\Customer\Block\Seller\Admin\ProductListing");
        $products = array();
        foreach($ids_render as $_id){
            $_product = $productDependency->load($_id);
            $visibility = $_product->getVisibility();
            if($visibility == 4){
                $image_url = $productListingBlock->resizeImage($_product,"product_base_image",165,165)->getUrl();
                $item = array(
                    "product_id" => $_product->getData("entity_id"),
                    "specialfromdate" => $_product->getSpecialFromDate(),
                    "specialtodate" => $_product->getSpecialToDate(),
                    "newsfromdate" => $_product->getNewsFromDate(),
                    "newstodate" => $_product->getNewsToDate(),
                    "is_sale" => $_product->getIsSalable(),
                    "name" => $_product->getData("name"),
                    "small_image" => $image_url,
                    "price" => $util->formatPrice($_product->getData("price")),
                    "special_price" => $util->formatPrice($_product->getData("special_price")),
                    "url_key" => $_product->getProductUrl(),
                    "short_description" => $_product->getShortDescription(),
                    "product_object" => $_product
                );
                $_product_category = $_product->getLastCategoryId();
                    if($_product_category == ""){
                        foreach($_product->getCategoryIds() as $_cate_id){
                            $_product_category .= $_cate_id . "|";
                        }
                        $_product_category = substr($_product_category,0,strlen($_product_category)-1);
                }
                $item["product_category"] = $_product_category;
                $products[] = $item;
            }
        }
        
        $result["products"] = $products;




 
        $curPage = ($util->getCurPageArray() < $lastPage) ? $util->getCurPageArray() : $lastPage;
        $curPage = ($curPage > 0) ? $curPage : 1;
        $firstNumber = $util->getFirstNumArray();

        $result = array(
            "id_seller" => $id_seller,
            "search_cate" => $search_cate,
            "search_key" => $search_key,
            "total" => $total,
            "pageSize" => $pageSize,
            "curPage" => $curPage,
            "lastPage" => $lastPage,
            "listRecords" => $products,
            "firstNumber" => $firstNumber,
            "product_list_mode" => $product_list_mode
        );
        return $result;
    }

    public function getSearchResultsAdvanced(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->create("\Libero\Customer\Block\Seller\Admin\Util");
        $requestHttp = $objectManager->create("\Magento\Framework\App\Request\Http");
        $searchHelper = $objectManager->get('Libero\SearchProduct\Helper\Data');
        $product_list_mode = "";
        if($requestHttp->getParam("product_list_mode")) {
            $product_list_mode = $requestHttp->getParam("product_list_mode");
        }
        
        $pageSize = 20;
        $curPage= 1;
        $total = 0;
        if($requestHttp->getParam("pageSize")) {
            $pageSize = $requestHttp->getParam("pageSize");
        }

        if($requestHttp->getParam("page")) {
            $curPage = $requestHttp->getParam("page");
        }

        $search_cate = $requestHttp->getParam("cat");
        $helperData = $objectManager->create("\Libero\Customer\Helper\Data");
        $search_ket_have_sign = trim(str_replace("+"," ",$requestHttp->getParam("q")));
        //$search_key = $helperData->RemoveSign($search_ket_have_sign);
        $search_key = $search_ket_have_sign;
        $search_key_arr = explode(" ", $search_key);

        $id_seller = "";
        if($requestHttp->getParam("id_seller")){
            $id_seller = $requestHttp->getParam("id_seller");
        }

        $id_seller_attribute= ($id_seller != "") ? $this->getIdSellerAttribute() : "";
        $ids_render = array();
        $ids_by_description = array();

        /* use full keyword to search by name without Vietnamese character */
        /*$product_meta_name = $this->getProductNameAttribute();
        $name_sql = "";
        if($id_seller != ""){
            $name_sql = $this->getSearchSqlWithIdSeller("catalog_product_entity_varchar", "catalog_product_entity_varchar", $id_seller_attribute, $id_seller, $product_meta_name, array($search_key));
        } else {
            if($search_cate!=""){
                $name_sql = $this->getSearchSqlWithCate("catalog_product_entity_varchar","catalog_category_product",$product_meta_name, array($search_key),$search_cate);
            } else {
                $name_sql = $this->getSearchSqlWithoutCate("catalog_product_entity_varchar",$product_meta_name, array($search_key));
            }
        }
        $name_result = $this->_connection->fetchAll($name_sql);

        $total = $util->getTotalArray($name_result);
        $total_ids = array();
        $exactSearchPoints = array();
        
        foreach($name_result as $_id) {
            $total_ids[] = $_id["product_id"];
            $exactSearchPoints[] = 1;
        }

        $name_sql .= " LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";
        $name_result = $this->_connection->fetchAll($name_sql);
        foreach( $name_result as $_id) {
            $ids_render[] = $_id["product_id"];
        }
        */



        /* use split keyword to search by name without Vietnamese character */
        $name_sql_split = "";
        /*foreach($search_key_arr as $_key){
            if($id_seller != ""){
                $name_sql_split = $this->getSearchSqlWithIdSeller("catalog_product_entity_varchar", "catalog_product_entity_varchar", $id_seller_attribute, $id_seller, $product_meta_name, array($_key));
            } else {
                if($search_cate!=""){
                    $name_sql_split = $this->getSearchSqlWithCate("catalog_product_entity_varchar","catalog_category_product",$product_meta_name,array($_key),$search_cate);
                } else {
                    $name_sql_split = $this->getSearchSqlWithoutCate("catalog_product_entity_varchar",$product_meta_name,array($_key));
                }
            }
            $name_result_split = $this->_connection->fetchAll($name_sql_split);
            foreach( $name_result_split as $_id) {
                if(!in_array($_id["product_id"],$total_ids)){
                    $total++;
                    $total_ids[] = $_id["product_id"];
                    $exactSearchPoints[] = ;
                }
            }
            if(count($ids_render) < $pageSize){
                $count_ids_remaining = $pageSize - count($ids_render);
                $name_result_split .= " LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";
                $name_result_split = $this->_connection->fetchAll($name_result_split);
                foreach( $name_result_split as $_id) {
                    if($count_ids_remaining > 0) {
                        if(!in_array($_id["product_id"],$ids_render)){
                            $ids_render[] = $_id["product_id"];
                        }
                    }
                    $count_ids_remaining--;
                }
            }
        }*/
        //Bước 1 : lấy key word đã được xử lý trong bảng key word search
        //Xử lý keyword ngay tại đây tức là get ra được best keyword
        $best_key_word = $searchHelper->getBestKeyWord($search_key);
        //Cam best keyword dem di tim tiep trong danh sach keyword input
        $data_search_output = $searchHelper->getKeyWordFromCache($search_key);
        //Neu co trong database thi get danh sach id product 
		$temp_product = array();
        if(count($data_search_output) > 0 ){
            //danh dau va render ra product id va category id chang han
            foreach($data_search_output as $_output){   
                $list_product = $_output["list_product"];
                $ids = explode(",",$list_product);
                foreach($ids as $_id_product){
                    $temp_product[] = $_id_product;
                }
            }
        }else{
            //nguoc lai thi xu ly bang thuat toan AI
            //Viec dau tien take rat nhieu time cho keyword khong tim thay => 404 not found of key
            //Nen suy nghi co nen dung API ben ngoai get vao khong vi magento get keyword va query rat lau
            $array_result = $searchHelper->getProductList($search_key,$id_seller,$id_seller_attribute);
            $array_entity_id = $array_result["correct_product"];
            $array_entity_id_error = $array_result["error_product"];
            foreach($array_entity_id as $_entity_id){
                $temp_product[] = $_entity_id;
                $total++;
            }
            foreach($array_entity_id_error as $_entity_id){
                $temp_product[] = $_entity_id;
                $total++;
            }
        }
		if($curPage < 2){
			$start = 0;
			$end = $pageSize;
		}else{
			$start = ($curPage - 1) * $pageSize;
			$end = $pageSize * $curPage;
		}
		$total = 0 ;
		for($p = $start; $p < $end ; $p++){
			$total++;
			$ids_render[] = $temp_product[$p];
		}
        //var_dump($ids_render);
        //die;
        //var_dump($array_entity_id_error);
        //var_dump($ids_render);
        //die;
        /* search by meta keyword */
        /*$meta_key_attribute = $this->getMetaKeywordsAttribute();
        $meta_key_sql = "";
        if($id_seller != ""){
            $meta_key_sql = $this->getSearchSqlWithIdSeller("catalog_product_entity_text", "catalog_product_entity_varchar", $id_seller_attribute, $id_seller, $meta_key_attribute, array($search_key));
        } else {
            if($search_cate!=""){
                $meta_key_sql = $this->getSearchSqlWithCate("catalog_product_entity_text","catalog_category_product",$meta_key_attribute,array($search_key),$search_cate);
            } else {
                $meta_key_sql = $this->getSearchSqlWithoutCate("catalog_product_entity_text",$meta_key_attribute,array($search_key));
            }
        }
        $meta_key_total = $this->_connection->fetchAll($meta_key_sql);
        foreach( $meta_key_total as $_id) {
            if(!in_array($_id["product_id"],$total_ids)){
                $total++;
                $total_ids[] = $_id["product_id"];
                $exactSearchPoints[] = 1;
            }
        }
        if(count($ids_render) < $pageSize){
            $count_ids_remaining = $pageSize - count($ids_render);
            $meta_key_sql .= " LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";
            $meta_key_total = $this->_connection->fetchAll($meta_key_sql);
            foreach( $meta_key_total as $_id) {
                if($count_ids_remaining > 0) {
                    if(!in_array($_id["product_id"],$ids_render)){
                        $ids_render[] = $_id["product_id"];
                    }
                }
                $count_ids_remaining--;
            }
        }*/


        /* search by description */
        /*$description_attribute = $this->getDescriptionAttribute();
        $description_sql = "";
        if($id_seller != ""){
            $description_sql = $this->getSearchSqlWithIdSeller("catalog_product_entity_text", "catalog_product_entity_varchar", $id_seller_attribute, $id_seller, $description_attribute, array($search_key));
        } else {
            if($search_cate!=""){
                $description_sql = $this->getSearchSqlWithCate("catalog_product_entity_text","catalog_category_product",$description_attribute,array($search_key),$search_cate);
            } else {
                $description_sql = $this->getSearchSqlWithoutCate("catalog_product_entity_text",$description_attribute,array($search_key));
            }
        }
        $description_result = $this->_connection->fetchAll($description_sql);
        foreach( $description_result as $_id) {
            if(!in_array($_id["product_id"],$total_ids)){
                $total++;
                $total_ids[] = $_id["product_id"];
                //$exactSearchPoints[] = ;
            }
        }
        if(count($ids_render) < $pageSize){
            $count_ids_remaining = $pageSize - count($ids_render);
            $description_sql .= " LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";
            $description_result = $this->_connection->fetchAll($description_sql);
            foreach( $description_result as $_id) {
                if($count_ids_remaining > 0) {
                    if(!in_array($_id["product_id"],$ids_render)){
                        $ids_render[] = $_id["product_id"];
                    }
                }
                $count_ids_remaining--;
            }
        }*/
        $lastPage = $util->getLastPageByTotal($total,$pageSize);
        $productListingBlock = $objectManager->create("Libero\Customer\Block\Seller\Admin\ProductListing");
        $products = array();
        $ids_render = array_unique($ids_render);
        foreach($ids_render as $_id){
            $productDependency = $objectManager->create("Magento\Catalog\Model\Product");
            $_product = $productDependency->load($_id);
            $visibility = $_product->getVisibility();
            $enable = $_product->getStatus();
            if ($visibility == 4 && $enable == 1) {
                $image_url = $productListingBlock->resizeImage($_product,"product_base_image",165,165)->getUrl();
                $item = array(
                    "product_id" => $_product->getData("entity_id"),
                    "specialfromdate" => $_product->getSpecialFromDate(),
                    "specialtodate" => $_product->getSpecialToDate(),
                    "newsfromdate" => $_product->getNewsFromDate(),
                    "newstodate" => $_product->getNewsToDate(),
                    "is_sale" => $_product->getIsSalable(),
                    "name" => $_product->getData("name"),
                    "small_image" => $image_url,
                    "price" => $util->formatPrice($_product->getData("price")),
                    "special_price" => $util->formatPrice($_product->getData("special_price")),
                    "url_key" => $_product->getProductUrl(),
                    "short_description" => $_product->getShortDescription(),
                    "product_object" => $_product
                );
                $_product_category = $_product->getLastCategoryId();
                    if($_product_category == ""){
                        foreach($_product->getCategoryIds() as $_cate_id){
                            $_product_category .= $_cate_id . "|";
                        }
                        $_product_category = substr($_product_category,0,strlen($_product_category)-1);
                }
                $item["product_category"] = $_product_category;
                $products[] = $item;
            }
        }
        


 
        $curPage = ($util->getCurPageArray() < $lastPage) ? $util->getCurPageArray() : $lastPage;
        $curPage = ($curPage > 0) ? $curPage : 1;
        $firstNumber = $util->getFirstNumArray();

        $result = array(
            "id_seller" => $id_seller,
            "search_cate" => $search_cate,
            "search_key" => $search_key,
            "total" => $total,
            "pageSize" => $pageSize,
            "curPage" => $curPage,
            "lastPage" => $lastPage,
            "listRecords" => $products,
            "firstNumber" => $firstNumber,
            "product_list_mode" => $product_list_mode
        );
        return $result;
    }

    public function getProductNameAttribute(){
        $sql = "SELECT * FROM eav_attribute WHERE attribute_code = 'name' AND entity_type_id = 4;";
        $result = $this->_connection->fetchAll($sql)[0]["attribute_id"];
        return $result;
    }

    public function getIdSellerAttribute(){
        $sql = "SELECT * FROM eav_attribute WHERE attribute_code = 'id_seller';";
        $result = $this->_connection->fetchAll($sql)[0]["attribute_id"];
        return $result;
    }

    public function getDescriptionAttribute(){
        $sql = "SELECT * FROM eav_attribute WHERE attribute_code = 'description' AND entity_type_id = 4;";
        $result = $this->_connection->fetchAll($sql)[0]["attribute_id"];
        return $result;
    }

    public function getMetaKeywordsAttribute(){
        $sql = "SELECT * FROM eav_attribute WHERE attribute_code = 'meta_keyword';";
        $result = $this->_connection->fetchAll($sql)[0]["attribute_id"];
        return $result;
    }

    //getSearchSqlWithCate( string , string , string , array, string)
    public function getSearchSqlWithCate($search_key_table, $search_cate_table, $attribute_id, $search_key, $search_cate){
        $sql = "
            SELECT 
                *, a.entity_id as product_id
            FROM
                $search_key_table AS a
                    JOIN
                $search_cate_table AS b 
                    ON a.entity_id = b.product_id
            WHERE
                attribute_id = $attribute_id ";
        foreach($search_key as $_key){
            $sql .= "AND value LIKE '%$_key%' ";
        }
        
        $sql .= "   AND category_id = $search_cate GROUP BY a.entity_id ";
        return $sql;
    }

    //getSearchSqlWithoutCate( string , string , array)
    public function getSearchSqlWithoutCate($search_key_table, $attribute_id, $search_key){
        $sql = "
            SELECT 
                *, entity_id AS product_id
            FROM
                $search_key_table
            WHERE
                attribute_id = $attribute_id ";
        
        foreach($search_key as $_key){
            $sql .= "AND value LIKE '%$_key%' ";
        }

        $sql .="GROUP BY entity_id ";
        return $sql;
    }
    
    //getSearchSqlWithIdSeller( string , string , string , string , string , array)
    public function getSearchSqlWithIdSeller($search_key_table, $seller_attribute_table, $attribute_id_seller, $id_seller, $product_name_attribute, $search_key){
        $sql = "
            SELECT 
                *, a.entity_id AS product_id
            FROM
                $search_key_table as a
                    JOIN
                (SELECT 
                    *
                FROM
                    $seller_attribute_table
                WHERE
                    attribute_id = $attribute_id_seller AND value = $id_seller
                GROUP BY entity_id) as b
                ON a.entity_id = b.entity_id
            WHERE
                a.attribute_id = $product_name_attribute ";
        
        foreach($search_key as $_key){
            $sql .= "AND value LIKE '%$_key%' ";
        }
        
        $sql .= "GROUP BY a.entity_id ";
        return $sql;
    }
}