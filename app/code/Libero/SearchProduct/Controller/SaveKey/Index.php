<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Libero\SearchProduct\Controller\SaveKey;

use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Search\Model\QueryFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Display search result
     *
     * @return void
     */
    public function execute()
    {
        $this->_objectManager->get('Magento\CatalogSearch\Helper\Data')->checkNotes();
        $util = $this->_objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        
        $helper = $this->_objectManager->get("Libero\Onestepcheckout\Helper\Data");
        $connection = $helper->getConnectionDatabaseDirect();

        $resultRedirect = $this->resultRedirectFactory->create();
        date_default_timezone_set("Asia/Bangkok");
        $curDateTime = date("Y-m-d H:i:s");

        try {
            $postData = $this->getRequest()->getPostValue();
            $keyword = $util->removeQuoteInString($postData["keyword"]);
            $product_id = $util->removeQuoteInString($postData["product_id"]);
            $product_category = $util->removeQuoteInString($postData["product_category"]);
            $access_ip = $_SERVER["REMOTE_ADDR"];
            $user_agent = $_SERVER["HTTP_USER_AGENT"];
            $created_at = $curDateTime;
            
            $sql_search_keyword = "INSERT INTO `libero_search_keyword` (`keyword`, `product_id`, `product_category`, `access_ip` , `user_agent` , `created_at`) VALUES ('$keyword','$product_id', '$product_category', '$access_ip', '$user_agent', '$created_at');";
            $connection->query($sql_search_keyword);



            
            $sql_search_product_output = "INSERT INTO `libero_search_product_output` (
            `keyword`,
            `product_id`,
            `product_category`,
            `access_ip` ,
            `user_agent` ,
            `created_at`) VALUES ('$keyword','$product_id', '$product_category', '$access_ip', '$user_agent', '$created_at');";

            
            $connection->query($sql_search_product_output);

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
            $resultRedirect->setPath('*/*');
            return $resultRedirect;
        }
    }
}
