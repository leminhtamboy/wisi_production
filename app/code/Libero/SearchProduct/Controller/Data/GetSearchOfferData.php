<?php
/**
 *
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Libero\SearchProduct\Controller\Data;

use Magento\Catalog\Model\Layer\Resolver;
use Magento\Catalog\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResourceConnection;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Search\Model\QueryFactory;

class GetSearchOfferData extends \Magento\Framework\App\Action\Action
{
    /**
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Display search result
     *
     * @return void
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $searchHelper = $objectManager->get('Libero\SearchProduct\Helper\Data');
        $connection = $resource->getConnection();

        $postData = $this->getRequest()->getPostValue();
        $keyword = $util->removeQuoteInString($postData["keyword"]);
        $result = array();
        $keywords = array();

        /* get related keyword */
        $relatedKeywords = $searchHelper->getRelatedKeyword($keyword);
        foreach($relatedKeywords as $_relatedKeyword){
            $keywords[] = $_relatedKeyword["keyword"];
        }
        $result["keywords"] = $keywords;


        /* get search category */
        $categories = $searchHelper->getCategoryIdsByKeyword($keyword);
        $categoryNames = array();
        $categoryIds = array();
        foreach($categories as $_category){
            $categoryNames[] = $searchHelper->getCategoryNameById($_category["product_category"]);
            $categoryIds[] = $_category["product_category"];
            
        }
        $result["categoryNames"] = $categoryNames;
        $result["categoryIds"] = $categoryIds;


        /* get top search product by keyword */
        $searchProductIds = $searchHelper->GetTopSearchProductByKeyword($keyword);
        $productIds = array();
        foreach($searchProductIds as $_searchProductId){
            $productIds[] = $_searchProductId["product_id"];
        }


        $products = array();
        $productDependency = $objectManager->create("Magento\Catalog\Model\Product");
        $productListingBlock = $objectManager->create("Libero\Customer\Block\Seller\Admin\ProductListing");
        $collectionProductFull = $productDependency->getCollection()->addAttributeToSelect("*")->addFieldToFilter("entity_id", array("in" => $productIds));
        foreach($collectionProductFull as $_product){
            $image_url = $productListingBlock->resizeImage($_product,"product_base_image",50,50)->getUrl();
            $item = array(
                "name" => $_product->getData("name"),
                "small_image" => $image_url,
                "price" => $util->formatPrice($_product->getData("price")),
                "special_price" => $util->formatPrice($_product->getData("special_price")),
                "url_key" => $_product->getData("url_key")
            );
            $products[] = $item;
        }
        $result["products"] = $products;
        
        

        $jsonData = json_encode($result);
        $jsonResultFactory->setData($jsonData);
        return $jsonResultFactory;

    }
}

