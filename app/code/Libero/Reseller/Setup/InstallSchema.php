<?php
namespace Libero\Reseller\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements  InstallSchemaInterface{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        //Do something
        $setup->getConnection();
        $setup->run("CREATE TABLE `wisi_production`.`libero_reseller` ( `id_reseller` INT NOT NULL AUTO_INCREMENT , `bussines_type` INT NOT NULL COMMENT '1 la personal , 2 la company' , `id_customer` INT NOT NULL , `email` INT NOT NULL , `shop_name` INT NOT NULL , `is_owner_ware_house` INT NOT NULL , `id_type_card` INT NOT NULL , `addtional_information` TEXT NOT NULL , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id_reseller`)) ENGINE = InnoDB;");
        $setup->endSetup();
    }
}
