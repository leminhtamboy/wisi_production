<?php
namespace Libero\Reseller\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.1') < 0) {
            $setup->run("CREATE TABLE `wisi_production`.`libero_reseller_ware_house` ( `id_ware_house` INT NOT NULL AUTO_INCREMENT , `id_reseller` INT NOT NULL , `street` VARCHAR(350) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `city` INT NOT NULL , `region` INT NOT NULL , `ward` INT NOT NULL , `addtional_information` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `etc` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_ware_house`)) ENGINE = InnoDB;");
        }
        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            $setup->run("CREATE TABLE `wisi_production`.`libero_reseller_bank` ( `id_bank` INT NOT NULL AUTO_INCREMENT , `id_reseller` INT NOT NULL , `bank_name` VARCHAR(150) NOT NULL , `bank_number` VARCHAR(100) NOT NULL , `bank_id` INT NOT NULL , `plus_information` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_bank`)) ENGINE = InnoDB;");    
        }
        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $setup->run("ALTER TABLE `libero_reseller` CHANGE `addtional_information` `plus_information` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        }
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $setup->run("ALTER TABLE `libero_reseller_ware_house` CHANGE `addtional_information` `plus_information` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;");
        }
        if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $setup->run("ALTER TABLE `libero_reseller` CHANGE `email` `email` VARCHAR(255) NOT NULL;");
        }
        if (version_compare($context->getVersion(), '1.0.6') < 0) {
            $setup->run("ALTER TABLE `libero_reseller` CHANGE `shop_name` `shop_name` VARCHAR(255) NOT NULL;");
        }
        if(version_compare($context->getVersion(), '1.0.7') < 0){
            $setup->run("ALTER TABLE `libero_reseller` ADD `status` INT NOT NULL COMMENT '1 : la dang cho duyet , 2 : la da chap nhan , 3: la tu choi' AFTER `updated_at`");
        }
        if(version_compare($context->getVersion(), '1.0.8') < 0){
            $setup->run("CREATE TABLE `wisi_production`.`libero_reseller_products` ( `id_reseller_product` INT NOT NULL AUTO_INCREMENT , `id_reseller` INT NOT NULL , `id_product` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `price` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `qty` INT NOT NULL , `etc` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_reseller_product`)) ENGINE = InnoDB;");
        }
        $setup->endSetup();
    }
}
