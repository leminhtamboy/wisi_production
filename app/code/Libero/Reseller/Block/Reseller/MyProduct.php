<?php
namespace Libero\Reseller\Block\Reseller;

use Magento\Framework\View\Element\Template;

class MyProduct extends \Magento\Framework\View\Element\Template{

    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }
    public function getUrlViewMyProducts()
    {
        $route = "reseller/account/view/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getUrlAddToStore()
    {
        $route = "reseller/addtostore/addtostore/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    
}