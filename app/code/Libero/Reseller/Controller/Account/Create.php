<?php
namespace Libero\Reseller\Controller\Account;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class Create extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultRedirect = $this->resultRedirectFactory->create();
        $postData = $this->getRequest()->getPostValue();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('\Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        
        try{
            /// Parameters
            $bussiness_type  = $postData["business_type"];
            if($bussiness_type == "personal"){
                $bussiness_type = 1;
            }else{
                $bussiness_type = 2;
            }
            $address_street  = $postData["address_street"];
            $password = $postData["password"];
            $password_confirmation = $postData["password_confirmation"];
            if ($password != $password_confirmation) {
                $this->messageManager->addError(__("Password confirm does not match"));
                return $resultRedirect->setPath("*/*");
            }
            $email = $postData["email"];
            $shopname = $postData["shopname"];
            $address_ware_house_reseller = $postData["address_ware_house_reseller"];
            $is_libero_warehouse = "";
            $warehouseAddress = [];
            $warehouseProvince = [];
            $warehouseDistrict = [];
            $warehouseWard = [];
            $wareProvinceText = [];
            $wareDistrictText = [];
            $wareWardText = [];

            if ($address_ware_house_reseller == "1") {              //Wisi warehouse
                $warehouseAddress = $postData["warehouseAddress"];
                $warehouseProvince = $postData["warehouseProvince"];
                $warehouseDistrict = $postData["warehouseDistrict"];
                $warehouseWard = $postData["warehouseWard"];
                $is_libero_warehouse = "1";
                $wareProvinceText = $postData["wareProvinceText"];
                $wareDistrictText = $postData["wareDistrictText"];
                $wareWardText = $postData["wareWardText"];
            } else {                                                //Reseller warehouse
                $address_ware_house_reseller = $postData["address_ware_house_reseller"];
                $warehouseAddress = $postData["sellerWarehouseAddress"];
                $warehouseProvince = $postData["sellerWarehouseProvince"];
                $warehouseDistrict = $postData["sellerWarehouseDistrict"];
                $warehouseWard = $postData["sellerWarehouseWard"];
                $is_libero_warehouse = "0";
                $wareProvinceText = $postData["sellerWareProvinceText"];
                $wareDistrictText = $postData["sellerWareDistrictText"];
                $wareWardText = $postData["sellerWareWardText"];
            }

            $branchName = $postData["branchName"];
            //$bankCode = $postData["bankCode"];
            $bankCode = "";
            $swift = "";
            //$bankDocument = $postData["bankDocument"];
            $cartType = $postData["idType"];
            $full_name = $postData["fullname"];
            $idNumber = $postData["idNumber"];
            $postCode = $postData["district"]."-".$postData["ward"];
            $tax = $postData["phone"];
            $city = $postData["provinceText"];
            $district = $postData["districtText"];
            $ward = $postData["ward_street"];
            $street = $postData["address_street"];
            $phone = $postData["phone"];

            //The Bank
            $bank_name = $postData["bank_name"];
            $bank_number = $postData["bankAccountNumber"];

            $array_bank = array();
            $array_bank["branch_name"] = $postData["branchName"];
            $array_bank["bank_code"] = $bankCode ;
            $array_bank["swift"] = $swift;
            $array_bank["bank_document"] = '';
            $json_bank = json_encode($array_bank);
            //End The Bank

            //Parameter For Upload Images
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $url = \Magento\Framework\App\ObjectManager::getInstance();
            $imagepath_front_id = "";
            $imagepath_back_id = "";
            //Upload image
            /*$fileUploaderFactory = $objectManager->get("\Magento\MediaStorage\Model\File\UploaderFactory");
            $uploader = $fileUploaderFactory->create(['fileId' => 'idFrontImg']);
            $uploader->setAllowedExtensions(['jpg','jpeg','png']);
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);
            $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
			$resellerNameRewrite = $this->rewriteUrl($full_name);
            $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('upload_manager/'.$resellerNameRewrite."/".$email."/".date("d-m-Y"));
			$pathSave = 'upload_manager/'.$resellerNameRewrite."/".$email."/".date("d-m-Y");
            $result = $uploader->save($path);
            $imagepath_front_id = $pathSave."/".$result['file'];

            $fileUploaderFactory = $objectManager->get("\Magento\MediaStorage\Model\File\UploaderFactory");
            $uploader_1 = $fileUploaderFactory->create(['fileId' => 'idBackImg']);
            $uploader_1->setAllowedExtensions(['jpg','jpeg','png']);
            $uploader_1->setAllowRenameFiles(false);
            $uploader_1->setFilesDispersion(false);
            $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
			$resellerNameRewrite = $this->rewriteUrl($full_name);
            $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('upload_manager/'.$resellerNameRewrite."/".$email."/".date("d-m-Y"));
			$pathSave = 'upload_manager/'.$resellerNameRewrite."/".$email."/".date("d-m-Y");
            $result = $uploader_1->save($path);
            $imagepath_back_id = $pathSave."/".$result['file'];*/
            //End Upload Image

            //Create Json For Plus Information
            $array_plus_information_reseller = array();
            $array_plus_information_reseller["id_name"] = $full_name;
            $array_plus_information_reseller["id_number"] = $idNumber;
            $array_plus_information_reseller["id_front_end"] = $imagepath_front_id;
            $array_plus_information_reseller["id_back_end"] = $imagepath_back_id;
            $json_cmnd = json_encode($array_plus_information_reseller,JSON_UNESCAPED_UNICODE);
            //End Json

            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $websiteId = $storeManager->getWebsite()->getWebsiteId();
            $customerFactory = $this->_objectManager->get('\Magento\Customer\Model\CustomerFactory');

            //Create Reseller For Get ID
            $sql_insert = "INSERT INTO `libero_reseller` (`id_reseller`, 
            `bussines_type`, 
            `id_customer`, 
            `email`, 
            `shop_name`, 
            `is_owner_ware_house`, 
            `id_type_card`, 
            `plus_information`, 
            `created_at`, 
            `updated_at`) 
            VALUES (NULL, '$bussiness_type', 
            '1', '$email', '$shopname', '$address_ware_house_reseller', '$cartType', '$json_cmnd', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";
            $connection->query($sql_insert);
            $id_reseller = $connection->lastInsertId();
            //Create Customer On Magento With Another Group
            $customer = $customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->setEmail($email);
            $customer->setFirstname($full_name);
            $customer->setLastname("");
            $customer->setPassword($password);
            $customer->setGroupId(3); //Group id la2 3 tuc la cua reseller
            $customer->save();
            $customer_id = $customer->getId();
            $addresssModel = $this->_objectManager->get('\Magento\Customer\Model\AddressFactory');
            $addressModel = $addresssModel->create();
            $addressModel->setCustomerId($customer_id)
                ->setFirstname($full_name)
                ->setLastname($full_name)
                ->setCountryId('VN')
                ->setPostcode($postCode)
                ->setCity($city)    
                ->setTelephone($phone)
                ->setFax($tax)
                ->setStreet($street.", ".$ward.", ".$district)
                ->setIsDefaultBilling('1')
                ->setIsDefaultShipping('1')
                ->setSaveInAddressBook('1');
            $addressModel->save();
            $sql_update_customer_id = "UPDATE `libero_reseller` SET `id_customer` = '$customer_id' WHERE `id_reseller` = '$id_reseller'";
            $connection->query($sql_update_customer_id);


            //Create Ware House
            $warehouseId = "";
            $addrLength = count($warehouseAddress);
            for ($i = 0; $i < $addrLength; $i++) {
                $_addr = $warehouseAddress[$i];
                $_prov = $warehouseProvince[$i];
                $_dist = $warehouseDistrict[$i];
                $_ward = $warehouseWard[$i];
                $_region = $_dist . "-" . $_ward;
                $_fulladdr = "$_addr, $wareWardText[$i], $wareDistrictText[$i], $wareProvinceText[$i]";
                $_plusInfo = '{"full_address":"'.$_fulladdr.'", "address":"'. $_addr .'", "province":"'.$wareProvinceText[$i].'", "district":"'.$wareDistrictText[$i].'", "ward":"'.$wareWardText[$i].'"}';
                
                $sql_insert_warehouse = "INSERT INTO `libero_reseller_ware_house` 
                (`id_reseller`, `is_libero_warehouse`, `street`, `city`, `region`, `district`, `ward`, `plus_information`) VALUES 
                ('$id_reseller', '$is_libero_warehouse', '$_addr', '$_prov', '$_region', '$_dist', '$_ward', '$_plusInfo')";
                $connection->query($sql_insert_warehouse);
                if ($i == 0) {
                    $warehouseId .= $connection->lastInsertId();
                } else {
                    $warehouseId .= "|$connection->lastInsertId()";
                }
            }

            $is_owner_ware_house = "";
            if ($address_ware_house_reseller == "1") {              //Wisi warehouse
                $is_owner_ware_house = "1";
            } else {                                                //Reseller warehouse
                $is_owner_ware_house = "2";
            }
            
            $sql_update_warehouse = "UPDATE `libero_reseller` SET `id_customer` = '$customer_id', `id_warehouse` = '$warehouseId', `is_owner_ware_house` = '$is_owner_ware_house' WHERE `id_reseller` = '$id_reseller'";
            $connection->query($sql_update_warehouse);

            //Create Bank Account
            $sql_insert_bank = "INSERT INTO `libero_reseller_bank`(`id_bank`
            , `id_reseller`
            , `bank_name`
            , `bank_number`
            , `bank_id`
            , `plus_information`)  VALUES (NULL, '$id_reseller', '$bank_name', '$bank_number', '1', '$json_bank')";

            $connection->query($sql_insert_bank);
            /////////////
            /*if ($customer && $customer->getId()) {
                $session  = $objectManager->create("\Magento\Customer\Model\Session");
                $session->setCustomerAsLoggedIn($customer);
                $session->regenerateId();
            }*/
            $this->messageManager->addSuccess(__("Create Reseller Successfully"));
            $resultRedirect->setPath('/');
            return $resultRedirect;
        }catch(Excepton $e){

        }
    }
    /**
     * Function rewrite Url
     *
     * @param [type] $store_name
     * @return void
     */
    public function rewriteUrl($store_name){
        $urlRewrite = trim($this->removeSign($store_name));
        $urlRewrite = preg_replace('#[^0-9a-z]+#i', '-', $urlRewrite);
        $urlRewrite = strtolower($urlRewrite);
        return $urlRewrite;
    }

    /**
     * Function bo dau
     *
     * @param [type] $str
     * @return void
     */
    function removeSign($str){
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }
}
