<?php
namespace Libero\Reseller\Controller\AddToStore;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class AddToStore extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $dataPost = $this->getRequest()->getPostValue();
        $id_reseller = $dataPost["id_reseller"];
        $qty = $dataPost["qty"];
        $id_product = $dataPost["id_product"];
        $etc = "";
        $sql_select_product = "SELECT * FROM `libero_reseller_products` WHERE `id_reseller` = '$id_reseller' AND `id_product` = '$id_product';";
        $data_products = $connection->fetchAll($sql_select_product);

        if(count($data_products) > 0){
            $old_qty = intval($data_products[0]["qty"]);
            $new_qty = $old_qty + intval($qty);
            $id_reseller_product = $data_products[0]["id_reseller_product"];
            $sql_update = "UPDATE `libero_reseller_products` SET `qty` = '$new_qty' WHERE `id_reseller_product` = '$id_reseller_product'";
            $connection->query($sql_update);
            $result_json["success"] = true;
            $result_json["code"] = 200;
            $result_json["message"] = "Added Product To Your Store";
            $resultJson = $jsonResultFactory->setData($result_json);
        }else{
            $sql_insert = "INSERT INTO `libero_reseller_products` (`id_reseller_product`, `id_reseller`, `id_product`, `price`, `qty`, `etc`) 
            VALUES (NULL, '$id_reseller', '$id_product', '1', '$qty', '$etc')";
            $connection->query($sql_insert);
            $last_insert_id = $connection->lastInsertId();
            if($last_insert_id){
                $result_json["success"] = true;
                $result_json["code"] = 200;
                $result_json["message"] = "Added Product To Your Store";
                $resultJson = $jsonResultFactory->setData($result_json);
            }else{
                $result_json["success"] = false;
                $result_json["code"] = 400;
                $result_json["message"] = "Can not add this product to store";
                $resultJson = $jsonResultFactory->setData($result_json);
            }   
        }
        return $resultJson;
    }
}