<?php
namespace Libero\Reseller\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    public function getReseller()
    {

    }
    public function checkifResellerLogin()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        if($customerSession->isLoggedIn()) {
            return true;
        }
        return false;
    }
    public function checkIsReseller()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $customer = $customerSession->getCustomer();
        if($customer->getId()){
            $group_id = $customer->getGroupId();
            if($group_id == 3){
                return true;
            }
            else{
                false;
            }
        }else{
            return false;
        }
    }
    
    public function checkIsResellerWithCustomer($customer)
    {
        if($customer->getId()){
            $group_id = $customer->getGroupId();
            if($group_id == 3){
                return true;
            }
            else{
                false;
            }
        }else{
            return false;
        }
    }
    public function getUrlViewMyProducts()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $blockMyProduct = $objectManager->get('\Libero\Reseller\Block\Reseller\MyProduct');
        $url = $blockMyProduct->getUrlViewMyProducts();
        return $url;
    }
    public function getUrlAddToStore()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $blockMyProduct = $objectManager->get('\Libero\Reseller\Block\Reseller\MyProduct');
        $url = $blockMyProduct->getUrlAddToStore();
        return $url;
    }
    public function getCustomer()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('Magento\Customer\Model\Session');
        $customer = $customerSession->getCustomer();
        if($customer->getId()){

            return $customer;

        }
        return null;
    }
    public function getResellerInformation()
    {
        $customer = $this->getCustomer();
        $id_customer = $customer->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = "SELECT * FROM `libero_reseller` WHERE id_customer = '$id_customer'";
        $data_reseller = $connection->fetchAll($sql);
        return $data_reseller[0];
    }
    public function getProductOfReseller()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $reseller = $this->getResellerInformation();
        $id_reseller = $reseller['id_reseller'];
        $sql_get_product = "SELECT * FROM `libero_reseller_products` WHERE id_reseller = '$id_reseller'";
        $data_products = $connection->fetchAll($sql_get_product);
        return $data_products;
    }
    
    public function resizeImage($product, $imageId, $width, $height = null)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productImageHelper = $objectManager->get('\Magento\Catalog\Helper\Image');
        $resizedImage = $productImageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }

}