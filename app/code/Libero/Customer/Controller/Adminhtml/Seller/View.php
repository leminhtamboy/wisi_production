<?php
namespace Libero\Customer\Controller\Adminhtml\Seller;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Webapi\Exception;

class View extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $registry;
    public function __construct(\Magento\Backend\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $post = $this->getRequest()->getPostValue();
            $idSeller = $post["id_seller"];
            $id_customer = $post["id_customer"];
            $customer = $this->_objectManager->get('\Magento\Customer\Model\Customer')->load($id_customer);
            if ($customer && $customer->getId()) {
                $session  = $this->_objectManager->create("\Magento\Customer\Model\Session");
                $session->setCustomerAsLoggedIn($customer);
                $session->regenerateId();
            }
            $storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $store = $storeManager->getStore();
            $jsonResultFactory = $this->_objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $url_ret = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB)."/customer/admin/";
            $result = json_encode(['url'=>"$url_ret"]);
            $resultJson = $jsonResultFactory->setData($result);
            return $resultJson;
        }catch (Exception $e){
            $this->messageManager->addError(__($e->getMessage()));
        }
    }
}