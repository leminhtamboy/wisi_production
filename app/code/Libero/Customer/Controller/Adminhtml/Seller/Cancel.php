<?php
namespace Libero\Customer\Controller\Adminhtml\Seller;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Webapi\Exception;

class Cancel extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $registry;
    public function __construct(\Magento\Backend\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $post = $this->getRequest()->getPostValue();
            $idSeller = $post["id_seller"];
            $message = $post["message"];
            //Create
            $modelSeller = $this->_objectManager->create("\Libero\Customer\Model\CustomerSeller");
            $seller = $modelSeller->load($idSeller);
            $seller->setData("status",2);
            $seller->setData("message_denied",$message);
            $seller->save();
            $this->messageManager->addSuccess(__('Cancel Seller Successfully.'));
            //return $resultRedirect->setPath("*/*");
        }catch (Exception $e){
            $this->messageManager->addError(__($e->getMessage()));
        }
    }
}