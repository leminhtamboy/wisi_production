<?php
namespace Libero\Customer\Controller\Adminhtml\Seller;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Webapi\Exception;

class Save extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $registry;
    public function __construct(\Magento\Backend\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        try {

            $resultRedirect = $this->resultRedirectFactory->create();
            $post = $this->getRequest()->getPostValue();
            $idSeller = $post["id_seller"];
            $id_category = $post["category_id"];
            $delivery_type = $post["shipping_method"];
            $string_cate = "";
            foreach($id_category as $_cate){
                $string_cate.=$_cate."|";
            }
            $string_cate = substr($string_cate,0,strlen($string_cate) -1 );

            //Create
            $modelSeller = $this->_objectManager->create("\Libero\Customer\Model\CustomerSeller");
            $seller = $modelSeller->load($idSeller);
            $seller->setData("id_category",$string_cate);
            $seller->setData('delivery_type',$delivery_type);
            $seller->save();
            $this->messageManager->addSuccess(__('Save Seller Successfully.'));

            return $resultRedirect->setPath('*/*');

        }catch (Exception $e){

            $this->messageManager->addError(__($e->getMessage()));

        }
    }
}