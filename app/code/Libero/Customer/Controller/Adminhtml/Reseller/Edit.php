<?php
namespace Libero\Customer\Controller\Adminhtml\Reseller;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $registry;
    public function __construct(\Magento\Backend\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory,\Magento\Framework\Registry $registry)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->registry = $registry;
    }

    public function execute()
    {
        $customerId = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create("Libero\Customer\Model\CustomerReseller")->load($customerId,"id_customer");
        //Restore previously entered form data from session
        $this->registry->register('libero_customer_reseller', $model);
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Libero_Customer::reseller');
        $resultPage->getConfig()->getTitle()->prepend(__("Manger Reseller"));
        return $resultPage;
    }
}