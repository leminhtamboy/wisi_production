<?php
namespace Libero\Customer\Controller\Adminhtml\Balance;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    public function __construct(\Magento\Backend\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        parent::__construct($context); // missing wil make error  Uncaught Error: Call to a member function isLoggedIn() on null on admin magento 2
        $this->resultPageFactory = $resultPageFactory;
    }
    
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $orderDetail = $objectManager->get("\Libero\Customer\Block\Seller\Admin\OrderDetail");

        $requestHttp = $objectManager->get("\Magento\Framework\App\Request\Http");
        $order_id = $requestHttp->getParam("order_id");
        $this->getBalanceInfo($order_id);

        $json_result  = array();
        $json_result["success"] = true;
        $json_result["msg"] = "Send Request Successfully";
        $result = $jsonResultFactory->setData($json_result);
        return $result;
    }

    public function getBalanceInfo($order_id)
    {
        $connection = $this->_resource->getConnection();
        $sql = "
            SELECT
                *, ssoi.qty_ordered AS qty, ssoi.item_id AS order_item_id
            FROM
                seller_sales_order_grid AS ssog
                    JOIN
                seller_sales_order AS sso
                    JOIN
                seller_sales_order_item AS ssoi
                    ON ssog.order_id = ssoi.order_id
                    AND sso.entity_id = ssoi.order_id
            WHERE
                ssog.seller_id = '$id_seller'
                AND ssog.order_id = '$order_id'
            ;
            ";
        $result = $connection->fetchAll($sql);
        return $result;
    }

}