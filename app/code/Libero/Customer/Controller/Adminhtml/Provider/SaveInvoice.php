<?php
namespace Libero\Customer\Controller\Adminhtml\Provider;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class SaveInvoice extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Sales::sales_invoice';

    /**
     * @var InvoiceSender
     */
    protected $invoiceSender;

    /**
     * @var ShipmentSender
     */
    protected $shipmentSender;

    /**
     * @var ShipmentFactory
     */
    protected $shipmentFactory;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var \Libero\Customer\Block\Seller\Admin\OrderDetail
     */
    protected $_orderDetail;

    /**
     * @param Action\Context $context
     * @param Registry $registry
     * @param InvoiceSender $invoiceSender
     * @param ShipmentSender $shipmentSender
     * @param ShipmentFactory $shipmentFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Libero\Customer\Block\Seller\Admin\OrderDetail $orderDetail
    ) {
        $this->_orderDetail = $orderDetail;
        parent::__construct($context);
    }
    public function getOrderDetail($order_id = null){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('\Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql = "
            SELECT
                *, ssoi.qty_ordered AS qty, ssoi.item_id AS order_item_id
            FROM
                seller_sales_order_grid AS ssog
                    JOIN
                seller_sales_order AS sso
                    JOIN
                seller_sales_order_item AS ssoi
                    ON ssog.order_id = ssoi.order_id
                    AND sso.entity_id = ssoi.order_id
            WHERE
                ssog.order_id = '$order_id'
            ;
            ";
        $result = $connection->fetchAll($sql);
        return $result;
    }
    /**
     * Save invoice
     * We can save only new invoice. Existing invoices are not editable
     *
     * @return \Magento\Framework\Controller\ResultInterface
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPost();
        $order_id = $this->getRequest()->getParam('order_id');
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $curDateTime = date("Y-m-d H:i:s");
        try {
            /** @var \Magento\Sales\Model\Order $order */
            $order = $objectManager->create('Libero\Customer\Model\Order')->load($order_id);
            if (!$order->getId()) {
                throw new \Magento\Framework\Exception\LocalizedException(__('The order no longer exists.'));
            }
            $total_qty = $data["total_qty"];
            $seller_id = $order->getSellerId();

            $data_sale_order = $order->getData();
            $data_sale_order['total_qty'] = $total_qty;
            $data_sale_order['seller_id'] = $seller_id;
            $data_sale_order['order_id'] = $order_id;
            $data_sale_order['comment'] = $data['history_comment'];
            $data_sale_orderArr = array();
            foreach($data_sale_order as $key => $value){
                array_push($data_sale_orderArr,$key);
            }
            $resource = $objectManager->get('\Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();


            /*************** seller_sales_invoice ***************/
            $into = "";
            $values = "";
            $sql = "";

            $tableName1 = $resource->getTableName("seller_sales_invoice");
            $tableFieldTotal1 = $connection->describeTable($tableName1);
            $tableFieldArr1 = array();
            foreach($tableFieldTotal1 as $key => $value){
                array_push($tableFieldArr1,$key);
            }
        
            foreach($data_sale_order as $key => $value){
                if($key == "entity_id"){continue;};
                if ($key == "created_at" || $key == "updated_at"){
                    $into .= "`$key`,";
                    $values .= "'$curDateTime',";
                } else if(in_array($key, $tableFieldArr1)){
                    $into .= "`$key`,";
                    $values .= "'$value',";
                }
            }

            $into.="|";
            $into = str_replace(",|","",$into);

            $values.="|";
            $values = str_replace(",|","",$values);

            $sql = "INSERT INTO `$tableName1` ($into) VALUES ($values)";
       
            $connection->query($sql);

            /*************** seller_sales_invoice_comment ***************/
            $into = "";
            $values = "";
            $sql = "";

            $tableName2 = $resource->getTableName("seller_sales_invoice_comment");
            $tableFieldTotal2 = $connection->describeTable($tableName2);
            $tableFieldArr2 = array();
            foreach($tableFieldTotal2 as $key => $value){
                array_push($tableFieldArr2,$key);
            }

            foreach($data_sale_order as $key => $value){
                if($key == "entity_id"){continue;};
                if ($key == "created_at"){
                    $into .= "`$key`,";
                    $values .= "'$curDateTime',";
                } else if(in_array($key, $tableFieldArr2)){
                    $into .= "`$key`,";
                    $values .= "'$value',";
                }
            }
            
            $into.="|";
            $into = str_replace(",|","",$into);

            $values.="|";
            $values = str_replace(",|","",$values);

            $sql = "INSERT INTO `$tableName2` ($into) VALUES ($values)";
   
            $connection->query($sql);

            /*************** seller_sales_invoice_grid ***************/
            $into = "";
            $values = "";
            $sql = "";

            $orderDetail = $this->getOrderDetail($order_id);
         
            $data_sale_order['order_increment_id'] = $orderDetail[0]['increment_id'];
            $data_sale_order['customer_name'] = $orderDetail[0]['customer_name'];
            $data_sale_order['order_created_at'] = $orderDetail[0]['created_at'];
            $data_sale_order['billing_name'] = $orderDetail[0]['billing_name'];

            $data_sale_order['billing_address'] = $orderDetail[0]['billing_address'];
            $data_sale_order['shipping_address'] = $orderDetail[0]['shipping_address'];
            $data_sale_order['shipping_information'] = $orderDetail[0]['shipping_information'];
            $data_sale_order['shipping_and_handling'] = $orderDetail[0]['shipping_and_handling'];
            $data_sale_order['payment_method'] = $orderDetail[0]['payment_method'];

            $tableName3 = $resource->getTableName("seller_sales_invoice_grid");
            $tableFieldTotal3 = $connection->describeTable($tableName3);
            $tableFieldArr3 = array();
            foreach($tableFieldTotal3 as $key => $value){
                array_push($tableFieldArr3,$key);
            }

            foreach($data_sale_order as $key => $value){
                if($key == "entity_id"){continue;};
                if ($key == "created_at" || $key == "updated_at"){
                    $into .= "`$key`,";
                    $values .= "'$curDateTime',";
                } else if(in_array($key, $tableFieldArr3)){
                    $into .= "`$key`,";
                    $values .= "'$value',";
                }
            }

            $into.="|";
            $into = str_replace(",|","",$into);

            $values.="|";
            $values = str_replace(",|","",$values);
       
            $sql = "INSERT INTO `$tableName3` ($into) VALUES ($values)";

            $connection->query($sql);

            /*************** seller_sales_invoice_item ***************/
            $tableName4 = $resource->getTableName("seller_sales_invoice_item");
            $tableFieldTotal4 = $connection->describeTable($tableName4);
            $tableFieldArr4 = array();
            foreach($tableFieldTotal4 as $key => $value){
                array_push($tableFieldArr4,$key);
            }

            $keyInsertDb = array("base_price", "base_row_total", "row_total", "price_incl_tax", "base_price_incl_tax", "qty", "base_cost", "price", "base_row_total_incl_tax", "row_total_incl_tax", "product_id", "order_item_id", "additional_data", "description", "sku", "name", "weee_tax_applied", "weee_tax_applied_amount", "weee_tax_applied_row_amount", "weee_tax_disposition", "weee_tax_row_disposition", "base_weee_tax_applied_amount", "base_weee_tax_applied_row_amnt", "base_weee_tax_disposition", "base_weee_tax_row_disposition");
            
            foreach ($orderDetail as $order) {
                $into = "";
                $values = "";
                $sql = "";

                foreach($data_sale_order as $key2 => $value2){
                    if($key2 == "entity_id"){continue;};
                    if ($key2 == "created_at" || $key2 == "updated_at"){
                        $into .= "`$key2`,";
                        $values .= "'$curDateTime',";
                    } else if(in_array($key2, $tableFieldArr4)){
                        $into .= "`$key2`,";
                        $values .= "'$value2',";
                    }
                }
                foreach($keyInsertDb as $keyInsert){
                    $into .= "`$keyInsert`,";
                    $values .= "'$order[$keyInsert]',";
                }
                

                $into.="|";
                $into = str_replace(",|","",$into);

                $values.="|";
                $values = str_replace(",|","",$values);

                $sql = "INSERT INTO `$tableName4` ($into) VALUES ($values)";
                //$sql_update_qty_invoiced = "UPDATE `seller_sales_order_item` set "
                $connection->query($sql);
            }
            $status = $orderDetail[0]["status"];
            $content_logs = "Seller Update Order $order_id From Status [ $status ] To Status [ processing ]";
            $sql_update_status_order = "UPDATE seller_sales_order SET `status` = 'Processing', `status_code` = '2', `updated_at` = '$curDateTime' WHERE entity_id = '$order_id';";
            $connection->query($sql_update_status_order);
            $sql_update_status_order_grid = "UPDATE seller_sales_order_grid SET `status` = 'Processing', `status_code` = '2', `updated_at` = '$curDateTime' WHERE order_id = '$order_id';";
            $connection->query($sql_update_status_order_grid);
            $seller_id = $orderDetail[0]["seller_id"];
            $sql_insert_log = "INSERT INTO `sales_order_seller_logs` (`id_logs`, `seller_id`, `order_id`, `content`, `etc`,`created_at`) 
            VALUES (NULL, '$seller_id', '$order_id', '$content_logs', '','$curDateTime')";
            $connection->query($sql_insert_log);
            $this->messageManager->addSuccess(__('Create Invoice Successfully.'));
            return $resultRedirect->setPath("manager/provider/detail/id/$order_id");
            
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError(__('We can\'t save the invoice right now.'));
            $objectManager->get('Psr\Log\LoggerInterface')->critical($e);
        }
    }
}