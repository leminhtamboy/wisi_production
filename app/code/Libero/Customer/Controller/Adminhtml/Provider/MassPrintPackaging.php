<?php
namespace Libero\Customer\Controller\Adminhtml\Provider;

class MassPrintPackaging extends \Magento\Backend\App\Action
{
    protected $registry;

    protected $fileFactory;

    protected $_table_provider_order = "seller_sales_order";

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->fileFactory = $fileFactory;
        parent::__construct($context);
        $this->registry = $registry;
    }

    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $array_ids = $this->getRequest()->getPostValue();
        $order_id_string = $array_ids["array_ids"];
        $order_id_string = substr($order_id_string,0,strlen($order_id_string) - 1);
        $array_ids = explode("|",$order_id_string);
		$array_ids = array_unique($array_ids);
        $this->createPacking($array_ids);
        //return $resultPage;
    }
    public function getPackaging($order_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $database = $connection;
        $sql_get_order_provider = "SELECT * FROM  seller_sales_order WHERE `status` = 'Request to packaging' and entity_id = '$order_id'";
        $data = $database->fetchAll($sql_get_order_provider);
        return $data;
    }
    public function getProduct($order_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $blockOrder = $objectManager->get("Libero\Customer\Block\Adminhtml\Provider\Order");
        return $blockOrder->getProducts($order_id);
    }
    
    public function createPacking($array_order_ids)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $blockOrder = $objectManager->get("Libero\Customer\Block\Adminhtml\Provider\Order");
        $stringUtil = $objectManager->get("\Magento\Framework\Stdlib\StringUtils");
        $pdf = new \Zend_Pdf();
        $pdf->pages[] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $page = $pdf->pages[0]; // this will get reference to the first page.
        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Rgb(0,0,0));
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $rootDirectory = $filesystem->getRoot();
        $path_of_font = $rootDirectory."/app/design/frontend/Sm/market/web/fonts/Roboto-Regular.ttf";
        $path_of_logo = $rootDirectory."/pub/media/logo/stores/1/wisi-text-logo-200x77-w.png";
        $image = \Zend_Pdf_Image::imageWithPath($path_of_logo);
        $font = \Zend_Pdf_Font::fontWithPath($path_of_font);
        $style->setFont($font,15);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $x = 100;
        $pageTopalign = 595; //default PDF page height
        $this->y = 595 - 100; //print table row from page top – 100px
        $style->setFont($font,30);
        $page->setStyle($style);
        //Function Show IMG Product
        $top = $this->y - 70;
        //End Region
        //Left content
        //Region Logo
        $top_logo = 530;
        $width_image_logo = $image->getPixelWidth() / 2;
        $height_image_logo = $image->getPixelHeight() / 2;
        //top border of the page
        $widthLimit = 270;
        //half of the page width
        $heightLimit = 270;
        $ratio = $width_image_logo / $height_image_logo;
        if ($ratio > 1 && $width_image_logo > $widthLimit) {
            $width_image_logo = $widthLimit;
            $width_image_logo = $width_image_logo / $ratio;
        } elseif ($ratio < 1 && $height_image_logo > $heightLimit) {
            $width_image_logo = $heightLimit;
            $width_image_logo = $height_image_logo * $ratio;
        } elseif ($ratio == 1 && $height_image_logo > $heightLimit) {
            $height_image_logo = $heightLimit;
            $width_image_logo = $widthLimit;
        }
        $y1 = $top_logo - $height_image_logo;
        $y2 = $top_logo;
        $x1 = 30;
        $x2 = $x1 + $width_image_logo;
        $page->drawImage($image,$x1, $y1, $x2, $y2);
        //Title page
        $page->drawText(__("PACKAGING"), $x + 250, $this->y+10, 'UTF-8');
        $style->setFont($font,10);
        $page->setStyle($style);
        $page->drawText(__("Create at: %1", "21/09/2018"), $x + 280, $this->y-10, 'UTF-8');
        // right content
        $style->setFont($font,10);
        $page->setStyle($style);
        $page->drawRectangle($x + 500,$this->y+40, $x + 630, $this->y+20, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawText(__("Customer service manager"),$x + 505 ,$this->y+26 , 'UTF-8');
        $page->drawRectangle($x + 630,$this->y+40, $page->getWidth()-30, $this->y+20, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

        $page->drawRectangle($x + 500,$this->y, $x + 630, $this->y+20, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawText(__("Treasury Manager"),$x + 505 ,$this->y+7 , 'UTF-8');
        $page->drawRectangle($x + 630,$this->y, $page->getWidth()-30, $this->y+20, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

        $page->drawRectangle($x + 500,$this->y-20, $x + 630, $this->y, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawText(__("Checker"),$x + 505 ,$this->y-13 , 'UTF-8');
        $page->drawRectangle($x + 630,$this->y-20, $page->getWidth()-30, $this->y, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        // TABLE PACKAGING
            //start - header table
                // var
                $line_y = $this->y - 50;
                $text_y = $this->y -55;
                $orderID_x = $x - 5;
                $createDate_x = $x + 40;
                $provider_x = $x + 110;
                $reSeller_x = $x + 185;
                $nameProduct_x = $x + 310;
                $option_x = $x + 450;
                $qty_x = $x + 505;
                $shipping_x = $x + 545;
                $total_x = $x + 635;
                $style->setLineColor(new \Zend_Pdf_Color_Html('#354157'));
                $style->setFillColor(new \Zend_Pdf_Color_Html('#ffffff'));
                $style->setFont($font,9);
                $page->setStyle($style);
                $page->setLineWidth(30);
                $page->drawLine(30, $line_y, $x-10, $line_y);
                $page->drawText(__("ID"), 40, $text_y, 'UTF-8');

                $page->drawLine($x-10, $line_y, $x + 33 , $line_y);
                $page->drawText(__("Order Id"), $orderID_x, $text_y, 'UTF-8');

                $page->drawLine($x + 33, $line_y, $x + 105, $line_y);
                $page->drawText(__("Purchase Date"),  $createDate_x, $text_y, 'UTF-8');

                $page->drawLine( $x + 105, $line_y, $x + 180, $line_y);
                $page->drawText(__("Provider Name"), $provider_x, $text_y, 'UTF-8');

                $page->drawLine($x + 180, $line_y, $x + 250, $line_y);
                $page->drawText(__("Reseller Name"), $reSeller_x, $text_y, 'UTF-8');
                $page->drawLine($x + 250, $line_y, $x + 305, $line_y);
                $page->drawText(__("Image"), $x + 255, $text_y, 'UTF-8');
                $page->drawLine($x + 305, $line_y, $x + 440, $line_y);
                $page->drawText(__("Name - SKU"), $nameProduct_x, $text_y, 'UTF-8');
                $page->drawLine($x + 440, $line_y, $x + 485, $line_y);//45
                $page->drawText(__("Option"), $option_x, $text_y, 'UTF-8');
                $page->drawLine($x + 485, $line_y, $x + 540, $line_y);//55
                $page->drawText(__("Qty"), $qty_x, $text_y, 'UTF-8');
                $page->drawLine($x + 540, $line_y,$x + 630, $line_y);
                $page->drawText(__("Shipping Company"), $shipping_x, $text_y, 'UTF-8');
                $page->drawLine($x + 630, $line_y, $page->getWidth()-30, $line_y);
                $page->drawText(__("Grand Total"), $total_x, $text_y, 'UTF-8');
                // end - header table
                // - Show Value - table
                $value_row = $this->y - 80;
                $row = 0;
                $tung_do_hinh = 0;
                foreach($array_order_ids as $_id)
                {
                    $packing = $this->getPackaging($_id)[0];
                    $id_seller = $packing["seller_id"];
                    $provider = $blockOrder->getProvider($id_seller);
                    $name_provider = $provider->getData("store_name");
                    $name_reseller = trim($packing["customer_lastname"])." ".trim($packing["customer_firstname"]);
                    $grand_total = $blockOrder->formartPrice($packing["grand_total"]);
                    $base_grand_total = str_replace(".00","",$blockOrder->formartPrice($packing["base_grand_total"]));
                    $products = $this->getProduct($_id);
                    $shipping_method = $packing["shipping_description"];
                    $shipping_price = str_replace(".00","",$blockOrder->formartPrice($packing["base_shipping_amount"]));
                    $grand_total_text = $shipping_price."/".$base_grand_total;
                    $created_at = $packing["created_at"];
                    foreach($products as $_product){
                        $name = $_product["name"];
                        $sku = $_product["sku"];
                        $img = $_product["image"];
                        $base_url = $blockOrder->getBaseUrl();
                        $img = str_replace($base_url,"",$img);
                        $qty_ordered = $blockOrder->formartQty($_product["qty_ordered"]);
                        //$path_of_product = $rootDirectory."/pub/media/catalog/product/resized/100x100/db4e66100f55e6657d33f65beabfc3e5/7688e41c827ddff816ef2131c77da3f4.jpg";
                        $path_of_product = $rootDirectory."/pub/".$img."";
                        $product_img = \Zend_Pdf_Image::imageWithPath($path_of_product);
                        $width_image = $product_img->getPixelWidth();
                        $height_image = $product_img->getPixelHeight();
                        $widthLimit = 45;
                        $heightLimit = 30;
                        $ratio = $width_image / $height_image;
                        if ($ratio > 1 && $width_image > $widthLimit) {
                            $width_image = $widthLimit;
                            $height_image = $width_image / $ratio;
                        } elseif ($ratio < 1 && $height_image > $heightLimit) {
                            $height_image = $heightLimit;
                            $width_image = $height_image * $ratio;
                        } elseif ($ratio == 1 && $height_image > $heightLimit) {
                            $height_image = $heightLimit;
                            $width_image = $widthLimit;
                        }
                        $y1 = $top - $height_image;
                        $y2 = $top;
                        $x1 = $x + 255;
                        $x2 = $x1 + $width_image;
                        //$page->drawImage($product_img,$x1, $y1, $x2, $y2);
                        //$page->drawImage($product_img,$x1, $y1, $x2, $y2);
                        //$page->drawImage($product_img,$x1, $y1, $x2, $y2);
                        if($row > 0){
                            $value_row = $value_row - 30;
                        }
                        $style->setLineColor(new \Zend_Pdf_Color_Html('#000000'));
                        $style->setFillColor(new \Zend_Pdf_Color_Html('#000000'));
                        $style->setFont($font,7);
                        $page->setStyle($style);
                        $page->setLineWidth(0); 
                        $page->drawText(__($packing["increment_id"]), 40, $value_row, 'UTF-8');
                        $page->drawText(__($packing["entity_id"]), $orderID_x, $value_row, 'UTF-8');
                        $created_at = preg_replace('/<br[^>]*>/i', "\n", $created_at);
                        $yPayments = $value_row;
                        foreach (str_split($created_at, 10) as $_value) {
                            $page->drawText(strip_tags(trim($_value)), $createDate_x, $yPayments, 'UTF-8');
                            $yPayments -= 15;
                        }
                        //$page->drawText(__($packing["created_at"]), $createDate_x, $value_row, 'UTF-8');
                        $text = [];
                        $page->drawText(__($name_provider), $provider_x, $value_row, 'UTF-8');
                        foreach ($stringUtil->split($name_reseller, 15, true, true) as $_value) {
                            $text[] = $_value;
                        }
                        $yNameReseller = $value_row;
                        foreach ($text as $part) {
                            $page->drawText(strip_tags(ltrim($part)), $reSeller_x, $yNameReseller, 'UTF-8');
                            //$page->drawText(__($name_reseller), $reSeller_x, $value_row, 'UTF-8');
                            //$this->y -= 15;
                            $yNameReseller -= 15;
                        }
                        //$page->drawText(__($name_reseller), $reSeller_x, $value_row, 'UTF-8');
                        $tung_do_hinh = $row * (30);
                        $y1 = $y1 - $tung_do_hinh;
                        $y2 = $y2 - $tung_do_hinh;
                        $page->drawImage($product_img,$x1, $y1, $x2, $y2);
						$text = [];
						foreach ($stringUtil->split($name, 40, true, true) as $_value) {
                            $text[] = $_value;
                        }
                        $yNameProduct = $value_row;
                        foreach ($text as $part) {
                            $page->drawText(strip_tags(ltrim($part)), $nameProduct_x, $yNameProduct, 'UTF-8');
                            //$page->drawText(__($name_reseller), $reSeller_x, $value_row, 'UTF-8');
                            //$this->y -= 15;
                            $yNameProduct -= 15;
                        }
                        //$page->drawText(__($name), $nameProduct_x, $value_row - 5, 'UTF-8');
                        $page->drawText(__("None"), $option_x, $value_row, 'UTF-8');
                        $page->drawText(__($qty_ordered), $qty_x, $value_row, 'UTF-8');
                        $page->drawText(__($shipping_method), $shipping_x, $value_row, 'UTF-8');
                        $page->drawText(__($grand_total_text), $total_x, $value_row, 'UTF-8');
                        $row++;
                    }
                }
        $fileName = 'packaging.pdf';
        $this->fileFactory->create(
           $fileName,
           $pdf->render(),
           \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR, // this pdf will be saved in var directory with the name example.pdf
           'application/pdf'
        );
    }
    protected function _isAllowed()
    {
        //return parent::_isAllowed(); // TODO: Change the autogenerated stub
        return $this->_authorization->isAllowed('Libero_Customer::order_provider');
    }
}