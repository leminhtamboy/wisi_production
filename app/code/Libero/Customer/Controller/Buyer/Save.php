<?php
namespace Libero\Customer\Controller\Buyer;
use Magento\Framework\App\Action\Context;

class Save extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(


        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        /*$APIKey="524CCDB001614966979CD792CE6AAA";
        $SecretKey="3EC716E99FC747A9C8E1703DF37EF3";
        $YourPhone="0907441201";
        $Content="Welcome to magento everything can work";

        $SendContent=urlencode($Content);
        $data="http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?Phone=$YourPhone&ApiKey=$APIKey&SecretKey=$SecretKey&Content=$SendContent&SmsType=4";

        $curl = curl_init($data);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);

        $obj = json_decode($result,true);
        if($obj['CodeResult']==100)
        {
            print "<br>";
            print "CodeResult:".$obj['CodeResult'];
            print "<br>";
            print "CountRegenerate:".$obj['CountRegenerate'];
            print "<br>";
            print "SMSID:".$obj['SMSID'];
            print "<br>";
        }
        else
        {
            print "ErrorMessage:".$obj['ErrorMessage'];
        }
        die();*/
        $resultRedirect = $this->resultRedirectFactory->create();
        $postData = $this->getRequest()->getPostValue();
        $phone = $postData["phone"];
        $token = $postData["token"];
        $otp = $postData["otp"];
        $email = $postData["login"]["username"];
        $password = $postData["login"]["password"];
        $sex = $postData["sex"];
        $dateOfBirth = $postData["yy"]."-".$postData["mm"]."-".$postData["dd"];
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $url = \Magento\Framework\App\ObjectManager::getInstance();
        $customerFactory = $this->_objectManager->get('\Magento\Customer\Model\CustomerFactory');
        $storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');
        $websiteId = $storeManager->getWebsite()->getWebsiteId();
        $store = $storeManager->getStore();  // Get Store ID
        $storeId = $store->getStoreId();
        $storeId = $store->getStoreId();
        $modelCustomerOtp = $this->_objectManager->create("\Libero\Customer\Model\CustomerOtp");
        $dataCustomerOtp = $modelCustomerOtp->getCollection()->addFieldToFilter("token",array("eq"=>$token))->addFieldToFilter("phone",array("eq"=>$phone))->addFieldToFilter("otp",array("eq"=>$otp));
        if(count($dataCustomerOtp) > 0){
            $customer = $customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->setEmail($email);
            $customer->setFirstname("Buyer First Name");
            $customer->setLastname("No Last Name");
            $customer->setPassword($password);
            $customer->setGender($sex);
            $customer->setDob($dateOfBirth);
            $customer->setGroupId(3);
            $customer->save();
            $this->messageManager->addSuccess(__("Create Buyer Successfully"));
        }else {
            $this->messageManager->addError(__("Wrong Otp"));
        }
        $resultRedirect->setPath('*/*');
        return $resultRedirect;
    }
}
