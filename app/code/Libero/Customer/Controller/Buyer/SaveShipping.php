<?php
namespace Libero\Customer\Controller\Buyer;

class SaveShipping extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $postData = $this->getRequest()->getPostValue();
        $order_id = $postData["order_id"];
        $connection = $this->_objectManager->get("\Magento\Framework\App\ResourceConnection")->getConnection();
        $sql_update = "UPDATE `seller_sales_order` set status = 'complete',state = 'completed' WHERE entity_id = '$order_id'";
        $connection->query($sql_update);

        $sql_update_grid_status = "UPDATE `seller_sales_order_grid` SET `status` = 'complete' WHERE order_id = '$order_id'";
        $connection->query($sql_update_grid_status);

        $sql_update_shipment = "UPDATE `seller_sales_shipment_grid` set order_status = 'complete' WHERE order_id = '$order_id'";
        $connection->query($sql_update_shipment);
        //update status order now
        $this->messageManager->addSuccess(__("Make Complete Order Successfully"));
        $resultRedirect->setPath("customer/buyer/shiptracking/id_shipment/$order_id/");
        return $resultRedirect;
    }
}
