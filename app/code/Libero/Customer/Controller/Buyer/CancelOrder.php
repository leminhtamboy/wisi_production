<?php
namespace Libero\Customer\Controller\Buyer;
use Magento\Framework\App\Action\Context;

class CancelOrder extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $postData = $this->getRequest()->getPostValue();
        $increment_id = $postData["increment_id"];
        $order_ids = explode("|", $postData["order_ids"]);
        $status = "cancel";
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $cancelProducts = [
            "product_id" => [],
            "qty" => []
        ];
        $orderManagement = $objectManager->create('\Magento\Sales\Api\OrderManagementInterface');
        $stockItemRepository = $objectManager->get('\Magento\CatalogInventory\Model\Stock\StockItemRepository');
        
        foreach ($order_ids as $order_id) {
            $sql_update_status = "UPDATE `seller_sales_order` SET `status` = '$status' WHERE entity_id = $order_id";
            $connection->query($sql_update_status);

            $sql_update_grid_status = "UPDATE `seller_sales_order_grid` SET `status` = '$status' WHERE order_id = $order_id";
            $connection->query($sql_update_grid_status);
            $sql_get_cancel_product = "SELECT * FROM `seller_sales_order_item` WHERE order_id = $order_id";
            $cancel_products = $connection->query($sql_get_cancel_product);
            foreach($cancel_products as $_product) {
                $qty_ordered = $_product["qty_ordered"];
                $product_id = $_product["product_id"];

                $sql_update_product_cancel_qty = "UPDATE `seller_sales_order_item` SET `qty_canceled` = '$qty_ordered' WHERE `order_id` = '$order_id' AND `product_id` = '$product_id'";
                $connection->query($sql_update_product_cancel_qty);

                /*$curQty = $stockItemRepository->get($product_id)->getQty();
                $newQty = $curQty + $qty_ordered;
                $productModel = $objectManager->create('\Magento\Catalog\Model\Product');
                $stockRegistry = $objectManager->create("\Magento\CatalogInventory\Api\StockRegistryInterface");
                $stockItem = $stockRegistry->getStockItem($product_id);
                $stockItem->setData("is_in_stock",1);
                $stockItem->setData("qty",$newQty);
                $stockItem->setData("manage_stock",1);
                $stockItem->setData("use_config_notify_stock_qty",1);
                $stockItem->save();*/
                $orderManagement->cancel($increment_id);
            }
        }
        return $resultRedirect->setPath('customer/buyer/view/increment_id/'.$increment_id);
        //return $resultRedirect;
    }
}
