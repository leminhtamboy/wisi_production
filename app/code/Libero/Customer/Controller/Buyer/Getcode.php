<?php
namespace Libero\Customer\Controller\Buyer;
use Magento\Framework\App\Action\Context;

class Getcode extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $postData = $this->getRequest()->getPostValue();
        $phone = $postData["phone"];
        $token = $postData["token"];
        $OTP_CODE = rand(1000,9999);
        $APIKey="6B0390B4E77D3016F1D5D76ABD9E94";
        $SecretKey="5D73B98F1B9523DC06A0861055E8A2";
        $YourPhone=$phone;
        $Content="Your opt code to authenticated register buyer ".$OTP_CODE;
        $SendContent=urlencode($Content);
        $data="http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?Phone=$YourPhone&ApiKey=$APIKey&SecretKey=$SecretKey&Content=$SendContent&SmsType=4";
        $curl = curl_init($data);
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        $obj = json_decode($result,true);
        if($obj['CodeResult']==100) {
            $modelCustomerOtp = $this->_objectManager->create("\Libero\Customer\Model\CustomerOtp");
            $modelCustomerOtp->setData("token",$token);
            $modelCustomerOtp->setData("otp",$OTP_CODE);
            $modelCustomerOtp->setData("phone",$phone);
            $modelCustomerOtp->save();
        }
        else {
            $this->messageManager->addError(__("ErrorMessage:".$obj['ErrorMessage']));
        }
        $modelCustomerOtp = $this->_objectManager->create("\Libero\Customer\Model\CustomerOtp");
        $modelCustomerOtp->setData("token",$token);
        $modelCustomerOtp->setData("otp",$OTP_CODE);
        $modelCustomerOtp->setData("phone",$phone);
        $modelCustomerOtp->save();
    }
}
