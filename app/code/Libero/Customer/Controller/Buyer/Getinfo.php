<?php
namespace Libero\Customer\Controller\Buyer;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Api\AccountManagementInterface;
use Mageplaza\SocialLogin\Helper\Social as SocialHelper;
use Mageplaza\SocialLogin\Model\Social;
use Magento\Customer\Model\Session;

class Getinfo extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    /**
     * @type \Magento\Customer\Model\Session
     */
    protected $session;

    /**
     * @type \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @type \Magento\Customer\Api\AccountManagementInterface
     */
    protected $accountManager;

    /**
     * @type \Mageplaza\SocialLogin\Helper\Social
     */
    protected $apiHelper;

    /**
     * @type \Mageplaza\SocialLogin\Model\Social
     */
    protected $apiObject;

    /**
     * @var AccountRedirect
     */
    private $accountRedirect;

    /**
     * @type
     */
    private $cookieMetadataManager;

    /**
     * @type
     */
    private $cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Controller\Result\RawFactory
     */
    protected $resultRawFactory;

    private $_registry;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        StoreManagerInterface $storeManager,
        AccountManagementInterface $accountManager,
        SocialHelper $apiHelper,
        Social $apiObject,
        Session $customerSession,
        AccountRedirect $accountRedirect,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->storeManager     = $storeManager;
        $this->accountManager   = $accountManager;
        $this->apiHelper        = $apiHelper;
        $this->apiObject        = $apiObject;
        $this->session          = $customerSession;
        $this->accountRedirect  = $accountRedirect;
        $this->urlBuilder       = $context->getUrl();
        $this->resultRawFactory = $resultRawFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        //$type = $this->apiHelper->setType($this->getRequest()->getParam('type', null));
        /*if (!$type) {
            $this->_forward('noroute');

            return;
        }*/
        $type = $this->apiHelper->setType("google");
        $userProfile = $this->apiObject->getUserProfile($type);
        $dataUser = json_decode(json_encode($userProfile),TRUE);
        /*if (!$userProfile->identifier) {
            return $this->emailRedirect($type);
        }*/
        //$customer = $this->apiObject->getCustomerBySocial($userProfile->identifier, $type);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customer = $objectManager->create('Magento\Customer\Model\Customer')->getCollection()->addFieldToFilter("email",array("eq" =>$dataUser["email"]))->getFirstItem();



        //var_dump($customer->getData());
        //die();
        if (!$customer->getId()) {
            $objectRegistryManager = \Magento\Framework\App\ObjectManager::getInstance();
            $modelRegistry = $objectRegistryManager->get("Magento\Customer\Model\Session");
            $modelRegistry->setData('data_info_customer', $userProfile);
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('*/*');
            return $resultRedirect;
        }else {
            $url =  $this->_appendJs($customer);
            ?>
            <script type="text/javascript">
                top.location.href="<?php echo$url; ?>";
            </script>
        <?php
            //$resultRedirect = $this->resultRedirectFactory->create();
            //$resultRedirect->setPath($url);
        }

    }
    protected function getUserData($profile)
    {
        return [];
    }

    /**
     * Get Store object
     *
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    public function getStore()
    {
        return $this->storeManager->getStore();
    }

    /**
     * Redirect to login page if social data is not contain email address
     *
     * @param $apiLabel
     * @return $this
     */
    public function emailRedirect($apiLabel, $needTranslate = true)
    {
        $message = $needTranslate ? __('Email is Null, Please enter email in your %1 profile', $apiLabel) : $apiLabel;
        $this->messageManager->addErrorMessage($message);
        $this->_redirect('customer/account/login');

        return $this;
    }

    /**
     * Create customer from social data
     *
     * @param $user
     * @return bool|\Magento\Customer\Model\Customer|mixed
     */
    public function createCustomer($user, $type)
    {
        $customer = $this->apiObject->getCustomerByEmail($user['email'], $this->getStore()->getWebsiteId());
        if (!$customer->getId()) {
            try {
                $customer = $this->apiObject->createCustomerSocial($user, $this->getStore());
                if ($this->apiHelper->canSendPassword()) {

                    //$customer->sendPasswordReminderEmail();
                    /**
                     * Magento\Customer\Api\AccountManagementInterface
                     */
                    $this->accountManager->initiatePasswordReset(
                        $user['email'],
                        \Magento\Customer\Model\AccountManagement::EMAIL_RESET
                    );
                }
            } catch (\Exception $e) {
                $this->emailRedirect($e->getMessage(), false);

                return false;
            }
        }
        $this->apiObject->setAuthorCustomer($user['identifier'], $customer->getId(), $type);

        return $customer;
    }

    /**
     * Return javascript to redirect when login success
     *
     * @param $customer
     * @return $this
     */
    public function _appendJs($customer)
    {
        if ($customer && $customer->getId()) {
            $this->session->setCustomerAsLoggedIn($customer);
            $this->session->regenerateId();

            if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                $metadata->setPath('/');
                $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
            }
        }

        /** @var \Magento\Framework\Controller\Result\Raw $resultRaw */
        //$resultRaw = $this->resultRawFactory->create();
        //echo $this->_loginPostRedirect();
        //die();
        //return $resultRaw->setContents(sprintf("<script>window.opener.socialCallback('%s', window);</script>", $this->_loginPostRedirect()));
        return $this->_loginPostRedirect();
    }

    /**
     * Retrieve cookie manager
     *
     * @deprecated
     * @return \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private function getCookieManager()
    {
        if (!$this->cookieMetadataManager) {
            $this->cookieMetadataManager = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\PhpCookieManager::class
            );
        }

        return $this->cookieMetadataManager;
    }

    /**
     * Retrieve cookie metadata factory
     *
     * @deprecated
     * @return \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private function getCookieMetadataFactory()
    {
        if (!$this->cookieMetadataFactory) {
            $this->cookieMetadataFactory = \Magento\Framework\App\ObjectManager::getInstance()->get(
                \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory::class
            );
        }

        return $this->cookieMetadataFactory;
    }

    /**
     * Return redirect url by config
     *
     * @return mixed
     */
    protected function _loginPostRedirect()
    {
        $url = $this->urlBuilder->getUrl('customer/account');

        if ($this->_request->getParam('authen') == 'popup') {
            $url = $this->urlBuilder->getUrl('checkout');
        } else {
            $requestedRedirect = $this->accountRedirect->getRedirectCookie();
            if (!$this->apiHelper->getConfigValue('customer/startup/redirect_dashboard') && $requestedRedirect) {
                $url = $this->_redirect->success($requestedRedirect);
                $this->accountRedirect->clearRedirectCookie();
            }
        }

        return $url;
    }
}
