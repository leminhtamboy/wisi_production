<?php
namespace Libero\Customer\Controller\Buyer;
use Magento\Framework\App\Action\Context;

class CompleteOrder extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $resultRedirect = $this->resultRedirectFactory->create();
        $requestHttp = $objectManager->get('\Magento\Framework\App\Request\Http');
        $increment_id = $requestHttp->getParam("increment_id");

        date_default_timezone_set("Asia/Bangkok");
        $curDateTime = date("Y-m-d H:i:s");

        $status = "Complete";
        
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        
        $orderManagement = $objectManager->create('\Magento\Sales\Api\OrderManagementInterface');
        $stockItemRepository = $objectManager->get('\Magento\CatalogInventory\Model\Stock\StockItemRepository');
        
        $sql_update_status = "UPDATE `seller_sales_order` SET `status` = '$status', `complete_date` = '$curDateTime' WHERE increment_id = $increment_id";
        $connection->query($sql_update_status);

        $sql_update_grid_status = "UPDATE `seller_sales_order_grid` SET `status` = '$status' WHERE increment_id = $increment_id";
        $connection->query($sql_update_grid_status);

        return $resultRedirect->setPath('customer/buyer/view/increment_id/'.$increment_id);
        //return $resultRedirect;
    }
}
