<?php
namespace Libero\Customer\Controller\Admin;
class RemoveShipping extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $resultRedirect = $this->resultRedirectFactory->create();
        try {

            $id_shipping = $this->getRequest()->getParam("id_shipping");
            $model_shipping = $objectManager->create("Libero\Onestepcheckout\Model\Shipping")->load($id_shipping);
            $model_shipping->delete();
            $resultRedirect->setPath('customer/admin/delivery/');
            return $resultRedirect;

        }catch (Exception $e){

        }
    }
}
