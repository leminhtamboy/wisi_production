<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Customer\Model\Group;
class SaveProduct extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    function RemoveSign($str)
    {
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $helper = $objectManager->create('\Libero\Customer\Helper\Data');
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        try {

            $postData = $this->getRequest()->getPostValue();            
            $name = $util->removeQuoteInString($postData["product_name"]);
            $sku = $util->removeQuoteInString($postData["sku"]);
            $price = $helper->removeDecimalSymbol($postData["price"]);
            $qty = $helper->removeDecimalSymbol($postData["qty"]);
            $isInStock = $postData["is_stock"];
            $status = 1;
            $weight = $postData["weight"];
            $description = $postData["description"];
            $short_description = $postData["short_description"];
            $id_seller = $postData["id_seller"];
            $array_category = explode("|",$postData["id_category"]);
            $last_category_id = $postData["last_category_id"];
            $special_price = $helper->removeDecimalSymbol($postData["special_price"]);
            $length = $postData["length"];
            $width = $postData["width"];
            $height = $postData["height"];
            /*$support_shipment_amount = $helper->removeDecimalSymbol($postData["support_shipment_amount"]);
            $free_shipment = $postData['free_shipment'];*/
            $support_shipment_amount = "";
            $free_shipment = $postData['free_shipment'];
            if($free_shipment == 1){
                $weight = 0;
            }
            $tax_class_id = $postData["tax_class_id"];
            //$id_shipping = $postData["shipping_methos"];
            //Quick Detail
            $array_quickdetail_title = $postData["quick_detail_title"];
            $quick_detail_content = $postData["quick_detail_content"];
            $supply_ability_amount = $postData["supply_ability_amount"];
            $supply_ability_type = $postData["supply_ability_type"];
            $supply_ability_date = $postData["supply_ability_date"];
            //Processing to Json Array
            $q = 0 ;
            $length_quick_detail = count($array_quickdetail_title);
            $array_json_quick_detail = array();
            $array_json_quick_detail["data"] = array();
            $array_json_quick_detail["is_have"] = "false";
            $array_content_quick_detail = array();
            for($q = 0 ; $q < $length_quick_detail ; $q++){
                $array_child  = array();
                $array_child["title"]  = $array_quickdetail_title[$q];
                $array_child["content"]  = $quick_detail_content[$q];
                $array_content_quick_detail[] = $array_child;
            }
            if(count($array_content_quick_detail) > 0){
                $array_json_quick_detail["data"] = $array_content_quick_detail;
                $array_json_quick_detail["is_have"] = "true";
            }
            $json_quick_detail = json_encode($array_json_quick_detail);

            $array_supply_ability = array();
            $array_supply_ability["supply_ability_amount"] = $supply_ability_amount;
            $array_supply_ability["supply_ability_type"] = $supply_ability_type;
            $array_supply_ability["supply_ability_date"] = $supply_ability_date;
            $json_supply_ability = json_encode($array_supply_ability);
            //End Quick Detail
            $array_path_img = $postData["image_all"];
            if($postData["url_key"] == ""){
                $url_key = $this->RemoveSign($name)."-".$id_seller;
            }else{
                $url_key = $postData["url_key"]."-".$id_seller;
            }
            $meta_title = $postData["meta_title"];
            $product_meta_name = $this->RemoveSign($name);
            $meta_keywords = trim($postData["meta_keyword"]);
            $meta_description = $postData["meta_description"];
            $base_img = $postData["select-base-img"];
            //Upload image
            /*$i = 0;
            $pathSave = 'temp_image_product/';
            $arrayResultImage = array();
            foreach($_FILES["image"]["tmp_name"] as $key=>$tmp_name) {
                $fileUploaderFactory = $objectManager->get("\Magento\MediaStorage\Model\File\UploaderFactory");
                $uploader = $fileUploaderFactory->create(['fileId' => "image[$i]"]);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png', 'pdf']);
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
                $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('temp_image_product');
                $result = $uploader->save($path);
                $imagePath = $pathSave.$result['file'];
                $arrayResultImage[] = $imagePath;
                $i++;
            }*/
            $tier_price_qty = $helper->removeDecimalSymbol(explode("|",$postData["tier_price_qty"]));
            $tier_price_price = $helper->removeDecimalSymbol(explode("|",$postData["tier_price_price"]));
            //$customerGroup = 1;
            $i = 0;
            $arrayParentTier = array();
            foreach($tier_price_qty as $_tier){
                $tier_qty = $_tier;
                $tier_price = $tier_price_price[$i];
                $array_tier_product = array();
                $array_tier_product['website_id'] = 0;
                $array_tier_product['cust_group'] = Group::CUST_GROUP_ALL;
                $array_tier_product['price_qty'] = $tier_qty;
                $array_tier_product['price'] = $tier_price;
                $arrayParentTier[] = $array_tier_product;
                $i++;
            }
            $product = $objectManager->create("\Magento\Catalog\Model\Product");
            $product->setSku($sku); // Set your sku here
            $product->setName($name); // Name of Product
            $product->setAttributeSetId(4); // Attribute set id
            $product->setStatus($status); // Status on product enabled/ disabled 1/0
            $product->setWeight($weight); // weight of product
            $product->setData("length",$length); // weight of product
            $product->setData("height",$height); // weight of product
            $product->setData("width",$width); // weight of product
            $product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
            $product->setTaxClassId($tax_class_id); // Tax class id
            $product->setTypeId('simple'); // type of product (simple/virtual/downloadable/configurable)
            $product->setPrice($price); // price of product
            $product->setSpecialPrice($special_price);
            $product->setSupportShipmentAmount($support_shipment_amount);
            $product->setFreeShipment($free_shipment); // free_shipment enabled/ disabled 1/0
            $product->setData("supply_ability",$json_supply_ability);
            $product->setData("quick_detail",$json_quick_detail);
            $product->setDescription($description);
            $product->setCategoryIds($array_category);
            $product->setShortDescription($short_description);
            $product->setLastCategoryId($last_category_id);
            $product->setData("url_key",$url_key);
            $product->setData("meta_title",$meta_title);
            $product->setData("meta_keyword",$meta_keywords);
            $product->setData("meta_description",$meta_description);
            $product->setData("product_meta_name",$product_meta_name);
            $product->setStockData(
                array(
                    'use_config_manage_stock' => 0,
                    'manage_stock' => 1,
                    'is_in_stock' => $isInStock,
                    'qty' => $qty
                )
            );
            $product->setData("id_seller",$id_seller);
            //$product->setData("id_shipping",$id_shipping);
            $product->setWebsiteIds(array(1));
            $i = 0;
            foreach($array_path_img as $_img_path) {
                if($base_img == $_img_path){
                    $product->addImageToMediaGallery($_img_path, array('image', 'small_image', 'thumbnail'), false, false);
                }else{
                    $product->addImageToMediaGallery($_img_path, array(), false, false);
                }
                $i++;
            }
            $product->setTierPrice($arrayParentTier);
            $product->save();
            date_default_timezone_set("Asia/Ho_Chi_Minh");
            $time_update = date("Y-m-d H:i:s");
            $product_id = $product->getId();
            $product_name = $name;
            $product_sku = $sku;
            $jon_data_save = json_encode(array());
            $json_diff = json_encode(array());
            $provider_id = $id_seller;
            $data_provider = $helper->getProviderDetail($id_seller);
            $provider_name = $data_provider->getData('store_name');
            $sql_insert = "INSERT INTO `libero_temp_save_product` (`id`, `product_id`, `product_sku`, `product_name`, `data_save`,`data_diff`,`provider_id`, `provider_name`, `time_update`) VALUES 
            (NULL, '$product_id', '$product_sku', '$product_name', '$jon_data_save','$json_diff','$provider_id', '$provider_name', '$time_update')";
            $connection->query($sql_insert);
            $this->messageManager->addSuccess("Add Product Successfully !");
            $resultRedirect->setPath('customer/admin/productlisting');
            return $resultRedirect;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
            $resultRedirect->setPath('*/*');
            return $resultRedirect;
        }
    }
}
