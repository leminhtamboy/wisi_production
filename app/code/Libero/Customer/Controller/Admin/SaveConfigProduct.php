<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Customer\Model\Group;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;

class SaveConfigProduct extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    function RemoveSign($str)
    {
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }
    public function xu_ly_dau_cuoi($string_temp)
    {
        $string_temp = substr($string_temp,0,strlen($string_temp) - 1);
        return $string_temp;
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $helper = $objectManager->create('\Libero\Customer\Helper\Data');
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $postData = $this->getRequest()->getPostValue();  
        $base_url = $storeManager->getStore()->getBaseUrl();
        $productRepository = $objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface");
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        try {
            //echo "<pre>";
            //var_dump($postData);
            //echo "</pre>";
            //$this->messageManager->addSuccess("Add Configruable Product ".$name." Successfully.");
            //$resultRedirect->setPath('customer/admin/productlisting');
            //return $resultRedirect;
            //die();
            //return ;
            //die();
            $attribute_string = $postData["attribute_selected_string"];
            $attribute_string = $this->xu_ly_dau_cuoi($attribute_string);
            $attribute_array = explode("|",$attribute_string);
            $attributeValues_json = $postData["attribute_value"];
            $associatedProductString = $postData["variant_object"];
            $associatedProductString  = $this->xu_ly_dau_cuoi($associatedProductString);
            $associatedProductIds = explode("|",$associatedProductString);
            $isSimle = true;
            if(isset($_POST["configurable-matrix-variant-id"])){
                $isSimle = false;
            }
            $name = $util->removeQuoteInString($postData["product_name"]);
            $sku = $util->removeQuoteInString($postData["sku"]);
            $price = $helper->removeDecimalSymbol($postData["price"]);
            $qty = $helper->removeDecimalSymbol($postData["qty"]);
            $tax_class = 0;
            $min_qty = $postData["min_qty"];
            $isInStock = $postData["is_stock"];
            $status = 0;
            $weight = $helper->removeDecimalSymbol($postData["weight"]);
            $description = $postData["description"];
            //$description = str_replace("../../../",$base_url,$description);
            $short_description = $postData["short_description"];
            //$short_description = str_replace("../../../",$base_url,$short_description);
            $id_seller = $postData["id_seller"];
            $array_category = explode("|",$postData["id_category"]);
            $last_category_id = $postData["last_category_id"];
            $special_price = $helper->removeDecimalSymbol($postData["special_price"]);
            $base_price = $postData["base-price"];
            $VAT = $postData["tax_class_id"];
            $price = $base_price +  ($base_price * ($VAT/100));
            $price = round($price,0);
            $length = $postData["length"];
            $width = $postData["width"];
            $height = $postData["height"];
            if($special_price == 0){
                $special_price = "";
            }
            //$special_to_date = $postData["special_to_date"];
            //$special_from_date = $postData["special_from_date"];
            //Quick Detail
            $array_quickdetail_title = $postData["quick_detail_title"];
            $quick_detail_content = $postData["quick_detail_content"];

            $supply_ability_amount = $postData["supply_ability_amount"];
            $supply_ability_type = $postData["supply_ability_type"];
            $supply_ability_date = $postData["supply_ability_date"];
            $free_shipment = $postData["free_shipment"];
            if($free_shipment == 1){
                $weight = 0;
            }
            //Processing to Json Array
            $q = 0 ;
            $length_quick_detail = count($array_quickdetail_title);
            $array_json_quick_detail = array();
            $array_json_quick_detail["data"] = array();
            $array_json_quick_detail["is_have"] = "false";
            $array_content_quick_detail = array();
            for($q = 0 ; $q < $length_quick_detail ; $q++){
                $array_child  = array();
                $array_child["title"]  = $array_quickdetail_title[$q];
                $array_child["content"]  = $quick_detail_content[$q];
                $array_content_quick_detail[] = $array_child;
            }
            if(count($array_content_quick_detail) > 0){
                $array_json_quick_detail["data"] = $array_content_quick_detail;
                $array_json_quick_detail["is_have"] = "true";
            }
            $json_quick_detail = json_encode($array_json_quick_detail,JSON_UNESCAPED_UNICODE);

            $array_supply_ability = array();
            $array_supply_ability["supply_ability_amount"] = $supply_ability_amount;
            $array_supply_ability["supply_ability_type"] = $supply_ability_type;
            $array_supply_ability["supply_ability_date"] = $supply_ability_date;
            $json_supply_ability = json_encode($array_supply_ability,JSON_UNESCAPED_UNICODE);
            //End Quick Detail


            $array_path_img = $postData["image_all"];
            if($postData["url_key"] == ""){
                $url_key = $this->RemoveSign($name)."-".$id_seller;
            }else{
                $url_key = $postData["url_key"]."-".$id_seller;
            }
            $meta_title = $postData["meta_title"];
            $product_meta_name = $this->RemoveSign($name);
            $meta_keywords = trim($postData["meta_keyword"]);
            $meta_description = $postData["meta_description"];
            $base_img = $postData["select-base-img"];
            $tier_price_qty = $helper->removeDecimalSymbol(explode("|",$postData["tier_price_qty"]));
            $tier_price_price = $helper->removeDecimalSymbol(explode("|",$postData["tier_price_price"]));
            //$customerGroup = 1;
            $i = 0;
            $arrayParentTier = array();
            foreach($tier_price_qty as $_tier){
                $tier_qty = $_tier;
                $tier_price = $tier_price_price[$i];
                $array_tier_product = array();
                $array_tier_product['website_id'] = 0;
                $array_tier_product['cust_group'] = Group::CUST_GROUP_ALL;
                $array_tier_product['price_qty'] = $tier_qty;
                $array_tier_product['price'] = $tier_price;
                $arrayParentTier[] = $array_tier_product;
                $i++;
            }
            $product = $objectManager->get("\Magento\Catalog\Model\Product");
            $product->setSku($sku); // Set your sku here
            $product->setName($name); // Name of Product
            $product->setAttributeSetId(4); // Attribute set id
            $product->setStatus($status); // Status on product enabled/ disabled 1/0
            $product->setWeight($weight);
            $product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
            $product->setTaxClassId($tax_class); // Tax class id
            if($isSimle){
                $product->setTypeId('simple'); // type of product (simple/virtual/downloadable/configurable)
                $product->setStockData(['use_config_manage_stock' => 1,'min_sale_qty' => $min_qty,'is_in_stock' => 1,"qty" => $qty]);
                $product->setData("length",$length);
                $product->setData("height",$height);
                $product->setData("width",$width);
            }else{
                $product->setTypeId('configurable'); // type of product (simple/virtual/downloadable/configurable)
            }
            $product->setPrice($price); // price of product
            $product->setSpecialPrice($special_price); // special price of product
            $product->setFreeShipment($free_shipment); // free_shipment enabled/ disabled 1/0
            $product->setData("supply_ability",$json_supply_ability);
            $product->setData("quick_detail",$json_quick_detail);
            $product->setDescription($description);
            $product->setCategoryIds($array_category);
            $product->setShortDescription($short_description);
            $product->setLastCategoryId($last_category_id);
            $product->setData("url_key",$url_key);
            $product->setData("meta_title",$meta_title);
            $product->setMetaKeyword($meta_keywords);
            $product->setData("meta_description",$meta_description);
            $product->setData("product_meta_name",$product_meta_name);
            $product->setData("id_seller",$id_seller);
            $product->setData("Vat",$VAT);
            $product->setWebsiteIds(array(1));
            $product->setTierPrice($arrayParentTier);
            //Modifier Cho Chỗ này để xác đình simple hoặc configruable
            /*$stockItem->setData("is_in_stock",$is_in_stock);
            $stockItem->setData("qty",$variant_qty);
            $stockItem->setData("manage_stock",1);
            $stockItem->setData("use_config_notify_stock_qty",1);*/
            //End modifier
            $i = 0;
            /*
            $product->addImageToMediaGallery($base_img, array('image', 'small_image', 'thumbnail'), false, false);
            foreach($array_path_img as $_img_path) {
                $product->addImageToMediaGallery($_img_path, array(), false, false);
            }
            */
            $product->save();
            $product_after = $objectManager->create("\Magento\Catalog\Model\Product")->load($product->getId());
            //Processing Image
            $image_data = array(); // Remember this case save json as key 'main' and variant (if is config) in main is key id main product
            $main_img_child = array();
            $array_main_child_image = array();
            $array_temp_sub_img = array();
                //First create for main product , in main have key is product id in key have base image is key ann value is path image
            $array_main_child_image['base_img'] = $base_img;        
            foreach($array_path_img as $_img_path) {
                $array_temp_sub_img[] = $_img_path;
            }
            $array_main_child_image['sub_img'] = $array_temp_sub_img;
            $main_img_child[$product->getId()] = $array_main_child_image;
            $image_data["main"] = $main_img_child;
                //Done for Main Product Image
            //End Processing Image

            //Region Configruable Product
            if($isSimle == false){
                $array_variant_image_data = array();
                $image_variant_array = $postData["image_variant_array"];
                /*$image_variant_id = $postData["image_variant_id"];
                $length_of_variant_id = count($image_variant_id);
                $length_of_variant_array = count($image_variant_array);
                $img_index = 0;
                for($img_index = 0 ; $img_index < $length_of_variant_id ; $img_index++){
                    $id_variant = $image_variant_id[$img_index]; 
                    $variant = $objectManager->create("\Magento\Catalog\Model\Product")->load($id_variant);
                    $variant_img = $image_variant_array[$img_index];
                    $variant->addImageToMediaGallery($variant_img, array('image', 'small_image', 'thumbnail'), false, false);
                    $variant->save();
                }*/
                $variant_id_array = $postData["configurable-matrix-variant-id"];
                $variant_qty_array = $postData["configurable-matrix-qty"];
                $variant_weight_array = $postData["configurable-matrix-weight"];
                
                $variant_length_array = $postData["configurable-matrix-length"];
                $variant_height_array = $postData["configurable-matrix-height"];
                $variant_width_array = $postData["configurable-matrix-width"];

                $variant_price_array = $postData["configurable-matrix-price"];
                $variant_special_price_array  =  $postData["configurable-matrix-special-price"];
                $variant_stock_array = $postData['configurable-matrix-variant-stock'];
                $variant_index = 0;
                $length_of_variant = count($variant_id_array);
                for($variant_index=0; $variant_index < $length_of_variant ; $variant_index++){
                    $_variant_id = $variant_id_array[$variant_index];
                    $variant = $objectManager->create("\Magento\Catalog\Model\Product")->load($_variant_id);
                    $stockRegistry = $objectManager->create("\Magento\CatalogInventory\Api\StockRegistryInterface");
                    $stockItem = $stockRegistry->getStockItem($_variant_id);
                    $variant_qty = $helper->removeDecimalSymbol($variant_qty_array[$variant_index]);
                    
                    $variant_weight = $helper->removeDecimalSymbol($variant_weight_array[$variant_index]);
                    $variant_length = $helper->removeDecimalSymbol($variant_length_array[$variant_index]);
                    $variant_height = $helper->removeDecimalSymbol($variant_height_array[$variant_index]);
                    $variant_width = $helper->removeDecimalSymbol($variant_width_array[$variant_index]);

                    $variant_price = $helper->removeDecimalSymbol($variant_price_array[$variant_index]);
                    $variant_sepecial_price =  $helper->removeDecimalSymbol($variant_special_price_array[$variant_index]);
                    $is_in_stock = $variant_stock_array[$variant_index];
                    if($free_shipment == 1){
                        $variant_weight = 0;
                    }
                    if($variant_sepecial_price == 0){
                        $variant_sepecial_price = "";
                    }
                    $variant_img = $image_variant_array[$variant_index];
                    //Image
                    /*$variant_img = $image_variant_array[$variant_index];
                    $variant->addImageToMediaGallery($variant_img, array('image', 'small_image', 'thumbnail'), false, false);
                    foreach($array_path_img as $_img_path) {
                        $variant->addImageToMediaGallery($_img_path, array(), false, false);
                    }*/
                    $array_variant_children_image = array();
                    $array_variant_sub_img = array();
                    $array_variant_children_image["base_img"] = $variant_img;
                    //Thumbnail Image
                    foreach($array_path_img as $_img_path) {
                        $array_variant_sub_img[] = $_img_path;
                    }
                    $array_variant_children_image["sub_img"] = $array_variant_sub_img;
                    $array_variant_image_data[$_variant_id] = $array_variant_children_image;
                    //End Region
                    $variant->setWeight($variant_weight);
                    $variant->setPrice($variant_price);
                    $variant->setSpecialPrice($variant_sepecial_price);
                    $variant->setTaxClassId($tax_class);
                    $variant->setData("length",$variant_length);
                    $variant->setData("height",$variant_height);
                    $variant->setData("width",$variant_width);
                    $variant->setVisibility(1);
                    $variant->setCategoryIds($array_category);
                    $variant->setData("supply_ability",$json_supply_ability);
                    $variant->setData("quick_detail",$json_quick_detail);
                    $variant->setData("id_seller",$id_seller);
                    $variant->setData("Vat",$VAT);
                    $variant->setLastCategoryId($last_category_id);
                    $variant->setTierPrice($arrayParentTier);
                    $variant->setWebsiteIds(array(1));
                    $stockItem->setData("is_in_stock",$is_in_stock);
                    $stockItem->setData("qty",$variant_qty);
                    $stockItem->setData("min_sale_qty",$min_qty);
                    $stockItem->setData("manage_stock",1);
                    $stockItem->setData("use_config_notify_stock_qty",1);
                    $stockItem->save();
                    $variant->save();
                }
                $image_data["variant"] = $array_variant_image_data;
                /** @var $product Product */
                /** @var Factory $optionsFactory */
                $configurableAttributesData = array();
                $attributeValues = json_decode($attributeValues_json,true);
                foreach($attribute_array as $attribute_id){
                    $eavConfig = $objectManager->create("\Magento\Eav\Model\Config");
                    $eavConfig->clear();
                    $attribute = $eavConfig->getAttribute('catalog_product', $attribute_id);
                    $array_entity = array();
                    $array_entity["attribute_id"] = $attribute->getId();
                    $array_entity["code"] = $attribute->getAttributeCode();
                    $array_entity["label"] = $attribute->getStoreLabel();
                    $array_entity["values"] = $attributeValues;
                    $configurableAttributesData[] = $array_entity;
                }
                $optionsFactory = $objectManager->create("\Magento\ConfigurableProduct\Helper\Product\Options\Factory");
                $configurableOptions = $optionsFactory->create($configurableAttributesData);
                $extensionConfigurableAttributes = $product_after->getExtensionAttributes();
                $extensionConfigurableAttributes->setConfigurableProductOptions($configurableOptions);
                $extensionConfigurableAttributes->setConfigurableProductLinks($variant_id_array);
                $product_after->setExtensionAttributes($extensionConfigurableAttributes);
                $product_after->setTypeId("configurable")
                    ->setAttributeSetId(4)
                    ->setWebsiteIds([1])
                    ->setStockData(['use_config_manage_stock' => 1, 'is_in_stock' => 1]);
            }
            /*$this->_objectManager->create('Magento\Catalog\Model\Product')
            ->setStoreId(1)
            ->load($productId)
            ->setStoreId($copyTo)
            ->setCopyFromView(true)
            ->save();*/
            $json_image_data = json_encode($image_data,JSON_UNESCAPED_UNICODE);
            $product_after->setData("image_data",$json_image_data);
            $product_after->save();
            date_default_timezone_set("Asia/Ho_Chi_Minh");
            $time_update = date("Y-m-d H:i:s");
            $product_id = $product->getId();
            $product_name = $name;
            $product_sku = $sku;
            $jon_data_save = json_encode(array());
            $json_diff = json_encode(array());
            $provider_id = $id_seller;
            $data_provider = $helper->getProviderDetail($id_seller);
            $provider_name = $data_provider->getData('store_name');
            $sql_insert = "INSERT INTO `libero_temp_save_product` (`id`, `product_id`, `product_sku`, `product_name`, `data_save`,`data_diff`,`provider_id`, `provider_name`, `time_update`) VALUES 
            (NULL, '$product_id', '$product_sku', '$product_name', '$jon_data_save','$json_diff','$provider_id', '$provider_name', '$time_update')";
            $connection->query($sql_insert);
            //Add to cron job image
                //Put save image to cron job
                $type_cron = "CRON_PRODUCT_IMAGE";
                $cron_code = "CRON_EX15G79_PRODUCT_".$product_id."_UPDATE";
                $content_cron_excute = $product_after->getData("image_data");
                date_default_timezone_set("Asia/Ho_Chi_Minh");
                $time_update = date("Y-m-d H:i:s");
                $created_at = $time_update;
                $status = 0; // Processsing;
                $etc = "";
                $sql_insert_cron = "INSERT INTO `libero_cron_execute_background` (`id_cron`, `type_cron`, `cron_code`, `content_excute`, `created_at`, `status`, `etc`) 
                VALUES (NULL, '$type_cron', '$cron_code', '$content_cron_excute', '$created_at', '0', '$etc');";
                $connection->query($sql_insert_cron);
            //End add to cron job image
            $this->messageManager->addSuccess(__("Add Product %1 successfully.",$name));
            $resultRedirect->setPath('customer/admin/productlisting');
            return $resultRedirect;

        }catch (\Exception $e){
            $this->messageManager->addError($e->getMessage());
            $resultRedirect->setPath('customer/admin/productlisting');
            /*$associatedProductString = $postData["variant_object"];
            $associatedProductString  = $this->xu_ly_dau_cuoi($associatedProductString);
            $associatedProductIds = explode("|",$associatedProductString);
            $resultRedirect->setPath('customer/admin/productlisting');
            $objectManager->get('Magento\Framework\Registry')->register('isSecureArea', true);
            foreach($associatedProductIds as $_id){
                $product = $objectManager->create("\Magento\Catalog\Model\Product")->load($_id);
                $product->delete();
            }*/
            return $resultRedirect;
        }
    }
}
