<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
class SaveShipment extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    protected $_resource;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->_resource = $resourceConnection;
        parent::__construct($context);
    }

    public function getOrderDetail($order_id,$id_seller)
    {
        $connection = $this->_resource->getConnection();
        $sql = "
            SELECT
                *, ssoi.qty_ordered AS qty, ssoi.item_id AS order_item_id
            FROM
                seller_sales_order_grid AS ssog
                    JOIN
                seller_sales_order AS sso
                    JOIN
                seller_sales_order_item AS ssoi
                    ON ssog.order_id = ssoi.order_id
                    AND sso.entity_id = ssoi.order_id
            WHERE
                ssog.seller_id = '$id_seller'
                AND ssog.order_id = '$order_id'
            ;
            ";
        $result = $connection->fetchAll($sql);
        return $result;
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $helperCheckout = $objectManager->get("\Libero\Onestepcheckout\Helper\Data");
        try {
            $postData = $this->getRequest()->getPostValue();
            $order_id = $postData["order_id"];
            $id_seller = $postData["id_seller"];
            $order_detail = $this->getOrderDetail($order_id,$id_seller);
            $order = $order_detail[0];
            $increment_id = $order["increment_id"];
            $store_id = $order["store_id"];
            $order_created_at = $order["created_at"];
            $customer_name = $order["customer_name"];
            $total_qty = $order["total_qty_ordered"];
            $order_status = "Prepare to ship";
            $billing_address = $order["billing_address"];
            $shipping_address = $order["shipping_address"];
            $billing_name = $order["customer_name"];
            $shipping_name = $order["customer_name"];
            $customer_email = $order["customer_email"];
            $customer_group_id = $order["customer_group_id"];
            $payment_method = $order["payment_method"];
            $shipping_information = $order["shipping_description"];
            $base_grand_total = floor($order["base_grand_total"]);
            $sql_insert = "
                INSERT INTO `seller_sales_shipment_grid` 
                    (`entity_id`, 
                    `increment_id`, 
                    `store_id`, 
                    `order_increment_id`, 
                    `order_id`, 
                    `order_created_at`, 
                    `customer_name`, 
                    `total_qty`, 
                    `shipment_status`, 
                    `order_status`,
                     `billing_address`, 
                     `shipping_address`, 
                     `billing_name`, 
                     `shipping_name`, 
                     `customer_email`, 
                     `customer_group_id`, 
                     `payment_method`, `shipping_information`, `created_at`, `updated_at`, `seller_id`) 
                    VALUES (NULL,'$increment_id','$store_id', '$increment_id', '$order_id', '$order_created_at', '$customer_name','$total_qty', NULL, '$order_status', '$billing_address', '$shipping_address','$billing_name', '$shipping_name','$customer_email','$customer_group_id','$payment_method','$shipping_information', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '$id_seller');
            ";
            
            $connection = $this->_resource->getConnection();
            $connection->query($sql_insert);
            $modelCustomer = $objectManager->create('Magento\Customer\Model\Customer');
            $modelCustomer->setWebsiteId(1); 
            $customer = $modelCustomer->loadByEmail($customer_email);
            $customer_id = $customer->getId();
            //Update status order
            $sql_update_status_order = "UPDATE seller_sales_order SET `status` = '$order_status' WHERE entity_id = '$order_id';";
            $connection->query($sql_update_status_order);
            $sql_update_status_order_grid = "UPDATE seller_sales_order_grid SET `status` = '$order_status' WHERE order_id = '$order_id';";
            $connection->query($sql_update_status_order_grid);
            $sql_get_information_shipping_quote = "SELECT * FROM `libero_quote_shipping_price` WHERE `order_id` = '$order_id' AND `id_seller` = '$id_seller'";
            $data_shipping_quote = $connection->fetchAll($sql_get_information_shipping_quote);

            //Lay customer ra
            $sql = "select * from customer_address_entity where parent_id = '$customer_id'";
            $data_address_customer =  $connection->fetchAll($sql);
            //Lay thong tin provider ra
            $providerModel = $objectManager->get('Magento\Customer\Helper\Session\CurrentCustomer');
            $provider = $providerModel->getCustomer();
            $provider_id  = $provider->getId();
            $sql_provider = "select * from customer_address_entity where parent_id = '$provider_id'";
            $data_address_provider =  $connection->fetchAll($sql_provider);
            $provider_name  = "";
            $provider_phone = "";
            $provider_address = "";

            $customer_name = "";
            $customer_phone = "";
            $customer_address = "";

            if(count($data_address_customer) > 0){
                $provider_name  = $provider->getFirstname()."".$provider->getLastName();
                $provider_phone = $data_address_provider[0]["telephone"];
                $provider_address = $data_address_provider[0]["street"].",".$data_address_provider[0]["region"].",".$data_address_provider[0]["city"];
                $customer_name = $data_address_customer[0]["firstname"]."".$data_address_customer[0]["lastname"];
                $customer_phone = $data_address_customer[0]["telephone"];
                $customer_address = $data_address_customer[0]["street"].",".$data_address_customer[0]["region"].",".$data_address_customer[0]["city"];;
            }
            /* Region Create Order On Giao Hang Nhanh */
            $shipping_code = $data_shipping_quote[0]['code_delivery'];
            if($shipping_code == $helperCheckout->_CODE_GIAO_HANG_NHANH){
                $post_data["weight"] = $data_shipping_quote[0]['weight'];
                $post_data["length"] = $data_shipping_quote[0]['length'];
                $post_data["width"] = $data_shipping_quote[0]['width'];
                $post_data["height"] = $data_shipping_quote[0]['height'];
                $post_data["from_district_id"] = $data_shipping_quote[0]['from_district'];
                $post_data["to_district_id"] = $data_shipping_quote[0]['to_district'];
                $post_data["service_id"] = $data_shipping_quote[0]['service_id'];
                $post_data["coupon_code"] = "";
                $post_data["insurance_free"] = 0;
                $post_data["note"] = "";
                //Region Seller Address
                $post_data["client_contact_name"] = $provider_name;
                $post_data["client_contact_phone"] = $provider_phone;
                $post_data["client_address"] = $provider_address;
                //Region Customer Address
                $post_data["customer_name"] = $customer_name;
                $post_data["customer_phone"] = $customer_phone;
                $post_data["shipping_address"] = $billing_address;
                $post_data["cod_amount"] = $base_grand_total;
                $response = $helperCheckout->pushOrderToGiaoHangNhanh($post_data);
                //$response = json_encode(array("data" => array("OrderCode" => $response[""])));
                $json_data = json_decode($response);
                $order_code = $json_data->data->OrderCode;
            }
            if($shipping_code == $helperCheckout->_CODE_GIAO_HANG_TIET_KIEM){
                $post_data["order_id"] = $increment_id;
                $post_data["weight"] = $data_shipping_quote[0]['weight'];
                $post_data["length"] = $data_shipping_quote[0]['length'];
                $post_data["width"] = $data_shipping_quote[0]['width'];
                $post_data["height"] = $data_shipping_quote[0]['height'];
                $post_data["from_district_id"] = $data_shipping_quote[0]['from_district'];
                $post_data["to_district_id"] = $data_shipping_quote[0]['to_district'];

                $post_data["from_province"] = $data_shipping_quote[0]['from_province'];
                $post_data["to_province"] = $data_shipping_quote[0]['to_province'];

                $post_data["service_id"] = $data_shipping_quote[0]['service_id'];
                $post_data["coupon_code"] = "";
                if($postData["agree"] == 1){
                    //là có
                    $post_data["insurance_free"] = "co_bao_hiem";
                }else{
                    $post_data["insurance_free"] = 0;    
                }
                $post_data["note"] = "";
                //Region Seller Address
                $post_data["client_contact_name"] = $provider_name;
                $post_data["client_contact_phone"] = $provider_phone;
                $post_data["client_address"] = $provider_address;
                //Region Customer Address
                $post_data["customer_name"] = $customer_name;
                $post_data["customer_phone"] = $customer_phone;
                $post_data["shipping_address"] = $billing_address;
                
                $post_data["cod_amount"] = $base_grand_total;
                $post_data["comment_text"] = $postData["comment_text"];
                $response = $helperCheckout->pushOrderToGiaoHangTietKiem($post_data);
                $response_decode = json_decode($response,true);
                $ma_don_ghtk = $response_decode["order"]["label"];
                $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/test.log');
                $logger = new \Zend\Log\Logger();
                $logger->addWriter($writer);
                $logger->info($ma_don_ghtk);
                $logger->info($order_id);
                $logger->info(print_r($response_decode, true));
                $order_code = $ma_don_ghtk;
            }
            $sql_update = "UPDATE `seller_sales_order` SET `order_code_api` = '$order_code'	WHERE entity_id = $order_id ";
            $connection->query($sql_update);
            /* End Region */
            $status = $order_detail[0]["status"];
            $content_logs = "Seller Update Order $order_id From Status [ $status ] To Status [ Prepare To Ship ]";
            $seller_id = $order_detail[0]["seller_id"];
            $sql_insert_log = "INSERT INTO `sales_order_seller_logs` (`id_logs`, `seller_id`, `order_id`, `content`, `etc`) 
            VALUES (NULL, '$seller_id', '$order_id', '$content_logs', '')";
            $connection->query($sql_insert_log);
            $blockAdmin = $objectManager->create("\Libero\Customer\Block\Seller\Admin");
            $blockAdmin->setMessage("Create Shipment Successfully !","success");
            $resultRedirect->setPath('customer/admin/shipment/order_id/'.$order_id);

            /* Start Barcode */
            $dir_orderbarcode = "orderbarcode";
            $dir = str_replace("\\","/",BP.'/'."pub/media".'/'.$dir_orderbarcode);
            if (!is_dir($dir))
            {
                mkdir($dir);
            }
            $upload_path = $dir . '/'.$increment_id."_barcode.png";
            $generatorPNG = $objectManager->get('\Picqer\Barcode\BarcodeGeneratorPNG');
            file_put_contents($upload_path, $generatorPNG->getBarcodeNo($increment_id, $generatorPNG::TYPE_CODE_128));
            /* End Barcode*/
            return $resultRedirect;

        }catch (Exception $e){
           
        }
    }
}
