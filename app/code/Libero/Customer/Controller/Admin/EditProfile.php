<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
class EditProfile extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $resultRedirect = $this->resultRedirectFactory->create();
        try {

            $postData = $this->getRequest()->getPostValue();
            $description = trim($postData["description"]);
            $company_info = trim($postData["company_info"]);
            $id_seller = $postData["id_seller"];
            $injectSellerCompany = $objectManager->get("\Libero\Customer\Model\CustomerSeller");
            $objectSeller = $injectSellerCompany->load($id_seller);
            $fileUploaderFactory = $objectManager->get("\Magento\MediaStorage\Model\File\UploaderFactory");
            $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');

            $files = $this->getRequest()->getFiles();
            
            if($files['seller_logo']['name'] != ""){
                $uploader = $fileUploaderFactory->create(['fileId' => 'seller_logo']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png' , 'pdf']);
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('seller_logo');
                $pathSave = 'seller_logo/';
                $result = $uploader->save($path);
                $imagePathLogo = $pathSave.$result['file'];
                $objectSeller->setData('logo_url',$imagePathLogo);
            }
            //Upload image
            
            if($files['banner_logo1']['name'] != ""){
                $uploader = $fileUploaderFactory->create(['fileId' => 'banner_logo1']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png' , 'pdf']);
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('seller_banner');
                $pathSave = 'seller_banner/';
                $result = $uploader->save($path);
                $imagePathBanner = $pathSave.$result['file'];
                $objectSeller->setData('banner_url1',$imagePathBanner);
            }

            if($files['banner_logo2']['name'] != ""){
                $uploader = $fileUploaderFactory->create(['fileId' => 'banner_logo2']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png' , 'pdf']);
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('seller_banner');
                $pathSave = 'seller_banner/';
                $result = $uploader->save($path);
                $imagePathBanner = $pathSave.$result['file'];
                $objectSeller->setData('banner_url2',$imagePathBanner);
            }

            if($files['banner_logo3']['name'] != ""){
                $uploader = $fileUploaderFactory->create(['fileId' => 'banner_logo3']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png' , 'pdf']);
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('seller_banner');
                $pathSave = 'seller_banner/';
                $result = $uploader->save($path);
                $imagePathBanner = $pathSave.$result['file'];
                $objectSeller->setData('banner_url3',$imagePathBanner);
            }

            if($files['banner_logo4']['name'] != ""){
                $uploader = $fileUploaderFactory->create(['fileId' => 'banner_logo4']);
                $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png' , 'pdf']);
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('seller_banner');
                $pathSave = 'seller_banner/';
                $result = $uploader->save($path);
                $imagePathBanner = $pathSave.$result['file'];
                $objectSeller->setData('banner_url4',$imagePathBanner);
            }
            
            $objectSeller->setData('description',$description);
            $objectSeller->setData('company_info',$company_info);
            $objectSeller->save();
            $resultRedirect->setPath('*/*');
            return $resultRedirect;

        }catch (Exception $e){
            
        }
    }
}
