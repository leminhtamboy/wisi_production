<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
class SaveRmaEdit extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();

        try {

            $postData = $this->getRequest()->getPostValue();
            $message = $postData["message_note"];
            $id_seller = $postData["id_seller"];
            $id_request = $postData["id_request"];
            $resolution = $postData["resolution"];
            $status = $postData["status"];

            $modelAccountAcl = $this->_objectManager->create("\Libero\RMA\Model\RequestProduct")->load($id_request);
            $modelAccountAcl->setData("message",$message);
            $modelAccountAcl->setData("resolution",$resolution);
            $modelAccountAcl->setData("status",$status);
            $modelAccountAcl->save();
            $resultRedirect->setPath('customer/admin/requestrma');
            return $resultRedirect;

        }catch (Exception $e){
            
        }
    }
}
