<?php

namespace Libero\Customer\Controller\Admin;

use Magento\Framework\App\ResponseInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Libero\Customer\Model\InvoicePdf;

class PrintInvoice extends \Magento\Framework\App\Action\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Sales::sales_invoice';

    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $_fileFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    protected $_invoice_pdf;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        \Libero\Customer\Model\InvoicePdf $invoicePdf
    ) {
        $this->_fileFactory = $fileFactory;
        $this->_invoice_pdf = $invoicePdf;
        parent::__construct($context);
        $this->resultForwardFactory = $resultForwardFactory;
    }

    /**
     * @return ResponseInterface|void
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $dateTime = $objectManager->get("Magento\Framework\Stdlib\DateTime\DateTime");
        $resource = $objectManager->get('\Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $invoiceId = $this->getRequest()->getParam('order_id');
        if ($invoiceId) {
            $sql = "SELECT * FROM seller_sales_invoice WHERE `order_id` = '$invoiceId'";
            $invoiceData = $connection->fetchAll($sql);
            if (count($invoiceData) > 0 ) {
                $date = $this->_objectManager->get('Magento\Framework\Stdlib\DateTime\DateTime')->date('Y-m-d_H-i-s');
                return $this->_fileFactory->create(
                    sprintf('invoice%s.pdf', $dateTime->date('Y-m-d_H-i-s')),
                    $this->_invoice_pdf->getPdf($invoiceData)->render(),
                    DirectoryList::VAR_DIR,
                    'application/pdf'
                );
            }
        } else {
            return $this->resultForwardFactory->create()->forward('noroute');
        }
    }
}
