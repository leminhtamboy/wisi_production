<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
class UploadFile extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $pathSave = 'temp_image_product/';
            //$pathSave = 'import/';
            $arrayResultImage = array();
            $fileUploaderFactory = $objectManager->get("\Magento\MediaStorage\Model\File\UploaderFactory");
            $uploader = $fileUploaderFactory->create(['fileId' => "image"]);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png', 'pdf']);
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(false);
            $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
            $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('temp_image_product');
            //$path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('import');
            $new_file_name = str_replace(".","",$uploader->getFileNameInForm());
            $file_name = $new_file_name."_".time().".".$uploader->getFileExtension();
            $result = $uploader->save($path,$file_name);
            $imagePath = $pathSave.$result['file'];
            $arrayResultImage[] = $imagePath;
            $arrayJson["url"] = $imagePath;
            $result = $jsonResultFactory->setData(json_encode($arrayJson));
            return $result;
        } catch (Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }
}
