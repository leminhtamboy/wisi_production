<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Customer\Model\Group;
class SaveConfigruable extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    function RemoveSign($str)
    {
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }
    public function execute(){
        //$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        try{
            $simple_product = $this->_objectManager->create('\Magento\Catalog\Model\Product');
    $simple_product->setSku('test-simple-1');
    $simple_product->setName('test name simple 1');
    $simple_product->setAttributeSetId(4);
    $simple_product->setSize_general(193); // value id of S size
    $simple_product->setStatus(1);
    $simple_product->setTypeId('simple');
    $simple_product->setPrice(10);
    $simple_product->setWebsiteIds(array(1));
    $simple_product->setCategoryIds(array(31));
    $simple_product->setData("id_seller",1);
    $simple_product->setStockData(array(
        'use_config_manage_stock' => 0, //'Use config settings' checkbox
        'manage_stock' => 1, //manage stock
        'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
        'max_sale_qty' => 2, //Maximum Qty Allowed in Shopping Cart
        'is_in_stock' => 1, //Stock Availability
        'qty' => 100 //qty
        )
    );
 
    $simple_product->save();
 
    $simple_product_id = $simple_product->getId();
    echo "simple product id: ".$simple_product_id."\n";
 
    //configurable product
    $configurable_product = $this->_objectManager->create('\Magento\Catalog\Model\Product');
    $configurable_product->setSku('test-configurable-1');
    $configurable_product->setName('test name configurable 1');
    $configurable_product->setAttributeSetId(4);
    $configurable_product->setStatus(1);
    $configurable_product->setTypeId('configurable');
    $configurable_product->setPrice(11);
    $configurable_product->setWebsiteIds(array(1));
    $configurable_product->setCategoryIds(array(31));
    $configurable_product->setData("id_seller",1);
    $configurable_product->setStockData(array(
        'use_config_manage_stock' => 0, //'Use config settings' checkbox
        'manage_stock' => 1, //manage stock
        'is_in_stock' => 1, //Stock Availability
        )
    );
 
    $configurable_product->getTypeInstance()->setUsedProductAttributeIds(array(141),$configurable_product); //attribute ID of attribute 'size_general' in my store
    $configurableAttributesData = $configurable_product->getTypeInstance()->getConfigurableAttributesAsArray($configurable_product);
 
    $configurable_product->setCanSaveConfigurableAttributes(true);
    $configurable_product->setConfigurableAttributesData($configurableAttributesData);
 
    $configurableProductsData = array();
    $configurableProductsData[$simple_product_id] = array( //[$simple_product_id] = id of a simple product associated with this configurable
        '0' => array(
            'label' => '65 cm', //attribute label
            'attribute_id' => '141', //attribute ID of attribute 'size_general' in my store
            'value_index' => '92', //value of 'S' index of the attribute 'size_general'
            'is_percent'    => 0,
            'pricing_value' => '100',
        )
    );
    $configurable_product->setConfigurableProductsData($configurableProductsData);
 
    $configurable_product->save();
 
    echo "configurable product id: ".$configurable_product->getId()."\n";
die();
        }catch (Exception $e){

        }
    }
}
