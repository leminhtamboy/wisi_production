<?php
namespace Libero\Customer\Controller\Admin;

class SearchOrder extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    protected $_coreRegistry;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $resultRedirect = $this->resultRedirectFactory->create();
        $helper = $objectManager->create('\Libero\Customer\Helper\Data');
        try {

            $postData = $this->getRequest()->getPostValue();
            $id = $postData["id"];
            $date_from = $postData["date_from"];
            $date_to = $postData["date_to"];
            $status = $postData["order_status"];
            $resultPage = $this->resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->set(__('Seller Order'));
            $resultPage->getLayout()->getBlock("customer_seller_admin_order")->setHaveSearch("1");
            $resultPage->getLayout()->getBlock("customer_seller_admin_order")->setId($id);
            $resultPage->getLayout()->getBlock("customer_seller_admin_order")->setDateFrom($date_from);
            $resultPage->getLayout()->getBlock("customer_seller_admin_order")->setDateTo($date_to);
            $resultPage->getLayout()->getBlock("customer_seller_admin_order")->setOrderStatus($status);
            return $resultPage;
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
            $resultRedirect->setPath('*/*');
            return $resultRedirect;
        }
    }
}
