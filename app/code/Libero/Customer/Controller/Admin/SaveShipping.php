<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
class SaveShipping extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $blockAdmin = $objectManager->create("\Libero\Customer\Block\Seller\Admin");
        $resultRedirect = $this->resultRedirectFactory->create();
        try {
            $postData = $this->getRequest()->getPostValue();
            $deli_arr = explode("|",$postData["entity"]);
            $id_seller = $postData["id_seller"];
            $code = $deli_arr[0];
            $name = $deli_arr[1];
            $price = 0;
            $status  = 1;
            
            $model_shipping = $objectManager->create("Libero\Onestepcheckout\Model\Shipping");
            $shipping_collection = $model_shipping->getCollection()->addFieldToFilter("id_seller",array("eq" => $id_seller));
            
            date_default_timezone_set("Asia/Bangkok");
            $curDateTime = date("Y-m-d H:i:s");
           
            $shipping = $objectManager->create("Libero\Onestepcheckout\Model\Shipping");
            $shipping->setData("shipping_code",$code);
            $shipping->setData("name_shipping_method",$name);
            $shipping->setData("title_shipping_method",$name); 
            $shipping->setData("status_shipping_method",$status);
            $shipping->setData("id_seller",$id_seller); 
            $shipping->setData("price",$price);
            $shipping->setData('created_at', $curDateTime);
            $shipping->setData('updated_at', $curDateTime);
            $shipping->save();
            
            $blockAdmin->setMessage("Change Delivery Successfully !","success");
            $resultRedirect->setPath('customer/admin/delivery/');
            return $resultRedirect;

        }catch (Exception $e){
           
        }
    }
}
