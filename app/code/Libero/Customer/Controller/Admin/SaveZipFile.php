<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Customer\Model\Group;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;

class SaveZipFile extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $helper = $objectManager->create('\Libero\Customer\Helper\Data');
        $postData = $this->getRequest()->getPostValue();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $base_url = $storeManager->getStore()->getBaseUrl();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        try {
            $dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
            $path = $dir->getRoot()."/pub/media/zip_image_provider";
            $fileUploaderFactory = $objectManager->get("\Magento\MediaStorage\Model\File\UploaderFactory");
            $uploader_1 = $fileUploaderFactory->create(['fileId' => 'zip']);
            $uploader_1->setAllowedExtensions(['zip']);
            $uploader_1->setAllowRenameFiles(false);
            $uploader_1->setFilesDispersion(false);
            $result_1 = $uploader_1->save($path);
            $excel_upload = $result_1["file"];
            $fileZip = $path."/".$excel_upload;
            //Extract now
            $destination_extract = $dir->getRoot()."/pub/media/temp_image_product/";
            //$destination_extract = $dir->getRoot()."/pub/media/tmp/";
            $zip = new \ZipArchive;
            $res = $zip->open($fileZip);
            if ($res === TRUE) {
                $zip->extractTo($destination_extract);
                $zip->close();
                $arrayResult["error"] = false;
                $arrayResult["message"] = "success";
            } else {
                $arrayResult["error"] = true;
                $arrayResult["message"] = "fail";
            }
            $arrayJson = array();
            if($arrayResult["error"] == false){
                $arrayJson["file"] = $excel_upload;
                $arrayJson["error"] = "false";
            }else{
                $arrayJson["file"] = "Không thể giải nén file hình";
                $arrayJson["error"] = "true";
            }
            $result = $jsonResultFactory->setData(json_encode($arrayJson));
            return $result;

        }catch (\Exception $e){
            $arrayJson = array();
            $arrayJson["file"] = "____LỖI ĐẨY HÌNH____ ".$e->getMessage();
            $arrayJson["error"] = "true";
            $result = $jsonResultFactory->setData(json_encode($arrayJson));
            return $result;
        }
    }
}
