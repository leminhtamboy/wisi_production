<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
class SaveRoleEdit extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $blockAdmin = $objectManager->create("\Libero\Customer\Block\Seller\Admin");
        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();

        try {
            $postData = $this->getRequest()->getPostValue();
            $id_account_acl = $postData["id_account_acl"];
            $role_name = $postData["rolename"];
            $id_seller = $postData["id_seller"];
            $all = $postData["all"];
            $stringResource = "";
            if(isset($postData["resource"])){
                if($all == 0){
                    $resource = $postData["resource"]; // array
                    foreach($resource as $_source){
                        $stringResource.=$_source."|";
                    }
                    $stringResource = substr($stringResource,0,strlen($stringResource) -1);
                }else{
                    $stringResource = "admin|sale|product|report|store|system";
                }       
                //Save  ACL
                
       
                $resource = $objectManager->get('\Magento\Framework\App\ResourceConnection');
                $connection = $resource->getConnection();

                $sql = "UPDATE libero_customer_seller_account_acl SET `acl_permmision` = '$stringResource', `acl_name` = '$role_name', `id_seller` = '$id_seller' where `id_account_acl` = '$id_account_acl';";
                $connection->query($sql);
                $redirectUrl = 'customer/admin/editrole/id/' . $id_account_acl;
                $resultRedirect->setPath($redirectUrl);
                return $resultRedirect;
            }else{
                $blockAdmin->setMessage("The resource not found. Please add role to your user !","error");
                $resultRedirect->setPath($redirectUrl);
                return $resultRedirect;
            }

        }catch (Exception $e){
            
        }
    }
}
