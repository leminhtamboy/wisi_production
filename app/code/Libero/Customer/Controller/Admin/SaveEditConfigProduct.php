<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Customer\Model\Group;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;

class SaveEditConfigProduct extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    function RemoveSign($str)
    {
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }
    public function xu_ly_dau_cuoi($string_temp)
    {
        $string_temp = substr($string_temp,0,strlen($string_temp) - 1);
        return $string_temp;
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $helper = $objectManager->create('\Libero\Customer\Helper\Data');
        $postData = $this->getRequest()->getPostValue();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $base_url = $storeManager->getStore()->getBaseUrl();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        try {
            //$this->messageManager->addSuccess("Add Configruable Product ".$name." Successfully.");
            //$resultRedirect->setPath('customer/admin/productlisting');
            //return $resultRedirect;
            //die();
            //return ;
            $product_id = $postData["product_id"];
            $attribute_string = $postData["attribute_selected_string"];
            $attribute_string = $this->xu_ly_dau_cuoi($attribute_string);
            $attribute_array = explode("|",$attribute_string);
            $attributeValues_json = $postData["attribute_value"];
            $associatedProductString = $postData["variant_object"];
            $associatedProductString  = $this->xu_ly_dau_cuoi($associatedProductString);
            $associatedProductIds = explode("|",$associatedProductString);
            $name = $util->removeQuoteInString($postData["product_name"]);
            $sku = $util->removeQuoteInString($postData["sku"]);
            $price = $helper->removeDecimalSymbol($postData["price"]);
            $base_price = $postData["base-price"];
            $VAT = $postData["tax_class_id"];
            $price = $base_price +  ($base_price * ($VAT/100));
            $price = round($price,0);
            $qty = 0;
            if(isset($_POST["qty"])){
                $qty = $helper->removeDecimalSymbol($postData["qty"]);
            }
            $isInStock = $postData["is_stock"];
            $tax_class = 0;
            $status = 1;
            $isSimle = true;
            if(isset($_POST["configurable-matrix-variant-id"])){
                $isSimle = false;
            }
            $weight = 0;
            if(isset($postData["weight"])){
                $weight = $helper->removeDecimalSymbol($postData["weight"]);
            }
            $description = $postData["description"];
            $short_description = $postData["short_description"];
            $id_seller = $postData["id_seller"];
            $array_category = explode("|",$postData["id_category"]);
            $last_category_id = $postData["last_category_id"];
            $last_category_name = $postData["last_category_name"];
            $special_price = $helper->removeDecimalSymbol($postData["special_price"]);
            if($special_price == 0){
                $special_price = "";
            }
            //$special_to_date = $postData["special_to_date"];
            //$special_from_date = $postData["special_from_date"];
            //$id_shipping = $postData["shipping_methos"];
            //Quick Detail
            $array_quickdetail_title = array();
            if(isset($postData["quick_detail_title"])){
                $array_quickdetail_title = $postData["quick_detail_title"];
            }
            if(isset($postData["quick_detail_content"])){
                $quick_detail_content = $postData["quick_detail_content"];
            }
            $supply_ability_amount = $postData["supply_ability_amount"];
            $supply_ability_type = $postData["supply_ability_type"];
            $supply_ability_date = $postData["supply_ability_date"];
            $min_qty = $postData["min_qty"];
            $free_shipment = $postData["free_shipment"];
            if($free_shipment == 1){
                $weight = 0;
            }
            //Processing to Json Array
            $data_diff = array();
            $q = 0 ;
            $length_quick_detail = count($array_quickdetail_title);
            $array_json_quick_detail = array();
            $array_json_quick_detail["data"] = array();
            $array_json_quick_detail["is_have"] = "false";
            $array_content_quick_detail = array();
            for($q = 0 ; $q < $length_quick_detail ; $q++){
                $array_child  = array();
                $array_child["title"]  = $array_quickdetail_title[$q];
                $array_child["content"]  = $quick_detail_content[$q];
                $array_content_quick_detail[] = $array_child;
            }
            if(count($array_content_quick_detail) > 0){
                $array_json_quick_detail["data"] = $array_content_quick_detail;
                $array_json_quick_detail["is_have"] = "true";
            }
            $json_quick_detail = json_encode($array_json_quick_detail,JSON_UNESCAPED_UNICODE);

            $array_supply_ability = array();
            $array_supply_ability["supply_ability_amount"] = $supply_ability_amount;
            $array_supply_ability["supply_ability_type"] = $supply_ability_type;
            $array_supply_ability["supply_ability_date"] = $supply_ability_date;
            $json_supply_ability = json_encode($array_supply_ability,JSON_UNESCAPED_UNICODE);
            //End Quick Detail

            $array_path_img = array();
            if(isset($postData["image_all"])){
                $array_path_img = $postData["image_all"];
            }
            //die;
            if($postData["url_key"] == ""){
                $url_key = $this->RemoveSign($name)."-".$id_seller;
            }else{
                $url_key = $postData["url_key"];
            }
            $meta_title = $postData["meta_title"];
            $product_meta_name = $this->RemoveSign($name);
            $meta_keywords = trim($postData["meta_keyword"]);
            $meta_description = $postData["meta_description"];
            $base_img = "";
            if(isset($postData["select-base-img"])){
                $base_img = $postData["select-base-img"];
            }
            $tier_price_qty = $helper->removeDecimalSymbol(explode("|",$postData["tier_price_qty"]));
            $tier_price_price = $helper->removeDecimalSymbol(explode("|",$postData["tier_price_price"]));
            //$customerGroup = 1;
            $i = 0;
            $arrayParentTier = array();
            foreach($tier_price_qty as $_tier){
                $tier_qty = $_tier;
                $tier_price = $tier_price_price[$i];
                $array_tier_product = array();
                $array_tier_product['website_id'] = 0;
                $array_tier_product['cust_group'] = Group::CUST_GROUP_ALL;
                $array_tier_product['price_qty'] = $tier_qty;
                $array_tier_product['price'] = $tier_price;
                $arrayParentTier[] = $array_tier_product;
                $i++;
            }
            $product = $objectManager->create("\Magento\Catalog\Model\Product")->load($product_id);
            $stockRegistry = $objectManager->create("\Magento\CatalogInventory\Api\StockRegistryInterface");
            $stockItem = $stockRegistry->getStockItem($product_id);

            $status_product = $product->getData("status");
            /********* Detect Different ******/

            $price_check = $helper->removeDecimalSymbol($helper->formartPrice($product->getPrice()));
            $special_price_check = $helper->removeDecimalSymbol($helper->formartPrice($product->getSpecialPrice()));
            $qty_check = $helper->removeDecimalSymbol($helper->formartPrice($stockItem->getData("qty")));
            /*if($sku != $product->getData("sku")){
                $data_diff["sku"] = $sku;
            }*/
            if($name != $product->getName()){
                $data_diff["product_name"] = $name;
                $status = 0;
            }else{
                $product->setName($name); // Name of Product
            }
            /*if($weight != $product->getWeight()){
                $data_diff["weight"] = $weight;
            }
            if($price != $price_check){
                $data_diff["price"] = $price;
            }
            if($special_price != $special_price_check && $special_price_check != 0){
                $data_diff["special_price"] = $special_price;
            }*/
            if($description != $product->getDescription()){
                //$description = str_replace("\"","\'",$description);
                //$data_diff["description"] = $description;
            }else{
                //$product->setDescription($description);
            }

            if($short_description != $product->getShortDescription()){
                //$data_diff["short_description"] = json_encode($short_description,JSON_UNESCAPED_UNICODE);
            }else{
                $product->setShortDescription($short_description);
            }

            if(count(array_diff($array_category,$product->getCategoryIds())) > 0){
                $data_diff["category_ids"] = $array_category;
                $array_name_cate = array();
                foreach($array_category as $_cate){
                    $category = $objectManager->create("\Magento\Catalog\Model\Category")->load($_cate);
                    $array_name_cate[] = $category->getName();
                }
                $data_diff["category_name"] = $array_name_cate;
                $status = 0;
            }else{
                $product->setCategoryIds($array_category);
            }

            if($last_category_id != $product->getLastCategoryId()){
                $data_diff["last_category_id"] = $last_category_id;
                $data_diff["last_category_name"] = $last_category_name;
                $status = 0;
            }else{
                $product->setLastCategoryId($last_category_id);
                $product->setLastCategoryName($last_category_name);
            }
            /*if($url_key != $product->getData("url_key"))
            {
                $data_diff["url_key"] = $url_key;
            }
            if($meta_title != $product->getData("meta_title"))
            {
                $data_diff["meta_title"] = $meta_title;
            }
            if($meta_keywords != $product->getData("meta_keyword"))
            {
                $data_diff["meta_keyword"] = $meta_keywords;
            }
            if($meta_description != $product->getData("meta_description"))
            {
                $data_diff["meta_description"] = $meta_description;
            }*/
            $json_diff = json_encode($data_diff,JSON_UNESCAPED_UNICODE);
            $data_provider = $helper->getProviderDetail($id_seller);
            $data_save = array();
            $data_save["sku"] = $sku;
            $data_save["name"] = $name;
            $data_save["weight"] = $weight;
            $data_save["visibility"] = 4;
            $data_save["type_id"] = "simple";
            $data_save["price"] = $price;
            $data_save["special_price"] = $special_price;
            $data_save["free_shipment"] = $free_shipment;
            //$data_save["description"] = $description;
            $data_save["category_ids"] = $array_category;
            //$data_save["short_description"] = $short_description;
            $data_save["last_category_id"] = $last_category_id;
            $data_save["last_category_name"] = $last_category_name;
            $data_save["url_key"] = $url_key;
            $data_save["meta_title"] = $meta_title;
            $data_save["meta_keyword"] = $meta_keywords;
            $data_save["meta_description"] = $meta_description;
            $data_save["product_meta_name"] = $product_meta_name;
            $data_save["stock_items"] = array(
                "is_in_stock" => $isInStock,
                "qty" => $qty
            );
            $data_save["tier_price"] = $arrayParentTier;
            $data_save["id_seller"] = $id_seller;
            $jon_data_save = json_encode($data_save,JSON_UNESCAPED_UNICODE);
            $product_sku = $sku;
            $product_name = $name;
            $provider_id = $id_seller;
            $provider_name = $data_provider->getData('store_name');
            date_default_timezone_set("Asia/Ho_Chi_Minh");
            $time_update = date("Y-m-d H:i:s");
            $has_log = false;
            if(count($data_diff) > 0){
                $sql_check_has_log = "SELECT * FROM `libero_temp_save_product` WHERE `product_sku` = '$sku' and `status` = '0'";
                $data_check_has_log = $connection->fetchAll($sql_check_has_log);
                if(count($data_check_has_log) == 0 ){
                    $sql_insert = "INSERT INTO `libero_temp_save_product` (`id`, `product_id`, `product_sku`, `product_name`, `data_save`,`data_diff`,`provider_id`, `provider_name`, `time_update`) VALUES 
                    (NULL, '$product_id', '$product_sku', '$product_name', '$jon_data_save','$json_diff','$provider_id', '$provider_name', '$time_update')";
                    $connection->query($sql_insert);
                    $product->setData("message_deny","");
                }else{
                    $has_log = true;
                }
            }
            /********* End Different **********/
            
            /********* Xử lý product bị từ chối khi edit xong là phải xóa message nếu như có chỉnh sửa ***********/
            /********* Kết Thúc xử lý product bị từ chối ***********/
            $product->setSku($sku); // Set your sku here
            $product->setAttributeSetId(4); // Attribute set id
            if($isSimle){
                $length = $postData["length"];
                $width = $postData["width"];
                $height = $postData["height"];
                $product->setTypeId('simple'); // type of product (simple/virtual/downloadable/configurable)
                $stockItem->setData("is_in_stock",$isInStock);
                $stockItem->setData("qty",$qty);
                $stockItem->setData("manage_stock",1);
                $stockItem->setData("min_sale_qty",$min_qty);
                $stockItem->setData("use_config_notify_stock_qty",1);
                $stockItem->save();
                $product->setData("length",$length);
                $product->setData("height",$height);
                $product->setData("width",$width);
            }else{
                $product->setTypeId('configurable'); // type of product (simple/virtual/downloadable/configurable)
            }
            $product->setStatus($status); // Status on product enabled/ disabled 1/0
            $product->setWeight($weight); // weight of product
            $product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
            $product->setTaxClassId($tax_class); // Tax class id
            $product->setPrice($price); // price of product
            $product->setSpecialPrice($special_price);
            $product->setFreeShipment($free_shipment); // free_shipment enabled/ disabled 1/0
            $product->setData("supply_ability",$json_supply_ability);
            $product->setData("quick_detail",$json_quick_detail);
            $product->setDescription($description);
            $product->setShortDescription($short_description);
            $product->setData("url_key",$url_key);
            $product->setData("meta_title",$meta_title);
            $product->setMetaKeyword($meta_keywords);
            $product->setData("meta_description",$meta_description);
            $product->setData("product_meta_name",$product_meta_name);
            $product->setData("id_seller",$id_seller);
            $product->setData("Vat",$VAT);
            $product->setWebsiteIds(array(1));
            $i = 0;
            $dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
            $imageProcessor = $objectManager->create('\Magento\Catalog\Model\Product\Gallery\Processor');
            /*$arrayImages = $product->getMediaGalleryImages();
            foreach($arrayImages as $_child){
                $imageProcessor->removeImage($product, $_child->getFile());
            }*/
            $image_data = $product->getData("image_data");
            $array_image_data = json_decode($image_data,true);
            $array_new_image_data = array();
            $array_new_image_data["main"][$product_id]["base_img"] = $base_img;
            if($base_img != ""){
                //$product->addImageToMediaGallery($dir->getPath('media')."/".$base_img, array('image', 'small_image', 'thumbnail'), false, false);
            }
            $array_sub_main_img = array();
            foreach($array_path_img as $_img_path) {
                //$product->addImageToMediaGallery($dir->getPath('media')."/".$_img_path, array(), false, false);
                if($_img_path != ""){
                    $array_sub_main_img[] = $_img_path;
                }
            }
            $array_new_image_data["main"][$product_id]["sub_img"] = $array_sub_main_img;
            //Update Image data
            //End update image data
            $product->setTierPrice($arrayParentTier);
            $productRepository = $objectManager->get("Magento\Catalog\Api\ProductRepositoryInterface");
            if($isSimle == false){
                $image_variant_id = array();
                if(isset($postData["image_variant_id"])){
                    $image_variant_id = $postData["image_variant_id"];
                }
                $image_variant_array = array();
                if(isset($postData["image_variant_array"])){
                    $image_variant_array = $postData["image_variant_array"];
                }
                $length_of_variant_id = count($image_variant_id);
                $length_of_variant_array = count($image_variant_array);
                $img_index = 0;
                $array_new_variant = array();
                for($img_index = 0 ; $img_index < $length_of_variant_id ; $img_index++){
                    $id_variant = $image_variant_id[$img_index]; 
                    $variant = $objectManager->create("\Magento\Catalog\Model\Product")->load($id_variant);
                    $variant_img = $image_variant_array[$img_index];
                    //$variant->addImageToMediaGallery($dir->getPath('media')."/".$variant_img, array('image', 'small_image', 'thumbnail'), false, false);
                    //$variant->save();
                    $array_new_variant[$id_variant]["base_img"] = $variant_img;
                }
                $array_new_image_data["variant"] = $array_new_variant;
                $variant_id_array = $postData["configurable-matrix-variant-id"];
                $variant_qty_array = $postData["configurable-matrix-qty"];
                $variant_weight_array = $postData["configurable-matrix-weight"];

                $variant_length_array = $postData["configurable-matrix-length"];
                $variant_height_array = $postData["configurable-matrix-height"];
                $variant_width_array = $postData["configurable-matrix-width"];

                $variant_price_array = $postData["configurable-matrix-price"];
                $variant_special_price_array  =  $postData["configurable-matrix-special-price"];
                $variant_stock_array = $postData['configurable-matrix-variant-stock'];
                $variant_index = 0;
                $length_of_variant = count($variant_id_array);
                for($variant_index=0; $variant_index < $length_of_variant ; $variant_index++){
                    $_variant_id = $variant_id_array[$variant_index];
                    $variant = $objectManager->create("\Magento\Catalog\Model\Product")->load($_variant_id);
                    $stockRegistry = $objectManager->create("\Magento\CatalogInventory\Api\StockRegistryInterface");
                    $stockItem = $stockRegistry->getStockItem($_variant_id );
                    $variant_qty = $helper->removeDecimalSymbol($variant_qty_array[$variant_index]);
                    $variant_weight = $helper->removeDecimalSymbol($variant_weight_array[$variant_index]);

                    $variant_length = $helper->removeDecimalSymbol($variant_length_array[$variant_index]);
                    $variant_height = $helper->removeDecimalSymbol($variant_height_array[$variant_index]);
                    $variant_width = $helper->removeDecimalSymbol($variant_width_array[$variant_index]);

                    $variant_price = $helper->removeDecimalSymbol($variant_price_array[$variant_index]);
                    $variant_sepecial_price =  $helper->removeDecimalSymbol($variant_special_price_array[$variant_index]);
                    $is_in_stock = $variant_stock_array[$variant_index];
                    if($free_shipment == 1){
                        $variant_weight = 0;
                    }
                    if($variant_sepecial_price == 0){
                        $variant_sepecial_price = "";
                    }
                    $variant->setWeight($variant_weight);
                    $variant->setPrice($variant_price);
                    $variant->setSpecialPrice($variant_sepecial_price);
                    $variant->setTaxClassId($tax_class);
                    $variant->setData("length",$variant_length);
                    $variant->setData("height",$variant_height);
                    $variant->setData("width",$variant_width);
                    $variant->setCategoryIds($array_category);
                    $variant->setData("supply_ability",$json_supply_ability);
                    $variant->setVisibility(1); 
                    $variant->setTierPrice($arrayParentTier);
                    $variant->setData("quick_detail",$json_quick_detail);
                    $variant->setData("id_seller",$id_seller);
                    $variant->setData("Vat",$VAT);
                    $variant->setLastCategoryId($last_category_id);
                    $variant->setLastCategoryName($last_category_name);
                    $variant->setWebsiteIds(array(1));
                    $stockItem->setData("is_in_stock",$is_in_stock);
                    $stockItem->setData("qty",$variant_qty);
                    $stockItem->setData("manage_stock",1);
                    $stockItem->setData("min_sale_qty",$min_qty);
                    $stockItem->setData("use_config_notify_stock_qty",1);
                    $stockItem->save();
                    $variant->save();
                }
                $extensionConfigurableAttributes = $product->getExtensionAttributes();
                $extensionConfigurableAttributes->setConfigurableProductLinks($variant_id_array);
                $product->setExtensionAttributes($extensionConfigurableAttributes);
                //$product->setTypeId("configurable");
                    //$product->save();
            }
            $json_image_data = json_encode($array_new_image_data,JSON_UNESCAPED_UNICODE);
            $product->setData("image_data",$json_image_data);
            //Add to cron job image
                //Put save image to cron job
                $type_cron = "CRON_PRODUCT_IMAGE";
                $cron_code = "CRON_EX15G80_EDIT_PRODUCT_".$product_id."_UPDATE";
                $content_cron_excute = $json_image_data;
                date_default_timezone_set("Asia/Ho_Chi_Minh");
                $time_update = date("Y-m-d H:i:s");
                $created_at = $time_update;
                $status = 0; // Processsing;
                $etc = "";
                $sql_insert_cron = "INSERT INTO `libero_cron_execute_background` (`id_cron`, `type_cron`, `cron_code`, `content_excute`, `created_at`, `status`, `etc`) 
                VALUES (NULL, '$type_cron', '$cron_code', '$content_cron_excute', '$created_at', '0', '$etc');";
                $connection->query($sql_insert_cron);
            //End add to cron job image
            $product->save();
            if($status == 1){
                $this->messageManager->addSuccess("Edit Product %1 Successfully.",$name);
            }else{
                if($has_log == true){
                    $this->messageManager->addNotice("Sorry. Last time you edited data of product %1. Please waiting 48 hours to WISI approve your last change.",$name);
                }else{
                    $this->messageManager->addSuccess("You just edit data of product ".$name.". Please waiting for WISI approve your change.");
                }
            }
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;

        }catch (\Exception $e){
            $this->messageManager->addError($e->getMessage());
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            //$resultRedirect->setPath('*/*');
            return $resultRedirect;
        }
    }
}
