<?php
namespace Libero\Customer\Controller\Admin;

class OrderReleaseInvoice extends \Magento\Framework\App\Action\Action
{
    protected $_resource;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->_resource = $resourceConnection;
        parent::__construct($context);
    }

    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $orderDetail = $objectManager->get("\Libero\Customer\Block\Seller\Admin\OrderDetail");

        $requestHttp = $objectManager->get("\Magento\Framework\App\Request\Http");
        $order_ids = explode(",",$requestHttp->getParam("order_id"));
        foreach($order_ids as $order_id){
            if(!$orderDetail->isReleasedInvoiceByOrderId($order_id) && !$orderDetail->isReleasedShipmentByOrderId($order_id)) {
                $this->releaseInvoice($order_id);
            }
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('customer/admin/invoice');
        return $resultRedirect;
    }

    public function releaseInvoice($order_id){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        try {
            /** @var \Magento\Sales\Model\Order $order */
            $order = $objectManager->create('Libero\Customer\Model\Order')->load($order_id);
            
            if (!$order->getId()) {
                throw new \Magento\Framework\Exception\LocalizedException(__('The order no longer exists.'));
            }

            $seller_id = $order->getSellerId();

            $data_sale_order = $order->getData();
            $data_sale_order['total_qty'] = "";
            $data_sale_order['seller_id'] = $seller_id;
            $data_sale_order['order_id'] = $order_id;
            $data_sale_order['comment'] = "";
            $data_sale_orderArr = array();
            foreach($data_sale_order as $key => $value){
                array_push($data_sale_orderArr,$key);
            }
            $resource = $objectManager->get('\Magento\Framework\App\ResourceConnection');
            $connection = $resource->getConnection();


            /*************** seller_sales_invoice ***************/
            $into = "";
            $values = "";
            $sql = "";

            $tableName1 = $resource->getTableName("seller_sales_invoice");
            $tableFieldTotal1 = $connection->describeTable($tableName1);
            $tableFieldArr1 = array();
            foreach($tableFieldTotal1 as $key => $value){
                array_push($tableFieldArr1,$key);
            }
        
            foreach($data_sale_order as $key => $value){
                if($key == "entity_id"){continue;};
                if(in_array($key, $tableFieldArr1)){
                    $into .= "`$key`,";
                    $values .= "'$value',";
                }
            }

            $into.="|";
            $into = str_replace(",|","",$into);

            $values.="|";
            $values = str_replace(",|","",$values);

            $sql = "INSERT INTO `$tableName1` ($into) VALUES ($values)";
       
            $connection->query($sql);

            /*************** seller_sales_invoice_comment ***************/
            $into = "";
            $values = "";
            $sql = "";

            $tableName2 = $resource->getTableName("seller_sales_invoice_comment");
            $tableFieldTotal2 = $connection->describeTable($tableName2);
            $tableFieldArr2 = array();
            foreach($tableFieldTotal2 as $key => $value){
                array_push($tableFieldArr2,$key);
            }

            foreach($data_sale_order as $key => $value){
                if($key == "entity_id"){continue;};
                if(in_array($key, $tableFieldArr2)){
                    $into .= "`$key`,";
                    $values .= "'$value',";
                }
            }
            
            $into.="|";
            $into = str_replace(",|","",$into);

            $values.="|";
            $values = str_replace(",|","",$values);

            $sql = "INSERT INTO `$tableName2` ($into) VALUES ($values)";
   
            $connection->query($sql);

            /*************** seller_sales_invoice_grid ***************/
            $into = "";
            $values = "";
            $sql = "";

            $orderDetailBlock = $objectManager->create("\Libero\Customer\Block\Seller\Admin\OrderDetail");
            $orderDetail = $orderDetailBlock->getOrderDetail($order_id);
         
            $data_sale_order['order_increment_id'] = $orderDetail[0]['increment_id'];
            $data_sale_order['customer_name'] = $orderDetail[0]['customer_name'];
            $data_sale_order['order_created_at'] = $orderDetail[0]['created_at'];
            $data_sale_order['billing_name'] = $orderDetail[0]['billing_name'];

            $data_sale_order['billing_address'] = $orderDetail[0]['billing_address'];
            $data_sale_order['shipping_address'] = $orderDetail[0]['shipping_address'];
            $data_sale_order['shipping_information'] = $orderDetail[0]['shipping_information'];
            $data_sale_order['shipping_and_handling'] = $orderDetail[0]['shipping_and_handling'];
            $data_sale_order['payment_method'] = $orderDetail[0]['payment_method'];

            $tableName3 = $resource->getTableName("seller_sales_invoice_grid");
            $tableFieldTotal3 = $connection->describeTable($tableName3);
            $tableFieldArr3 = array();
            foreach($tableFieldTotal3 as $key => $value){
                array_push($tableFieldArr3,$key);
            }

            foreach($data_sale_order as $key => $value){
                if($key == "entity_id"){continue;};
                if(in_array($key, $tableFieldArr3)){
                    $into .= "`$key`,";
                    $values .= "'$value',";
                }
            }

            $into.="|";
            $into = str_replace(",|","",$into);

            $values.="|";
            $values = str_replace(",|","",$values);
       
            $sql = "INSERT INTO `$tableName3` ($into) VALUES ($values)";

            $connection->query($sql);

            /*************** seller_sales_invoice_item ***************/
            $tableName4 = $resource->getTableName("seller_sales_invoice_item");
            $tableFieldTotal4 = $connection->describeTable($tableName4);
            $tableFieldArr4 = array();
            foreach($tableFieldTotal4 as $key => $value){
                array_push($tableFieldArr4,$key);
            }

            $keyInsertDb = array("base_price", "base_row_total", "row_total", "price_incl_tax", "base_price_incl_tax", "qty", "base_cost", "price", "base_row_total_incl_tax", "row_total_incl_tax", "product_id", "order_item_id", "additional_data", "description", "sku", "name", "weee_tax_applied", "weee_tax_applied_amount", "weee_tax_applied_row_amount", "weee_tax_disposition", "weee_tax_row_disposition", "base_weee_tax_applied_amount", "base_weee_tax_applied_row_amnt", "base_weee_tax_disposition", "base_weee_tax_row_disposition");
            
            foreach ($orderDetail as $order) {
                $into = "";
                $values = "";
                $sql = "";

                foreach($data_sale_order as $key2 => $value2){
                    if($key2 == "entity_id"){continue;};
                    if(in_array($key2, $tableFieldArr4)){
                        $into .= "`$key2`,";
                        $values .= "'$value2',";
                    }
                }
                foreach($keyInsertDb as $keyInsert){
                    $into .= "`$keyInsert`,";
                    $values .= "'$order[$keyInsert]',";
                }
                

                $into.="|";
                $into = str_replace(",|","",$into);

                $values.="|";
                $values = str_replace(",|","",$values);

                $sql = "INSERT INTO `$tableName4` ($into) VALUES ($values)";
                
                $connection->query($sql);
            }
            
            $sql_update_status_order = "UPDATE seller_sales_order SET `status` = 'processing' WHERE entity_id = '$order_id';";
            $connection->query($sql_update_status_order);

            $sql_update_status_order_grid = "UPDATE seller_sales_order_grid SET `status` = 'processing' WHERE order_id = '$order_id';";
            $connection->query($sql_update_status_order_grid);

        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError(__('We can\'t save the invoice right now.'));
            $objectManager->get('Psr\Log\LoggerInterface')->critical($e);
        }
    }

    public function getOrderDetail($order_id,$id_seller)
    {
        $connection = $this->_resource->getConnection();
        $sql = "
            SELECT
                *, ssoi.qty_ordered AS qty, ssoi.item_id AS order_item_id
            FROM
                seller_sales_order_grid AS ssog
                    JOIN
                seller_sales_order AS sso
                    JOIN
                seller_sales_order_item AS ssoi
                    ON ssog.order_id = ssoi.order_id
                    AND sso.entity_id = ssoi.order_id
            WHERE
                ssog.seller_id = '$id_seller'
                AND ssog.order_id = '$order_id'
            ;
            ";
        $result = $connection->fetchAll($sql);
        return $result;
    }
}
