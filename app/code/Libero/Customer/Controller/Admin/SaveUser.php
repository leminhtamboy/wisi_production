<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
class SaveUser extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();

        try {

            $postData = $this->getRequest()->getPostValue();
            $first_name = $postData["firstname"];
            $last_name = $postData["lastname"];
            $email = $postData["email"];
            $password = $postData["password"];
            $is_seller = $postData["id_seller"];
            $role = $postData["role_list"];
            //Create Customer
            $storeManager = $this->_objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $websiteId = $storeManager->getWebsite()->getWebsiteId();
            $customerFactory = $this->_objectManager->get('\Magento\Customer\Model\CustomerFactory');
            $customer = $customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->setEmail($email);
            $customer->setFirstname($first_name);
            $customer->setLastname($last_name);
            $customer->setPassword($password);
            $customer->setGroupId(2);
            $customer->save();       
            //Save account ACL
            $modelAccountAcl = $this->_objectManager->create("\Libero\Customer\Model\Account");
            $modelAccountAcl->setData("id_customer",$customer->getId());
            $modelAccountAcl->setData("id_seller",$is_seller);
            $modelAccountAcl->setData("id_role",$role);
            $modelAccountAcl->save();
            $resultRedirect->setPath('customer/admin/accountlisting');
            return $resultRedirect;

        }catch (Exception $e){
            
        }
    }
}
