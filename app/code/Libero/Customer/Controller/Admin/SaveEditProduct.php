<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Customer\Model\Group;
class SaveEditProduct extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    function RemoveSign($str)
    {
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $helper = $objectManager->create('\Libero\Customer\Helper\Data');
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        try {
            $postData = $this->getRequest()->getPostValue();
            $name = $util->removeQuoteInString($postData["product_name"]);
            $sku = $util->removeQuoteInString($postData["sku"]);
            $price = $helper->removeDecimalSymbol($postData["price"]);
            $qty = $helper->removeDecimalSymbol($postData["qty"]);
            $isInStock = $postData["is_stock"];
            $status = 1;
            $weight = $postData["weight"];
            $description = $postData["description"];
            $short_description = $postData["short_description"];
            $id_seller = $postData["id_seller"];
            $last_category_id = $postData["last_category_id"];
            $special_price = $helper->removeDecimalSymbol($postData["special_price"]);
            //$support_shipment_amount = $helper->removeDecimalSymbol($postData["support_shipment_amount"]);
            $support_shipment_amount = "";
            $id_product = $postData["id_product"];
            //$id_shipping = $postData["shipping_methos"];
            $array_category = explode("|",$postData["id_category"]);
            $array_path_img = array();
            if(isset($postData["image_all"])) {
                $array_path_img = $postData["image_all"];
            }
            if($postData["url_key"] == ""){
                $url_key = $this->RemoveSign($name)."-".$id_seller;
            }else{
                $url_key = $postData["url_key"];
            }
            $meta_title = $postData["meta_title"];
            $product_meta_name = $this->RemoveSign($name);
            $meta_keywords = $postData["meta_keyword"];
            $meta_description = $postData["meta_description"];
            $base_img = $postData["select-base-img"];
            $tax_class_id = $postData["tax_class_id"];
            $length = $postData["length"];
            $width = $postData["width"];
            $height = $postData["height"];
            $free_shipment = $postData['free_shipment'];
            if($free_shipment == 1){
                $weight = 0;
            }
            $array_quickdetail_title = $postData["quick_detail_title"];
            $quick_detail_content = $postData["quick_detail_content"];
            $supply_ability_amount = $postData["supply_ability_amount"];
            $supply_ability_type = $postData["supply_ability_type"];
            $supply_ability_date = $postData["supply_ability_date"];
            //Processing to Json Array
            $q = 0 ;
            $length_quick_detail = count($array_quickdetail_title);
            $array_json_quick_detail = array();
            $array_json_quick_detail["data"] = array();
            $array_json_quick_detail["is_have"] = "false";
            $array_content_quick_detail = array();
            for($q = 0 ; $q < $length_quick_detail ; $q++){
                $array_child  = array();
                $array_child["title"]  = $array_quickdetail_title[$q];
                $array_child["content"]  = $quick_detail_content[$q];
                $array_content_quick_detail[] = $array_child;
            }
            if(count($array_content_quick_detail) > 0){
                $array_json_quick_detail["data"] = $array_content_quick_detail;
                $array_json_quick_detail["is_have"] = "true";
            }
            $json_quick_detail = json_encode($array_json_quick_detail);

            $array_supply_ability = array();
            $array_supply_ability["supply_ability_amount"] = $supply_ability_amount;
            $array_supply_ability["supply_ability_type"] = $supply_ability_type;
            $array_supply_ability["supply_ability_date"] = $supply_ability_date;
            $json_supply_ability = json_encode($array_supply_ability);

            $tier_price_qty = $helper->removeDecimalSymbol(explode("|",$postData["tier_price_qty"]));
            $tier_price_price = $helper->removeDecimalSymbol(explode("|",$postData["tier_price_price"]));
            $i = 0;
            $arrayParentTier = array();
            foreach($tier_price_qty as $_tier){
                $tier_qty = $_tier;
                $tier_price = $tier_price_price[$i];
                $array_tier_product = array();
                $array_tier_product['website_id'] = 0;
                $array_tier_product['cust_group'] = Group::CUST_GROUP_ALL;
                $array_tier_product['price_qty'] = $tier_qty;
                $array_tier_product['price'] = $tier_price;
                $arrayParentTier[] = $array_tier_product;
                $i++;
            }

            $product = $objectManager->create("\Magento\Catalog\Model\Product")->load($id_product);
            $stockRegistry = $objectManager->create("\Magento\CatalogInventory\Api\StockRegistryInterface");
            $stockItem = $stockRegistry->getStockItem($id_product);
            $product->setSku($sku); // Set your sku here
            $product->setName($name); // Name of Product
            $product->setAttributeSetId(4); // Attribute set id
            $product->setStatus($status); // Status on product enabled/ disabled 1/0
            $product->setWeight($weight); // weight of product
            $product->setData("length",$length);
            $product->setData("height",$height);
            $product->setData("width",$width);
            $product->setVisibility(1); // visibilty of product ( 2 catalog / 3 search /  4 catalog, search /  1 Not visible individually)
            $product->setTaxClassId($tax_class_id); // Tax class id
            $product->setTypeId('simple'); // type of product (simple/virtual/downloadable/configurable)
            $product->setPrice($price); // price of product
            $product->setSpecialPrice($special_price);
            $product->setFreeShipment($free_shipment); // free_shipment enabled/ disabled 1/0
            $product->setDescription($description);
            $product->setCategoryIds($array_category);
            $product->setShortDescription($short_description);
            $product->setLastCategoryId($last_category_id);
            $product->setData("url_key",$url_key);
            $product->setData("meta_title",$meta_title);
            $product->setData("meta_keyword",$meta_keywords);
            $product->setData("meta_description",$meta_description);
            $product->setData("product_meta_name",$product_meta_name);
            $product->setData("supply_ability",$json_supply_ability);
            $product->setData("quick_detail",$json_quick_detail);
            $stockItem->setData("is_in_stock",$isInStock);
            $stockItem->setData("qty",$qty);
            $stockItem->setData("manage_stock",1);
            $stockItem->setData("use_config_notify_stock_qty",1);
            $product->setTierPrice($arrayParentTier);
            $product->setData("id_seller",$id_seller);
            $stockItem->save();
            $existingMediaGalleryEntries = $product->getMediaGalleryEntries();
            $base_img_temp = "";
            $array_img_path_temp = array();
            $diff_img_path = array();
            if(count($array_path_img) > 0){
                $i = 0;
                $arrayImages = $product->getMediaGalleryImages();
                $imageProcessor = $objectManager->create('\Magento\Catalog\Model\Product\Gallery\Processor');
                foreach($arrayImages as $_child){
                    //var_dump($array_path_img);
                    $diff_img_path[] = $_child->getData("path");
                    $imageProcessor->removeImage($product, $_child->getFile());
                }
                foreach($array_path_img as $_img_path) {
                    if($base_img == $_img_path){
                        $product->addImageToMediaGallery($_img_path, array('image', 'small_image', 'thumbnail'), false, false);
                        $base_img_temp = $_img_path;
                    }else{
                        $product->addImageToMediaGallery($_img_path, array(), false, false);
                        $array_img_path_temp[] = $_img_path;
                    }
                    $i++;
                }
            }
            $data_diff = array();
            $price_check = $helper->removeDecimalSymbol($helper->formartPrice($product->getPrice()));
            $special_price_check = $helper->removeDecimalSymbol($helper->formartPrice($product->getSpecialPrice()));
            $qty_check = $helper->removeDecimalSymbol($helper->formartPrice($stockItem->getData("qty")));
            if($sku != $product->getData("sku")){
                $data_diff["sku"] = $sku;
            }
            if($name != $product->getName()){
                $data_diff["product_name"] = $name;
            }
            if($weight != $product->getWeight()){
                $data_diff["weight"] = $weight;
            }
            if($price != $price_check){
                $data_diff["price"] = $price;
            }
            if($special_price != $special_price_check && $special_price_check != 0){
                $data_diff["special_price"] = $special_price;
            }
            if($description != $product->getDescription()){
                //$data_diff["description"] = $description;
            }
            if($short_description != $product->getShortDescription()){
                //$data_diff["short_description"] = $description;
            }
            if(count(array_diff($array_category,$product->getCategoryIds())) > 0){
                $data_diff["category_ids"] = $array_category;
                $array_name_cate = array();
                foreach($array_category as $_cate){
                    $category = $objectManager->create("\Magento\Catalog\Model\Category")->load($_cate);
                    $array_name_cate[] = $category->getName();
                }
                $data_diff["category_name"] = $array_name_cate;
            }
            if($last_category_id != $product->getLastCategoryId())
            {
                $data_diff["last_category_id"] = $last_category_id;
            }
            if($url_key != $product->getData("url_key"))
            {
                $data_diff["url_key"] = $url_key;
            }
            if($meta_title != $product->getData("meta_title"))
            {
                $data_diff["meta_title"] = $meta_title;
            }
            if($meta_keywords != $product->getData("meta_keyword"))
            {
                $data_diff["meta_keyword"] = $meta_keywords;
            }
            if($meta_description != $product->getData("meta_description"))
            {
                $data_diff["meta_description"] = $meta_description;
            }
            /*echo  $product_meta_name;
            echo  $product->getData("product_meta_name");
            if($product_meta_name != $product->getData("product_meta_name"))
            {
                $data_diff["product_meta_name"] = $product_meta_name;
            }*/
            if($qty !=  $qty_check)
            {
                $data_diff["qty"] = $helper->removeDecimalSymbol($qty);
            }
            if($isInStock !=  $stockItem->getData("is_in_stock"))
            {
                $data_diff["is_in_stock"] = $isInStock;
            }
            $tier_price_product = $product->getTierPrice();
            if(count($arrayParentTier) != count($tier_price_product) && count($tier_price_product) > 0)
            {
                $data_diff["tier_price"] = $arrayParentTier;
            }
            if(count(array_diff($diff_img_path,$array_path_img)) > 0){
                $data_diff["image"] = "true";
            }
            $json_diff = json_encode($data_diff,JSON_UNESCAPED_UNICODE);
            $data_provider = $helper->getProviderDetail($id_seller);
            $data_save = array();
            $data_save["sku"] = $sku;
            $data_save["name"] = $name;
            $data_save["weight"] = $weight;
            $data_save["visibility"] = 4;
            $data_save["type_id"] = "simple";
            $data_save["price"] = $price;
            $data_save["special_price"] = $special_price;
            $data_save["free_shipment"] = $free_shipment;
            //$data_save["description"] = $description;
            $data_save["category_ids"] = $array_category;
            //$data_save["short_description"] = $short_description;
            $data_save["last_category_id"] = $last_category_id;
            $data_save["url_key"] = $url_key;
            $data_save["meta_title"] = $meta_title;
            $data_save["meta_keyword"] = $meta_keywords;
            $data_save["meta_description"] = $meta_description;
            $data_save["product_meta_name"] = $product_meta_name;
            $data_save["stock_items"] = array(
                "is_in_stock" => $isInStock,
                "qty" => $qty
            );
            $data_save["tier_price"] = $arrayParentTier;
            $data_save["id_seller"] = $id_seller;
            $data_save["img_galery"] = array (
                "base_img" => $base_img_temp,
                "addition_img" => $array_img_path_temp
            );
            $jon_data_save = json_encode($data_save,JSON_UNESCAPED_UNICODE);
            $product_id = $id_product;
            $product_sku = $sku;
            $product_name = $name;
            $provider_id = $id_seller;
            $provider_name = $data_provider->getData('store_name');
            date_default_timezone_set("Asia/Ho_Chi_Minh");
            $time_update = date("Y-m-d H:i:s");
            $sql_insert = "INSERT INTO `libero_temp_save_product` (`id`, `product_id`, `product_sku`, `product_name`, `data_save`,`data_diff`,`provider_id`, `provider_name`, `time_update`) VALUES 
            (NULL, '$product_id', '$product_sku', '$product_name', '$jon_data_save','$json_diff','$provider_id', '$provider_name', '$time_update')";
            $connection->query($sql_insert);
            //$product->save();
            $arrayImages = $product->getMediaGalleryImages();
            $imageProcessor = $objectManager->create('\Magento\Catalog\Model\Product\Gallery\Processor');
            foreach($arrayImages as $_child){
                $imageProcessor->removeImage($product, $_child->getFile());
            }
            $product->save();
            $blockAdmin = $objectManager->create("\Libero\Customer\Block\Seller\Admin");
            $this->messageManager->addSuccess("Edit Request Product Successfully !");
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }catch (\Exception $e){
            $this->messageManager->addError($e->getMessage());
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            //$resultRedirect->setPath('*/*');
            return $resultRedirect;
        }
    }
}
