<?php
namespace Libero\Customer\Controller\Admin;

class RequestPayBalance extends \Magento\Framework\App\Action\Action
{
    protected $_resource;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->_resource = $resourceConnection;
        parent::__construct($context);
    }

    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $orderDetail = $objectManager->get("\Libero\Customer\Block\Seller\Admin\OrderDetail");

        $requestHttp = $objectManager->get("\Magento\Framework\App\Request\Http");
        $balanceIds = explode(",",$requestHttp->getParam("id"));
        foreach($balanceIds as $id){
            if($orderDetail->isValidateToRequestBalance($id)) {
                $this->sendRequestPayOrder($id);
            }
        }

        $json_result  = array();
        $json_result["success"] = true;
        $json_result["msg"] = "Send Request Successfully";
        $result = $jsonResultFactory->setData($json_result);
        return $result;
    }

    public function sendRequestPayOrder($balanceId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $this->_resource->getConnection();
        try {
            date_default_timezone_set("Asia/Bangkok");
            $curDateTime = date("Y-m-d H:i:s");
            $paid_status = "1";    //0: Pending | 1: Requested | 2: Paid | 3: Reject

            $sql_update_balance_status = "UPDATE libero_provider_balance SET `updated_at` = '$curDateTime' ,`request_at` = '$curDateTime', `status` = '$paid_status' WHERE id = '$balanceId';";
            $connection->query($sql_update_balance_status);

            $sql_update_order_paid_status = "SELECT order_ids FROM libero_provider_balance WHERE id = '$balanceId';";
            $order_ids = $connection->fetchAll($sql_update_order_paid_status)[0]["order_ids"];

            $sql_update_status = "UPDATE `seller_sales_order` SET `updated_at` = '$curDateTime', `paid_status` = '$paid_status', `request_at` = '$curDateTime' WHERE `entity_id` IN ($order_ids); ";
            $connection->query($sql_update_status);

        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addError(__('We can\'t save the invoice right now.'));
            $objectManager->get('Psr\Log\LoggerInterface')->critical($e);
        }
    }

    public function getOrderDetail($order_id,$id_seller)
    {
        $connection = $this->_resource->getConnection();
        $sql = "
            SELECT
                *, ssoi.qty_ordered AS qty, ssoi.item_id AS order_item_id
            FROM
                seller_sales_order_grid AS ssog
                    JOIN
                seller_sales_order AS sso
                    JOIN
                seller_sales_order_item AS ssoi
                    ON ssog.order_id = ssoi.order_id
                    AND sso.entity_id = ssoi.order_id
            WHERE
                ssog.seller_id = '$id_seller'
                AND ssog.order_id = '$order_id'
            ;
            ";
        $result = $connection->fetchAll($sql);
        return $result;
    }
}
