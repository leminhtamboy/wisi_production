<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Customer\Model\Group;
use Magento\ConfigurableProduct\Helper\Product\Options\Factory;

class SaveImport extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $helper = $objectManager->create('\Libero\Customer\Helper\Data');
        $postData = $this->getRequest()->getPostValue();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $base_url = $storeManager->getStore()->getBaseUrl();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        try {
            $dir = $objectManager->get('Magento\Framework\App\Filesystem\DirectoryList');
            $path = $dir->getRoot()."/test/import";
            $fileUploaderFactory = $objectManager->get("\Magento\MediaStorage\Model\File\UploaderFactory");
            $uploader_1 = $fileUploaderFactory->create(['fileId' => 'excel']);
            $uploader_1->setAllowedExtensions(['xlsx']);
            $uploader_1->setAllowRenameFiles(false);
            $uploader_1->setFilesDispersion(false);
            $result_1 = $uploader_1->save($path);
            $excel_upload = $result_1["file"];
            $arrayJson = array();
            $arrayJson["file"] = $excel_upload;
            $arrayJson["error"] = "false";
            $result = $jsonResultFactory->setData(json_encode($arrayJson));
            return $result;
        }catch (\Exception $e){
            $arrayJson = array();
            $arrayJson["file"] = "____LỖI ĐẨY FILE SẢN PHẨM____ ".$e->getMessage();
            $arrayJson["error"] = "true";
            $result = $jsonResultFactory->setData(json_encode($arrayJson));
            return $result;
        }
    }
}
