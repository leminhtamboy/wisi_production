<?php
namespace Libero\Customer\Controller\Admin;

class BalanceList extends \Magento\Framework\App\Action\Action
{
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Seller Balance List'));

        $postData = $this->getRequest()->getPostValue();
        if($postData){
            $id = $postData["id"];
            $date_from = $postData["date_from"];
            $date_to = $postData["date_to"];
            $status = $postData["order_status"];

            $resultPage->getLayout()->getBlock("customer_seller_admin_order")->setHaveSearch("1");
            $resultPage->getLayout()->getBlock("customer_seller_admin_order")->setId($id);
            $resultPage->getLayout()->getBlock("customer_seller_admin_order")->setDateFrom($date_from);
            $resultPage->getLayout()->getBlock("customer_seller_admin_order")->setDateTo($date_to);
            $resultPage->getLayout()->getBlock("customer_seller_admin_order")->setOrderStatus($status);
        }
        return $resultPage;
    }
}
