<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Filesystem\DirectoryList;
class SaveRole extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $blockAdmin = $objectManager->create("\Libero\Customer\Block\Seller\Admin");
        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();

        try {

            $postData = $this->getRequest()->getPostValue();
            $role_name = $postData["rolename"];
            $id_seller = $postData["id_seller"];
            $all = $postData["all"];
            $stringResource = "";
            if(isset($postData["resource"])){
                if($all == 0){
                    $resource = $postData["resource"]; // array
                    foreach($resource as $_source){
                        $stringResource.=$_source."|";
                    }
                    $stringResource = substr($stringResource,0,strlen($stringResource) -1);
                }else{
                    $stringResource = "admin|sale|product|report|store|system";
                }       
                //Save  ACL
                $modelAccountAcl = $this->_objectManager->create("\Libero\Customer\Model\Acl");
                $modelAccountAcl->setData("acl_permmision",$stringResource);
                $modelAccountAcl->setData("acl_name",$role_name);
                $modelAccountAcl->setData("id_seller",$id_seller);
                $modelAccountAcl->save();
                $resultRedirect->setPath('customer/admin/acllisting');
                return $resultRedirect;
            }else{
                $blockAdmin->setMessage("The resource not found. Please add role to your user !","error");
                $resultRedirect->setPath('customer/admin/acllisting');
                return $resultRedirect;
            }

        }catch (Exception $e){
            
        }
    }
}
