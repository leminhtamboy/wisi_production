<?php
namespace Libero\Customer\Controller\Admin;

class SearchProduct extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    protected $_coreRegistry;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $coreRegistry
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $resultRedirect = $this->resultRedirectFactory->create();

        $helper = $objectManager->create('\Libero\Customer\Helper\Data');
        try {

            $postData = $this->getRequest()->getPostValue();
            $name = $util->removeQuoteInString($postData["product_name"]);
            $sku = $util->removeQuoteInString($postData["product_sku"]);
            $status = $postData["product_status"];
            $id_shipping = $postData["product_shipping"];
            $id_seller = $postData["id_seller"];
            $category = $postData["product_category"];
            
            $product = $objectManager->create("\Magento\Catalog\Model\Product");

            $basicProductsBlock = $objectManager->create("\Sm\BasicProducts\Block\BasicProducts");
            $collectionProduct = $basicProductsBlock->getProductInCategoryByIdCate($category,$id_seller);

            /* $collectionProduct = $product->getCollection()->addAttributeToSelect("*")
                ->addAttributeToFilter("id_seller",array("eq" => $id_seller));
                 */
            if($name != ""){
                $collectionProduct = $collectionProduct->addAttributeToFilter("name",array("like" => "%".$name."%"));
            }
            if($sku != ""){
                $collectionProduct = $collectionProduct->addAttributeToFilter("sku",array("like" => "%".$sku."%"));
            }
            if($status != "-1"){
                $collectionProduct = $collectionProduct->addAttributeToFilter("status",array("eq" => $status));
            }
            if($id_shipping != "0"){
                $collectionProduct = $collectionProduct->addAttributeToFilter("id_shipping",array("eq" => $id_shipping));
            }
            $resultPage = $this->resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->set(__('Seller Product'));
            $resultPage->getLayout()->getBlock("customer_seller_admin_productlisting")->setCollectionSearch($collectionProduct);
            $resultPage->getLayout()->getBlock("customer_seller_admin_productlisting")->setNameSearch($name);
            $resultPage->getLayout()->getBlock("customer_seller_admin_productlisting")->setSkuSearch($sku);
            $resultPage->getLayout()->getBlock("customer_seller_admin_productlisting")->setStatusSearch($status);
            $resultPage->getLayout()->getBlock("customer_seller_admin_productlisting")->setCategorySearch($category);
            $resultPage->getLayout()->getBlock("customer_seller_admin_productlisting")->setShippingSearch($id_shipping);
            return $resultPage;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
            $resultRedirect->setPath('*/*');
            return $resultRedirect;
        }
    }
}
