<?php
namespace Libero\Customer\Controller\Admin;

class SearchDashboard extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $helper = $objectManager->create('\Libero\Customer\Helper\Data');
        try {
            $postData = $this->getRequest()->getPostValue();
            $date_from = $postData["date_from"];
            $date_to = $postData["date_to"];
            $resultPage = $this->_resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->set(__('Dashboard'));
            $resultPage->getLayout()->getBlock("customer_seller_admin_index")->setHaveSearch("1");
            $resultPage->getLayout()->getBlock("customer_seller_admin_index")->setDateFrom($date_from);
            $resultPage->getLayout()->getBlock("customer_seller_admin_index")->setDateTo($date_to);
            return $resultPage;
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
            $resultRedirect->setPath('*/*');
            return $resultRedirect;
        }
    }
}
