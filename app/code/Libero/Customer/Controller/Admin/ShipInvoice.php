<?php
namespace Libero\Customer\Controller\Admin;

class ShipInvoice extends \Magento\Framework\App\Action\Action
{
    protected $_resource;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->_resource = $resourceConnection;
        parent::__construct($context);
    }

    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $orderDetail = $objectManager->get("\Libero\Customer\Block\Seller\Admin\OrderDetail");

        $adminUtil = $objectManager->get("\Libero\Customer\Block\Seller\Admin\Util");
        $id_seller = $adminUtil->getIdSeller();
        if( $id_seller == null) {
            echo __("id_seller is null");
        }
        $requestHttp = $objectManager->get("\Magento\Framework\App\Request\Http");
        $order_ids = explode(",",$requestHttp->getParam("order_id"));
        foreach($order_ids as $order_id){
            if(!$orderDetail->isReleasedShipmentByOrderId($order_id)) {
                $this->shipInvoice($order_id, $id_seller);
            }
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('customer/admin/shipmentlisting');
        return $resultRedirect;
    }

    public function shipInvoice($order_id, $id_seller){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $orderDetail = $objectManager->get("\Libero\Customer\Block\Seller\Admin\OrderDetail");

        date_default_timezone_set("Asia/Bangkok");
        $curDateTime = date("Y-m-d H:i:s");

        try {
            $order_detail = $orderDetail->getOrderDetail($order_id);
            $order = $order_detail[0];
            $increment_id = $order["increment_id"];
            $store_id = $order["store_id"];
            $order_created_at = $order["created_at"];
            $customer_name = $order["customer_name"];
            $total_qty = $order["total_qty_ordered"];
            $order_status = "Prepare to ship";
            $billing_address = $order["billing_address"];
            $shipping_address = $order["shipping_address"];
            $billing_name = $order["customer_name"];
            $shipping_name = $order["customer_name"];
            $customer_email = $order["customer_email"];
            $customer_group_id = $order["customer_group_id"];
            $payment_method = $order["payment_method"];
            $shipping_information = $order["shipping_description"];
            $sql_insert = "
                INSERT INTO `seller_sales_shipment_grid` 
                    (`entity_id`, 
                    `increment_id`, 
                    `store_id`, 
                    `order_increment_id`, 
                    `order_id`, 
                    `order_created_at`, 
                    `customer_name`, 
                    `total_qty`, 
                    `shipment_status`, 
                    `order_status`,
                     `billing_address`, 
                     `shipping_address`, 
                     `billing_name`, 
                     `shipping_name`, 
                     `customer_email`, 
                     `customer_group_id`, 
                     `payment_method`, `shipping_information`, `created_at`, `updated_at`, `seller_id`) 
                    VALUES (NULL,'$increment_id','$store_id', '$increment_id', '$order_id', '$order_created_at', '$customer_name','$total_qty', NULL, '$order_status', '$billing_address', '$shipping_address','$billing_name', '$shipping_name','$customer_email','$customer_group_id','$payment_method','$shipping_information', '$curDateTime', '$curDateTime', '$id_seller');
            ";
            $connection = $this->_resource->getConnection();
            $connection->query($sql_insert);
            //Update status order

            $sql_update_status_order = "UPDATE seller_sales_order SET `status` = '$order_status', `status_code` = '3', `updated_at` = '$curDateTime' WHERE entity_id = '$order_id';";
            $connection->query($sql_update_status_order);

            $sql_update_status_order_grid = "UPDATE seller_sales_order_grid SET `status` = '$order_status', `status_code` = '3', `updated_at` = '$curDateTime' WHERE order_id = '$order_id';";
            $connection->query($sql_update_status_order_grid);
            $blockAdmin = $objectManager->create("\Libero\Customer\Block\Seller\Admin");
            $blockAdmin->setMessage("Create Shipment Successfully !","success");
            /* Start Barcode */
            $dir_orderbarcode = "orderbarcode";
            $dir = str_replace("\\","/",BP.'/'."pub/media".'/'.$dir_orderbarcode);
            if (!is_dir($dir))
            {
                mkdir($dir);
            }
            $upload_path = $dir . '/'.$increment_id."_barcode.png";
            $generatorPNG = $objectManager->get('\Picqer\Barcode\BarcodeGeneratorPNG');
            file_put_contents($upload_path, $generatorPNG->getBarcodeNo($increment_id, $generatorPNG::TYPE_CODE_128));
            /* End Barcode*/

        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
            $objectManager->get('Psr\Log\LoggerInterface')->critical($e);
        }
    }
}
