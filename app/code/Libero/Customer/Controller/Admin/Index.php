<?php
namespace Libero\Customer\Controller\Admin;
use Magento\Framework\App\Action\Context;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        if($this->getRequest()->getParam("is_admin")) {
            $id_customer = $this->getRequest()->getParam("is_admin");
            $customer = $objectManager->get('\Magento\Customer\Model\Customer')->load($id_customer);
            $session = $objectManager->create("\Magento\Customer\Model\Session");
            $session->setCustomerAsLoggedIn($customer);
            $session->regenerateId();
            return $resultRedirect->setPath('customer/admin');
        }
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Seller Dashboard'));
        return $resultPage;
    }
}
