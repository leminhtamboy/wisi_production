<?php
namespace Libero\Customer\Controller\Seller;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

class Copy extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
            $postData = $this->getRequest()->getPostValue();
            $storeName = $postData["store_name"];
            $arrayResult = array(
                "error" => false,
                "message"=> ""
            );
            if(count(explode("'",$storeName)) > 1 ){
                $arrayResult = array(
                    "error" => true,
                    "message"=> "Wrong store name"
                );
            };
            //copy file first and extract
            $rootWeb = $_SERVER['DOCUMENT_ROOT'];
            $baseDir=$rootWeb."/seller/".$storeName;
            $path = $baseDir;
            @mkdir($path, 0755, true);
            chmod($path,0755);
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
            $storeUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
            $remote_file_url = $storeUrl.'wisi2.zip';
            $toFolder = $rootWeb.'/seller/'.$storeName.'/files.zip';
            $copy = copy( $remote_file_url, $toFolder );
            if( !$copy ) {
                $arrayResult["error"] = true;
                $arrayResult["message"] = "fail";
                $resultJson = json_encode($arrayResult);
                $jsonResultFactory->setData($resultJson);
            }
            else{
                $arrayResult["error"] = false;
                $arrayResult["message"] = "success";
                $resultJson = json_encode($arrayResult);
                $jsonResultFactory->setData($resultJson);
            }
            $fileZip = $baseDir."/files.zip";
            //Extract now
            $zip = new \ZipArchive;
            $res = $zip->open($fileZip);
            if ($res === TRUE) {
                $zip->extractTo($baseDir);
                $zip->close();
                $arrayResult["error"] = false;
                $arrayResult["message"] = "success";
            } else {
                $arrayResult["error"] = true;
                $arrayResult["message"] = "fail";
            }
            //End copy
            //test purpose
            $arrayResult["error"] = false;
            $arrayResult["message"] = "success";
            //end test*/
            $resultJson = json_encode($arrayResult);
            $jsonResultFactory->setData($resultJson);
            return $jsonResultFactory;
        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
            return $resultRedirect->setPath('*/*');
        }
    }
}
