<?php
namespace Libero\Customer\Controller\Seller;

class CheckStoreName extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function rewriteUrl($store_name)
    {
        $urlRewrite = trim($this->removeSign($store_name));
        $urlRewrite = preg_replace('#[^0-9a-z]+#i', '-', $urlRewrite);
        $urlRewrite = strtolower($urlRewrite);
        return $urlRewrite;
    }

    function removeSign($str)
    {
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        try {
            $postData = $this->getRequest()->getPostValue();
            $store_name = $postData["store_name"];
            $dependenceCustomer = $objectManager->create("\Libero\Customer\Model\CustomerSellerStore");
            //$helper_factory = $objectManager->get('\Magento\Core\Model\Factory\Helper');
            /*$helperCustomer = $helper_factory->get('\Libero\Customer\Helper\Data');
            $store_name = $helperCustomer->rewriteUrl($store_name);*/
            $store_name = $this->rewriteUrl($store_name);
            $collectionCustomer = $dependenceCustomer->getCollection()->addFieldToFilter("name_store",array("eq" => $store_name))->getFirstItem();
            $arrayResult = array(
                "error" => false,
                "message"=> "success"
            );
            if(count($collectionCustomer->getData()) > 0) {
                $arrayResult["error"] = true;
                $arrayResult["message"] = __("This store name already exists");
            }            
            $jsonData = json_encode($arrayResult);
            $jsonResultFactory->setData($jsonData);
            return $jsonResultFactory;

        }catch (Exception $e){
            $arrayResult = array(
                "error" => true,
                "message"=> "fail"
            );
            $jsonData = json_encode($arrayResult);
            $jsonResultFactory->setData($jsonData);
            $this->messageManager->addError($e->getMessage());
            $jsonData = json_encode($arrayResult);
            $jsonResultFactory->setData($jsonData);
            return $jsonResultFactory;
        }
    }
}
