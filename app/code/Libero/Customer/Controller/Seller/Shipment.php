<?php
namespace Libero\Customer\Controller\Seller;
use Magento\Framework\App\Action\Context;

class Shipment extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    protected $customerSession;

    protected  $urlInterface;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Helper\Context $helperContext
    ){
        $this->resultPageFactory = $resultPageFactory;
        $this->customerSession = $customerSession;
        $this->urlInterface = $helperContext->getUrlBuilder();
        parent::__construct($context);
    }
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        return $resultPage;

        if(!$this->customerSession->isLoggedIn()){
            $this->customerSession->setAfterAuthUrl($this->urlInterface->getCurrentUrl());
            $this->customerSession->authenticate();
        }else {
            $resultPage = $this->resultPageFactory->create();
            return $resultPage;
        }
    }
}
