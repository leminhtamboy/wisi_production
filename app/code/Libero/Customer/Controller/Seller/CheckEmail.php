<?php
namespace Libero\Customer\Controller\Seller;

class CheckEmail extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(


        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();

        try {

            $postData = $this->getRequest()->getPostValue();

            $email = $postData["email"];

            $dependenceCustomer = $objectManager->create("\Magento\Customer\Model\Customer");

            $collectionCustomer = $dependenceCustomer->getCollection()->addAttributeToFilter("email",array("eq" => $email))->getFirstItem();
            $arrayResult = array(
                "error" => false,
                "message"=> "success"
            );
            if(count($collectionCustomer->getData()) > 0) {
                $arrayResult["error"] = true;
                $arrayResult["message"] = __("This email already exists");
            }
            
            $jsonData = json_encode($arrayResult);

            $jsonResultFactory->setData($jsonData);

            return $jsonResultFactory;

        }catch (Exception $e){
            $arrayResult = array(
                "error" => true,
                "message"=> "fail"
            );
            $jsonData = json_encode($arrayResult);

            $jsonResultFactory->setData($jsonData);

            return $jsonResultFactory;
            $this->messageManager->addError($e->getMessage());

            $jsonData = json_encode($arrayResult);

            $jsonResultFactory->setData($jsonData);

            return $jsonResultFactory;
        }
    }
}
