<?php
namespace Libero\Customer\Controller\Seller;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class SaveStore extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(


        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();

        try {

            $postData = $this->getRequest()->getPostValue();

            $id_seller = $postData["id_seller"];

            $sku_store = $postData["id_seller"]."|".$postData["store_name"]."|".$postData["email"];

            $dateCreate  = date("d-m-Y");

            $name_store = $postData["store_name"];

            $front_end_url = $postData["front_end_url"];

            $back_end_url = $postData["back_end_url"];

            $user_name = "admin";

            $password = "zxcasdqwe123";

            $injectionModelSellerStore = $this->_objectManager->create("\Libero\Customer\Model\CustomerSellerStore");

            $injectionModelSellerStore->setData("id_seller",$id_seller);

            $injectionModelSellerStore->setData("sku_store",$sku_store);

            $injectionModelSellerStore->setData("name_store",$name_store);

            $injectionModelSellerStore->setData("frontend_url",$front_end_url);

            $injectionModelSellerStore->setData("backend_url",$back_end_url);

            $injectionModelSellerStore->setData("user_name",$user_name);

            $injectionModelSellerStore->setData("password",$password);

            $injectionModelSellerStore->save();

            $arrayResult = array(
                "error" => false,
                "message"=> "success"
            );

            //$jsonData = \Zend_Json::encode($arrayResult);

            $jsonData = json_encode($arrayResult);

            $jsonResultFactory->setData($jsonData);

            return $jsonResultFactory;

        }catch (Exception $e){
            $arrayResult = array(
                "error" => true,
                "message"=> "fail"
            );
            $jsonData = json_encode($arrayResult);

            $jsonResultFactory->setData($jsonData);

            return $jsonResultFactory;
            $this->messageManager->addError($e->getMessage());

            $jsonData = json_encode($arrayResult);

            $jsonResultFactory->setData($jsonData);

            return $jsonResultFactory;
        }
    }
}
