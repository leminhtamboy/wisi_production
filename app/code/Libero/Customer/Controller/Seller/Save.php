<?php
namespace Libero\Customer\Controller\Seller;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
class Save extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(


        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
         

        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $postData = $this->getRequest()->getPostValue();
            $phone = $postData["phone"];
            $email = $postData["email"];
            $firstName = $postData["first_name"];
            $lastName = $postData["last_name"];
            $password = $postData["password"];
            $passwordConfirm = $postData["password_2"];
            if ($password != $passwordConfirm) {
                $this->messageManager->addError(__("Password confirm does not match"));
                return $resultRedirect->setPath("*/*");
            }
            $hot_line = $postData["hot_line"];
            $store_name = $postData["store_name"];
            //Company information
            $biz_type = $postData["biz_type"];
            $company = $postData["company"];
            $address = $postData["address"];
            $city = $postData["city"];
            $district = $postData["district"];
            $street = $postData["street"];
            $tax = $postData["tax"];
            $home_page = $postData["homepage"];
            $agent_name = $postData["agent_name"];
            $registration_number = $postData["registration_number"];
            $bank_name = $postData["bank_name"];
            $bank_account = $postData["bank_account"];
            $bank_account_name = $postData["bank_account_name"];
            //$etc = $postData["etc"];
            $is_manufacture = $postData["manufacture"];
            $is_genuine = $postData["genuine"];
            $id_category = explode(",",$postData["id_category"]);
            $logo_url = "seller_logo/logo_default_194x194.png";
            $banner_url = "seller_banner/banner_default_1370x310.png";
            $description = "";
            $rating = 0;
            $string_category = "";
            foreach($id_category as $_sell_cate){
                $string_category.= $_sell_cate."|";
            }
            //sub now
            $string_category = substr($string_category,0,strlen($string_category) -1);
            //$dateOfBirth = $postData["yy"] . "-" . $postData["mm"] . "-" . $postData["dd"];

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $url = \Magento\Framework\App\ObjectManager::getInstance();
            //Upload image
            $fileUploaderFactory = $objectManager->get("\Magento\MediaStorage\Model\File\UploaderFactory");
            $uploader = $fileUploaderFactory->create(['fileId' => 'certification']);
            $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','xlsx','doc', 'pdf']);
            $uploader->setAllowRenameFiles(false);
            $uploader->setFilesDispersion(false);
            $fileSystem = $objectManager->create('\Magento\Framework\Filesystem');
			$sellerNameRewrite = $this->rewriteUrl($firstName);
            $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('upload_manager/'.$sellerNameRewrite."/".$email."/".date("d-m-Y"));
			$pathSave = 'upload_manager/'.$sellerNameRewrite."/".$email."/".date("d-m-Y");
            $result = $uploader->save($path);
            $imagepath_1= $pathSave."/".$result['file'];
            //
            $uploader_1 = $fileUploaderFactory->create(['fileId' => 'certification_1']);
            $uploader_1->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','xlsx','doc', 'pdf']);
            $uploader_1->setAllowRenameFiles(false);
            $uploader_1->setFilesDispersion(false);
            $result_1 = $uploader_1->save($path);
            $imagepath_2 = $pathSave."/".$result_1["file"];
            //
            $uploader_2 = $fileUploaderFactory->create(['fileId' => 'certification_2']);
            $uploader_2->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png','xlsx','doc', 'pdf']);
            $uploader_2->setAllowRenameFiles(false);
            $uploader_2->setFilesDispersion(false);
            $result_2 = $uploader_2->save($path);
            $imagepath_3 = $pathSave."/".$result_2["file"];

            $uploader_3 = $fileUploaderFactory->create(['fileId' => 'logo_seller']);
            $uploader_3->setAllowedExtensions(['jpg', 'jpeg','png']);
            $uploader_3->setAllowRenameFiles(false);
            $uploader_3->setFilesDispersion(false);
            $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('seller_logo/'.$sellerNameRewrite."/".$email."/".date("d-m-Y"));
			$pathSave = 'seller_logo/'.$sellerNameRewrite."/".$email."/".date("d-m-Y");
            $result_3 = $uploader_3->save($path);
            $imagepath_logo = $pathSave."/".$result_3["file"];

            $uploader_4 = $fileUploaderFactory->create(['fileId' => 'banner_seller1']);
            $uploader_4->setAllowedExtensions(['jpg', 'jpeg','png']);
            $uploader_4->setAllowRenameFiles(false);
            $uploader_4->setFilesDispersion(false);
            $path = $fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath('seller_banner/'.$sellerNameRewrite."/".$email."/".date("d-m-Y"));
            $pathSave = 'seller_banner/'.$sellerNameRewrite."/".$email."/".date("d-m-Y");
            $delivery_type = $postData["shipping_method"];
            $result_4 = $uploader_4->save($path);
            $imagepath_4 = $pathSave."/".$result_4["file"];

            $description_seller = $postData["seller_description"];

            date_default_timezone_set("Asia/Bangkok");
            $curDateTime = date("Y-m-d H:i:s");
            
            //End upload
            $customerFactory = $this->_objectManager->get('\Magento\Customer\Model\CustomerFactory');
            $storeManager = $url->get('\Magento\Store\Model\StoreManagerInterface');
            $websiteId = $storeManager->getWebsite()->getWebsiteId();
            $store = $storeManager->getStore();  // Get Store ID
            $storeId = $store->getStoreId();
            $modelCustomerSellerCompany = $this->_objectManager->create("\Libero\Customer\Model\CustomerSeller");
            $modelCustomerSellerCompany->setData("company",$company);
            $modelCustomerSellerCompany->setData("address",$address);
            $modelCustomerSellerCompany->setData("city",$city);
            $modelCustomerSellerCompany->setData("district",$district);
            $modelCustomerSellerCompany->setData("street",$street);
            $modelCustomerSellerCompany->setData("full_address",$address." ".$street." ".$district." ".$city);
            $modelCustomerSellerCompany->setData("telephone",$phone);
            $modelCustomerSellerCompany->setData("tax",$tax);
            $modelCustomerSellerCompany->setData("home_page",$home_page);
            $modelCustomerSellerCompany->setData("agent_name",$agent_name);
            $modelCustomerSellerCompany->setData("bank_name",$bank_name);
            $modelCustomerSellerCompany->setData("bank_account",$bank_account);
            $modelCustomerSellerCompany->setData("bank_account_name",$bank_account_name);
            //$modelCustomerSellerCompany->setData("etc",$etc);
            $modelCustomerSellerCompany->setData("is_manufacture",$is_manufacture);
            $modelCustomerSellerCompany->setData("is_genuine",$is_genuine);
            $modelCustomerSellerCompany->setData("biz_type",$biz_type);
            $modelCustomerSellerCompany->setData("company_registration",$registration_number);
            $modelCustomerSellerCompany->setData("up_load_cer_1",$imagepath_1);
            $modelCustomerSellerCompany->setData("up_load_cer_2",$imagepath_2);
            $modelCustomerSellerCompany->setData("up_load_cer_3",$imagepath_3);
            $modelCustomerSellerCompany->setData("store_name",$store_name);
            $modelCustomerSellerCompany->setData("status",0);
            $modelCustomerSellerCompany->setData("message_denied","");
            $modelCustomerSellerCompany->setData("id_category",$string_category);
            $modelCustomerSellerCompany->setData("logo_url",$imagepath_logo);
            $modelCustomerSellerCompany->setData("banner_url1",$imagepath_4);
            $modelCustomerSellerCompany->setData("description",$description_seller);
            $modelCustomerSellerCompany->setData("hot_line",$hot_line);
            $modelCustomerSellerCompany->setData('created_at', $curDateTime);
            $modelCustomerSellerCompany->setData('updated_at', $curDateTime);
            $modelCustomerSellerCompany->setData('delivery_type',$delivery_type);
            //Save customer
            $customer = $customerFactory->create();
            $customer->setWebsiteId($websiteId);
            $customer->setEmail($email);
            $customer->setFirstname($firstName);
            $customer->setLastname($lastName);
            $customer->setPassword($password);
            //$customer->setDob($dateOfBirth);
            $customer->setGroupId(2);
            $customer->save();
            $postCode = $postData["province"]."-".$postData["ward"];
            //Save address customer
            $addresssModel = $this->_objectManager->get('\Magento\Customer\Model\AddressFactory');
            $addressModel = $addresssModel->create();
            $addressModel->setCustomerId($customer->getId())
                ->setFirstname($firstName)
                ->setLastname($lastName)
                ->setCountryId('VN')
                ->setPostcode($postCode)
                ->setCity($city)
                ->setTelephone($phone)
                ->setFax($phone)
                ->setCompany($company)
                ->setStreet($address." ".$street." ".$district)
                ->setIsDefaultBilling('1')
                ->setIsDefaultShipping('1')
                ->setSaveInAddressBook('1');
            $addressModel->save();
			$modelCustomerSellerCompany->setData("id_customer",$customer->getId());
            $modelCustomerSellerCompany->save();
            
            
            $id_seller = $modelCustomerSellerCompany->getId();
            $status  = 1;
            $price = 0;
            if ($customer && $customer->getId()) {
                $session  = $objectManager->create("\Magento\Customer\Model\Session");
                $session->setCustomerAsLoggedIn($customer);
                $session->regenerateId();

                /*if ($this->getCookieManager()->getCookie('mage-cache-sessid')) {
                    $metadata = $this->getCookieMetadataFactory()->createCookieMetadata();
                    $metadata->setPath('/');
                    $this->getCookieManager()->deleteCookie('mage-cache-sessid', $metadata);
                }*/

            }
            $this->messageManager->addSuccess(__("Create Provider Account Successfully"));
            $resultRedirect->setPath('customer/account');
            return $resultRedirect;
        }catch (\Exception $e){
            $this->messageManager->addError($e->getMessage());
            return $resultRedirect->setPath('*/*');
        }
    }
	public function rewriteUrl($store_name){
        $urlRewrite = trim($this->RemoveSign($store_name));
        $urlRewrite = preg_replace('#[^0-9a-z]+#i', '-', $urlRewrite);
        $urlRewrite = strtolower($urlRewrite);
        return $urlRewrite;
    }

    function RemoveSign($str)
    {
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }
}
