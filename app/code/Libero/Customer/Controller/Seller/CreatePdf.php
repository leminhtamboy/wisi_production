<?php
namespace Libero\Customer\Controller\Seller;
 
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
 
class CreatePdf extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;
 
    /**
     * @param Context                               $context
     */
    public function __construct(
        Context $context,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    ) {
        $this->fileFactory = $fileFactory;
        parent::__construct($context);
    }
 
    /**
     * to generate pdf
     *
     * @return void
     */
    public function execute()
    {
      /*  $this->createInvoiceExample(); */
          $this->createPackingExample();
    }
     /* public function createInvoiceExample()
    {
        $pdf = new \Zend_Pdf();
        $pdf->pages[] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);
        $page = $pdf->pages[0]; // this will get reference to the first page.
        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Rgb(0,0,0));
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $rootDirectory = $filesystem->getRoot();
        $path_of_font = $rootDirectory."/app/design/frontend/Sm/market/web/fonts/Roboto-Regular.ttf";
        $path_of_logo = $rootDirectory."/pub/media/logo/stores/1/wisi-text-logo-200x77-w.png";
        $image = \Zend_Pdf_Image::imageWithPath($path_of_logo);
        $font = \Zend_Pdf_Font::fontWithPath($path_of_font);
        $style->setFont($font,15);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $x = 30;
        $pageTopalign = 850; //default PDF page height
        $this->y = 850 - 100; //print table row from page top – 100px
        //Draw table header row’s
        $style->setFont($font,30);
        $page->setStyle($style);
        //Region Logo
        $top = 790;
        $width_image = $image->getPixelWidth() / 2;
        $height_image = $image->getPixelHeight() / 2;
        //top border of the page
        $widthLimit = 270;
        //half of the page width
        $heightLimit = 270;
        $ratio = $width_image / $height_image;
        if ($ratio > 1 && $width_image > $widthLimit) {
            $width_image = $widthLimit;
            $height_image = $width_image / $ratio;
        } elseif ($ratio < 1 && $height_image > $heightLimit) {
            $height_image = $heightLimit;
            $width_image = $height_image * $ratio;
        } elseif ($ratio == 1 && $height_image > $heightLimit) {
            $height_image = $heightLimit;
            $width_image = $widthLimit;
        }
        $y1 = $top - $height_image;
        $y2 = $top;
        $x1 = 25;
        $x2 = $x1 + $width_image;
        $page->drawImage($image,$x1, $y1, $x2, $y2);
        //End Region
        $page->drawText(__("INVOICE"), $x + 270, $this->y+10, 'UTF-8');
        
        // Info invoice
        $style->setFont($font,11);
        $style->setFillColor(new \Zend_Pdf_Color_Html('#000000'));
        $page->setStyle($style);
        $page->drawText(__("Storename: %1", "WISI Store"), $x + 10, $this->y-10, 'UTF-8');
        $page->drawText(__("Invoice # %1", "000000002"), $x + 270, $this->y-10, 'UTF-8');
        $page->drawText(__("Address: %1", "208 Nguyen Huu Canh, Binh Thanh"), $x + 10, $this->y-30, 'UTF-8');
        $page->drawText(__("Order # %1", "000000069"), $x + 270, $this->y-30, 'UTF-8');
        $page->drawText(__("Phone: %1", "0909090999"), $x + 10, $this->y-50, 'UTF-8');
        $page->drawText(__("Order Date: %1", "Sep 14, 2018"), $x + 270, $this->y-50, 'UTF-8');
        
        //Sold to and Ship to - title
        $style->setLineColor(new \Zend_Pdf_Color_Html('#354157'));
        $style->setFillColor(new \Zend_Pdf_Color_Html('#ffffff'));
        $page->setStyle($style);
        $page->setLineWidth(20);
        $page->drawLine(30, $this->y -80, $x + 255,$this->y -80); 
        $page->drawText(__("Sold to:"), $x + 10, $this->y -85, 'UTF-8');
        $page->drawLine($x + 260, $this->y -80, $page->getWidth()-30,$this->y -80); 
        $page->drawText(__("Ship to:"), $x + 270, $this->y -85, 'UTF-8');

        //Sold to and Ship to - value
        $style->setLineColor(new \Zend_Pdf_Color_Html('#000000'));
        $style->setFillColor(new \Zend_Pdf_Color_Html('#000000'));
        $page->setStyle($style);
        $page->setLineWidth(0);
        $string="135 nguyen huu canh, P.22, Bình Thạnh,TP.HCM, djj shfo  sbhfdh  khsidfh   gsfgsigsf ";
        $drawingString = iconv('UTF-8', 'UTF-16BE//IGNORE', $string);
        $characters = array();
        for ($i = 0; $i < strlen($drawingString); $i++) {
            $characters[] = (ord($drawingString[$i++]) << 8 ) | ord($drawingString[$i]);
        }
        $glyphs = $font->glyphNumbersForCharacters($characters);
        $widths = $font->widthsForGlyphs($glyphs);
        $stringWidth = (array_sum($widths) / $font->getUnitsPerEm());
        $page->drawRectangle(30.5, $this->y -170, $x + 254.5, $this->y - 90, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawText(__("Tâm Lê Minh"), $x + 10, $this->y -110, 'UTF-8');
        $page->drawText(__($stringWidth), $x + 10, $this->y -130,'UTF-8');
        $page->drawText(__("T: %1", "0489897890"), $x + 10, $this->y -150, 'UTF-8');

        $page->drawRectangle($x + 260.5, $this->y -170, $page->getWidth()-30.5, $this->y - 90, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawText(__("Tâm Lê Minh"), $x + 270,$this->y -110, 'UTF-8');
        $page->drawText(__("135 nguyen huu canh, P.22, Bình Thạnh,TP.HCM"), $x + 270, $this->y -130, 'UTF-8');
        $page->drawText(__("T: %1", "0489897890"), $x + 270, $this->y -150, 'UTF-8');

       //Payment method and Shipping method - title
        $style->setLineColor(new \Zend_Pdf_Color_Html('#354157'));
        $style->setFillColor(new \Zend_Pdf_Color_Html('#ffffff'));
        $page->setStyle($style);
        $page->setLineWidth(20);
        $page->drawLine(30, $this->y - 190, $x + 255,$this->y - 190);
        $page->drawText(__("Payment Method:"), $x + 10, $this->y -195, 'UTF-8');
        $page->drawLine($x + 260, $this->y - 190, $page->getWidth()-30, $this->y - 190); 
        $page->drawText(__("Shipping Method:"), $x + 270, $this->y -195, 'UTF-8');

       //Payment method and Shipping method - value
        $style->setLineColor(new \Zend_Pdf_Color_Html('#000000'));
        $style->setFillColor(new \Zend_Pdf_Color_Html('#000000'));
        $page->setStyle($style);
        $page->setLineWidth(0); 
        $page->drawRectangle(30.5, $this->y -280, $x + 254.5, $this->y - 200, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawText(__("Cash On Delivery"), $x + 10, $this->y -220, 'UTF-8');

        $page->drawRectangle($x + 260.5, $this->y -280, $page->getWidth()-30.5, $this->y - 200, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawText(__("Delivery company-Giao Hàng Nhanh"), $x + 270, $this->y -220, 'UTF-8');
        $page->drawText(__("(Total Shipping Charges đ67,300)"), $x + 270, $this->y -240, 'UTF-8');
        //Show table Products list
            //Title - table
        $style->setLineColor(new \Zend_Pdf_Color_Html('#354157'));
        $style->setFillColor(new \Zend_Pdf_Color_Html('#ffffff'));
        $page->setStyle($style);
        $page->setLineWidth(20);
        $page->drawLine(30, $this->y -300, $x + 220, $this->y -300);
        $page->drawText(__("Products"), $x + 10, $this->y -305, 'UTF-8');
        $page->drawLine($x + 220, $this->y -300, $x + 310 , $this->y -300);
        $page->drawText(__("Price"), $x + 255, $this->y -305, 'UTF-8');
        $page->drawLine($x + 310, $this->y -300, $x + 380, $this->y -300);
        $page->drawText(__("Qty"), $x + 325, $this->y -305, 'UTF-8');
        $page->drawLine($x + 380, $this->y -300, $x + 460, $this->y -300);
        $page->drawText(__("Tax"), $x + 410, $this->y -305, 'UTF-8');
        $page->drawLine($x + 460, $this->y -300, $page->getWidth()-30, $this->y -300);
        $page->drawText(__("Subtotal"), $x + 470, $this->y -305, 'UTF-8');
            //Value - table
        $style->setLineColor(new \Zend_Pdf_Color_Html('#000000'));
        $style->setFillColor(new \Zend_Pdf_Color_Html('#000000'));
        $page->setStyle($style);
        $page->setLineWidth(0); 
        $page->drawText(__("Smart TV Samsung 40inch 4K UHD"), $x + 10, $this->y -330, 'UTF-8');
        $page->drawText(__("₫8,879,888"), $x + 230, $this->y -330, 'UTF-8');
        $page->drawText(__("2"), $x + 330, $this->y -330, 'UTF-8');
        $page->drawText(__("đ0"), $x + 400, $this->y -330, 'UTF-8');
        $page->drawText(__("₫8,879,888"), $x + 470, $this->y -330, 'UTF-8');

        $page->drawText(__("Smart TV Samsung 40inch 4K UHD"), $x + 10, $this->y -350, 'UTF-8');
        $page->drawText(__("₫8,879,888"), $x + 230, $this->y -350, 'UTF-8');
        $page->drawText(__("2"), $x + 330, $this->y -350, 'UTF-8');
        $page->drawText(__("đ0"), $x + 400, $this->y -350, 'UTF-8');
        $page->drawText(__("₫8,879,888"), $x + 470, $this->y -350, 'UTF-8');

        $page->drawText(__("Smart TV Samsung 40inch 4K UHD"), $x + 10, $this->y -370, 'UTF-8');
        $page->drawText(__("₫8,879,888"), $x + 230, $this->y -370, 'UTF-8');
        $page->drawText(__("2"), $x + 330, $this->y -370, 'UTF-8');
        $page->drawText(__("đ0"), $x + 400, $this->y -370, 'UTF-8');
        $page->drawText(__("₫8,879,888"), $x + 470, $this->y -370, 'UTF-8');

        $page->drawText(__("Smart TV Samsung 40inch 4K UHD"), $x + 10, $this->y -390, 'UTF-8');
        $page->drawText(__("₫8,879,888"), $x + 230, $this->y -390, 'UTF-8');
        $page->drawText(__("2"), $x + 330, $this->y -390, 'UTF-8');
        $page->drawText(__("đ0"), $x + 400, $this->y -390, 'UTF-8');
        $page->drawText(__("₫8,879,888"), $x + 470, $this->y -390, 'UTF-8');

        // Total 
        $page->drawText(__("Subtotal : %1", "₫8,879,888"), $x + 370, $this->y-420, 'UTF-8');
        $page->drawText(__("Shipping & Handling : %1", "₫20,888"), $x + 370, $this->y-440, 'UTF-8');
        $page->drawText(__("Grand Total : %1", "₫8,879,888"), $x + 370, $this->y-460, 'UTF-8');
  
        $fileName = 'example.pdf';
 
        $this->fileFactory->create(
           $fileName,
           $pdf->render(),
           \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR, // this pdf will be saved in var directory with the name example.pdf
           'application/pdf'
        );
    } */
    public function createPackingExample()
    {
        $pdf = new \Zend_Pdf();
        $pdf->pages[] = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $page = $pdf->pages[0]; // this will get reference to the first page.
        $style = new \Zend_Pdf_Style();
        $style->setLineColor(new \Zend_Pdf_Color_Rgb(0,0,0));
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $filesystem = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $rootDirectory = $filesystem->getRoot();
        $path_of_font = $rootDirectory."/app/design/frontend/Sm/market/web/fonts/Roboto-Regular.ttf";
        $path_of_product = $rootDirectory."/pub/media/catalog/product/resized/100x100/db4e66100f55e6657d33f65beabfc3e5/7688e41c827ddff816ef2131c77da3f4.jpg";
        $path_of_logo = $rootDirectory."/pub/media/logo/stores/1/wisi-text-logo-200x77-w.png";
        $image = \Zend_Pdf_Image::imageWithPath($path_of_logo);
        $product_img = \Zend_Pdf_Image::imageWithPath($path_of_product);
        $font = \Zend_Pdf_Font::fontWithPath($path_of_font);
        $style->setFont($font,15);
        $page->setStyle($style);
        $width = $page->getWidth();
        $hight = $page->getHeight();
        $x = 100;
        $pageTopalign = 595; //default PDF page height
        $this->y = 595 - 100; //print table row from page top – 100px
        $style->setFont($font,30);
        $page->setStyle($style);
        //Function Show IMG Product
        $top = $this->y - 70;
        $width_image = $product_img->getPixelWidth();
        $height_image = $product_img->getPixelHeight();
        $widthLimit = 45;
        $heightLimit = 20;
        $ratio = $width_image / $height_image;
        if ($ratio > 1 && $width_image > $widthLimit) {
            $width_image = $widthLimit;
            $height_image = $width_image / $ratio;
        } elseif ($ratio < 1 && $height_image > $heightLimit) {
            $height_image = $heightLimit;
            $width_image = $height_image * $ratio;
        } elseif ($ratio == 1 && $height_image > $heightLimit) {
            $height_image = $heightLimit;
            $width_image = $widthLimit;
        }
        $y1 = $top - $height_image;
        $y2 = $top;
        $x1 = $x + 255;
        $x2 = $x1 + $width_image;
        $page->drawImage($product_img,$x1, $y1, $x2, $y2);
        $page->drawImage($product_img,$x1, $y1 - 30, $x2, $y2 - 30);
        $page->drawImage($product_img,$x1, $y1 - 30 - 30, $x2, $y2 - 30 - 30);
        $page->drawImage($product_img,$x1, $y1 - 30 - 30 - 30, $x2, $y2 - 30 - 30 - 30);
        //End Region
        //Left content
        //Region Logo
        $top_logo = 530;
        $width_image_logo = $image->getPixelWidth() / 2;
        $height_image_logo = $image->getPixelHeight() / 2;
        //top border of the page
        $widthLimit = 270;
        //half of the page width
        $heightLimit = 270;
        $ratio = $width_image_logo / $height_image_logo;
        if ($ratio > 1 && $width_image_logo > $widthLimit) {
            $width_image_logo = $widthLimit;
            $width_image_logo = $width_image_logo / $ratio;
        } elseif ($ratio < 1 && $height_image_logo > $heightLimit) {
            $width_image_logo = $heightLimit;
            $width_image_logo = $height_image_logo * $ratio;
        } elseif ($ratio == 1 && $height_image_logo > $heightLimit) {
            $height_image_logo = $heightLimit;
            $width_image_logo = $widthLimit;
        }
        $y1 = $top_logo - $height_image_logo;
        $y2 = $top_logo;
        $x1 = 30;
        $x2 = $x1 + $width_image_logo;
        $page->drawImage($image,$x1, $y1, $x2, $y2);
        //Title page
        $page->drawText(__("PACKAGING"), $x + 250, $this->y+10, 'UTF-8');
        $style->setFont($font,10);
        $page->setStyle($style);
        $page->drawText(__("Create at: %1", "21/09/2018"), $x + 280, $this->y-10, 'UTF-8');
        // right content
        $style->setFont($font,10);
        $page->setStyle($style);
        $page->drawRectangle($x + 500,$this->y+40, $x + 630, $this->y+20, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawText(__("Customer service manager"),$x + 505 ,$this->y+26 , 'UTF-8');
        $page->drawRectangle($x + 630,$this->y+40, $page->getWidth()-30, $this->y+20, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

        $page->drawRectangle($x + 500,$this->y, $x + 630, $this->y+20, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawText(__("Treasury Manager"),$x + 505 ,$this->y+7 , 'UTF-8');
        $page->drawRectangle($x + 630,$this->y, $page->getWidth()-30, $this->y+20, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);

        $page->drawRectangle($x + 500,$this->y-20, $x + 630, $this->y, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $page->drawText(__("Checker"),$x + 505 ,$this->y-13 , 'UTF-8');
        $page->drawRectangle($x + 630,$this->y-20, $page->getWidth()-30, $this->y, \Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        // TABLE PACKAGING
            //start - header table
                // var
                $line_y = $this->y - 50;
                $text_y = $this->y -55;
                $orderID_x = $x - 5;
                $createDate_x = $x + 50;
                $provider_x = $x + 110;
                $reSeller_x = $x + 185;
                $nameProduct_x = $x + 310;
                $option_x = $x + 445;
                $qty_x = $x + 490;
                $shipping_x = $x + 545;
                $total_x = $x + 635;
                $style->setLineColor(new \Zend_Pdf_Color_Html('#354157'));
                $style->setFillColor(new \Zend_Pdf_Color_Html('#ffffff'));
                $style->setFont($font,9);
                $page->setStyle($style);
                $page->setLineWidth(30);
                $page->drawLine(30, $line_y, $x-10, $line_y);
                $page->drawText(__("ID"), 40, $text_y, 'UTF-8');
                $page->drawLine($x-10, $line_y, $x + 45 , $line_y);
                $page->drawText(__("Order Id"), $orderID_x, $text_y, 'UTF-8');
                $page->drawLine($x + 45, $line_y, $x + 105, $line_y);
                $page->drawText(__("Purchase Date"),  $createDate_x, $text_y, 'UTF-8');
                $page->drawLine( $x + 105, $line_y, $x + 180, $line_y);
                $page->drawText(__("Provider Name"), $provider_x, $text_y, 'UTF-8');
                $page->drawLine($x + 180, $line_y, $x + 250, $line_y);
                $page->drawText(__("Reseller Name"), $reSeller_x, $text_y, 'UTF-8');
                $page->drawLine($x + 250, $line_y, $x + 305, $line_y);
                $page->drawText(__("Image"), $x + 255, $text_y, 'UTF-8');
                $page->drawLine($x + 305, $line_y, $x + 440, $line_y);
                $page->drawText(__("Name - SKU"), $nameProduct_x, $text_y, 'UTF-8');
                $page->drawLine($x + 440, $line_y, $x + 485, $line_y);//45
                $page->drawText(__("Option"), $option_x, $text_y, 'UTF-8');
                $page->drawLine($x + 485, $line_y, $x + 540, $line_y);//55
                $page->drawText(__("Qty"), $qty_x, $text_y, 'UTF-8');
                $page->drawLine($x + 540, $line_y,$x + 630, $line_y);
                $page->drawText(__("Shipping Company"), $shipping_x, $text_y, 'UTF-8');
                $page->drawLine($x + 630, $line_y, $page->getWidth()-30, $line_y);
                $page->drawText(__("Grand Total"), $total_x, $text_y, 'UTF-8');
                // end - header table
                // - Show Value - table
                $value_y1 = $this->y - 80;
                $style->setLineColor(new \Zend_Pdf_Color_Html('#000000'));
                $style->setFillColor(new \Zend_Pdf_Color_Html('#000000'));
                $style->setFont($font,7);
                $page->setStyle($style);
                $page->setLineWidth(0); 
                $page->drawText(__("000000181"), 40, $value_y1, 'UTF-8');
                $page->drawText(__("188"), $orderID_x, $value_y1, 'UTF-8');
                $page->drawText(__("2018-09-08"), $createDate_x, $value_y1, 'UTF-8');
                $page->drawText(__("seller_1"), $provider_x, $value_y1, 'UTF-8');
                $page->drawText(__("Duong Nam"), $reSeller_x, $value_y1, 'UTF-8');
                $page->drawText(__("Túi Đeo Chéo Dutti No.22-2232845"), $nameProduct_x, $value_y1, 'UTF-8');
                $page->drawText(__("None"), $option_x, $value_y1, 'UTF-8');
                $page->drawText(__("1"), $qty_x, $value_y1, 'UTF-8');
                $page->drawText(__("Giao Hàng Nhanh"), $shipping_x, $value_y1, 'UTF-8');
                $page->drawText(__("₫26,400.00/₫146,400.00"), $total_x, $value_y1, 'UTF-8');
                // end row
                $value_y2 =  $value_y1 - 30;
                $style->setLineColor(new \Zend_Pdf_Color_Html('#000000'));
                $style->setFillColor(new \Zend_Pdf_Color_Html('#000000'));
                $style->setFont($font,7);
                $page->setStyle($style);
                $page->setLineWidth(0); 
                $page->drawText(__("000000181"), 40, $value_y2, 'UTF-8');
                $page->drawText(__("188"), $orderID_x, $value_y2, 'UTF-8');
                $page->drawText(__("2018-09-08"), $createDate_x, $value_y2, 'UTF-8');
                $page->drawText(__("seller_1"), $provider_x, $value_y2, 'UTF-8');
                $page->drawText(__("Duong Nam"), $reSeller_x, $value_y2, 'UTF-8');
                $page->drawText(__("Túi Đeo Chéo Dutti No.22-2232845"), $nameProduct_x, $value_y2, 'UTF-8');
                $page->drawText(__("None"), $option_x, $value_y2, 'UTF-8');
                $page->drawText(__("1"), $qty_x, $value_y2, 'UTF-8');
                $page->drawText(__("Giao Hàng Nhanh"), $shipping_x, $value_y2, 'UTF-8');
                $page->drawText(__("₫26,400.00/₫146,400.00"), $total_x, $value_y2, 'UTF-8');
                // end row
                $value_y3 =  $value_y2 - 30;
                $style->setLineColor(new \Zend_Pdf_Color_Html('#000000'));
                $style->setFillColor(new \Zend_Pdf_Color_Html('#000000'));
                $style->setFont($font,7);
                $page->setStyle($style);
                $page->setLineWidth(0); 
                $page->drawText(__("000000181"), 40, $value_y3, 'UTF-8');
                $page->drawText(__("188"), $orderID_x, $value_y3, 'UTF-8');
                $page->drawText(__("2018-09-08"), $createDate_x, $value_y3, 'UTF-8');
                $page->drawText(__("seller_1"), $provider_x, $value_y3, 'UTF-8');
                $page->drawText(__("Duong Nam"), $reSeller_x, $value_y3, 'UTF-8');
                $page->drawText(__("Túi Đeo Chéo Dutti No.22-2232845"), $nameProduct_x, $value_y3, 'UTF-8');
                $page->drawText(__("None"), $option_x, $value_y3, 'UTF-8');
                $page->drawText(__("1"), $qty_x, $value_y3, 'UTF-8');
                $page->drawText(__("Giao Hàng Nhanh"), $shipping_x, $value_y3, 'UTF-8');
                $page->drawText(__("₫26,400.00/₫146,400.00"), $total_x, $value_y3, 'UTF-8');
                // end row
                $value_y4 =  $value_y3 - 30;
                $style->setLineColor(new \Zend_Pdf_Color_Html('#000000'));
                $style->setFillColor(new \Zend_Pdf_Color_Html('#000000'));
                $style->setFont($font,7);
                $page->setStyle($style);
                $page->setLineWidth(0); 
                $page->drawText(__("000000181"), 40, $value_y4, 'UTF-8');
                $page->drawText(__("188"), $orderID_x, $value_y4, 'UTF-8');
                $page->drawText(__("2018-09-08"), $createDate_x, $value_y4, 'UTF-8');
                $page->drawText(__("seller_1"), $provider_x, $value_y4, 'UTF-8');
                $page->drawText(__("Duong Nam"), $reSeller_x, $value_y4, 'UTF-8');
                $page->drawText(__("Túi Đeo Chéo Dutti No.22-2232845"), $nameProduct_x, $value_y4, 'UTF-8');
                $page->drawText(__("None"), $option_x, $value_y4, 'UTF-8');
                $page->drawText(__("1"), $qty_x, $value_y4, 'UTF-8');
                $page->drawText(__("Giao Hàng Nhanh"), $shipping_x, $value_y4, 'UTF-8');
                $page->drawText(__("₫26,400.00/₫146,400.00"), $total_x, $value_y4, 'UTF-8');

        $fileName = 'example.pdf';
 
        $this->fileFactory->create(
           $fileName,
           $pdf->render(),
           \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR, // this pdf will be saved in var directory with the name example.pdf
           'application/pdf'
        );
    }
}