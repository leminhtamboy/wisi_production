<?php
namespace Libero\Customer\Controller\Seller;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;

class Command extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

            $helperSeller = $objectManager->get("\Libero\Customer\Helper\Data");

            $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();

            $postData = $this->getRequest()->getPostValue();

            $store_name = $postData["store_name"];

            $baseDir = $helperSeller->getBaseDir($store_name);

            /*$cmdRemove = "php ".$baseDir."/bin/magento setup:upgrade";

            shell_exec($cmdRemove); //server have to enable this for all case

            $cmdRemove = "php ".$baseDir."/bin/magento setup:di:compile";

            shell_exec($cmdRemove); //server have to enable this for all case

            $cmdRemove = "php ".$baseDir."/bin/magento setup:static-content:deploy";

            shell_exec($cmdRemove);*/

            $cmdRemove = "php ".$baseDir."/bin/magento cache:clean";

            shell_exec($cmdRemove);

            $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');

            $storeUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);

            $front_end_url = $storeUrl."seller/".$store_name;

            $back_end_url = $storeUrl."seller/".$store_name."/admin";

            $arrayResult = array(

                "error" => false,

                "front_end_url" => $front_end_url,

                "back_end_url" => $back_end_url,

                "message"=> "cache clean success"
            );

            $resultJson = json_encode($arrayResult);

            $jsonResultFactory->setData($resultJson);

            return $jsonResultFactory;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
        }
    }
}
