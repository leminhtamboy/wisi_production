<?php
namespace Libero\Customer\Controller\Faq;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Customer\Model\Group;
class Save extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ){
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();

        $resultRedirect = $this->resultRedirectFactory->create();
        $jsonResultFactory = $objectManager->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
        $helper = $objectManager->create('\Libero\Customer\Helper\Data');
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        try {

            $postData = $this->getRequest()->getPostValue();            
            $array_reply_question = $util->removeQuoteInString($postData["reply"]);
            $array_id_reply = $postData["faq_children"];
            foreach($array_id_reply as $_id_reply){
                $id_faq = $_id_reply;
                $sender = $postData["sender"];
                $reply_question = $array_reply_question[$id_faq][0];
                if($reply_question == ""){
                    continue;
                }
                $provider_id = $postData["provider_id"];
                $update_question = "A";
                $product_sku = $postData["product_sku"];
                $product_name = $postData["product_name"];
                $id_room = $postData["id_room"];
                date_default_timezone_set("Asia/Bangkok");
                $curDateTime = date("Y-m-d H:i:s");
                $modelFAQ = $this->_objectManager->create("\Libero\Customer\Model\Faq");
                $modelFAQ->setData("sender",$sender);
                $modelFAQ->setData("provider_id",$provider_id);
                $modelFAQ->setData("type_faq",$update_question);
                $modelFAQ->setData("product_sku",$product_sku);
                $modelFAQ->setData("product_name",$product_name);
                $modelFAQ->setData("content_question",$reply_question);
                $modelFAQ->setData("answer",0);
                $modelFAQ->setData("id_room",$id_room);
                $modelFAQ->save();
                $parent_answer = $modelFAQ->getId();
                $faq = $this->_objectManager->create("\Libero\Customer\Model\Faq")->load($id_faq);
                $faq->setData("answer","1");
                $faq->setData("parent_question",$parent_answer);
                $faq->save();
    
                $sql_update_question = "UPDATE `libero_faq` SET `answered_at` = '$curDateTime' WHERE `id_faq` = '$id_faq' ";
                $connection->query($sql_update_question);
    
                $sql_update_answer = "UPDATE `libero_faq` SET `created_at` = '$curDateTime' WHERE `id_faq` = '$parent_answer' ";
                $connection->query($sql_update_answer);

                $sql_update_room = "UPDATE `libero_faq_room` SET `answered_at` = '$curDateTime' , `status` = 1 WHERE `id_room` = '$id_room' ";
                $connection->query($sql_update_room);
            }
            $this->messageManager->addSuccess("Answered Question !");
            $resultRedirect->setPath('customer/faq/index/');
            return $resultRedirect;

        }catch (Exception $e){
            $this->messageManager->addError($e->getMessage());
            $resultRedirect->setPath('*/*');
            return $resultRedirect;
        }
    }
}
