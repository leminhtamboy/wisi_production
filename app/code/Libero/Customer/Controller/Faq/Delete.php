<?php
namespace Libero\Customer\Controller\Faq;
use Magento\Framework\App\Action\Context;

class Delete extends \Magento\Framework\App\Action\Action
{
    protected $resultPageFactory;
    protected $productRepository;
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Catalog\Model\ProductRepository $productRepository
    ){
        $this->productRepository = $productRepository;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->get("Libero\Customer\Block\Seller\Admin\Util");
        $util->checkSellerLogin();
        $requestHttp = $objectManager->get("\Magento\Framework\App\Request\Http");
        $faq_id = $requestHttp->getParam("id");
        $registry = $objectManager->get('\Magento\Framework\Registry');
        $registry->register('isSecureArea', true);
        $modelFAQ = $objectManager->get("\Libero\Customer\Model\Faq")->load($faq_id);
        $modelFAQ->delete();
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('customer/faq/index/');
        return $resultRedirect;
    }
}
