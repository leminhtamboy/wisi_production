<?php
namespace Libero\Customer\Block\Buyer;

use Magento\Framework\View\Element\Template;

class Form extends \Magento\Review\Block\Form{


    /**
     * Function kiểm tra sku của product đã có tồn tại trong order completed của customer id nhất định
     *
     * @param [type] $sku
     * @param [type] $customer_id
     * @return void
     */
    public function checkCompletedOrder($sku,$customer_id,$product_id){
        //Check sku product và customer id có tồn tại trong item order chưa
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $sql = "SELECT `status` FROM `seller_sales_order` 
        right join `seller_sales_order_item` 
        on seller_sales_order.`entity_id` = seller_sales_order_item.order_id 
        WHERE `customer_id` = '$customer_id' AND seller_sales_order_item.`sku` like '$sku%' AND `status` = 'Complete'";
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $data_order = $connection->fetchAll($sql);
        $array_ret = array();
        if(count($data_order) > 0){
            //check đã rating hay chưa
            $sql_check_da_rate = "SELECT `main_table`.*, `detail`.`detail_id`, `detail`.`title`, `detail`.`detail`, `detail`.`nickname`, `detail`.`customer_id` FROM `review` AS `main_table` INNER JOIN `review_detail` AS `detail` ON main_table.review_id = detail.review_id WHERE (`entity_pk_value` IN('$product_id')) AND (main_table.status_id=1) AND (`customer_id`= '$customer_id')";
            $data_check_da_rate = $connection->fetchAll($sql_check_da_rate);
            $count_check_rate = count($data_check_da_rate);
            if($count_check_rate > 0 ){
                $array_ret["error"] = "2";
                $array_ret["msg"] = "Bạn đã đánh giá sản phẩm này.";
            }else{
                $array_ret["error"] = "1";
                $array_ret["msg"] = "";
            }
        }else{
            $array_ret["error"] = "3";
            $array_ret["msg"] = "Bạn không thể viết nhận xét vì bạn chưa mua mặt hàng này.";
        }
        return $array_ret;
        //Check tiếp tình trạng của order id là complete mới gọi là ok

    }

    public function checkRatingOneTime($sku,$customer_id)
    {

    }
    /**
     * Function kiểm tra đã login chưa
     *
     * @return void
     */
    public function checkLogin()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('\Magento\Customer\Model\Session');
        $customerId = null;
        $array_check_login = array ();
        if($customerSession->isLoggedIn()){
            $customerId = $customerSession->getCustomerId();
            $customerData = $customerSession->getCustomer();
            $array_check_login["da_login"] = true;
            $array_check_login["customer_id"] = $customerId;
            $array_check_login["customer_name"] = $customerData->getName();

        }else{
            $array_check_login["da_login"] = false;
            $array_check_login["customer_id"] = "-1";
            $array_check_login["customer_name"] = "";
        }
        return json_encode($array_check_login);
    }
}