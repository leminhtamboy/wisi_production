<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Sales order view items block
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace Libero\Customer\Block\Buyer;

class Items extends \Magento\Sales\Block\Items\AbstractItems
{
    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * Order items per page.
     *
     * @var int
     */
    private $itemsPerPage;

    /**
     * Sales order item collection factory.
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory
     */
    private $itemCollectionFactory;

    /**
     * Sales order item collection.
     *
     * @var \Magento\Sales\Model\ResourceModel\Order\Item\Collection|null
     */
    private $itemCollection;

    protected $_requestHttp;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     * @param \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory|null $itemCollectionFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = [],
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $itemCollectionFactory = null,
        \Magento\Framework\App\Request\Http $requestHttp
    ) {
        $this->_coreRegistry = $registry;
        $this->itemCollectionFactory = $itemCollectionFactory ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory::class);
        $this->_requestHttp = $requestHttp;
        parent::__construct($context, $data);
    }

    public function getSellerInfoByOrder(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $increment_id = $this->_requestHttp->getParam("increment_id");

        $resource = $objectManager->get("\Magento\Framework\App\ResourceConnection");
        $customer = $objectManager->get("\Magento\Customer\Block\Account\Dashboard\Info");

        $connection = $resource->getConnection();
        $customerId = $customer->getCustomer()->getId();

        $sql = "
            SELECT 
                sso.status, sso.entity_id, sso.base_shipping_amount, sso.shipping_description, lcsc.id_seller_company, lcsc.store_name,sso.order_code_api as ma_giao_hang
            FROM
                seller_sales_order as sso
                JOIN libero_customer_seller_company as lcsc
                on sso.seller_id = lcsc.id_seller_company
            WHERE
                sso.customer_id = '$customerId'
                    AND sso.increment_id = '$increment_id';
        ";
        $result = $connection->fetchAll($sql);
        return $result;
    }


    public function getOrderItemByOrderId($order_id){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get("\Magento\Framework\App\ResourceConnection");
        $connection = $resource->getConnection();

        $sql = "
            SELECT 
                *
            FROM
                seller_sales_order as sso
                JOIN 
                seller_sales_order_item as ssoi
                on sso.entity_id = ssoi.order_id
            WHERE
                ssoi.order_id = $order_id
            ;
        ";

        $result = $connection->fetchAll($sql);
        return $result;
    }

    public function getOrdersSummaryByCustomer(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $increment_id = $this->_requestHttp->getParam("increment_id");

        $resource = $objectManager->get("\Magento\Framework\App\ResourceConnection");
        $customer = $objectManager->get("\Magento\Customer\Block\Account\Dashboard\Info");

        $connection = $resource->getConnection();
        $customerId = $customer->getCustomer()->getId();

        $get_status_sql = "SELECT status FROM seller_sales_order_grid WHERE customer_id = '$customerId' AND increment_id = '$increment_id';";
        $get_status_result = $connection->fetchAll($get_status_sql);
        $isNotCanceled = false;
        foreach ($get_status_result as $status) {
            if ($status["status"] != "cancel") {
                $isNotCanceled = true;
            }
        }

        $result_sql = "
            SELECT 
                *, SUM(shipping_and_handling) as sum_shipping_and_handling, SUM(subtotal) as sum_subtotal, SUM(base_grand_total) AS sum_base_grand_total
            FROM
                seller_sales_order_grid
            WHERE
                customer_id = $customerId
                AND increment_id = '$increment_id' ";
        if ($isNotCanceled) {
            $result_sql .= "AND status != 'cancel' ";
        }
        $result_sql .= "GROUP BY increment_id;";
        //echo $result_sql;die;
        $result = $connection->fetchAll($result_sql);
        return $result;
    }
    public function getUrlCancelOrder()
    {
        return $this->getUrl("customer/buyer/cancelorder");
    }
}
