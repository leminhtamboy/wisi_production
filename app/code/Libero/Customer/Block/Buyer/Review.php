<?php
namespace Libero\Customer\Block\Buyer;

use Magento\Framework\View\Element\Template;

class Review extends \Magento\Review\Block\Product\Review{


    /**
     * Function kiểm tra sku của product đã có tồn tại trong order completed của customer id nhất định
     *
     * @param [type] $sku
     * @param [type] $customer_id
     * @return void
     */
    public function checkCompletedOrder($sku,$customer_id){
        //Check sku product và customer id có tồn tại trong item order chưa
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $sql = "SELECT `status` FROM `seller_sales_order` 
        right join `seller_sales_order_item` 
        on seller_sales_order.`entity_id` = seller_sales_order_item.order_id 
        WHERE `customer_id` = '$customer_id' AND seller_sales_order_item.`sku` = '$sku' AND `status` = 'Complete'";
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $data_order = $connection->fetchAll($sql);
        if(count($data_order) > 0){
            return true;
        }
        return false;
        //Check tiếp tình trạng của order id là complete mới gọi là ok

    }

    /**
     * Function kiểm tra đã login chưa
     *
     * @return void
     */
    public function checkLogin()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('\Magento\Customer\Model\Session');
        $customerId = null;
        $array_check_login = array ();
        if($customerSession->isLoggedIn()){
            $customerId = $customerSession->getCustomerId();
            $array_check_login["da_login"] = true;
            $array_check_login["customer_id"] = $customerId;
        }else{
            $array_check_login["da_login"] = false;
            $array_check_login["customer_id"] = "-1";
        }
        return json_encode($array_check_login);
    }

    public function getDataRating($product_id)
    {
        $result = $items = $starData = [];      
        $idArray = array($product_id);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $reviewsCollection = $objectManager->create("\Magento\Review\Model\ResourceModel\Review\CollectionFactory")->create()
            ->addFieldToFilter('entity_pk_value',array("in" => $idArray))
            ->addStatusFilter(\Magento\Review\Model\Review::STATUS_APPROVED)
            ->addRateVotes();
        $reviewsCollection->getSelect();
        foreach ($reviewsCollection->getItems() as $review) {
            foreach( $review->getRatingVotes() as $_vote ) {
                $rating = [];
                $percent = $_vote->getPercent();
                $star = ($percent/20);
                $productId = $_vote->getEntityPkValue();

                $productModel =  $objectManager->create("\Magento\Catalog\Model\ProductFactory")->create();
                $product = $productModel->load($productId);             
                $countReview = $objectManager->create("\Magento\Review\Model\ReviewFactory")->create()->getTotalReviews($productId,false);
                $review_id = $_vote->getReviewId();

                $rating['review_id'] = $review_id;
                $rating['product_id'] = $productId;     
                $rating['percent']   = $percent;
                $rating['star']      = $star;
                $rating['nickname']  = $review->getNickname();                  
                $items[] = $rating;
                $starData[$star][] = $rating;
            }

        }
        if(count($items) > 0)   {           
            $result['all'] = $items;
            $result['star'] = $starData;            

            if(isset($starData[1]))
                $result ['count'][1] = count($starData[1]);
            else
                $result ['count'][1] = 0;

            if(isset($starData[2]))
                $result ['count'][2] = count($starData[2]);
            else
                $result ['count'][2] = 0;

            if(isset($starData[3]))
                $result ['count'][3] = count($starData[3]);
            else
                $result ['count'][3] = 0;

            if(isset($starData[4]))
                $result ['count'][4] = count($starData[4]);
            else
                $result ['count'][4] = 0;

            if(isset($starData[5]))
                $result ['count'][5] = count($starData[5]);
            else
                $result ['count'][5] = 0;

            $sum = $startSum = 0;

            foreach($result['count'] as $number => $count) {
                $sum += $number * $count;
                $startSum +=  $count;   
            }

            $avg =  $sum/$startSum;

            $result ['count']['all'] = count($result['all']);
            $result ['count']['avg'] = round($avg,1);
        }else{
            $result ['count'][1] = 0;
            $result ['count'][2] = 0;
            $result ['count'][3] = 0;
            $result ['count'][4] = 0;
            $result ['count'][5] = 0;
            $result ['count']['all'] = 0;
            $result ['count']['avg'] = 0;
        }
        return $result; 
    }
}