<?php
namespace Libero\Customer\Block\Buyer;

class Order extends \Magento\Framework\View\Element\Template
{

    protected $_resource;
    protected $_customer;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Customer\Block\Account\Dashboard\Info $customer,
        array $data = [])
    {
        $this->_resource = $resourceConnection;
        $this->_customer = $customer;
        parent::__construct($context, $data);
    }

    public function getOrdersByCustomer(){
        $connection = $this->_resource->getConnection();
        $customerId = $this->_customer->getCustomer()->getId();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->create("\Libero\Customer\Block\Seller\Admin\Util");
        $requestHttp = $objectManager->create("\Magento\Framework\App\Request\Http");

        $pageSize = 20;
        $curPage= 1;

        if($requestHttp->getParam("pageSize")) {
            $pageSize = $requestHttp->getParam("pageSize");
        }

        if($requestHttp->getParam("page")) {
            $curPage = $requestHttp->getParam("page");
        }

        $sql = "
            SELECT 
                *, SUM(base_grand_total) AS base_grand_total
            FROM
                seller_sales_order_grid
            WHERE
                customer_id = $customerId
            GROUP BY increment_id
            ORDER BY created_at DESC
        ";

        $allRecords = $connection->fetchAll($sql);
        $total = $util->getTotalArray($allRecords);
        $lastPage = $util->getLastPageByTotal($total,$pageSize);
        $curPage = ($util->getCurPageArray() < $lastPage) ? $util->getCurPageArray() : $lastPage;
        $curPage = ($curPage > 0) ? $curPage : 1;

        $sql .= " LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";
        
        $orders = $connection->fetchAll($sql);
        $firstNumber = $util->getFirstNumArray($orders);

        $result = array(
            "total" => $total,
            "pageSize" => $pageSize,
            "curPage" => $curPage,
            "lastPage" => $lastPage,
            "listRecords" => $orders,
            "firstNumber" => $firstNumber
        );
        return $result;
    }

    public function getOrderStatusByIncrementId($increment_id, $orderDesc = true){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get("\Magento\Framework\App\ResourceConnection");
        $connection = $resource->getConnection();

        //$sql = "SELECT seller_sales_order.status as tinh_trang FROM seller_sales_order inner join seller_sales_order_grid on seller_sales_order.entity_id = seller_sales_order_grid.order_id WHERE seller_sales_order_grid.increment_id = '$increment_id' ";
        $sql = "SELECT status FROM seller_sales_order WHERE increment_id = '$increment_id' ";
        if($orderDesc){
            $sql .= "ORDER BY `created_at` DESC";
        }
        $sql .= ";";
        $result = $connection->fetchAll($sql);
        /* $status = "Request to packaging";
        $isProcessing = false;
        foreach ($result as $sts) {
            if($sts["tinh_trang"] == "pending") {
                return "pending";
            } else if ($sts["tinh_trang"] == "processing") {
                return "processing";
            } else if ($sts["tinh_trang"] == "complete") {
                return "Complete";
            } else if($sts["tinh_trang"] == "Prepare to ship"){
                return "Prepare to ship";
            }
            else if($sts["tinh_trang"] == "cancel"){
                return "Cancel";
            }
        } */
        $isPending = true;
        $isComplete = true;
        $isCancel = true;
        $isProcessing = false;
        //$result1 = ["pending","pending", "pending"];
        foreach ($result as $sts) {
            if($sts["status"] != "pending" && $sts["status"] != "Pending"){
                $isPending = false;
            }

            if($sts["status"] != "complete" && $sts["status"] != "Complete"){
                $isComplete = false;
            }

            if($sts["status"] != "cancel" && $sts["status"] != "Cancel"){
                $isCancel = false;
            }
        }
        $status = "";
        if ($isPending) {
            $status = "Pending";
        } else if ($isComplete) {
            $status = "Complete";
        } else if ($isCancel) {
            $status = "Cancel";
        } else {
            $status = "Processing";
        }
        return $status;
    }

}