<?php
namespace Libero\Customer\Block\Buyer;

use Magento\Framework\View\Element\Template;

class View extends \Magento\Framework\View\Element\Template{

    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }
    public function sendSms(){

    }
    public function sendEmail(){

    }
    public function getPostActionUrl(){
        return $this->getUrl("customer/buyer/save");
    }
    public function getPostActionSocialUrl(){
        return $this->getUrl("customer/buyer/getinfo");
    }
    public function getPostActionGetOtp(){
        return $this->getUrl("customer/buyer/getcode");
    }
    public function randomTokenonFormToSendOtp(){
        //Get Model random
        $stringRamdon = "Token:".rand(1,999999);
        return base64_encode($stringRamdon);
    }
    public function getDataFromRegistry(){
        $objectRegistryManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelRegistry = $objectRegistryManager->get("Magento\Customer\Model\Session");
        if($modelRegistry->getData('data_info_customer')) {
            return $modelRegistry->getData('data_info_customer');
        }else{
            return null;
        }
    }

}