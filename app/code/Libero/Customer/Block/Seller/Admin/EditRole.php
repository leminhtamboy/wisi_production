<?php
namespace Libero\Customer\Block\Seller\Admin;

use Magento\Framework\View\Element\Template;

class EditRole extends \Magento\Customer\Block\Account\Dashboard\Info{

    protected $_objectManager = null;
    
    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }
    public function getDetailRole()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            //Save  ACL
        $injectHTTP = $objectManager->get("\Magento\Framework\App\Request\Http");
        $id_role = $injectHTTP->getParam("id");
        $modelAccountAcl = $objectManager->create("\Libero\Customer\Model\Acl")->load($id_role);
        return $modelAccountAcl;
    }
    public function getSaveRole()
    {
        return $this->getUrl("customer/admin/saveroleedit");
    }
    public function getBackAction()
    {
        return $this->getUrl("customer/admin/acllisting");
    }
}