<?php
namespace Libero\Customer\Block\Seller\Admin;

class ShipmentListing extends \Magento\Framework\View\Element\Template
{

    protected $_resource;

    protected $_requestHttp;

    protected $_adminUtil;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\Request\Http $requestHttp,
        \Libero\Customer\Block\Seller\Admin\Util $adminUtil,
        array $data = [])
    {
        $this->_resource = $resourceConnection;
        $this->_requestHttp = $requestHttp;
        $this->_adminUtil = $adminUtil;
        parent::__construct($context, $data);
    }
    public function getShipments()
    {
        $id_seller = $this->_adminUtil->getIdSeller();
        $sql = "SELECT * FROM seller_sales_shipment_grid inner join seller_sales_order_grid on `seller_sales_shipment_grid`.order_id = `seller_sales_order_grid`.order_id   WHERE seller_sales_shipment_grid.seller_id = '$id_seller' ORDER BY seller_sales_shipment_grid.entity_id DESC;";
        $connection = $this->_resource->getConnection();
        $result = $connection->fetchAll($sql);
        return $result;
    }
    public function getOrderDetail($order_id = null){
        $connection = $this->_resource->getConnection();
        if ($order_id == null) {
            $order_id = $this->_requestHttp->getParam("order_id");
        }
        $id_seller = $this->_adminUtil->getIdSeller();
        if( $id_seller == null) {
            echo __("id_seller is null");
        }
        $sql = "
            SELECT
                *, ssoi.qty_ordered AS qty, ssoi.item_id AS order_item_id
            FROM
                seller_sales_order_grid AS ssog
                    JOIN
                seller_sales_order AS sso
                    JOIN
                seller_sales_order_item AS ssoi
                    ON ssog.order_id = ssoi.order_id
                    AND sso.entity_id = ssoi.order_id
            WHERE
                ssog.seller_id = '$id_seller'
                AND ssog.order_id = '$order_id'
            ;
            ";
        $result = $connection->fetchAll($sql);
        return $result;
    }
    public function getUrlSaveShipment()
    {
        return $this->getUrl("customer/admin/saveshipment");
    }
    public function getAddress()
    {
        $connection = $this->_resource->getConnection();
        $order_id = $this->_requestHttp->getParam("order_id");
        $sql = "SELECT * FROM seller_sales_order_grid WHERE order_id = '$order_id'";
        $result = $connection->fetchAll($sql);
        return $result;
    }
    public function getTelephone($customer_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($customer_id);
        $customerAddress = array();
        foreach ($customerObj->getAddresses() as $address)
        {
            $customerAddress[] = $address->toArray();
        }
        if(count($customerAddress) > 0 )
        {
            $item = $customerAddress[0]; 
            return $item["telephone"];
        }
        return "";
               
    }
    public function getOrderDetailTotal(){
        $connection = $this->_resource->getConnection();
        $order_id = $this->_requestHttp->getParam("order_id");
        $id_seller = $this->_adminUtil->getIdSeller();
        if( $id_seller == null) {
            echo __("id_seller is null");
        }
        $sql = "
            SELECT
                SUM(ssoi.row_total) AS row_total,
                ssog.shipping_and_handling
            FROM
                seller_sales_order_grid AS ssog
                    JOIN
                seller_sales_order AS sso
                    JOIN
                seller_sales_order_item AS ssoi
                    ON ssog.order_id = ssoi.order_id
                    AND sso.entity_id = ssoi.order_id
            WHERE
                ssog.seller_id = '$id_seller'
                AND ssog.order_id = '$order_id'
            ;
        ";
        $result = $connection->fetchAll($sql);
        return $result;
    }
    public function getItemsInOrder()
    {
        $connection = $this->_resource->getConnection();
        $order_id = $this->_requestHttp->getParam("order_id");
        $id_seller = $this->_adminUtil->getIdSeller();
        $sql = "SELECT * FROM seller_sales_order_item WHERE order_id = '$order_id';";
        $result = $connection->fetchAll($sql);
        return $result;
    }
}




