<?php
namespace Libero\Customer\Block\Seller\Admin;

use Magento\Framework\View\Element\Template;

class Delivery extends \Magento\Customer\Block\Account\Dashboard\Info{

    protected $_objectManager = null;
    protected $_productImageHelper = null;
    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }

    /**
     * Function get collection product
     *
     * @return void
     */
    public function getCollectionShipping($id_seller)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productDependency = $this->_objectManager->create("Libero\Onestepcheckout\Model\Shipping");
        $collectionProduct = $productDependency->getCollection()->addFieldToSelect("*")->addFieldToFilter("id_seller",array("eq" => $id_seller));
        return $collectionProduct;
    }
    public function getUrlSaveShipping()
    {
        return $this->getUrl("customer/admin/saveshipping");
    }
    public function getSaveProduct()
    {
        return $this->getUrl("customer/admin/saveproduct");
    }
    public function getUrlRemoveDelivery($id_shipping)
    {
        return $this->getUrl("customer/admin/removeshipping/id_shipping/".$id_shipping);

    }
}