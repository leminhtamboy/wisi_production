<?php
namespace Libero\Customer\Block\Seller\Admin;

class Order extends \Magento\Framework\View\Element\Template
{

    protected $_resource;

    protected $_adminUtil;

    protected $_table_provider_order = "seller_sales_order";

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Libero\Customer\Block\Seller\Admin\Util $adminUtil,
        array $data = [])
    {
        $this->_resource = $resourceConnection;
        $this->_adminUtil = $adminUtil;
        parent::__construct($context, $data);
    }

    public function processingDate($input_date){
        $arrayDate= explode("/",$input_date);
        return $arrayDate[2]."-".$arrayDate[0]."-".$arrayDate[1];
    }
    /**
     * Function get connectio database directly for case lazy create model
     * @return mixed
     */
    public function getConnectionDatabaseDirect()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        return $connection;
    }
    public function getOrders($pageSize = 20, $curPage= 1){
        $connection = $this->_resource->getConnection();
        $id_seller = $this->_adminUtil->getIdSeller();
        $id = "";
        $status = "";
        $date_from = "";
        $date_to = "";

        if( $id_seller == null) {
            echo __("id_seller is null");
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->create("\Libero\Customer\Block\Seller\Admin\Util");
        $requestHttp = $objectManager->create("\Magento\Framework\App\Request\Http");

        if($requestHttp->getParam("id")){
            $id = $requestHttp->getParam("id");
        }
        
        if($requestHttp->getParam("order_status")){
            $status = $requestHttp->getParam("order_status");
        }
        
        if($requestHttp->getParam("date_from")){
            $date_from = $requestHttp->getParam("date_from");
        }
        
        if($requestHttp->getParam("date_to")){
            $date_to = $requestHttp->getParam("date_to");
        }

        if($requestHttp->getParam("pageSize")) {
            $pageSize = $requestHttp->getParam("pageSize");
        }

        if($requestHttp->getParam("page")) {
            $curPage = $requestHttp->getParam("page");
        }


        $sql ="SELECT *,seller_sales_order.status as status_parent, seller_sales_order.paid_status as paid_status FROM seller_sales_order inner join  seller_sales_order_grid on seller_sales_order.entity_id = seller_sales_order_grid.order_id  where seller_sales_order.seller_id = '$id_seller' ";
        if($id != ""){
            $array_ids = explode("-",$id);
            if(count($array_ids) > 0){
                $real_id = $array_ids[1];
                $sql.= "AND `seller_sales_order`.`entity_id` like '%$real_id%'";
            }
        }
        if($status !=  "-1" && $status !=  ""){
            $sql.= "AND seller_sales_order.`status` = '$status' ";
        }
        if($date_from != "" && $date_to != ""){
            $dateProcessFrom = $this->processingDate($date_from) . " 00:00:00";
            $dateProcessTo = $this->processingDate($date_to) . " 23:59:59";
            $sql.=" AND seller_sales_order.updated_at >= '$dateProcessFrom' AND seller_sales_order.updated_at <= '$dateProcessTo' ";
        }
        
        $allRecords = $connection->fetchAll($sql);


        $total = $util->getTotalArray($allRecords);
        $lastPage = $util->getLastPageByTotal($total,$pageSize);
        $curPage = ($util->getCurPageArray() < $lastPage) ? $util->getCurPageArray() : $lastPage;
        $curPage = ($curPage > 0) ? $curPage : 1;
        
        $sql .= "ORDER BY seller_sales_order.updated_at DESC LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";

        $orders = $connection->fetchAll($sql);
        $firstNumber = $util->getFirstNumArray($orders);

        $result = array(
            "total" => $total,
            "pageSize" => $pageSize,
            "curPage" => $curPage,
            "lastPage" => $lastPage,
            "listRecords" => $orders,
            "firstNumber" => $firstNumber,
            "id" => $id,
            "order_status" => $status,
            "date_from" => $date_from,
            "date_to" => $date_to
        );
        return $result;
    }

    /**
     * Name : Nam
     * Date : 22/11/2018
     * Comments : get balance list
     * Filename : 
     * Param : pageSize, curPage
     */
    public function getBalanceList($pageSize = 20, $curPage= 1){
        $connection = $this->_resource->getConnection();
        $id_seller = $this->_adminUtil->getIdSeller();
        $id = "";
        $status = "";
        $date_from = "";
        $date_to = "";

        if( $id_seller == null) {
            echo __("id_seller is null");
        }

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->create("\Libero\Customer\Block\Seller\Admin\Util");
        $requestHttp = $objectManager->create("\Magento\Framework\App\Request\Http");

        if($requestHttp->getParam("id")){
            $id = $requestHttp->getParam("id");
        }
        
        if($requestHttp->getParam("status")){
            $status = $requestHttp->getParam("status");
        }
        
        if($requestHttp->getParam("date_from")){
            $date_from = $requestHttp->getParam("date_from");
        }
        
        if($requestHttp->getParam("date_to")){
            $date_to = $requestHttp->getParam("date_to");
        }

        if($requestHttp->getParam("pageSize")) {
            $pageSize = $requestHttp->getParam("pageSize");
        }

        if($requestHttp->getParam("page")) {
            $curPage = $requestHttp->getParam("page");
        }


        $sql ="SELECT * FROM libero_provider_balance WHERE provider_id = '$id_seller' ";
        
        if($id != ""){
            $sql.= "AND `id` = '$id' ";
        }

        if($status !=  "-1" && $status !=  ""){
            $sql.= "AND `status` = '$status' ";
        }

        if($date_from != "" && $date_to != ""){
            $dateProcessFrom = $this->processingDate($date_from) . " 00:00:00";
            $dateProcessTo = $this->processingDate($date_to) . " 23:59:59";
            $sql.=" AND complete_date >= '$dateProcessFrom' AND complete_date <= '$dateProcessTo' ";
        }
        
        $allRecords = $connection->fetchAll($sql);


        $total = $util->getTotalArray($allRecords);
        $lastPage = $util->getLastPageByTotal($total,$pageSize);
        $curPage = ($util->getCurPageArray() < $lastPage) ? $util->getCurPageArray() : $lastPage;
        $curPage = ($curPage > 0) ? $curPage : 1;
        
        $sql .= " ORDER BY complete_date, reply_request_date DESC LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";

        $orders = $connection->fetchAll($sql);
        $firstNumber = $util->getFirstNumArray($orders);

        $result = array(
            "total" => $total,
            "pageSize" => $pageSize,
            "curPage" => $curPage,
            "lastPage" => $lastPage,
            "listRecords" => $orders,
            "firstNumber" => $firstNumber,
            "id" => $id,
            "status" => $status,
            "date_from" => $date_from,
            "date_to" => $date_to
        );
        return $result;
    }

    /**
     * Function Searching 
     *
     * @return void
     */
    public function getUrlSearchOrder(){
        return $this->getUrl("customer/admin/order");
    }
    /**
     * Undocumented function
     *
     * @return void
     */
    protected function getObjectManager()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }
    /**
     * Undocumented function
     *
     * @param [type] $provider_id
     * @return void
     */
    public function getListInvoiceProvider()
    {
        $id_seller = $this->_adminUtil->getIdSeller();
        $objectManager = $this->getObjectManager();
        $database = $this->getConnectionDatabaseDirect();
        $sql_get_order_provider = "SELECT * FROM  ".$this->_table_provider_order." where `status` = 'Request to packaging' and `seller_id` = '$id_seller'  order by entity_id desc";
        $data = $database->fetchAll($sql_get_order_provider);
        return $data;
    }

    public function getUrlInvoice($id)
    {
        return $this->getUrl("customer/admin/newinvoice/order_id/$id");
    }
    public function getMassInvoice()
    {
        $route = "customer/admin/massinvoice/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getPrintPackagingMassAction()
    {
        $route = "customer/admin/massprintpackaging/";
        $params = [];
        return $this->getUrl($route, $params);
    }
}


