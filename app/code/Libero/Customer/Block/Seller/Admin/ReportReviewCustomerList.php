<?php
namespace Libero\Customer\Block\Seller\Admin;

use Magento\Framework\View\Element\Template;

class ReportReviewCustomerList extends \Magento\Customer\Block\Account\Dashboard\Info
{

    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        return $sellerObj;
    }

    public function getCustomerReviewDetail($seller_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $objectManager->get("\Magento\Framework\App\Request\Http");
        $customer_id = $injectHTTP->getParam("id");

        $reviewModel = $objectManager->create("Libero\Marketplace\Model\SellerReview");
        $collectionReview = $reviewModel
                                ->getCollection()
                                ->addFieldToFilter("seller_id",array("eq" => $seller_id));
        if($customer_id != NULL){
            $collectionReview->addFieldToFilter("customer_id",array("eq" => $customer_id));
        }else{
            $collectionReview->addFieldToFilter("customer_id",array("null" => true));
        }
                                
        return $collectionReview;
    }

    public function getReportCustomerListUrl()
    {
        return $this->getUrl('customer/admin/reportreviewcustomer');
    }
}



