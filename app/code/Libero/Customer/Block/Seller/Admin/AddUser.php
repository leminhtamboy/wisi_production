<?php
namespace Libero\Customer\Block\Seller\Admin;

use Magento\Framework\View\Element\Template;

class AddUser extends \Magento\Customer\Block\Account\Dashboard\Info{

    protected $_objectManager = null;
    
    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }
    public function getListRole($id_seller)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //Save  ACL
        $modelAccountAcl = $objectManager->create("\Libero\Customer\Model\Acl");
        $dataAcl = $modelAccountAcl->getCollection()->addFieldToSelect("*")->addFieldToFilter("id_seller",array("eq" => $id_seller));
        return $dataAcl;
    }
    public function getSaveUser()
    {
        return $this->getUrl("customer/admin/saveuser");
    }
    public function getBackAction()
    {
        return $this->getUrl("customer/admin/accountlisting");
    }
}