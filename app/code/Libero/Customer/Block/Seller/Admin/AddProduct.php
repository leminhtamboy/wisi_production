<?php
namespace Libero\Customer\Block\Seller\Admin;

use Magento\Framework\View\Element\Template;

class AddProduct extends \Magento\Customer\Block\Account\Dashboard\Info{

    protected $_objectManager = null;
    protected $_productImageHelper = null;
    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }

    /**
     * Function get collection product
     *
     * @return void
     */
    public function getCollectionProduct($id_seller)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productDependency = $this->_objectManager->create("Magento\Catalog\Model\Product");
        $collectionProduct = $productDependency->getCollection()
        ->addAttributeToSelect("*")
        ->addAttributeToFilter("id_seller",array("eq" => $id_seller))
        ->addAttributeToFilter("visibility",array("eq" => 4));
        return $collectionProduct;
    }
    public function getDetailProduct()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $this->_objectManager->get("\Magento\Framework\App\Request\Http");
        $id_product = $injectHTTP->getParam("id");
        $injectProduct = $this->_objectManager->create("Magento\Catalog\Model\Product");
        $dataProduct = $injectProduct->load($id_product);
        return $dataProduct; 
    }
    public function getStock($id_product)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productStockObj = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface')->getStockItem($id_product);
        return $productStockObj;
    }
    /**
     * Get Collection shipping by seller id
     *
     * @param [type] $id_seller
     * @return void
     */
    public function getCollectionShipping($id_seller)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collectionShipping = $objectManager->get('\Libero\Onestepcheckout\Model\Shipping')->getCollection()->addFieldToFilter("id_seller",array("eq" => $id_seller));
        return $collectionShipping;
    }
    public function resizeImage($product, $imageId, $width, $height = null)
    {
        
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $this->_productImageHelper = $objectManager->get("\Magento\Catalog\Helper\Image");
        $resizedImage = $this->_productImageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }
    public function getDetailCategory($id_category)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
        $category = $categoryFactory->create()->load($id_category);
        return $category;

    }
    public function getListCategoryOfParentCategory($id_category)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $categoryFactory = $objectManager->get('\Magento\Catalog\Model\CategoryFactory');
        $category = $categoryFactory->create()->load($id_category);
        $childrenCategories = $category->getChildrenCategories();
        return $childrenCategories;
    }
    public function renderCategory($id_category,$name_category,$category_spec = null,$category = null)
    {
        $sub_category = $this->getListCategoryOfParentCategory($id_category);
        $count_subcategory = count($sub_category);
        $category = $this->getDetailCategory($id_category);
        $commision = $category->getData("commission");
        if($commision == ""){
            $commision = 0;
        }
        if($count_subcategory > 0 )
        {

            ?>
            <li class="admin__action-multiselect-menu-inner-item _parent _hasChild" id="<?php echo $id_category; ?>">
                <div class="action-menu-item _with-checkbox">
                    <div class="admin__action-multiselect-dropdown"></div>
                        <!-- <input class="admin__control-checkbox id_category_chkbox" type="checkbox" name="id_category[]" <?php if(in_array($id_category,$category_spec)) echo "checked='checked'"; ?> value="<?php echo $id_category; ?>" tabindex="-1"> -->
                        <label class="admin__action-multiselect-label pl0"><?php echo __($name_category) ?> <?php echo $commision  ?>%</label>
                        <ul class="admin__action-multiselect-menu-inner" data-level="4" style="display: none;">
                            <?php foreach($sub_category as $_sub) {  ?>
                                <?php $this->renderCategory($_sub->getId(),$_sub->getName(),$category_spec,$category) ?>
                            <?php } ?>
                        </ul>
                </div>
            </li>
        <?php
        }
        else{
            ?>
            <li class="admin__action-multiselect-menu-inner-item" id="<?php echo $id_category; ?>">
                <div class="action-menu-item _last _with-checkbox">
                    <!-- <input class="admin__control-checkbox id_category_chkbox" type="checkbox" name="id_category[]" <?php if(in_array($id_category,$category_spec)) echo "checked='checked'"; ?> value="<?php echo $id_category; ?>" tabindex="-1"> -->
                    <input class="admin__control-checkbox name_id_category_chkbox" type="hidden" name="id_cate_name" <?php if(in_array($id_category,$category_spec)) echo "checked='checked'"; ?> value="<?php echo $name_category; ?>" tabindex="-1">
                    <input class="admin__control-checkbox id_category_chkbox" type="radio" name="id_cate" <?php if(in_array($id_category,$category_spec)) echo "checked='checked'"; ?> value="<?php echo $id_category; ?>" tabindex="-1">
                    <label class="admin__action-multiselect-label"><?php echo __($name_category); ?> <?php echo $commision  ?>%</label>
                </div>
            </li>
        <?php
        }
    }
    /**
     * Get Configruable Product
     *
     * @return void
     */
    public function getConfigruableProduct()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $this->_objectManager->get("\Magento\Framework\App\Request\Http");
        $id_product = $injectHTTP->getParam("id");
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $productFactory = $objectManager->get('\Magento\Catalog\Model\Product');
        $product = $productFactory->load($id_product);
        return $product;
    }
    /**
     * Get Variant Product
     *
     * @param [type] $configProduct
     * @return void
     */
    public function getChildrenProduct($configProduct)
    {
        $_children = $configProduct->getTypeInstance()->getUsedProducts($configProduct);
        return $_children;
    }
    public function getUrlEditProduct()
    {
        return $this->getUrl("customer/admin/saveeditproduct");
    }
    public function getSaveProduct()
    {
        return $this->getUrl("customer/admin/saveproduct");
    }
    public function getSaveConfigruableProduct()
    {
        return $this->getUrl("customer/admin/saveconfigproduct");
    }
    public function getSaveEditConigruableProduct()
    {
        return $this->getUrl("customer/admin/saveeditconfigproduct");
    }
    public function getUrlProductListing()
    {
        return $this->getUrl("customer/admin/productlisting");
    }

    public function getUrlPreviewProduct()
    {
        return $this->getUrl("customer/product/previewproduct");
    }

    
}