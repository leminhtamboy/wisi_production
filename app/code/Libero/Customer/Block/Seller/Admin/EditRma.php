<?php
namespace Libero\Customer\Block\Seller\Admin;

use Magento\Framework\View\Element\Template;

class EditRma extends \Magento\Customer\Block\Account\Dashboard\Info{

    protected $_objectManager = null;
    
    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }
    public function getDetailRma()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $objectManager->get("\Magento\Framework\App\Request\Http");
        $id_rma = $injectHTTP->getParam("id");
        $modelRma = $objectManager->create("\Libero\RMA\Model\RequestProduct")->load($id_rma);
        return $modelRma;
    }
    public function getOrderDetail($id_order)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create("\Magento\Sales\Model\Order")->load($id_order);
        return $order;
    }
    public function getSaveUrl()
    {
        return $this->getUrl("customer/admin/savermaedit");
    }
    public function getBackAction()
    {
        return $this->getUrl("customer/admin/requestrma");
    }
    public function getDetailProduct($id_product)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model_product = $this->_objectManager->create("\Magento\Catalog\Model\Product");
        $product = $model_product->getCollection()->addAttributeToSelect("name")->addAttributeToFilter("entity_id",array("eq" => $id_product))->getFirstItem();
        return $product;
    }
    public function getTextReason($id_reason)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $this->_objectManager->create("\Libero\RMA\Model\RequestReason")->load($id_reason);
        $resutl = $model->getData("reason_label");
        return $resutl;
    }

    public function getTextPackage($id_package)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $this->_objectManager->create("\Libero\RMA\Model\RequestPackage")->load($id_package);
        $resutl = $model->getData("package_label");
        return $resutl;
    }
}