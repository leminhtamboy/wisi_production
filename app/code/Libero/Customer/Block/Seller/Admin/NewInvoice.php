<?php
namespace Libero\Customer\Block\Seller\Admin;

class NewInvoice extends \Magento\Framework\View\Element\Template
{

    protected $_resource;

    protected $_requestHttp;

    protected $_adminUtil;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\Request\Http $requestHttp,
        \Libero\Customer\Block\Seller\Admin\Util $adminUtil,
        array $data = [])
    {
        $this->_resource = $resourceConnection;
        $this->_requestHttp = $requestHttp;
        $this->_adminUtil = $adminUtil;
        parent::__construct($context, $data);
    }
    
    public function getOrderDetail(){
        $connection = $this->_resource->getConnection();
        $order_id = $this->_requestHttp->getParam("order_id");
        $id_seller = $this->_adminUtil->getIdSeller();
        if( $id_seller == null) {
            echo __("id_seller is null");
        }
        $sql = "";
        $sql .="SELECT ";
        $sql .="    * ";
        $sql .="FROM ";
        $sql .="    seller_sales_order_grid AS ssog ";
        $sql .="        JOIN ";
        $sql .="    seller_sales_order AS sso ";
        $sql .="        JOIN ";
        $sql .="    seller_sales_order_item AS ssoi ";
        $sql .="        ON ssog.order_id = ssoi.order_id ";
        $sql .="        AND sso.entity_id = ssoi.order_id ";
        $sql .="WHERE ";
        $sql .="    ssog.seller_id = '$id_seller'";
        $sql .="    AND ssog.order_id = '$order_id'";
        $sql .=";";
        $result = $connection->fetchAll($sql);
        return $result;
    }

    public function getOrderDetailTotal(){
        $connection = $this->_resource->getConnection();
        $order_id = $this->_requestHttp->getParam("order_id");
        $id_seller = $this->_adminUtil->getIdSeller();
        if( $id_seller == null) {
            echo __("id_seller is null");
        }
        $sql = "";
        $sql .="SELECT ";
        $sql .="    SUM(ssoi.row_total) AS row_total, ";
        $sql .="    ssog.shipping_and_handling ";
        $sql .="FROM ";
        $sql .="    seller_sales_order_grid AS ssog ";
        $sql .="        JOIN ";
        $sql .="    seller_sales_order AS sso ";
        $sql .="        JOIN ";
        $sql .="    seller_sales_order_item AS ssoi ";
        $sql .="        ON ssog.order_id = ssoi.order_id ";
        $sql .="        AND sso.entity_id = ssoi.order_id ";
        $sql .="WHERE ";
        $sql .="    ssog.seller_id = '$id_seller'";
        $sql .="    AND ssog.order_id = '$order_id'";
        $sql .=";";
        $result = $connection->fetchAll($sql);
        return $result;
    }

    public function getOrderDetailTotalQty(){
        $connection = $this->_resource->getConnection();
        $order_id = $this->_requestHttp->getParam("order_id");
        $id_seller = $this->_adminUtil->getIdSeller();
        if( $id_seller == null) {
            echo __("id_seller is null");
        }
        $sql = "";
        $sql .="SELECT ";
        $sql .="    SUM(ssoi.qty_ordered) AS qty_ordered ";
        $sql .="FROM ";
        $sql .="    seller_sales_order_grid AS ssog ";
        $sql .="        JOIN ";
        $sql .="    seller_sales_order AS sso ";
        $sql .="        JOIN ";
        $sql .="    seller_sales_order_item AS ssoi ";
        $sql .="        ON ssog.order_id = ssoi.order_id ";
        $sql .="        AND sso.entity_id = ssoi.order_id ";
        $sql .="WHERE ";
        $sql .="    ssog.seller_id = '$id_seller' ";
        $sql .="    AND ssog.order_id = '$order_id' ";
        $sql .=";";
        $result = $connection->fetchAll($sql);
        return $result;
    }

}