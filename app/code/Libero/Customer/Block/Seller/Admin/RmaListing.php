<?php
namespace Libero\Customer\Block\Seller\Admin;

use Magento\Framework\View\Element\Template;

class RmaListing extends \Magento\Customer\Block\Account\Dashboard\Info{

    protected $_objectManager = null;
    protected $_productImageHelper = null;

    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }

    public function processingDate($input_date){
        $arrayDate= explode("/",$input_date);
        return $arrayDate[2]."-".$arrayDate[0]."-".$arrayDate[1];
    }

    /**
     * Function get collection rma by seller id
     *
     * @return void
     */
    public function getCollectionRMA($id_seller, $pageSize = 20, $curPage= 1)
    {
        /* $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelAccountListing = $this->_objectManager->create("\Libero\RMA\Model\RequestProduct");
        $collectionProduct = $modelAccountListing->getCollection()->addFieldToSelect("*")->addFieldToFilter("id_seller",array("eq" => $id_seller));
        return $collectionProduct; */

        
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $this->_objectManager->create("\Magento\Framework\App\ResourceConnection");
        $requestHttp = $this->_objectManager->create("\Magento\Framework\App\Request\Http");
        $util = $this->_objectManager->get('\Libero\Customer\Block\Seller\Admin\Util');

        $customer_name = "";
        $increment_id = "";
        $customer_email = "";
        $product_name = "";
        $status = "";
        $date_from = "";
        $date_to = "";

        if($requestHttp->getParam("customer_name")){ $customer_name = $requestHttp->getParam("customer_name"); }
        if($requestHttp->getParam("increment_id")){ $increment_id = $requestHttp->getParam("increment_id"); }
        if($requestHttp->getParam("customer_email")){ $customer_email = $requestHttp->getParam("customer_email"); }
        if($requestHttp->getParam("product_name")){ $product_name = $requestHttp->getParam("product_name"); }
        if($requestHttp->getParam("status")){ $status = $requestHttp->getParam("status"); }
        if($requestHttp->getParam("date_from")){ $date_from = $requestHttp->getParam("date_from"); }
        if($requestHttp->getParam("date_to")){ $date_to = $requestHttp->getParam("date_to"); }
        

        if($requestHttp->getParam("pageSize")) {
            $pageSize = $requestHttp->getParam("pageSize");
        }

        if($requestHttp->getParam("page")) {
            $curPage = $requestHttp->getParam("page");
        }
        $result= array();

        $connection = $resource->getConnection();
        $sql = "";
        $sql .= "SELECT  ";
        $sql .= "    rrp.id_request AS id, ";
        $sql .= "    rrp.customer_name, ";
        $sql .= "    rrp.customer_email, ";
        $sql .= "    rrp.product_id, ";
        $sql .= "    rrp.price_return, ";
        $sql .= "    rrp.reason, ";
        $sql .= "    rrp.resolution, ";
        $sql .= "    rrp.status, ";
        $sql .= "    rrp.message, ";
        $sql .= "    rrp.package, ";
        $sql .= "    rrp.order_id, ";
        $sql .= "    rrp.create_at, ";
        $sql .= "    rrp.update_at, ";
        $sql .= "    sog.increment_id, ";
        $sql .= "    cdev.value as product_name ";
        $sql .= "FROM ";
        $sql .= "    rma_request_product AS rrp ";
        $sql .= "        JOIN ";
        $sql .= "    sales_order_grid AS sog ";
        $sql .= "        JOIN ";
        $sql .= "    catalog_product_entity_varchar AS cdev ON rrp.order_id = sog.entity_id ";
        $sql .= "        AND rrp.product_id = cdev.entity_id ";
        $sql .= "        AND cdev.attribute_id = 73 ";
        $sql .= "WHERE ";
        $sql .= "    id_seller = $id_seller ";

        if ($customer_name != "") { $sql .= " AND rrp.customer_name like '%$customer_name%' ";}
        if ($increment_id != "") { $sql .= " AND sog.increment_id like '%$increment_id%' ";}
        if ($customer_email != "") { $sql .= " AND rrp.customer_email like '%$customer_email%' ";}
        if ($product_name != "") { $sql .= " AND cdev.value like '%$product_name%' ";}
        if ($status != "") { $sql .= " AND rrp.status = '$status' ";}
        if($date_from != "" && $date_to != ""){
            $dateProcessFrom = $this->processingDate($date_from) . " 00:00:00";
            $dateProcessTo = $this->processingDate($date_to) . " 23:59:59";
            $sql.=" AND rrp.create_at >= '$dateProcessFrom' AND rrp.create_at <= '$dateProcessTo' ";
        }

        $collectionFull = $connection->fetchAll($sql);

        $total = $util->getTotalArray($collectionFull);

        $sql .= " LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";

        $collectionPaging = $connection->fetchAll($sql);
        $lastPage = $util->getLastPageByTotal($total,$pageSize);

        $result = array(
            "total" => $total,
            "pageSize" => $pageSize,
            "curPage" => $curPage,
            "lastPage" => $lastPage,
            "listRecords" => $collectionPaging,
            "customer_name" => $customer_name,
            "increment_id" => $increment_id,
            "customer_email" => $customer_email,
            "product_name" => $product_name,
            "status" => $status,
            "date_from" => $date_from,
            "date_to" => $date_to,
        );
        return $result;        
    }
    public function getUrlEditRma($id_role)
    {
        return $this->getUrl("customer/admin/editrma/id/".$id_role);
    }

    /**
     * Function get detail product
     *
     * @param [type] $id_product
     * @return void
     */
    public function getDetailProduct($id_product)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model_product = $this->_objectManager->create("\Magento\Catalog\Model\Product");
        $product = $model_product->getCollection()->addAttributeToSelect("name")->addAttributeToFilter("entity_id",array("eq" => $id_product))->getFirstItem();
        return $product;
    }
    public function getTextReason($id_reason)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $this->_objectManager->create("\Libero\RMA\Model\RequestReason")->load($id_reason);
        $resutl = $model->getData("reason_label");
        return $resutl;
    }

    public function getTextResolution($id_solution)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $this->_objectManager->create("\Libero\RMA\Model\RequestResolution")->load($id_solution);
        $resutl = $model->getData("resolution_label");
        return $resutl;
    }

    public function getTextStatus($id_status)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model = $this->_objectManager->create("\Libero\RMA\Model\RequestStatus")->load($id_status);
        $resutl = $model->getData("status_label");
        return $resutl;
    }
}