<?php
namespace Libero\Customer\Block\Seller\Admin;

use Magento\Framework\View\Element\Template;

class Faq extends \Magento\Customer\Block\Account\Dashboard\Info{

    protected $_objectManager = null;
    protected $_productImageHelper = null;
    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }

    public function getCollectionFAQ($id_seller,$pageSize = 20, $curPage= 1)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $requestHttp = $this->_objectManager->create("\Magento\Framework\App\Request\Http");
        $util = $this->_objectManager->get("Libero\Customer\Block\Seller\Admin\Util");

        $name = $util->removeQuoteInString($requestHttp->getParam("product_name"));
        $sku = $util->removeQuoteInString($requestHttp->getParam("product_sku"));
        $status = $requestHttp->getParam("product_status");
        $id_shipping = $requestHttp->getParam("id_shipping");
        $product_category = $requestHttp->getParam("product_category");

        if($requestHttp->getParam("pageSize")) {
            $pageSize = $requestHttp->getParam("pageSize");
        }

        if($requestHttp->getParam("page")) {
            $curPage = $requestHttp->getParam("page");
        }

        $collectionSearchProduct = $this->getCollectionSearch();
        $result= array();

        $modelFAQRoom = $this->_objectManager->create("Libero\Customer\Model\FaqRoom");
        $collectionFAQFull = $modelFAQRoom->getCollection()->addFieldToSelect("*")
        ->addFieldToFilter("provider_id", array("eq" => $id_seller));
        if($name != ""){
            $collectionFAQFull = $collectionFAQFull->addFieldToFilter("sender_name",array("like" => "%".$name."%"));
        }
        if($sku != ""){
            $collectionFAQFull = $collectionFAQFull->addFieldToFilter("sku",array("like" => "%".$sku."%"));
        }
        if($status > "-1" && $status !=  ""){
            $collectionFAQFull = $collectionFAQFull->addFieldToFilter("status",array("eq" => $status));
        }
        $total = count($collectionFAQFull);
        $collectionProductPage = $collectionFAQFull->setPageSize($pageSize)->setCurPage($curPage);
        $collectionProductPage->setOrder('id_room', 'DESC');
        $lastPage = $collectionProductPage->getLastPageNumber();
        $result = array(
            "total" => $total,
            "pageSize" => $pageSize,
            "curPage" => $curPage,
            "lastPage" => $lastPage,
            "listRecords" => $collectionProductPage,
            "name" => $name,
            "sku" => $sku,
            "product_status" => $status,
            "id_shipping" => $id_shipping,
            "product_category" => $product_category
        );
        return $result;
    }
    public function getDetailQuestion()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $this->_objectManager->get("\Magento\Framework\App\Request\Http");
        $id_room = $injectHTTP->getParam("id");
        $modelFAQ = $this->_objectManager->create("Libero\Customer\Model\Faq")->addFieldToFilter("id_room",array("eq" => $id_room));
        $dataFAQ = $modelFAQ;
        return $dataFAQ; 
    }
    public function getDetailRoom()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $this->_objectManager->get("\Magento\Framework\App\Request\Http");
        $id_room = $injectHTTP->getParam("id");
        $modelFAQ = $this->_objectManager->create("Libero\Customer\Model\Faq")->getCollection()
        ->addFieldToFilter("id_room",array("eq" => $id_room))
        ->addFieldToFilter("type_faq",array("eq" => "Q"));
        $dataFAQ = $modelFAQ;
        return $dataFAQ; 
    }
    public function getAnswer($parent_question_id)
    {
        $dataFAQ = $this->_objectManager->create("Libero\Customer\Model\Faq")->load($parent_question_id);
        return $dataFAQ;
    }
    public function getUrlAnswerFAQ($faq_id)
    {
        return $this->getUrl("customer/faq/answer/id/".$faq_id);
    }
    public function getBackUrl()
    {
        return $this->getUrl("customer/faq/index/");
    }
    public function getSaveFaq(){
        return $this->getUrl("customer/faq/save/");
    }
    public function getFormKey()
    {

    }
}