<?php
namespace Libero\Customer\Block\Seller\Admin;

use Magento\Framework\View\Element\Template;

class AccountListing extends \Magento\Customer\Block\Account\Dashboard\Info{

    protected $_objectManager = null;
    protected $_productImageHelper = null;

    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }

    /**
     * Function get collection product
     *
     * @return void
     */
    public function getCollectionUsers($id_seller)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelAccountListing = $this->_objectManager->create("\Libero\Customer\Model\Account");
        $collectionProduct = $modelAccountListing->getCollection()->addFieldToSelect("*")->addFieldToFilter("id_seller",array("eq" => $id_seller));
        return $collectionProduct;
    }
    public function getDetailCustomer($id_customer)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerObj = $objectManager->create('Magento\Customer\Model\Customer')->load($id_customer);
        return $customerObj;
    }
    /**
     * 
     */
    public function getDetailCategory($id_category)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $dependency_category = $objectManager->get("\Magento\Catalog\Model\Category")->load($id_category);
        return $dependency_category;
    }
    public function getUrlEditProduct($id_product)
    {
        return $this->getUrl("customer/admin/editproduct/id/".$id_product);
    }
    public function getUrlAddUser()
    {
        return $this->getUrl("customer/admin/adduser");
    }
    public function resizeImage($product, $imageId, $width, $height = null)
    {
        
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $this->_productImageHelper = $objectManager->get("\Magento\Catalog\Helper\Image");
        $resizedImage = $this->_productImageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }
}