<?php
namespace Libero\Customer\Block\Seller\Admin;

class Invoice extends \Magento\Framework\View\Element\Template
{

    protected $_resource;

    protected $_adminUtil;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Libero\Customer\Block\Seller\Admin\Util $adminUtil,
        array $data = [])
    {
        $this->_resource = $resourceConnection;
        $this->_adminUtil = $adminUtil;
        parent::__construct($context, $data);
    }

    public function getInvoices(){
        $connection = $this->_resource->getConnection();
        $id_seller = $this->_adminUtil->getIdSeller();
        if( $id_seller == null) {
            echo __("id_seller is null");
        }

        $id_seller_attribute_sql = "SELECT attribute_id FROM eav_attribute where attribute_code = 'id_seller';";
        $id_seller_attribute = $connection->fetchAll($id_seller_attribute_sql)[0]["attribute_id"];

        $order_id_sql = "";
        $order_id_sql .= "SELECT ";
        $order_id_sql .= "    * ";
        $order_id_sql .= "FROM ";
        $order_id_sql .= "    (seller_sales_order_item AS soi ";
        $order_id_sql .= "       JOIN ";
        $order_id_sql .= "    catalog_product_entity_varchar AS cpev ";
        $order_id_sql .= "       JOIN ";
        $order_id_sql .= "    seller_sales_order_grid AS sog ON ";
        $order_id_sql .= "       soi.product_id = cpev.entity_id ";
        $order_id_sql .= "       AND soi.order_id = sog.order_id) ";
        $order_id_sql .= "WHERE ";
        $order_id_sql .= "    cpev.value = '" . $id_seller . "'";
        $order_id_sql .= "       AND cpev.attribute_id = '$id_seller_attribute' AND  `status`='Processing'";
        $order_id_sql .= "       group by increment_id ";
        $order_id_sql .= ";";
        
        /*$order_id_arr = $connection->fetchAll($order_id_sql);
        $order_id = "";
        if(count($order_id_arr)<=0) {
            return;
        }
        foreach ($order_id_arr as $id) {
            $order_id .= $id['increment_id'] . ",";
        }
        $order_id = substr($order_id,0,strlen($order_id)-1);

        $invoices_sql = "";
        $invoices_sql .= "SELECT ";
        $invoices_sql .= "    * ";
        $invoices_sql .= "FROM ";
        $invoices_sql .= "    seller_sales_invoice_grid ";
        $invoices_sql .= "WHERE ";
        $invoices_sql .= "    order_increment_id IN (" . $order_id . ")";
        $invoices_sql .= "ORDER BY updated_at DESC;";
        $result = $connection->fetchAll($invoices_sql);*/
        $result = $connection->fetchAll($order_id_sql);
        return $result;
    }

}


