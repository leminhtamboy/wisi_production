<?php
namespace Libero\Customer\Block\Seller\Admin;


class ProductListing extends \Magento\Customer\Block\Account\Dashboard\Info{

    protected $_objectManager = null;
    protected $_productImageHelper = null;
    protected $_catalogSession = null;
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
        \Magento\Customer\Helper\View $helperView,
        \Magento\Catalog\Model\Session $catalogSession,
        array $data = []
    ){
        $this->_catalogSession = $catalogSession;
        parent::__construct($context,$currentCustomer,$subscriberFactory,$helperView,$data);
    }
    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }

    /**
     * Function get collection product
     *
     * @return void
     */
    public function getCollectionProduct($id_seller,$pageSize = 20, $curPage= 1)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $requestHttp = $this->_objectManager->create("\Magento\Framework\App\Request\Http");
        $util = $this->_objectManager->get("Libero\Customer\Block\Seller\Admin\Util");

        $name = $util->removeQuoteInString($requestHttp->getParam("product_name"));
        $sku = $util->removeQuoteInString($requestHttp->getParam("product_sku"));
        $status = $requestHttp->getParam("product_status");
        $id_shipping = $requestHttp->getParam("id_shipping");
        $product_category = $requestHttp->getParam("product_category");

        if($requestHttp->getParam("pageSize")) {
            $pageSize = $requestHttp->getParam("pageSize");
        }

        if($requestHttp->getParam("page")) {
            $curPage = $requestHttp->getParam("page");
        }

        $collectionSearchProduct = $this->getCollectionSearch();
        $result= array();

        $productDependency = $this->_objectManager->create("Magento\Catalog\Model\Product");
        $collectionProductFull = $productDependency->getCollection()->addAttributeToSelect("*")->addAttributeToFilter("id_seller", array("eq" => $id_seller))->addAttributeToFilter("visibility",array("eq" => 4));
        
        if($name != ""){
            $collectionProductFull->addAttributeToFilter("name",array("like" => "%".$name."%"));
        }
        if($sku != ""){
            $collectionProductFull->addAttributeToFilter("sku",array("like" => "%".$sku."%"));
        }
        if($status > "-1" && $status !=  ""){
            $collectionProductFull->addAttributeToFilter("status",array("eq" => $status));
        }
        if($id_shipping != "0" && $id_shipping !=  ""){
            $collectionProductFull->addAttributeToFilter("id_shipping",array("eq" => $id_shipping));
        }
        $collectionTotal = clone $collectionProductFull;
        $total = count($collectionTotal);
        $collectionProductFull->setPageSize($pageSize)->setCurPage($curPage);
        $collectionProductFull->setOrder('entity_id', 'DESC');
        $lastPage = $collectionProductFull->getLastPageNumber();
        $result = array(
            "total" => $total,
            "pageSize" => $pageSize,
            "curPage" => $curPage,
            "lastPage" => $lastPage,
            "listRecords" => $collectionProductFull,
            "name" => $name,
            "sku" => $sku,
            "product_status" => $status,
            "id_shipping" => $id_shipping,
            "product_category" => $product_category
        );
        return $result;
    }

    /**
     * 
     */
    public function getDetailCategory($id_category)
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $dependency_category = $objectManager->get("\Magento\Catalog\Model\Category")->load($id_category);
        return $dependency_category;
    }
    public function getUrlEditProduct($id_product)
    {
        return $this->getUrl("customer/admin/editproduct/id/".$id_product);
    }
    public function getUrlEditConfigProduct($id_product)
    {
        return $this->getUrl("customer/admin/editconfigruableproduct/id/".$id_product);
    }
    public function getUrlAddProduct()
    {
        return $this->getUrl("customer/admin/addproduct");
    }
    public function getAddConfigruableProduct()
    {
        return $this->getUrl("customer/admin/addconfigruableproduct");
    }
    public function getUrlDeleteProduct($id_product)
    {
        return $this->getUrl("customer/admin/deleteproduct/id/".$id_product);
    }
    public function getSearchUrl(){
        return $this->getUrl("customer/admin/productlisting");
    }
    public function resizeImage($product, $imageId, $width, $height = null)
    {
        
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $this->_productImageHelper = $objectManager->get("\Magento\Catalog\Helper\Image");
        $resizedImage = $this->_productImageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }

    public function getShippingMethods($id_seller){
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $productDependency = $objectManager->create("Libero\Onestepcheckout\Model\Shipping");
        $collectionProduct = $productDependency->getCollection()->addFieldToSelect("*")->addFieldToFilter("id_seller",array("eq" => $id_seller))->getData();
        return $collectionProduct;
    }

}