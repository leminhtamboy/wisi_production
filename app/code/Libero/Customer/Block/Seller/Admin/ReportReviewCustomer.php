<?php
namespace Libero\Customer\Block\Seller\Admin;

use Magento\Framework\View\Element\Template;

class ReportReviewCustomer extends \Magento\Customer\Block\Account\Dashboard\Info{

    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }
    /**
     * Function get collection review by customer
     *
     * @return void
     */
    public function getCollectionReview($seller_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $objectManager->get("\Magento\Framework\App\Request\Http");
        $reviewModel = $objectManager->create("Libero\Marketplace\Model\SellerReview");
        $collectionReview = $reviewModel
                                ->getCollection()
                                ->addFieldToSelect('customer_id')
                                ->addFieldToSelect('rating')
                                ->addFieldToFilter("seller_id",array("eq" => $seller_id));


        $collectionReview       ->getSelect()
                                ->columns(['customer_id','customer.firstname','COUNT(*) as review_count','SUM(rating) AS rating_count'])
                                ->joinLeft(array("customer" => 'customer_entity'), "customer_id = customer.entity_id",['customer.firstname'])
                                ->group('customer_id');
        return $collectionReview;
    }
   
    public function getCustomerReviewDetailUrl($customer_id)
    {
        return $this->getUrl("customer/admin/reportreviewcustomerlist/id/".$customer_id);
    }
}