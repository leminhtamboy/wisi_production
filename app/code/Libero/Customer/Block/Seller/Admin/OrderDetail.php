<?php
namespace Libero\Customer\Block\Seller\Admin;

class OrderDetail extends \Magento\Framework\View\Element\Template
{

    protected $_resource;

    protected $_requestHttp;

    protected $_adminUtil;


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\Request\Http $requestHttp,
        \Libero\Customer\Block\Seller\Admin\Util $adminUtil,
        array $data = [])
    {
        $this->_resource = $resourceConnection;
        $this->_requestHttp = $requestHttp;
        $this->_adminUtil = $adminUtil;
        parent::__construct($context, $data);
    }
    
    public function getOrderDetail($order_id = null){
        $connection = $this->_resource->getConnection();
        if ($order_id == null) {
            $order_id = $this->_requestHttp->getParam("order_id");
        }
        $id_seller = $this->_adminUtil->getIdSeller();
        if( $id_seller == null) {
            echo __("id_seller is null");
        }
        $sql = "
            SELECT
                *,ssog.customer_name as buyer_name,ssog.shipping_address as dia_chi_giao_hang, ssoi.qty_ordered AS qty, ssoi.item_id AS order_item_id
            FROM
                seller_sales_order_grid AS ssog
                    JOIN
                seller_sales_order AS sso
                    JOIN
                seller_sales_order_item AS ssoi
                    ON ssog.order_id = ssoi.order_id
                    AND sso.entity_id = ssoi.order_id
            WHERE
                ssog.seller_id = '$id_seller'
                AND ssog.order_id = '$order_id'
            ;
            ";
        $result = $connection->fetchAll($sql);
        return $result;
    }
    public function getOrderHistory()
    {
        $connection = $this->_resource->getConnection();
        $order_id = $this->_requestHttp->getParam("order_id");
        $sql_history = "select * from sales_order_seller_logs where order_id = '$order_id'";
        return $connection->fetchAll($sql_history);
    }
    public function getOrderDetailTotal(){
        $connection = $this->_resource->getConnection();
        $order_id = $this->_requestHttp->getParam("order_id");
        $id_seller = $this->_adminUtil->getIdSeller();
        if( $id_seller == null) {
            echo __("id_seller is null");
        }
        $sql = "
            SELECT
                SUM(ssoi.row_total) AS row_total,
                ssog.shipping_and_handling,
                sso.grand_total as tong_cong
            FROM
                seller_sales_order_grid AS ssog
                    JOIN
                seller_sales_order AS sso
                    JOIN
                seller_sales_order_item AS ssoi
                    ON ssog.order_id = ssoi.order_id
                    AND sso.entity_id = ssoi.order_id
            WHERE
                ssog.seller_id = '$id_seller'
                AND ssog.order_id = '$order_id'
            ;
        ";
        $result = $connection->fetchAll($sql);
        return $result;
    }

    public function isReleaseInvoice()
    {
        $connection = $this->_resource->getConnection();
        $order_id = $this->_requestHttp->getParam("order_id");
        $id_seller = $this->_adminUtil->getIdSeller();
        if( $id_seller == null) {
            echo __("id_seller is null");
        }
        $sql = "SELECT * FROM seller_sales_invoice where order_id = '$order_id';";
        $isReleaed = false;
        $result = $connection->fetchAll($sql);
        if(count($result)>0){
            $isReleaed = true;
        }
        return $isReleaed;
    }

    public function isReleaseShipment()
    {
        $connection = $this->_resource->getConnection();
        $order_id = $this->_requestHttp->getParam("order_id");
        $id_seller = $this->_adminUtil->getIdSeller();
        if( $id_seller == null) {
            echo __("id_seller is null");
        }
        $sql = "SELECT * FROM seller_sales_shipment_grid where order_id = '$order_id';";
        $isReleaed = false;
        $result = $connection->fetchAll($sql);
        if(count($result)>0){
            $isReleaed = true;
        }
        return $isReleaed;
    }

    public function isReleasedInvoiceByOrderId($order_id)
    {
        $connection = $this->_resource->getConnection();
        $sql = "SELECT * FROM seller_sales_invoice where order_id = '$order_id';";
        $isReleaed = false;
        $result = $connection->fetchAll($sql);
        if(count($result)>0){
            $isReleaed = true;
        }
        return $isReleaed;
    }

    public function isReleasedShipmentByOrderId($order_id)
    {
        $connection = $this->_resource->getConnection();
        $sql = "SELECT * FROM seller_sales_shipment_grid where order_id = '$order_id';";
        $isReleaed = false;
        $result = $connection->fetchAll($sql);
        if(count($result)>0){
            $isReleaed = true;
        }
        return $isReleaed;
    }

    public function isValidateToRequestBalance($balanceId)
    {
        $connection = $this->_resource->getConnection();
        $sql = "SELECT * FROM libero_provider_balance where id = '$balanceId';";
        $isCompleted = false;
        $result = $connection->fetchAll($sql);
        $validTime = false;
        if ($result[0]["estimate_pay_at"])
        if($result[0]["estimate_pay_at"] && ($result[0]["status"] == "0" || $result[0]["status"] == "3")){          //0: Pending | 1: Requested | 2: Paid | 3: Reject
            
            $isCompleted = true;
        }

        return $isCompleted;
    }
}




