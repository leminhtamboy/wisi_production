<?php
namespace Libero\Customer\Block\Seller\Admin;

use Magento\Framework\View\Element\Template;

class AclListing extends \Magento\Customer\Block\Account\Dashboard\Info{

    protected $_objectManager = null;
    protected $_productImageHelper = null;

    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }

    /**
     * Function get collection product
     *
     * @return void
     */
    public function getCollectionAcl($id_seller)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelAccountListing = $this->_objectManager->create("\Libero\Customer\Model\Acl");
        $collectionProduct = $modelAccountListing->getCollection()->addFieldToSelect("*")->addFieldToFilter("id_seller",array("eq" => $id_seller));
        return $collectionProduct;
    }
    public function getUrlEditRole($id_role)
    {
        return $this->getUrl("customer/admin/editrole/id/".$id_role);
    }
    public function getUrlAddRole()
    {
        return $this->getUrl("customer/admin/addrole");
    }
}