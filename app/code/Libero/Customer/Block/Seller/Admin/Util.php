<?php

namespace Libero\Customer\Block\Seller\Admin;

class Util
{
    protected $_dashInfo;
    protected $_customerSellerModel;
    protected $_sellerModel;
    protected $_resource;


    public function __construct(
        \Magento\Customer\Block\Account\Dashboard\Info $dashInfo,
        \Libero\Customer\Model\CustomerSeller $customerSellerModel,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Libero\Customer\Model\Seller $sellerModel
        )
    {
        $this->_dashInfo = $dashInfo;
        $this->_sellerModel = $sellerModel;
        $this->_resource = $resourceConnection;
        $this->_customerSellerModel = $customerSellerModel;
    }

    public function getCustomerId()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('\Magento\Customer\Model\Session');
        $customerId = null;
        if($customerSession->isLoggedIn()){
            $customerId = $this->_dashInfo->getCustomer()->getId();
        }
        return $customerId;
    }
    public function getIdSeller(){
        $this->checkSellerLogin();
        $customerId = $this->_dashInfo->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj->getData("id_seller_company");
    }

    public function getSellerInfo($id_seller){
        $connection = $this->_resource->getConnection();
        $sql = "SELECT * FROM libero_customer_seller_company WHERE id_seller_company = $id_seller;";
        $result = $connection->fetchAll($sql)[0];
        return $result;
    }
    public function formatPriceOneStepCheckout($number){
        if($number > -1){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $blockOrderAdmin = $objectManager->get("Libero\Customer\Block\Adminhtml\Provider\Order");
            return  $blockOrderAdmin->formartPrice($number);
        }
        else{
            return "";
        }
    }
    public function formatPrice($number){
        if($number > 0){
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $blockOrderAdmin = $objectManager->get("Libero\Customer\Block\Adminhtml\Provider\Order");
            return  $blockOrderAdmin->formartPrice($number);
        }
        else{
            return  0;
        }
    }
    public function formatPriceNoCurrency($number){
        if($number > 0){
            return number_format($number,0);
        }
        else{
            return "";
        }
    }
    public function formatNumber($number){
        return round($number,0);
    }
    public function checkSellerLogin(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $this->_resource->getConnection();

        $status;

        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        if(!$this->_dashInfo->getCustomer()){
            ?>
            <script>
                top.location.href = '<?php echo $baseUrl; ?>customer/account/login/';
            </script>
            <?php
        } else {
            $customerId = $this->_dashInfo->getCustomer()->getId();
            $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
            $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
            $sellerObj = null;
            if($dataAccount->getId()){
                $id_seller = $dataAccount->getData("id_seller");
                $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
            }else{
                $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
            }
            $id_seller = $sellerObj->getData("id_seller_company");

            $sql = "SELECT * FROM libero_customer_seller_company WHERE id_seller_company = '$id_seller';";
            $result = $connection->fetchAll($sql);
            if(count($result) > 0 ){
            $status = $result[0]["status"];
            if($status != "1" ) { ?>
                <script>
                    top.location.href = '<?php echo $baseUrl; ?>customer/seller/manager/';
                </script>
                <?php
                }
            }else{ ?>
                <script>
                    top.location.href = '<?php echo $baseUrl; ?>customer/account/';
                </script>
            <?php }

        }
    }

    public function checkBuyerLogin(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
        if(!$this->_dashInfo->getCustomer()){
            ?>
            <script>
                top.location.href = '<?php echo $baseUrl; ?>customer/account/login/';
            </script>
            <?php
        }
    }
    public function checkCustomerLogin(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerSession = $objectManager->get('\Magento\Customer\Model\Session');
        if(!$customerSession->isLoggedIn()){
            $urlInterface = $objectManager->get("Magento\Framework\App\Helper\Context");
            $customerSession->setAfterAuthUrl($urlInterface->getUrlBuilder()->getCurrentUrl());
            $customerSession->authenticate();
        }
    }
    public function getBankList($parent_id){
        $connection = $this->_resource->getConnection();
        $sql = "SELECT * FROM bank_list WHERE parent_id = $parent_id;";
        $result = $connection->fetchAll($sql);
        return $result;       
    }

    public function getBankParents(){
        $connection = $this->_resource->getConnection();
        $sql = "SELECT * FROM bank_list WHERE type='1' ;";
        $result = $connection->fetchAll($sql);
        return $result;       
    }

    /* paging */
    
    public function getTotalArray($arr_source){
        return count($arr_source);
    }

    public function getPageSizeArray($size_key = "pageSize"){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $requestHttp = $objectManager->create("\Magento\Framework\App\Request\Http");
        $pageSize = 20;
        if($requestHttp->getParam($size_key)){
            $pageSize = $requestHttp->getParam($size_key);    
        }
        return $pageSize;
    }

    public function getCurPageArray($page_key = "page"){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $requestHttp = $objectManager->create("\Magento\Framework\App\Request\Http");
        $curPage = 1;
        if($requestHttp->getParam($page_key)){
            $curPage = $requestHttp->getParam($page_key);
        }
        return $curPage;
    }

    public function getFirstNumArray(){
        return $this->getPageSizeArray() * ($this->getCurPageArray() - 1) + 1;
    }

    public function getLastPageArray($arr_source, $pageSize = 20){
        $lastPage = floor($this->getTotalArray($arr_source) / $pageSize);
        if(fmod($this->getTotalArray($arr_source),$pageSize) > 0) {
            $lastPage++;
        }
        return $lastPage;
    }

    public function getLastPageByTotal($total, $pageSize = 20){
        $lastPage = floor($total / $pageSize);
        if(fmod($total,$pageSize) > 0) {
            $lastPage++;
        }
        return $lastPage;
    }

    public function getDeliveryCompanyList(){
        $connection = $this->_resource->getConnection();
        $sql = "SELECT * FROM libero_delivery_company;";
        $result = $connection->fetchAll($sql);
        return $result;       
    }

    public function getDeliveryCompaniesByIdSeller($id_seller){
        $connection = $this->_resource->getConnection();
        $sql = "SELECT * FROM libero_onestepcheckout_shipping_method WHERE id_seller = $id_seller;";
        $result = $connection->fetchAll($sql);
        return $result; 
    }

    public function getRemainDeliveryCompanyList($id_seller){
        $connection = $this->_resource->getConnection();
        $sellerDeliveryInfos = $this->getDeliveryCompaniesByIdSeller($id_seller);
        $sellerDeliveryIds = "";
        foreach($sellerDeliveryInfos as $_info){
            $sellerDeliveryIds .= $_info["shipping_code"] . ",";
        }
        $sellerDeliveryIds = substr($sellerDeliveryIds,0,strlen($sellerDeliveryIds)-1);
        $sql= "";
        if(count($sellerDeliveryIds)>0) {
            $sql = "SELECT * FROM libero_delivery_company";
        } else {
            $sql = "SELECT * FROM libero_delivery_company WHERE id_delivery not in ($sellerDeliveryIds)";
        }
        $result = $connection->fetchAll($sql);
        return $result; 
    }

    public function getDeliveryCompanyByShippingId($id_delivery){
        $connection = $this->_resource->getConnection();
        $sql = "SELECT * FROM libero_delivery_company WHERE id_delivery = $id_delivery;";
        $result = $connection->fetchAll($sql);
        return $result[0]; 
    }

    public function removeQuoteInString($string){
        $string = str_replace("'","",$string);
        $string = str_replace('"','',$string);
        return $string; 
    }

    public function getAddressById($addressId){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $connection = $this->_resource->getConnection();

        $sql = "SELECT * FROM customer_address_entity WHERE entity_id = $addressId";
        $result = $connection->fetchAll($sql)[0];

        $cityName = $result["city"];
        
        $cityCodeSql = "SELECT province_id FROM libero_province WHERE province_name = '$cityName';";
        $cityCode = $connection->fetchAll($cityCodeSql)[0]["province_id"];

        $district_id = explode("-",$result["postcode"])[0];
        $ward_id = explode("-",$result["postcode"])[1];
        
        $result["city_id"] = $cityCode;
        $result["district_id"] = $district_id;
        $result["ward_id"] = $ward_id;

        return $result;
    }

}