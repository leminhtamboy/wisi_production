<?php
namespace Libero\Customer\Block\Seller;

use Magento\Framework\View\Element\Template;

class Manager extends \Magento\Customer\Block\Account\Dashboard\Info{

    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        return $sellerObj;
    }

    public function getCustomerDataSeller(){
        return $this->getCustomer();
    }

    public function getUrlSellerManager(){
        return $this->getUrl("customer/seller/manager/");
    }

    public function getUrlCopySourcce(){
        return $this->getUrl("customer/seller/copy/");
    }

    public function getUrlImportDatabase(){
        return $this->getUrl("customer/seller/import/");
    }

    public function getUrlRunCommandLine(){
        return $this->getUrl("customer/seller/command/");
    }

    public function getUrlSaveStore(){

        return $this->getUrl("customer/seller/savestore/");

    }
    public function getSellerHaveCreatedStore($id_seller){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSellerStore')->load($id_seller,"id_seller");

        return $sellerObj;

        if(count($sellerObj->getData()) > 0){

            return true;

        }

        return false;
    }
    public function rewriteUrl($store_name){
        $urlRewrite = trim($this->RemoveSign($store_name));
        $urlRewrite = preg_replace('#[^0-9a-z]+#i', '-', $urlRewrite);
        $urlRewrite = strtolower($urlRewrite);
        return $urlRewrite;
    }

    function RemoveSign($str)
    {
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }

}