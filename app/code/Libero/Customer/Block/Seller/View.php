<?php
namespace Libero\Customer\Block\Seller;

use Magento\Framework\View\Element\Template;

class View extends \Magento\Framework\View\Element\Template{

    protected $_category = null;    
    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }
    public function sendSms(){

    }
    public function sendEmail(){

    }
    public function getPostActionUrl(){
        return $this->getUrl("customer/seller/save");
    }
    public function getPostActionGetOtp(){
        return $this->getUrl("customer/seller/getcode");
    }
    public function randomTokenonFormToSendOtp(){
        //Get Model random
        $stringRamdon = "Token:".rand(1,999999);
        return base64_encode($stringRamdon);
    }
    public function getCategoriesTree()
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $categories = $objectManager->create(
            'Magento\Catalog\Ui\Component\Product\Form\Categories\Options'
        )->toOptionArray();
        return json_encode($categories);
    }
    public function getCategories()
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $dependency_category = $objectManager->get("\Magento\Catalog\Model\Category");
        $categories = $dependency_category->getCollection()->addAttributeToSelect("*")->addAttributeToFilter("level",array("eq" => "2"));
        /*foreach($categories as $_cate)
        {
            echo $_cate->getName();
            echo "<br/>";
            echo $_cate->getPath();
            echo "<br/>";
        }*/
        //var_dump($categories->getData());
        return $categories;
    }
    public function getParentsCategory()
    {
        $objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
        $categoryFactory = $objectManager->create("\Magento\Catalog\Model\CategoryFactory"); 
        $this->_category = $categoryFactory->create();
        if ($this->_category) {
            return $this->_category->getParentCategory();
        } else {
            return $this->getCategory($categoryId)->getParentCategory();        
        }
    }

}