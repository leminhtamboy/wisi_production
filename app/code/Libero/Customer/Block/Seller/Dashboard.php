<?php
namespace Libero\Customer\Block\Seller;

class Dashboard extends \Magento\Framework\View\Element\Template
{

    protected $_orderCollection;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Reports\Model\ResourceModel\Order\Collection $orderCollection,
        array $data = [])
    {
        $this->_orderCollection = $orderCollection;
        parent::__construct($context, $data);
    }

    public function getOrderStat(){
        /* $customStart = "2018-03-01";
        $customEnd = "2018-03-31";
        $collection = $this->_orderCollection->prepareSummary("1m", $customStart, $customEnd);
        return $collection->getData(); */
    }
}