<?php
namespace Libero\Customer\Block\Seller;

class Invoice extends \Magento\Framework\View\Element\Template
{

    protected $_invoiceModel;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Libero\Customer\Model\Invoice $invoiceModel,
        array $data = [])
    {
        $this->_invoiceModel = $invoiceModel;
        parent::__construct($context, $data);
    }

    public function getInvoices(){
        $invoice = $this->_invoiceModel->getCollection();
        return $invoice;
    }
    
    
}