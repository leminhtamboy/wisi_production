<?php
namespace Libero\Customer\Block\Seller;

class Shipment extends \Magento\Framework\View\Element\Template
{

    protected $_resource;

    protected $_requestHttp;

    protected $_adminUtil;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Framework\App\Request\Http $requestHttp,
        \Libero\Customer\Block\Seller\Admin\Util $adminUtil,
        array $data = []){
        $this->_adminUtil  = $adminUtil;
        $this->_requestHttp  = $requestHttp;
        $this->_resource  = $resourceConnection;
        parent::__construct($context, $data);
    }

    public function getShipments(){
        $this->_adminUtil->checkBuyerLogin();
        $id_customer = $this->_adminUtil->getCustomerId();
        $sql = "SELECT * FROM seller_sales_shipment_grid 
                inner join seller_sales_order 
                on seller_sales_shipment_grid.order_increment_id = seller_sales_order.increment_id 
                WHERE  `seller_sales_order`.customer_id = '$id_customer' and `order_code_api`  != ''";
        $connection = $this->_resource->getConnection();
        $result = $connection->fetchAll($sql);
        return $result;
    }

    public function getDetailShipment()
    {
        $this->_adminUtil->checkBuyerLogin();
        $id_shiping = $this->_requestHttp->getParam("id_shipment");
        $sql = "SELECT * FROM seller_sales_shipment_grid inner join seller_sales_order 
        on seller_sales_shipment_grid.order_increment_id = seller_sales_order.increment_id WHERE order_id = '$id_shiping'";
        $connection = $this->_resource->getConnection();
        $result = $connection->fetchAll($sql);
        return $result[0];
    }
    public function getDetailShipmentByOrderId($order_id)
    {
        $sql = "SELECT * FROM seller_sales_shipment_grid inner join seller_sales_order 
        on seller_sales_shipment_grid.order_increment_id = seller_sales_order.increment_id WHERE order_id = '$order_id'";
        $connection = $this->_resource->getConnection();
        $result = $connection->fetchAll($sql);
        return $result[0];
    }
    public function getCodeShipping()
    {
        $order_id = $this->_requestHttp->getParam("id_shipment");
        $sql = "SELECT * FROM `libero_quote_shipping_price` WHERE `order_id` = '$order_id'";
        $connection = $this->_resource->getConnection();
        $result = $connection->fetchAll($sql);
        if(count($result) > 0){
            return $result[0];
        }else{
            return null;
        }
    }
    public function getCodeShippingByOrderId($order_id)
    {
        $sql = "SELECT * FROM `libero_quote_shipping_price` WHERE `order_id` = '$order_id'";
        $connection = $this->_resource->getConnection();
        $result = $connection->fetchAll($sql);
        if(count($result) > 0){
            return $result[0];
        }else{
            return null;
        }
    }
    public function getOrderStatusAfterShip()
    {
        $sql = "SELECT * FROM `libero_order_status` WHERE id_status in (7,8,9,10)";
        $connection = $this->_resource->getConnection();
        $result = $connection->fetchAll($sql);
        if(count($result) > 0){
            return $result;
        }else{
            return null;
        }
    }
    public function getMappingStatusToSetDoneTracking($status_shipping,$type_company)
    {
        $sql = "SELECT * FROM `libero_order_mapping_status_shipping` WHERE `shipping_company` = '$type_company' AND `code_shipping_status` = '$status_shipping'";
        $connection = $this->_resource->getConnection();
        $result = $connection->fetchAll($sql);
        if(count($result) > 0){
            return $result[0];
        }else{
            return null;
        }
    }
}