<?php
namespace Libero\Customer\Block\Seller;

use Magento\Framework\View\Element\Template;

class BestProviderRanking extends \Magento\Framework\View\Element\Template {
    
    public function getIdSellers(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get("\Magento\Framework\App\ResourceConnection");
        $connection = $resource->getConnection();

        $sql = "SELECT seller_id, sum(grand_total) as grand_total FROM seller_sales_order WHERE status != 'Prepare to ship' group by seller_id ORDER BY grand_total DESC;";
        $result = $connection->fetchAll($sql);
        $seller_ids = array();
        $count = 0;
        $limit = $this->getData('limit');
        foreach($result as $seller_id) {
            if ($count < $limit) {
                $seller_ids[] = $seller_id["seller_id"];
            }
            $count++;
        }
        return $seller_ids;
    }

    public function getSellersJoinedDate($id_customer){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get("\Magento\Framework\App\ResourceConnection");
        $connection = $resource->getConnection();
        $sql = "SELECT * FROM customer_entity WHERE entity_id = $id_customer;";
        $result = $connection->fetchAll($sql);
        return $result[0];
    }

    public function getSellerDetail($id_seller)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectSeller = $objectManager->create("Libero\Customer\Model\CustomerSeller");
        $dataSeller = $injectSeller->load($id_seller);
        return $dataSeller; 
    }

    public function getSellerReviewSummary($id_seller) {
        $result = array('review_count' => 0, 'rating_count' => 0,'percent' => 0);
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $sellerSummaryModel = $objectManager->create("Libero\Marketplace\Model\SellerReviewSummary");
        $sellerSummary = $sellerSummaryModel->load($id_seller,"seller_id");
        $review_count = $sellerSummary->getData("review_count");
        $rating_count = $sellerSummary->getData("rating_count");
        
        if ($review_count != "" && $rating_count != "") {
            $result['review_count'] = $review_count;
            $result['rating_count'] = $rating_count;
            $result['percent'] = $rating_count/($review_count*5)*100;
        }
        return $result;
    }

    public function getCategoryName($id_category){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get("\Magento\Framework\App\ResourceConnection");
        $connection = $resource->getConnection();
        
        $attr_cate_id_sql = "SELECT * FROM eav_attribute WHERE attribute_code = 'name' AND entity_type_id = 3;";
        $attr_cate_id = $connection->fetchAll($attr_cate_id_sql)[0]['attribute_id'];

        $sql = "SELECT * FROM catalog_category_entity_varchar WHERE attribute_id = $attr_cate_id AND entity_id = $id_category;";
        $result = $connection->fetchAll($sql);
        return $result[0]["value"];
    }

    public function getTopOrder(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get("\Magento\Framework\App\ResourceConnection");
        $connection = $resource->getConnection();
        $sql = "SELECT seller_id, COUNT(seller_id) AS count FROM seller_sales_order GROUP BY seller_id ORDER BY count DESC;";
        $result = $connection->fetchAll($sql)[0]["seller_id"];
        return $result;
    }
    
}