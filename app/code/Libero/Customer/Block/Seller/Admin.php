<?php
namespace Libero\Customer\Block\Seller;

use Magento\Framework\View\Element\Template;

class Admin extends \Magento\Customer\Block\Account\Dashboard\Info{

    public function getSellerDetail(){
        
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }
    public function getAclPermission()
    {
        $objectSeller = $this->getSellerDetail();
        $customerCheck = $objectSeller->getData("id_customer");
        $customerId = $this->getCustomer()->getId();
        $id_seller = $objectSeller->getId();
        $permission = "admin|sale|product|report|store|system";
        if($customerCheck == $customerId){
            //Full permission
            $permission = "admin|sale|product|report|store|system";
        }else{
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
            $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
            $id_acl = $dataAccount->getData("id_role");
            $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Acl")->load($id_acl);
            $permission = $aclAccountSeler->getData("acl_permmision");
        }
        $splitPermission = explode("|",$permission);
        return $splitPermission;
    }
    public function getActionName()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $objectManager->get("\Magento\Framework\App\Request\Http");
        $action_name = $injectHTTP->getActionName();
        return $action_name;
    }
    /**
     * Function set url product listing
     *
     * @return void
     */
    public function getUrlProduct()
    {
        return $this->getUrl("customer/admin/productlisting");
    }
    public function getUrlDashBoard()
    {
        return $this->getUrl("customer/admin/");
    }
    public function getUrlUserListing()
    {
        return $this->getUrl("customer/admin/accountlisting");
    }
    public function getImportUrl()
    {
        return $this->getUrl("customer/admin/importproduct");
    }
    public function getExportUrl()
    {
        return $this->getUrl("customer/admin/exportproduct");
    }
    public function getRmaListing()
    {
        return $this->getUrl("customer/admin/requestrma");
    }
    public function getUrlFaq()
    {
        return $this->getUrl("customer/faq/index");
    }
    public function getRoleListing()
    {
        return $this->getUrl("customer/admin/acllisting");
    }
    public function getShipmentListing()
    {
        return $this->getUrl("customer/admin/shipmentlisting");
    }
    public function getUrlSaveSellerDetail()
    {
        return $this->getUrl("customer/admin/editprofile");
    }

    public function getUrlDeliveryManager()
    {
        return $this->getUrl("customer/admin/delivery");
    }
    public function getAddConfigruableProduct()
    {
        return $this->getUrl("customer/admin/addconfigruableproduct");
    }
    public function getUrlConfiguration()
    {
        return $this->getUrl("customer/admin/configuration");
    }

    public function getCustomerDataSeller(){
        return $this->getCustomer();
    }

    public function getUrlViewStore($id_seller)
    {
       return $this->getUrl("marketplace/store/store/id_seller/".$id_seller); 
    }
    public function getUrlSellerManager(){
        return $this->getUrl("customer/seller/manager/");
    }

    public function getUrlCopySourcce(){
        return $this->getUrl("customer/seller/copy/");
    }

    public function getUrlImportDatabase(){
        return $this->getUrl("customer/seller/import/");
    }

    public function getUrlRunCommandLine(){
        return $this->getUrl("customer/seller/command/");
    }

    public function getUrlSaveStore(){
        return $this->getUrl("customer/seller/savestore/");
    }

    public function getUrlReportReviewByCustomer()
    {
        return $this->getUrl("customer/admin/reportreviewcustomer");
    }

    

    public function getSellerHaveCreatedStore($id_seller){

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSellerStore')->load($id_seller,"id_seller");

        return $sellerObj;

        if(count($sellerObj->getData()) > 0){

            return true;

        }

        return false;
    }
    public function rewriteUrl($store_name){
        $urlRewrite = trim($this->RemoveSign($store_name));
        $urlRewrite = preg_replace('#[^0-9a-z]+#i', '-', $urlRewrite);
        $urlRewrite = strtolower($urlRewrite);
        return $urlRewrite;
    }

    function RemoveSign($str)
    {
        $coDau=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ"
        ,"ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ","ì","í","ị","ỉ","ĩ",
            "ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
        ,"ờ","ớ","ợ","ở","ỡ",
            "ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
            "ỳ","ý","ỵ","ỷ","ỹ",
            "đ",
            "À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
        ,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
            "È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
            "Ì","Í","Ị","Ỉ","Ĩ",
            "Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
        ,"Ờ","Ớ","Ợ","Ở","Ỡ",
            "Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
            "Ỳ","Ý","Ỵ","Ỷ","Ỹ",
            "Đ","ê","ù","à");
        $khongDau=array("a","a","a","a","a","a","a","a","a","a","a"
        ,"a","a","a","a","a","a",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o"
        ,"o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",
            "d",
            "A","A","A","A","A","A","A","A","A","A","A","A"
        ,"A","A","A","A","A",
            "E","E","E","E","E","E","E","E","E","E","E",
            "I","I","I","I","I",
            "O","O","O","O","O","O","O","O","O","O","O","O"
        ,"O","O","O","O","O",
            "U","U","U","U","U","U","U","U","U","U","U",
            "Y","Y","Y","Y","Y",
            "D","e","u","a");
        return str_replace($coDau,$khongDau,$str);
    }

    public function connect(){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get("\Magento\Framework\App\ResourceConnection");
        $connection = $resource->getConnection();
        return $connection;
    }

    public function processingDate($input_date){
        if($input_date != ""){
            $arrayDate= explode("/",$input_date);
            return $result = $arrayDate[2]."-".$arrayDate[0]."-".$arrayDate[1];
        } else {
            return "";
        }
    }
    /**
     * Region Dash board Header
     *
     * @param [type] $id_seller
     * @return void
     */
        //Function get danh sach các order từ lúc tạo đến h
     public function getCountOrder($id_seller)
     {
        $connection = $this->connect();
        $sql = "
            SELECT 
                COUNT(entity_id) AS count 
            FROM 
                seller_sales_order 
            WHERE 
                seller_id = '$id_seller'"
        ;
        $sql .=";";
        $result = $connection->fetchAll($sql);
        return $result[0]['count'];
     }
     public function getCountCustomer($id_seller){
        $connection = $this->connect();
        $sql = "
            SELECT 
                COUNT(`customer_id`) AS count 
            FROM 
                seller_sales_order 
            WHERE 
                seller_id = '$id_seller' GROUP BY `customer_id` "
        ;
        $sql .=";";
        $result = $connection->fetchAll($sql);
        return count($result);
     }
     public function getCountProduct($id_seller)
     {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $modelProduct =  $objectManager->get("\Magento\Catalog\Model\Product");
        $collectionProduct = $modelProduct->getCollection()
        ->addAttributeToFilter("id_seller",array("eq"=>$id_seller))
        ->addAttributeToFilter("visibility",array("eq"=>4));
        return count($collectionProduct);
     }
     public function getRatingCount($id_seller)
     {
        $connection = $this->connect();
        $sql = "
            SELECT 
                COUNT(`id`) AS count 
            FROM 
                seller_review 
            WHERE 
                seller_id = '$id_seller';";
        $result = $connection->fetchAll($sql);
        return $result[0]['count'];
     }
     /**
      * End Region
      *
      * @param [type] $id_seller
      * @return void
      */
    public function getSalesOrder($id_seller){
        $haveSearch = $this->getHaveSearch();
        $customStart = $this->processingDate($this->getDateFrom()) . " 00:00:00";
        $customEnd = $this->processingDate($this->getDateTo()) . " 23:59:59";
        $connection = $this->connect();
        $sql = "
            SELECT 
                *, COUNT(ssoi.qty_ordered) as qty_ordered 
            FROM 
                seller_sales_order_grid AS ssog 
                    JOIN 
                seller_sales_order_item AS ssoi ON ssog.order_id = ssoi.order_id 
            WHERE 
                ssog.seller_id = '$id_seller' 
                AND status != 'cancel' "
        ;

        if($haveSearch == "1" && $customStart != "" && $customEnd != ""){
            $sql .="AND ssog.created_at >= '$customStart' AND ssog.created_at <= '$customEnd' ";
        }
        $sql .= "
            GROUP BY ssog.order_id 
            ORDER BY ssog.created_at DESC 
            LIMIT 10
            ;"
        ;
        $result = $connection->fetchAll($sql);
        return $result;
    }

    public function getTotalSales($id_seller){
        $haveSearch = $this->getHaveSearch();
        $customStart = $this->processingDate($this->getDateFrom()) . " 00:00:00";
        $customEnd = $this->processingDate($this->getDateTo()) . " 23:59:59";
        $connection = $this->connect();
        $sql = "SELECT  SUM(grand_total) AS grand_total  FROM  seller_sales_order  WHERE  seller_id = '$id_seller'  AND `status` = 'Complete'";
        /*if($haveSearch == "1" && $customStart != "" && $customEnd != ""){
            $sql .="AND created_at >= '$customStart' AND created_at <= '$customEnd' ";
        }*/
        $sql .=";";
        $result = $connection->fetchAll($sql);
        
        return $result[0]['grand_total'];
    }

    public function getCountOrders($id_seller){
        $haveSearch = $this->getHaveSearch();
        $customStart = $this->processingDate($this->getDateFrom()) . " 00:00:00";
        $customEnd = $this->processingDate($this->getDateTo()) . " 23:59:59";
        $connection = $this->connect();
        $sql = "
            SELECT 
                COUNT(entity_id) AS count 
            FROM 
                seller_sales_order 
            WHERE 
                seller_id = '$id_seller' 
                AND status != 'cancel' "
        ;
        if($haveSearch == "1" && $customStart != "" && $customEnd != ""){
            $sql .="AND created_at >= '$customStart' AND created_at <= '$customEnd' ";
        }
        $sql .=";";
        $result = $connection->fetchAll($sql);
        return $result[0]['count'];
    }

    public function getBestSellerProducts($id_seller){
        $haveSearch = $this->getHaveSearch();
        $customStart = $this->processingDate($this->getDateFrom()) . " 00:00:00";
        $customEnd = $this->processingDate($this->getDateTo()) . " 23:59:59";
        $connection = $this->connect();
        $sql =" SELECT *, 
            COUNT(ssoi.sku) AS count,
            SUM(ssoi.row_total) AS total,
            SUM(ssoi.qty_ordered) AS qty,
            ssoi.row_total,
            ssoi.qty_ordered,
            ssoi.name
        FROM
            seller_sales_order_grid AS ssog
                JOIN
            seller_sales_order_item AS ssoi ON ssog.order_id = ssoi.order_id
        WHERE
            ssog.seller_id = '$id_seller'
            AND ssog.status != 'cancel' ";
        
        if($haveSearch == "1" && $customStart != "" && $customEnd != ""){   
            $sql .="AND ssog.created_at >= '$customStart' AND ssog.created_at <= '$customEnd' ";
        }

        $sql .= "
            GROUP BY ssoi.sku 
            ORDER BY ssoi.row_total DESC
            ;"
        ;
        
        $result = $connection->fetchAll($sql);
        
        return $result;
    }

    public function setMessage($message,$type)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $registry = $objectManager->create("\Magento\Checkout\Model\Session");
        if($type == "success"){
            $registry->setSellerMessage($message);
            $registry->setSellerTypeMessage($type);
        }
        if($type == "error"){
            $registry->setSellerMessage($message);
            $registry->setSellerTypeMessage($type);
        }
    }
    public function getMessage()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $registry = $objectManager->create("\Magento\Checkout\Model\Session");
        return $registry->getSellerMessage();
    }
    public function getTypeMessage()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $registry = $objectManager->create("\Magento\Checkout\Model\Session");
        return $registry->getSellerTypeMessage();
    }
    public function unsetMessage()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $registry = $objectManager->create("\Magento\Checkout\Model\Session");
        $registry->unsSellerMessage();
        $registry->unsTypeMessage();
    }

    public function getSearchCondition(){
        $date_from = $this->getDateFrom();
        $date_to = $this->getDateTo();
        $condition = [
            "date_from" => $date_from,
            "date_to" => $date_to
        ];
        return $condition;
    }

    public function getUrlSearchOrder(){
        return $this->getUrl("customer/admin/searchdashboard");
    }

    public function nam(){
        echo $this->getHaveSearch();
    }






}