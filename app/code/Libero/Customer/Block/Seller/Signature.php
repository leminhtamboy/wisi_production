<?php
namespace Libero\Customer\Block\Seller;

use Magento\Framework\View\Element\Template;

class Signature extends \Magento\Framework\View\Element\Template{

    public function __construct(Template\Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }
    public function getPostActionUrl(){
        return $this->getUrl("customer/seller/save");
    }
}