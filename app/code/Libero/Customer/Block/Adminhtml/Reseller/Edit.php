<?php
namespace Libero\Customer\Block\Adminhtml\Reseller;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Edit extends \Magento\Framework\View\Element\Template{
    protected $_coreRegistry = null;

    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }
    public function getDataReSeller(){
        $model = $this->_coreRegistry->registry('libero_customer_reseller');
        $dataRet = null;
        $dataRet = $model->getData();
        return $dataRet;
    }
    public function getApproveUrl()
    {
        $route = "manager/reseller/approve/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getViewSeller()
    {
        $route = "manager/reseller/view/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getSaveUrl(){
        $route = "manager/reseller/save/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getBackUrl(){
        $route = "manager/reseller/index/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getCancelUrl(){
        $route = "manager/reseller/cancel/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getFormKey(){

    }
}