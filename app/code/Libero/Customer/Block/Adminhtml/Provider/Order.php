<?php
namespace Libero\Customer\Block\Adminhtml\Provider;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Order extends \Magento\Framework\View\Element\Template{
    protected $_coreRegistry = null;
    protected $_table_provider_order = "seller_sales_order";
    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }
    /**
     * Function get Order Detail
     *
     * @return void
     */
    public function getOrderDetail()
    {
        $order = $this->_coreRegistry->registry('libero_provider_order_detail');
        return $order;
    }
    public function getOrderDetailById($order_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $sql_get_order_detail_provider = "SELECT * FROM  ".$this->_table_provider_order." where entity_id = '$order_id'";
        $data = $connection->fetchAll($sql_get_order_detail_provider);
        return $data[0];
    }
    protected function getObjectManager()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }
    protected function getHelperOneStepCheckout()
    {
        $objectManager = $this->getObjectManager();
        $helperCheckout = $objectManager->get("\Libero\Onestepcheckout\Helper\Data");
        return $helperCheckout;
    }
    /**
     * Function get Detail Seller Information
     *
     * @param [type] $id_seller
     * @return void
     */
    public function getDetailSeller($id_seller)
    {
        //Model Customer
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }
    /**
     * Function get Billing Address Of Seller
     *
     * @param [type] $increment_id to reference sale_order
     * @return void
     */
    public function getAddressOfSeller($increment_id)
    {
        $database = $this->getConnectionDatabaseDirect();
        //Get entity_id of table sale order by increment id to get billing address and shipping address
        $sql_get_order_provider = "SELECT entity_id,increment_id FROM  `sales_order` WHERE increment_id = '$increment_id'";
        $data = $database->fetchAll($sql_get_order_provider);
        $entity_id = $data[0]["entity_id"];
        $sql_get_address =  "SELECT * FROM sales_order_address WHERE parent_id = '$entity_id'";
        $data_address = $database->fetchAll($sql_get_address);
        $array_address = array();
        $billing_address = array();
        $shipping_address = array();
        foreach($data_address as $_address){
            $type = $_address["address_type"];
            $seller_name = $_address["firstname"]." ".$_address["lastname"];
            $street = $_address["street"];
            $district = $_address["region"];
            $city = $_address["city"];
            $telephone = $_address["telephone"];
            if($type == "billing"){
                $billing_address["seller_name"] = $seller_name;
                $billing_address["street"] = $street;
                $billing_address["district"] = $district;
                $billing_address["city"] = $city;
                $billing_address["telephone"] = $telephone;
            }else{
                $shipping_address["seller_name"] = $seller_name;
                $shipping_address["street"] = $street;
                $shipping_address["district"] = $district;
                $shipping_address["city"] = $city;
                $shipping_address["telephone"] = $telephone;
            }
        }
        $array_address["billing"] = $billing_address;
        $array_address["shipping"] = $shipping_address;
        return $array_address;
    }
    /**
     * Function get products
     *
     * @param [type] $order_id
     * @return void
     */
    public function getProducts($order_id)
    {
        $database = $this->getConnectionDatabaseDirect();
        $sql_get_products =  "SELECT * FROM seller_sales_order_item WHERE order_id = '$order_id'";
        $data_product = $database->fetchAll($sql_get_products);
        $array_product = array();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        
        foreach($data_product as $_product){
            if($_product['parent_item_id'] > 0 && $_product['parent_item_id'] != "0"){
                continue;
            }
            $items = array();
            $items["name"] = $_product["name"];
            $items["sku"] = $_product["sku"];
            $sku = $_product["sku"];
            $items["original_price"] = $_product["original_price"];
            $items["price"] = $_product["price"];
            $items["qty_ordered"] = $_product["qty_ordered"];
            $items["sub_total"] = $_product["row_total"];
            $items["tax_amount"] = $_product["tax_amount"];
            $items["tax_percent"] = $_product["tax_percent"];
            $items["discount_amount"] = $_product["discount_amount"];
            $items["row_total"] = $_product["row_total"];
            $arrayProduct = array();
            $array_options = array();
            if($_product["product_type"]  ==  "configurable"){
                $data_options = $_product["product_options"];
                $value_options = unserialize($data_options);
                $array_attribute = $value_options["attributes_info"];
                $array_options_label_value = array();
                foreach($array_attribute as $attribute_id => $option){
                    $array_options_label_value["label"] = $option["label"];
                    $array_options_label_value["value_option"] = $option["value"];
                    $arrayProduct["options_configruable"][] = $array_options_label_value;
                }
                $array_options = $arrayProduct["options_configruable"];
            }
            $items["options"] = $array_options;
            $entity_product = $objectManager->create('Magento\Catalog\Model\Product')->loadByAttribute('sku',$sku);

            if($entity_product){
                $img_product = $this->resizeImage($entity_product,"product_thumbnail_image",150,150)->getUrl();
                $items["image"] = $img_product;
            }else{
                $items["image"] = "https://wisi.vn/media/logo/stores/2/wisi-text-logo-200x77-w.png";
            }
            $array_product []  = $items;
        }
        return $array_product;
    }
    /**
     * Function resize Image
     *
     * @param [type] $product
     * @param [type] $imageId
     * @param [type] $width
     * @param [type] $height
     * @return void
     */
    public function resizeImage($product, $imageId, $width, $height = null)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productImageHelper = $objectManager->get('\Magento\Catalog\Helper\Image');
        $resizedImage = $productImageHelper
            ->init($product, $imageId)
            ->constrainOnly(TRUE)
            ->keepAspectRatio(TRUE)
            ->keepTransparency(TRUE)
            ->keepFrame(FALSE)
            ->resize($width, $height);
        return $resizedImage;
    }
    public function getPaymentMethods($increment_id)
    {
        $database = $this->getConnectionDatabaseDirect();
        //Get entity_id of table sale order by increment id to get billing address and shipping address
        $sql_get_payment = "SELECT entity_id,increment_id FROM  `sales_order` WHERE increment_id = '$increment_id'";
        $data = $database->fetchAll($sql_get_payment);
        $entity_id = $data[0]["entity_id"];
        $sql_get_payment =  "SELECT * FROM sales_order_payment WHERE parent_id = '$entity_id'";
        $data_payment_method = $database->fetchAll($sql_get_payment);
        return $data_payment_method[0];
    }
    /**
     * Function get connectio database directly for case lazy create model
     * @return mixed
     */
    public function getConnectionDatabaseDirect()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        return $connection;
    }
    /**
     * Function get Base URL
     *
     * @return void
     */
    public function getBaseUrl()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $base_url = $storeManager->getStore()->getBaseUrl();
        return $base_url;
    }
    /**
     * Function get Invoice Url
     *
     * @param [type] $order_id 
     * @return void
     */
    public function getUrlInvoice($order_id)
    {
        $route = "manager/provider/invoice/id/".$order_id;
        $params = [];
        return $this->getUrl($route, $params);

    }
    /**
     * Get Url Submit Invoice
     *
     * @return void
     */
    public function getSubmitInvoiceUrl()
    {
        $route = "manager/provider/saveinvoice/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Function get Print Invoice
     *
     * @param [type] $order_id
     * @return void
     */
    public function getPrintInvoiceUrl($order_id)
    {
        $route = "manager/provider/printinvoice/id/$order_id";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Function print invoice mass action
     *
     * @return void
     */
    public function getPrintInvoiceMassAction()
    {
        $route = "manager/provider/massprintinvoice/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Function print packaging mass 
     *
     * @return void
     */
    public function getPrintPackagingMassAction()
    {
        $route = "manager/provider/massprintpackaging/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Function print shipment mass action
     *
     * @return void
     */
    public function getPrintShipmentMassAction()
    {
        $route = "manager/provider/massprintship/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Function Make Mass Invoice
     *
     * @return void
     */
    public function getMassInvoice()
    {
        $route = "manager/provider/massinvoice/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Function Make Mass Invoice
     *
     * @return void
     */
    public function getMassShipment()
    {
        $route = "manager/provider/massshipment/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Mass Action Packaging
     *
     * @return void
     */
    public function getPackagingUrl()
    {
        $route = "manager/provider/masspackaging/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Function mass Cancle Order
     *
     * @return void
     */
    public function getCancelUrl()
    {
        $route = "manager/provider/masscancel/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Get Array Ids Checkbox
     *
     * @return void
     */
    public function getArrayIds()
    {
        $array_ids = $this->_coreRegistry->registry('libero_provider_order_mass_print_invoice');
        return $array_ids;
    }
    /**
     * Function getInvoiceDetail For Print Invoice
     *
     * @param [type] $order_id
     * @return void
     */
    public function getInvoiceDetail($order_id)
    {
        $connection = $this->getConnectionDatabaseDirect();
        $sql = "";
        $sql .="SELECT ";
        $sql .="    *, ssoi.qty_ordered AS qty, ssoi.item_id AS order_item_id, sso.created_at AS order_created_at ";
        $sql .="FROM ";
        $sql .="    seller_sales_order_grid AS ssog ";
        $sql .="        JOIN ";
        $sql .="    seller_sales_order AS sso ";
        $sql .="        JOIN ";
        $sql .="    seller_sales_order_item AS ssoi ";
        $sql .="        ON ssog.order_id = ssoi.order_id ";
        $sql .="        AND sso.entity_id = ssoi.order_id ";
        $sql .="WHERE ";
        $sql .=" ssog.order_id = '$order_id'";
        $sql .=";";
        $result = $connection->fetchAll($sql);
        return $result;
    }
    /**
     * Function get Url Shipping
     *
     * @param [type] $order_id
     * @return void
     */
    public function getUrlShipping($order_id)
    {
        $route = "manager/provider/ship/id/".$order_id;
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Function get Submit Shipping URL
     *
     * @return void
     */
    public function getSubmitShipUrl()
    {
        $route = "manager/provider/saveship/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Function get Print Ship
     *
     * @param [type] $order_id
     * @return void
     */
    public function getPrintShipUrl($order_id)
    {
        $route = "manager/provider/printship/id/$order_id";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getListDelivery()
    {
        $sql_get_total = "select * from libero_delivery_company";
        $database = $this->getConnectionDatabaseDirect();
        $data = $database->fetchAll($sql_get_total);
        return $data[0];
    }
    /**
     * Get Formkey in Block
     *
     * @return void
     */
    public function getFormKey()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $key_form = $objectManager->get("Magento\Framework\Data\Form\FormKey");
        $form_key = $key_form->getFormKey(); // this will give you from key
        /*$urlBuinterf = $objectManager->get(‘\Magento\Framework\UrlInterface’);
        $urlsecret = $urlBuinterf->$this->urlBuilder->getUrl(‘url_path_here’);// this will give you admin secret key*/
        return $form_key;
    }
    /**
     * Get Total Ordered Qty
     *
     * @return void
     */
    public function getTotalQty($order_id)
    {
        $sql_get_total = "select sum(qty_ordered) as total_ordered from seller_sales_order_item where order_id = '$order_id'";
        $database = $this->getConnectionDatabaseDirect();
        $data = $database->fetchAll($sql_get_total);
        return $data[0]["total_ordered"];
    }
    /**
     * Function check if invoice created
     *
     * @param [type] $order_id
     * @return void
     */
    public function checkInvoiceCreated($order_id)
    {
        $sql_get_total = "select * from seller_sales_invoice where order_id = '$order_id'";
        $database = $this->getConnectionDatabaseDirect();
        $data = $database->fetchAll($sql_get_total);
        if(count($data) > 0 ){
            return  true;
        }
        return false;
    }
    /**
     * Function get list order
     *
     * @return void
     */

    public function getListOrderProvider($data_filter = array(),$from = null,$to = null,$limit = 20,$current_page = 1)
    {
        $objectManager = $this->getObjectManager();
        $database = $this->getConnectionDatabaseDirect();
        $sql_get_order_provider = "SELECT * FROM  ".$this->_table_provider_order;
        $sql_get_order_provider .= " where 1 = 1 ";
        $size_page = $limit * $current_page;
        $offset =  ($current_page - 1) * $limit;
        if(count($data_filter) > 0){
            foreach($data_filter as $key => $value){
                $operator = $value["operator"];
                $value_filter = trim($value["value"]);
                if($value_filter != "" && $value_filter != "-1"){
                    if($operator = "LIKE_PERCENT"){
                        $sql_get_order_provider .= "AND `$key` LIKE '%$value_filter%'";
                    }else{
                        $sql_get_order_provider .= "AND `$key` $operator '$value_filter'";
                    }
                }
            }
        }
        if($from != null && $to != null){
            $sql_get_order_provider .= " AND `created_at` >= '$from' AND `created_at` <= '$to 24:00:00'";
        }
        if($from != null && $to == null){
            $sql_get_order_provider .= " AND `created_at` >= '$from'";
        }
        if($from == null && $to != null){
            $sql_get_order_provider .= " AND `created_at` <= '$to 24:00:00'";
        }
        $sql_get_order_provider.= " order by entity_id desc limit $offset,$limit";
        $data = $database->fetchAll($sql_get_order_provider);
        return $data;
    }
    /**
     * Function get list order
     *
     * @return void
     */
    public function getListInvoiceProvider()
    {
        $objectManager = $this->getObjectManager();
        $database = $this->getConnectionDatabaseDirect();
        $sql_get_order_provider = "SELECT * FROM  ".$this->_table_provider_order." where `status` = 'Request to packaging' order by entity_id desc";
        $data = $database->fetchAll($sql_get_order_provider);
        return $data;
    }
    /**
     * Function get list order
     *
     * @return void
     */
    public function getListShipmentProvider()
    {
        $objectManager = $this->getObjectManager();
        $database = $this->getConnectionDatabaseDirect();
        $sql_get_order_provider = "SELECT * FROM  ".$this->_table_provider_order." where `status` = 'Processing' order by entity_id desc";
        $data = $database->fetchAll($sql_get_order_provider);
        return $data;
    }
    /**
     * Function get list shipping order
     *
     * @return void
     */
    public function getListShipping()
    {
        $objectManager = $this->getObjectManager();
        $database = $this->getConnectionDatabaseDirect();
        $sql_get_order_provider = "SELECT * FROM  ".$this->_table_provider_order." where `status` = 'Prepare to ship' order by entity_id desc";
        $data = $database->fetchAll($sql_get_order_provider);
        return $data;
    }
    /**
     * Function get Seller
     *
     * @return void
     */
    public function getProvider($id_seller)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $model_seller = $objectManager->create("\Libero\Customer\Model\Seller");
        $seller = $model_seller->load($id_seller);
        return $seller;
    }
    /**
     * Convert Float Qty To Interger Qty
     *
     * @param [float] $qty
     * @return void
     */
    public function formartQty($qty)
    {
        return number_format($qty,0);
    }
    /**
     * function formarPrice
     *
     * @param [type] $price
     * @return void
     */
    public function formartPrice($price){
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of Object Manager
        $priceHelper = $objectManager->create('Magento\Framework\Pricing\Helper\Data'); // Instance of Pricing Helper
        $formattedPrice = $priceHelper->currency($price, true, false);
        return $formattedPrice;
    }
    /**
     * Function get Type Delivery
     *
     * @param [type] $type
     * @return void
     */
    public function getTypeDelivery($type)
    {
        //$helper = $this->getHelperOneStepCheckout();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $helper = $objectManager->get("\Libero\Onestepcheckout\Helper\Data");
        if($type == $helper->_CODE_TYPE_MY_DELIVERY){
            return "Provider Delivery";
        }
        if($type == $helper->_CODE_TYPE_WISI_DELIVERY){
            return "Wisi Delivery";
        }
        if($type == $helper->_CODE_TYPE_TMS_REQUEST){
            return "TMS Request";
        }
        return "unknow";
    }
    /**
     * Function get URL order detail
     *
     * @param [type] $id
     * @return void
     */
    public function getDetailOrderUrl($id)
    {
        $route = "manager/provider/detail/id/$id";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Function get Back Url
     *
     * @return void
     */
    public function getBackUrl()
    {
        $route = "manager/provider/order";
        $params = [];
        return $this->getUrl($route, $params);
    }
    /**
     * Function getBackUrlInvoice to redirect detail page
     *
     * @return void
     */
    public function getBackUrlInvoice($order_id)
    {
        $route = "manager/provider/detail/id/$order_id";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getSubmitPackaging($order_id)
    {
        $route = "manager/provider/detail/id/$order_id";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getApproveUrl()
    {
        $route = "manager/seller/approve/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getViewSeller()
    {
        $route = "manager/seller/view/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getSaveUrl(){
        $route = "manager/seller/save/";
        $params = [];
        return $this->getUrl($route, $params);
    }
}