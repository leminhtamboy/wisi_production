<?php
namespace Libero\Customer\Block\Adminhtml\Provider;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Faq extends \Magento\Framework\View\Element\Template
{

    protected $_objectManager = null;
    protected $_productImageHelper = null;
    public function getSellerDetail(){
        $customerId = $this->getCustomer()->getId();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $aclAccountSeler = $objectManager->get("\Libero\Customer\Model\Account");
        $dataAccount = $aclAccountSeler->load($customerId,"id_customer");
        $sellerObj = null;
        if($dataAccount->getId()){
            $id_seller = $dataAccount->getData("id_seller");
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($id_seller,"id_seller_company");
        }else{
            $sellerObj = $objectManager->create('Libero\Customer\Model\CustomerSeller')->load($customerId,"id_customer");
        }
        return $sellerObj;
    }

    public function getCollectionFAQ($provider_id = null,$pageSize = 20, $curPage= 1)
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $requestHttp = $this->_objectManager->create("\Magento\Framework\App\Request\Http");
        $util = $this->_objectManager->get("Libero\Customer\Block\Seller\Admin\Util");

        $product_name = $util->removeQuoteInString($requestHttp->getParam("product_name"));
        $sku = $util->removeQuoteInString($requestHttp->getParam("product_sku"));
        $provider_id = $requestHttp->getParam("provider_id");
        $status = $requestHttp->getParam("product_status");

        if($requestHttp->getParam("pageSize")) {
            $pageSize = $requestHttp->getParam("pageSize");
        }

        if($requestHttp->getParam("page")) {
            $curPage = $requestHttp->getParam("page");
        }

        $collectionSearchProduct = $this->getCollectionSearch();
        $result= array();

        $productDependency = $this->_objectManager->create("Libero\Customer\Model\Faq");
        $collectionProductFull = $productDependency->getCollection()->addFieldToSelect("*")
        ->addFieldToFilter("type_faq", array("eq" => "Q"));
        if ($provider_id != null) {
            $collectionProductFull = $collectionProductFull->addFieldToFilter("provider_id", array("eq" => $provider_id));
        }
        
        if($product_name != ""){
            $collectionProductFull = $collectionProductFull->addFieldToFilter("product_name",array("like" => "%".$product_name."%"));
        }
        if($sku != ""){
            $collectionProductFull = $collectionProductFull->addFieldToFilter("product_sku",array("like" => "%".$sku."%"));
        }
        if($status > "-1" && $status !=  ""){
            $collectionProductFull = $collectionProductFull->addFieldToFilter("type_faq",array("eq" => $status));
        }
        $total = count($collectionProductFull);
        $collectionProductPage = $collectionProductFull->setPageSize($pageSize)->setCurPage($curPage);
        $collectionProductPage->setOrder('id_faq', 'DESC');
        $lastPage = $collectionProductPage->getLastPageNumber();
        $result = array(
            "total" => $total,
            "pageSize" => $pageSize,
            "curPage" => $curPage,
            "lastPage" => $lastPage,
            "listRecords" => $collectionProductPage,
            "product_name" => $product_name,
            "product_sku" => $sku,
            "provider_id" => $provider_id,
            "product_status" => $status,
        );
        return $result;
    }
    public function getDetailQuestion()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $injectHTTP = $this->_objectManager->get("\Magento\Framework\App\Request\Http");
        $id_faq = $injectHTTP->getParam("id");
        $modelFAQ = $this->_objectManager->create("Libero\Customer\Model\Faq");
        $dataFAQ = $modelFAQ->load($id_faq);
        return $dataFAQ; 
    }
    public function getUrlAnswerFAQ($faq_id)
    {
        return $this->getUrl("customer/faq/answer/id/".$faq_id);
    }
    public function getBackUrl()
    {
        return $this->getUrl("customer/faq/index/");
    }
    public function getSaveFaq(){
        return $this->getUrl("customer/faq/save/");
    }
    public function getFormKey()
    {

    }
}


