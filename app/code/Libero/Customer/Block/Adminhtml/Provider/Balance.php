<?php
namespace Libero\Customer\Block\Adminhtml\Provider;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Balance extends \Magento\Framework\View\Element\Template
{

    protected $_resource;

    protected $_adminUtil;

    protected $_table_provider_order = "seller_sales_order";

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Libero\Customer\Block\Seller\Admin\Util $adminUtil,
        array $data = [])
    {
        $this->_resource = $resourceConnection;
        $this->_adminUtil = $adminUtil;
        parent::__construct($context, $data);
    }

    public function processingDate($input_date){
        $arrayDate= explode("/",$input_date);
        return $arrayDate[2]."-".$arrayDate[0]."-".$arrayDate[1];
    }

    
    /**
     * Name : Nam
     * Date : 23/11/2018
     * Comments : get request pay balance
     * Filename : 
     * Param : id_seller, pageSize, curPage
     */
    public function getRequestBalanceList($pageSize = 20, $curPage= 1){
        $connection = $this->_resource->getConnection();
        $provider_id = "";
        $id = "";
        $status = "";
        $date_from = "";
        $date_to = "";

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $util = $objectManager->create("\Libero\Customer\Block\Seller\Admin\Util");
        $requestHttp = $objectManager->create("\Magento\Framework\App\Request\Http");

        if($requestHttp->getParam("id")){
            $id = $requestHttp->getParam("id");
        }

        if($requestHttp->getParam("provider_id")){
            $provider_id = $requestHttp->getParam("provider_id");
        }
        
        if($requestHttp->getParam("status")){
            $status = $requestHttp->getParam("status");
        }
        
        if($requestHttp->getParam("date_from")){
            $date_from = $requestHttp->getParam("date_from");
        }
        
        if($requestHttp->getParam("date_to")){
            $date_to = $requestHttp->getParam("date_to");
        }

        if($requestHttp->getParam("pageSize")) {
            $pageSize = $requestHttp->getParam("pageSize");
        }

        if($requestHttp->getParam("page")) {
            $curPage = $requestHttp->getParam("page");
        }


        $sql ="SELECT * FROM libero_provider_balance WHERE 1=1 ";

        if($provider_id != ""){
            $sql.= "AND provider_id = '$provider_id' ";
        }

        if($id != ""){
            $sql.= "AND `id` = '$id' ";
        }

        if($status !=  "-1" && $status !=  ""){
            $sql.= "AND `status` = '$status' ";
        }

        if($date_from != "" && $date_to != ""){
            $dateProcessFrom = $this->processingDate($date_from) . " 00:00:00";
            $dateProcessTo = $this->processingDate($date_to) . " 23:59:59";
            $sql.=" AND complete_date >= '$dateProcessFrom' AND complete_date <= '$dateProcessTo' ";
        }
        
        $allRecords = $connection->fetchAll($sql);

        $total = $util->getTotalArray($allRecords);
        $lastPage = $util->getLastPageByTotal($total,$pageSize);
        $curPage = ($util->getCurPageArray() < $lastPage) ? $util->getCurPageArray() : $lastPage;
        $curPage = ($curPage > 0) ? $curPage : 1;
        
        $sql .= " ORDER BY complete_date, reply_request_date DESC LIMIT $pageSize OFFSET " . ($curPage-1)*$pageSize . ";";

        $orders = $connection->fetchAll($sql);
        $firstNumber = $util->getFirstNumArray($orders);

        $result = array(
            "total" => $total,
            "pageSize" => $pageSize,
            "curPage" => $curPage,
            "lastPage" => $lastPage,
            "listRecords" => $orders,
            "firstNumber" => $firstNumber,
            "id" => $id,
            "status" => $status,
            "date_from" => $date_from,
            "date_to" => $date_to
        );
        return $result;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    protected function getObjectManager()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Name : Nam
     * Date : 26/11/2018
     * Comments : get provider bank info
     * Filename : 
     * Param : id_seller, pageSize, curPage
     */
    public function getProviderBankInfo($id_seller) {
        $objectManager = $this->getObjectManager();
        $connection = $this->_resource->getConnection();

        $sql_seller_info = "SELECT * FROM libero_customer_seller_company where `id_seller_company` = '$id_seller';";
        $seller_info = $connection->fetchAll($sql_seller_info);

        $bank_name = $seller_info["bank_name"];
        $bank_account = $seller_info["bank_account"];
        $bank_account_name = $seller_info["bank_account_name"];
        
        
        $result = array(
            "bank_name" => $bank_name,
            "bank_account" => $bank_account,
            "bank_account_name" => $bank_account_name
        );
        return $result;
    }
}


