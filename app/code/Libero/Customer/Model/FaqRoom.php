<?php
namespace Libero\Customer\Model;
class FaqRoom extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Libero\Customer\Model\ResourceModel\FaqRoom');
    }
}