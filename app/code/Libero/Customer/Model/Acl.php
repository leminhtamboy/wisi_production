<?php
namespace Libero\Customer\Model;
class Acl extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Libero\Customer\Model\ResourceModel\Acl');
    }
}