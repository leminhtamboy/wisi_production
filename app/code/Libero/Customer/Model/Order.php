<?php
namespace Libero\Customer\Model;

class Order extends \Magento\Framework\Model\AbstractModel
{
	protected function _construct()
	{
		$this->_init('Libero\Customer\Model\ResourceModel\Order');
    }
}
