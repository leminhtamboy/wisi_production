<?php
namespace Libero\Customer\Model\ResourceModel\CustomerSeller;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Libero\Customer\Model\CustomerSeller',
            'Libero\Customer\Model\ResourceModel\CustomerSeller'
        );
    }
}