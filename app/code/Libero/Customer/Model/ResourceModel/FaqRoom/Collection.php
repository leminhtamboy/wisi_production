<?php
namespace Libero\Customer\Model\ResourceModel\FaqRoom;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Libero\Customer\Model\FaqRoom',
            'Libero\Customer\Model\ResourceModel\FaqRoom'
        );
    }
}