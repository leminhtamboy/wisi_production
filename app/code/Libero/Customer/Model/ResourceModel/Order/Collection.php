<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Libero\Customer\Model\ResourceModel\Order;

/**
 * Order grid collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Initialize dependencies.
     */
    protected function _construct()
    {
        $this->_init("Libero\Customer\Model\Order","Libero\Customer\Model\ResourceModel\Order");
    }
}
