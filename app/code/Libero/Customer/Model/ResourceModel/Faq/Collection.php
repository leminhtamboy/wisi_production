<?php
namespace Libero\Customer\Model\ResourceModel\Faq;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Libero\Customer\Model\Faq',
            'Libero\Customer\Model\ResourceModel\Faq'
        );
    }
}