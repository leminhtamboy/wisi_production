<?php
namespace Libero\Customer\Model\ResourceModel;

class Account extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        // TODO: Implement _construct() method.
        $this->_init("libero_customer_seller_account","id_seller_account");
    }
}