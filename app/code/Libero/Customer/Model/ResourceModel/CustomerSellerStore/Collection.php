<?php
namespace Libero\Customer\Model\ResourceModel\CustomerSellerStore;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Libero\Customer\Model\CustomerSellerStore',
            'Libero\Customer\Model\ResourceModel\CustomerSellerStore'
        );
    }
}