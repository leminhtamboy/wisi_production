<?php
namespace Libero\Customer\Model\ResourceModel;

class Vat extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init("libero_vat","id_vat");
    }
}
