<?php
namespace Libero\Customer\Model\ResourceModel;

class Seller extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init("libero_customer_seller_company","id_seller_company");
    }
}
