<?php
namespace Libero\Customer\Model\ResourceModel\Acl;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Libero\Customer\Model\Acl',
            'Libero\Customer\Model\ResourceModel\Acl'
        );
    }
}