<?php
namespace Libero\Customer\Model\ResourceModel\CustomerReseller;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Libero\Customer\Model\CustomerReseller',
            'Libero\Customer\Model\ResourceModel\CustomerReseller'
        );
    }
}