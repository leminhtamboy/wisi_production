<?php
namespace Libero\Customer\Model\ResourceModel;

class SalesOrder extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init("seller_sales_order","entity_id");
    }
}
