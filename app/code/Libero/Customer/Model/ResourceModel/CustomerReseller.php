<?php
namespace Libero\Customer\Model\ResourceModel;

class CustomerReseller extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        // TODO: Implement _construct() method.
        $this->_init("libero_reseller","id_reseller");
    }
}