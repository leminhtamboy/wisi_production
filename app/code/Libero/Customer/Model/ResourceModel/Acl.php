<?php
namespace Libero\Customer\Model\ResourceModel;

class Acl extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        // TODO: Implement _construct() method.
        $this->_init("libero_customer_seller_account_acl","id_account_acl");
    }
}