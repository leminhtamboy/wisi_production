<?php
namespace Libero\Customer\Model\ResourceModel\Account;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            'Libero\Customer\Model\Account',
            'Libero\Customer\Model\ResourceModel\Account'
        );
    }
}