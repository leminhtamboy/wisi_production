<?php
namespace Libero\Customer\Model;
class CustomerSellerStore extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init('Libero\Customer\Model\ResourceModel\CustomerSellerStore');
    }
}