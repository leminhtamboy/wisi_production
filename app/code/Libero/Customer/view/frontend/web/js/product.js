require([
    'jquery', 
    'mage/template',
    'jquery/ui'
], function($, mageTemplate){

    $('.product-status-switcher').click(function(){
    	var pro_stas_ele = $('input[name="product_status"]');
		var cur_pro_stas = pro_stas_ele.val();
		console.log(cur_pro_stas);
		if(cur_pro_stas == 1){
			pro_stas_ele.val(0);
			pro_stas_ele.attr('checked',false);
		}else{
			pro_stas_ele.val(1);
			pro_stas_ele.attr('checked',true);
		}
	});

	function readURL(input) {

	  if (input.files && input.files[0]) {
	    var reader = new FileReader();

	    reader.onload = function(e) {
	      $('#product-img-preview').attr('src', e.target.result);
	    }
	    reader.readAsDataURL(input.files[0]);
	  }
	}

	$("#fileupload").change(function() {
		readURL(this);
	});

});