<?php
namespace Libero\Customer\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.2') < 0) {
            //code to upgrade to 1.0.1
            $setup->run("CREATE TABLE `libero_customer_seller_company` ( `id_seller_company` INT NOT NULL AUTO_INCREMENT , `company` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `address` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `city` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `district` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `street` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `tax` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `home_page` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `agent_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `bank_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `bank_account` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `etc` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `is_manufacture` INT NOT NULL , `is_genuine` INT NOT NULL , `biz_type` INT NOT NULL , `company_registration` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_seller_company`)) ENGINE = InnoDB;");
        }
        if (version_compare($context->getVersion(), '1.0.3') < 0) {
            $setup->run("CREATE TABLE `libero_customer_social_login` ( `social_id` INT NOT NULL AUTO_INCREMENT , `customer_id` VARCHAR(50) NOT NULL , `type` VARCHAR(50) NOT NULL , `is_send_pass` INT NOT NULL , PRIMARY KEY (`social_id`)) ENGINE = InnoDB;");
        }
        if (version_compare($context->getVersion(), '1.0.4') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `up_load_cer_1` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `company_registration`, ADD `up_load_cer_2` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `up_load_cer_1`, ADD `up_load_cer_3` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `up_load_cer_2`;");
		}
		if (version_compare($context->getVersion(), '1.0.5') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `id_customer` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `up_load_cer_3`;");
        }
        if (version_compare($context->getVersion(), '1.0.6') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `status` INT NOT NULL AFTER `id_customer`, ADD `message_denied` TEXT NOT NULL AFTER `status`");
        }
        if (version_compare($context->getVersion(), '1.0.7') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `store_name` VARCHAR(250) NOT NULL AFTER `message_denied`;");
        }
        if (version_compare($context->getVersion(), '1.0.8') < 0) {
            $setup->run("CREATE TABLE `libero_customer_seller_store` ( `id_store` INT NOT NULL AUTO_INCREMENT , `id_seller` INT NOT NULL , `sku_store` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `name_store` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `frontend_url` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `backend_url` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `user_name` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `password` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `alias` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_store`)) ENGINE = InnoDB;");
        }
        //remove last name required
        if(version_compare($context->getVersion(),'1.0.9') < 0){
            $setup->run("UPDATE eav_attribute SET is_required = 0 WHERE attribute_code = 'lastname' and entity_type_id = 1;");
        }
        if(version_compare($context->getVersion(),'1.0.10') < 0){
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `id_category` VARCHAR(10) NOT NULL AFTER `store_name`;");
        }
        if(version_compare($context->getVersion(),'1.0.11') < 0){
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `logo_url` VARCHAR(500) NULL AFTER `id_category`, ADD `banner_url` VARCHAR(500) NULL AFTER `logo_url`, ADD `description` TEXT NULL AFTER `banner_url`, ADD `rating_sumary` INT NULL AFTER `description`;");
        }
        if(version_compare($context->getVersion(),'1.0.12') < 0){
            $setup->run("ALTER TABLE `libero_onestepcheckout_shipping_method` ADD `id_seller` INT NULL AFTER `status_shipping_method`;");
        }
        if(version_compare($context->getVersion(),'1.0.13') < 0){
            $setup->run("ALTER TABLE `libero_onestepcheckout_shipping_method` ADD `price` INT NULL AFTER `id_seller`;");
        }
        if(version_compare($context->getVersion(),'1.0.14') < 0)
        {
            $setup->run("CREATE TABLE `libero_customer_seller_account` ( `id_seller_account` INT NOT NULL AUTO_INCREMENT , `id_customer` INT NOT NULL , `id_seller` INT NOT NULL , `id_role` INT NOT NULL , PRIMARY KEY (`id_seller_account`)) ENGINE = InnoDB;");
        }
        if(version_compare($context->getVersion(),'1.0.15') < 0)
        {
            $setup->run("CREATE TABLE `libero_customer_seller_account_acl` ( `id_account_acl` INT NOT NULL AUTO_INCREMENT , `acl_permmision` TEXT NOT NULL , `acl_name` VARCHAR(255) NOT NULL , `id_seller` INT NOT NULL , PRIMARY KEY (`id_account_acl`)) ENGINE = InnoDB;");
        }
        if(version_compare($context->getVersion(),'1.0.16') < 0)
        {
            $setup->run("ALTER TABLE `rma_request_product` ADD `id_seller` INT(10) NOT NULL AFTER `update_at`;");
        }
        if(version_compare($context->getVersion(),'1.0.17') < 0)
        {
            $setup->run("CREATE TABLE `bank_list` ( `id` INT UNSIGNED NOT NULL AUTO_INCREMENT, `name` VARCHAR(255) NOT NULL, `parent_id` INT UNSIGNED NOT NULL DEFAULT '0', `short_name` VARCHAR(255) NULL, `description` VARCHAR(255) NULL, `sort_order` VARCHAR(5) NULL, `type` VARCHAR(5) NULL COMMENT '1: Group Label\n2: Name of Bank', PRIMARY KEY (`id`)) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;");
        }
        if(version_compare($context->getVersion(),'1.0.18') < 0)
        {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD COLUMN `telephone` VARCHAR(45) NOT NULL AFTER `street`;");
        }
        if(version_compare($context->getVersion(),'1.0.19') < 0)
        {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD COLUMN `full_address` VARCHAR(255) NOT NULL AFTER `street`;");
        }
        if (version_compare($context->getVersion(), '1.0.20') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `hot_line` VARCHAR(250) NOT NULL AFTER `rating_sumary`;");
        }
        if (version_compare($context->getVersion(), '1.0.21') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` MODIFY `id_category` VARCHAR(250);");
        }
        if (version_compare($context->getVersion(), '1.0.22') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` CHANGE COLUMN `banner_url` `banner_url1` VARCHAR(500) NULL DEFAULT NULL , ADD COLUMN `banner_url2` VARCHAR(500) NULL DEFAULT NULL AFTER `banner_url1`, ADD COLUMN `banner_url3` VARCHAR(500) NULL DEFAULT NULL AFTER `banner_url2`;");
        }

        if (version_compare($context->getVersion(), '1.0.23') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD COLUMN `banner_url4` VARCHAR(500) NULL DEFAULT NULL AFTER `banner_url3`;");
        }

        if (version_compare($context->getVersion(), '1.0.24') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD COLUMN `city_province_code` VARCHAR(255) NOT NULL AFTER `city`, ADD COLUMN `district_code` VARCHAR(255) NOT NULL AFTER `district`, ADD COLUMN `street_ward_code` VARCHAR(255) NOT NULL AFTER `street`;");
        }

        if (version_compare($context->getVersion(), '1.0.25') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD COLUMN `company_info` VARCHAR(500) NULL AFTER `updated_at`;");
        }

        if (version_compare($context->getVersion(), '1.0.26') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` CHANGE COLUMN `company_info` `company_info` LONGTEXT NULL DEFAULT NULL ;");
        }
        if (version_compare($context->getVersion(), '1.0.27') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order` ADD `type_of_order` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `weight_api`;");
        }
        if (version_compare($context->getVersion(), '1.0.28') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order` ADD `type_delivery` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `status`;");
        }
        if (version_compare($context->getVersion(), '1.0.29') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD `delivery_type` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `company_info`;");
        }
        if (version_compare($context->getVersion(), '1.0.30') < 0) {
            $setup->run("ALTER TABLE `catalog_product_entity_decimal` CHANGE `value` `value` DECIMAL(20,4) NULL DEFAULT NULL COMMENT 'Value';");
        }
        if (version_compare($context->getVersion(), '1.0.31') < 0) {
            $setup->run("CREATE TABLE `libero_temp_save_product` ( `id` INT NOT NULL AUTO_INCREMENT , `product_id` VARCHAR(100) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci NOT NULL , `product_sku` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `product_name` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `data_save` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `provider_id` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `provider_name` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `time_update` TIMESTAMP NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
        }
        if (version_compare($context->getVersion(), '1.0.32') < 0) {
            $setup->run("ALTER TABLE `libero_temp_save_product` ADD `data_diff` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `data_save`;");
        }
        if (version_compare($context->getVersion(), '1.0.33') < 0) {
            $setup->run("ALTER TABLE `libero_temp_save_product` ADD `status` INT NOT NULL COMMENT '1 : chờ duyệt , 2 : đã duyệt , 3: từ chối' AFTER `time_update`;");
        }
        if (version_compare($context->getVersion(), '1.0.34') < 0) {
            $setup->run("ALTER TABLE `libero_reseller` ADD COLUMN `id_warehouse` VARCHAR(255) NULL AFTER `is_owner_ware_house`;");
            $setup->run("ALTER TABLE `libero_reseller_ware_house` ADD COLUMN `district` INT(11) NOT NULL AFTER `etc`;");
        }
        if (version_compare($context->getVersion(), '1.0.35') < 0) {
            $setup->run("ALTER TABLE `libero_reseller_ware_house` CHANGE COLUMN `district` `district` INT(11) NOT NULL AFTER `region`;");
        }
        if (version_compare($context->getVersion(), '1.0.36') < 0) {
            $setup->run("ALTER TABLE `libero_reseller_ware_house` ADD COLUMN `is_libero_warehouse` INT(11) NULL AFTER `id_reseller`;");
        }
        if (version_compare($context->getVersion(), '1.0.37') < 0) {
            $setup->run("CREATE TABLE `libero_faq` ( `id_faq` INT NOT NULL AUTO_INCREMENT , `sender` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `provider_id` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `type_faq` VARCHAR(5) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `parent_question` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `provider_name` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `content_question` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id_faq`)) ENGINE = InnoDB;");
        }
        if(version_compare($context->getVersion(), '1.0.38') < 0) {
            $setup->run("ALTER TABLE `libero_faq` ADD `product_sku` VARCHAR(300) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `content_question`, ADD `product_name` VARCHAR(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `product_sku`;");
        }
        if(version_compare($context->getVersion(), '1.0.39') < 0) {
            $setup->run("ALTER TABLE `libero_faq` ADD `answer` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `product_name`;");
        }
        if (version_compare($context->getVersion(), '1.0.40') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order` ADD COLUMN `paid_status` INT(11) NULL DEFAULT 0 AFTER `customer_note`, ADD COLUMN `complete_date` VARCHAR(255) NULL AFTER `paid_status`;");
        }
        if (version_compare($context->getVersion(), '1.0.41') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order` CHANGE COLUMN `paid_status` `paid_status` INT(11) NULL DEFAULT '0' COMMENT '0: Pending\n1: Requested\n2: Paid\n3: Reject\n' ;");
        }
        if (version_compare($context->getVersion(), '1.0.42') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order` ADD COLUMN `request_at` VARCHAR(255) NULL AFTER `paid_status`;");
        }
        if (version_compare($context->getVersion(), '1.0.43') < 0) {
            $setup->run("ALTER TABLE `libero_customer_seller_company` ADD COLUMN `bank_account_name` VARCHAR(255) NOT NULL AFTER `bank_account`;");
            $setup->run("ALTER TABLE `seller_sales_order` CHANGE COLUMN `complete_date` `complete_date` VARCHAR(255) NULL DEFAULT NULL AFTER `paid_status`, ADD COLUMN `paid_confirm_date` VARCHAR(255) NULL AFTER `request_at`;");
        }
        if (version_compare($context->getVersion(), '1.0.44') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order` ADD COLUMN `cancel_pay_balance_reason` VARCHAR(255) NULL AFTER `paid_status`;");
        }
        if (version_compare($context->getVersion(), '1.0.45') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order` CHANGE COLUMN `paid_confirm_date` `reply_request_date` VARCHAR(255) NULL DEFAULT NULL ;");
        }
        if (version_compare($context->getVersion(), '1.0.46') < 0) {
            $setup->run("CREATE TABLE `libero_faq_type` ( `id` INT NOT NULL AUTO_INCREMENT, `question_type` VARCHAR(255) NOT NULL, PRIMARY KEY (`id`)) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;");
            $setup->run("INSERT INTO `libero_faq_type` (`question_type`) VALUES ('Product Quality');");
            $setup->run("INSERT INTO `libero_faq_type` (`question_type`) VALUES ('Supply Ability');");
            $setup->run("INSERT INTO `libero_faq_type` (`question_type`) VALUES ('Promotion Schedule');");
            $setup->run("INSERT INTO `libero_faq_type` (`question_type`) VALUES ('Warranty');");
        }
        if (version_compare($context->getVersion(), '1.0.47') < 0) {
            $setup->run("ALTER TABLE `libero_faq` ADD COLUMN `created_at` VARCHAR(255) NULL AFTER `answer`, ADD COLUMN `answered_at` VARCHAR(255) NULL AFTER `created_at`;");
        }
        if (version_compare($context->getVersion(), '1.0.48') < 0) {
            $setup->run("ALTER TABLE `sm_megamenu_items` CHANGE COLUMN `category_banner_image_url` `category_banner_image_url` VARCHAR(255) NULL DEFAULT NULL ;");
        }
        if (version_compare($context->getVersion(), '1.0.49') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order` ADD COLUMN `is_grouped_to_balance` INT(11) NULL AFTER `reply_request_date`;");
            $setup->run("CREATE TABLE `libero_provider_balance` ( `id` INT NOT NULL AUTO_INCREMENT, `order_ids` VARCHAR(255) NOT NULL, `status` INT(11) NULL COMMENT '0: Pending\n1: Requested\n2: Paid\n3: Reject', `show_request_button` INT(11) NULL, `reply_request_date` VARCHAR(255) NULL, `in_charge_confirm` VARCHAR(255) NULL, `updated_at` VARCHAR(255) NULL, `created_at` VARCHAR(255) NULL, `provider_id` VARCHAR(45) NULL, PRIMARY KEY (`id`));");
        }
        if (version_compare($context->getVersion(), '1.0.50') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order` CHANGE COLUMN `is_grouped_to_balance` `is_grouped_to_balance` INT(11) NULL DEFAULT 0 ;");
        }
        if (version_compare($context->getVersion(), '1.0.51') < 0) {
            $setup->run("ALTER TABLE `seller_sales_order` CHANGE COLUMN `is_grouped_to_balance` `is_grouped_to_balance` VARCHAR(11) NULL DEFAULT '0' ;");
        }
        if (version_compare($context->getVersion(), '1.0.52') < 0) {
            $setup->run("ALTER TABLE `libero_provider_balance` CHANGE COLUMN `provider_id` `provider_id` VARCHAR(255) NULL DEFAULT NULL , ADD COLUMN `increment_ids` VARCHAR(255) NULL AFTER `order_ids`, ADD COLUMN `group_total` VARCHAR(255) NULL AFTER `increment_ids`;");
            $setup->run("ALTER TABLE `seller_sales_order` CHANGE COLUMN `is_grouped_to_balance` `balance_grouped_id` VARCHAR(11) NULL DEFAULT '0' ;");
        }
        if (version_compare($context->getVersion(), '1.0.53') < 0) {
            $setup->run("ALTER TABLE `libero_provider_balance` DROP COLUMN `show_request_button`, ADD COLUMN `estimate_pay_at` VARCHAR(255) NULL AFTER `created_at`;");
        }
        if (version_compare($context->getVersion(), '1.0.54') < 0) {
            $setup->run("ALTER TABLE `libero_provider_balance` ADD COLUMN `complete_date` VARCHAR(255) NULL AFTER `status`;");
        }
        if (version_compare($context->getVersion(), '1.0.55') < 0) {
            $setup->run("ALTER TABLE `libero_provider_balance` ADD COLUMN `request_at` VARCHAR(255) NULL AFTER `complete_date`;");
        }
        if (version_compare($context->getVersion(), '1.0.56') < 0) {
            $setup->run("ALTER TABLE `libero_provider_balance` ADD COLUMN `provider_name` VARCHAR(255) NULL AFTER `provider_id`;");
        }
        if (version_compare($context->getVersion(), '1.0.57') < 0) {
            $setup->run("ALTER TABLE `libero_provider_balance` ADD `commision` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `provider_name`;");
        }
        if (version_compare($context->getVersion(), '1.0.58') < 0) {
            $setup->run("ALTER TABLE `libero_provider_balance` ADD `real_receive` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL AFTER `commision`;");
        }
        if (version_compare($context->getVersion(), '1.0.59') < 0) {
            $setup->run("ALTER TABLE `libero_provider_balance` ADD COLUMN `cancel_pay_balance_reason` VARCHAR(255) NULL AFTER `reply_request_date`;");
        }
        if (version_compare($context->getVersion(), '1.0.60') < 0) {
            $setup->run("ALTER TABLE `libero_provider_balance` ADD COLUMN `group_base_subtotal` VARCHAR(255) NULL AFTER `increment_ids`, ADD COLUMN `group_base_tax_amount` VARCHAR(255) NULL AFTER `group_base_subtotal`, ADD COLUMN `group_base_shipping_amount` VARCHAR(255) NULL AFTER `group_base_tax_amount`, ADD COLUMN `shipping_description` VARCHAR(255) NULL AFTER `real_receive`, ADD COLUMN `payment_method` VARCHAR(255) NULL AFTER `shipping_description`;");
        }
        if (version_compare($context->getVersion(), '1.0.61') < 0) {
            $setup->run("ALTER TABLE `libero_faq` ADD COLUMN `question_type_code` INT(11) NOT NULL AFTER `type_faq`, ADD COLUMN `question_type_code_text` VARCHAR(255) NULL DEFAULT NULL AFTER `question_type_code`;");
        }        
        $setup->endSetup();
    }
}





