<?php
namespace Libero\Log\Block\Adminhtml\Product;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Registry;

class Edit extends \Magento\Framework\View\Element\Template{
    protected $_coreRegistry = null;

    public function __construct(
        Context $context,
        Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }
    public function getDataLog(){
        $model = $this->_coreRegistry->registry('libero_log_product');
        $dataRet = null;
        $dataRet = $model->getData();
        return $dataRet;
    }
    public function getApproveUrl()
    {
        $route = "log/product/approve/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getViewSeller()
    {
        $route = "manager/reseller/view/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getSaveUrl(){
        $route = "log/product/save/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getProductDetailUrl($product_id)
    {
        $route = "catalog/product/edit/";
        $params = ["id" => $product_id];
        return $this->getUrl($route, $params);
    }
    public function getBackUrl(){
        $route = "log/product/index/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getCancelUrl(){
        $route = "log/product/cancel/";
        $params = [];
        return $this->getUrl($route, $params);
    }
    public function getStoreUrl($provider_id)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $base_url = $storeManager->getStore()->getBaseUrl();
        return $base_url.""."marketplace/store/store/id_seller/".$provider_id;
    }
    public function getFormKey(){

    }
}