<?php
namespace Libero\Log\Model;

class Product extends \Magento\Framework\Model\AbstractModel
{
	protected function _construct()
	{
		$this->_init('Libero\Log\Model\ResourceModel\Product');
    }
}
