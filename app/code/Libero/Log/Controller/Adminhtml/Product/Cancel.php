<?php
namespace Libero\Log\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Webapi\Exception;

class Cancel extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $registry;
    public function __construct(\Magento\Backend\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $post = $this->getRequest()->getPostValue();
            $product_id = $post["product_id"];
            $log_id = $post["log_id"];
            $message = $post["message"];
            $log = $this->_objectManager->create("Libero\Log\Model\Product")->load($log_id);
            $data_diff = $log->getData("data_diff");
            $product_name = $log->getData("product_name");
            $product = $this->_objectManager->create("\Magento\Catalog\Model\Product")->load($product_id);
            $product->setData("message_deny",$message);
            $product->setStatus(0);
            $product->save();
            $log->setData("status",2);
            $log->save();
            $this->messageManager->addError(__('Cancel Product ['.$product_name.'] Successfully.'));
            //return $resultRedirect->setPath("*/*");
        }catch (Exception $e){
            $this->messageManager->addError(__($e->getMessage()));
        }
    }
}