<?php
namespace Libero\Log\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Webapi\Exception;

class Approve extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $registry;
    public function __construct(\Magento\Backend\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $post = $this->getRequest()->getPostValue();
            $idSeller = $post["provider_id"];
            $product_id = $post["product_id"];
            $log_id = $post["log_id"];
            //Model Log
            $array_exclude = array("category_name");
            $array_name = array("product_name");
            $array_rename = array("product_name" => "name");
            $array_decode_special_name = array("description" => "description","short_description" => "short_description");
            $log = $this->_objectManager->create("Libero\Log\Model\Product")->load($log_id);
            $data_diff = $log->getData("data_diff");
            $product_name = $log->getData("product_name");
            $data_value = json_decode($data_diff,true);
            $product = $this->_objectManager->create("\Magento\Catalog\Model\Product")->load($product_id);
            foreach($data_value as $_key => $value){
                $key_save = $_key;
                if(in_array($key_save,$array_exclude)){
                    continue;
                }
                if(in_array($key_save,$array_name)){
                    $key_save = $array_rename[$key_save];
                }
                if(in_array($key_save,$array_decode_special_name)){
                    $value = json_decode($value);
                }
                $product->setData($key_save,$value);
            }
            $product->setData("message_deny","");
            $product->setStatus(1);
            $product->save();
            $log->setData("status",1);
            $log->save();
            //Create
            $this->messageManager->addSuccess(__('Approve Product ['.$product_name.'] Successfully.'));
            $resultRedirect->setPath('log/product/index');
            return $resultRedirect;
        }catch (Exception $e){
            $this->messageManager->addError(__($e->getMessage()));
        }
    }
}