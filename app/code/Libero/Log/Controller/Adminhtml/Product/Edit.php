<?php
namespace Libero\Log\Controller\Adminhtml\Product;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Magento\Backend\App\Action
{
    protected $resultPageFactory;
    protected $registry;
    public function __construct(\Magento\Backend\App\Action\Context $context,\Magento\Framework\View\Result\PageFactory $resultPageFactory,\Magento\Framework\Registry $registry)
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->registry = $registry;
    }

    public function execute()
    {
        $log_product_id = $this->getRequest()->getParam('id');
        $model = $this->_objectManager->create("Libero\Log\Model\Product")->load($log_product_id);
        $product_name = $model->getData("product_name");
        //Restore previously entered form data from session
        $this->registry->register('libero_log_product', $model);
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Libero_Customer::log');
        $data_save = $model->getData("data_save");
        $array_data_save = json_decode($data_save,true);
        $is_add = false;
        if(count($array_data_save) < 1 ){
            $is_add = true;
        }
        if($is_add == false){
            $resultPage->getConfig()->getTitle()->prepend(__("View Edited Product ".$product_name));
        }else{
            $resultPage->getConfig()->getTitle()->prepend(__("View Added Product ".$product_name));
        }
        return $resultPage;
    }
}