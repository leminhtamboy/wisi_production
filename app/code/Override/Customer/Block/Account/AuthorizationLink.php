<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Override\Customer\Block\Account;

/**
 * Customer authorization link
 *
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class AuthorizationLink extends \Magento\Customer\Block\Account\AuthorizationLink
{
    
    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->isLoggedIn() ? __('Sign Out') : __('Sign In');
    }

}
