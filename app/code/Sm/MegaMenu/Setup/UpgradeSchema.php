<?php
namespace Sm\MegaMenu\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements  UpgradeSchemaInterface{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '4.0.0') < 0) {
            //code to upgrade to 4.0.0
            $setup->run("ALTER TABLE `sm_megamenu_items` ADD COLUMN `category_banner_image_url` VARCHAR(45) NULL DEFAULT NULL AFTER `limit_sub_category`;");
        }
        $setup->endSetup();
    }
}