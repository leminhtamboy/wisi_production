<?php
session_start();
use Magento\Framework\App\Bootstrap;

require __DIR__ . '/../app/bootstrap.php';
$params = $_SERVER;
unset($_SESSION);
$bootstrap = Bootstrap::create(BP, $params);
session_write_close();
session_name("frontend");
$obj = $bootstrap->getObjectManager();

$state = $obj->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
$id_customer = 34;
$customer = $obj->get('\Magento\Customer\Model\Customer')->load($id_customer);
$session  = $obj->create("\Magento\Customer\Model\Session");
$session->setCustomerAsLoggedIn($customer);
$session->regenerateId();
$storeManager = $obj->get('\Magento\Store\Model\StoreManagerInterface');
$store = $storeManager->getStore();
$jsonResultFactory = $obj->create('\Magento\Framework\Controller\Result\JsonFactory')->create();
$url_ret = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB)."customer/account";
?>
<script>
    top.location.href = '<?php echo $url_ret; ?>';
</script>
