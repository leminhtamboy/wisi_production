<?php
@session_start();
date_default_timezone_set( 'Asia/Ho_Chi_Minh' );
ini_set('display_errors',0);
error_reporting(E_ALL ^ E_WARNING ^E_NOTICE);
$server_name = __DIR__;
include_once("Edit.php");
class database
{
	var $array = array( );
	var $connecta;
        var $stmt;
	function database(){
               $this->connecta=Edit::getInstance();
	}
	function Query( $sql )
	{
		$mang = array( );
		try{
            $this->connecta->query("SET NAMES UTF8;");
            $this->stmt=$this->connecta->query($sql);
			$mang=$this->stmt->fetchAll(PDO::FETCH_BOTH);
            $this->stmt->closeCursor();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
		return $mang;
	}
	function __destruct(){
		$this->connecta=null;
	}
	function ExcuteObject( $sql ){
		$data = $this->Query( $sql );
		return $data;
	}

	function ExcuteObjectList( $sql )
	{
		try{
			$data = $this->Query( $sql );
			return $data;
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		return $data;

	}

	function ExcuteNonquery( $sql )
	{
		try
		{
			$this->connecta->exec($sql);
			return $this->connecta->lastInsertId();
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}
	public function ExcuteNonqueryXien($sql){
		try
		{
			$result = "";
			//$this->connect_to();
			$this->connecta->exec($sql);
			$result = $this->connecta->lastInsertId();
			//mysql_close($this->connect);
			//$this->connect=null;
			return $result;
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
		return "01";
	}

}
function formatPrice($price){
	return number_format($price,2,',','.');
}
?>