<?php
date_default_timezone_set( 'Asia/Ho_Chi_Minh' );
class Edit
{
	
	private static $connect=NULL;
	/*function database()
	{
		$this->connect_to();
	}*/
        public function __construct() {
            /*** maybe set the db name here later ***/
        }
        public static function getInstance() {

            if (!self::$connect){
                self::$connect = new PDO("mysql:host=127.0.0.1;dbname=wisi_production", 'root', 'root');
                self::$connect-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            return self::$connect;
        }
}
?>